import argparse
import json
from zk import ZK, const

conn = None
parser = argparse.ArgumentParser(description='ZK Basic Reading Tests')
parser.add_argument('-a', '--address',
                    help='ZK device Address [192.168.1.201]', default='192.168.1.201')
parser.add_argument('-pwd', '--password',
                    help='comkey', default='12369')
parser.add_argument('-fr', '--dfr',
                    help='date from', default='')
parser.add_argument('-to', '--dto',
                    help='date to', default='')

args = parser.parse_args()
# create ZK instance
zk = ZK(args.address, port=4370, timeout=20, password=args.password, force_udp=True, ommit_ping=False,)
try:
    # connect to device
    conn = zk.connect()
    # disable device, this method ensures no activity on the device while the process is run
    conn.disable_device()

    attendances = conn.get_attendance()
    arr = []
    x = 0
    for attendance in attendances:
      if attendance.timestamp.strftime('%Y-%m-%d %H:%M:%S') > args.dfr and attendance.timestamp.strftime('%Y-%m-%d %H:%M:%S') < args.dto:
#        if x == 2:
#          break
#        else:
         arr.append({'PIN':attendance.user_id, 'DateTime_':attendance.timestamp.strftime('%Y-%m-%d %H:%M:%S'), 'ip':args.address})
#         x=x+1

    # Test Voice: Say Thank You
    # conn.test_voice()
    # re-enable device after all commands already executed
    conn.enable_device()
    j = json.dumps(arr)
    print(j)
except Exception as e:
    print ("Process terminate : {}".format(e))
finally:
    if conn:
        conn.disconnect()
