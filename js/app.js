jun.ajaxCounter = 0;
jun.bu_id = '';
jun.cabang_id = '';
jun.usercabang = '';
jun.securityroles = '';

/*
    Menu Tree
 */
jun.TreeUi = Ext.extend(Ext.tree.TreePanel, {
    title: "Menu",
    useArrows: !0,
    autoScroll: !0,
    rootVisible: false,
    floatable: !1,
    animate: true,
    autoLoad: false,
    containerScroll: false,
    dataUrl: "site/tree",
    initComponent: function () {
        this.root = {
            text: "Menu"
        };
        this.tbar = {
            xtype: 'toolbar',
            id: "treepanel-toolbar-menu",
            ref: 'treeToolbar',
            enableOverflow: true,
            items: [
                {
                    xtype: 'splitbutton',
                    text: 'Bisnis Unit',
                    class: '',
                    ref: '../btnbu',
                    menu: new Ext.menu.Menu()
                },
                {
                    text: '>>',
                    ref: '../separator',
                    hidden: true,
                    disabled: true
                },
                {
                    xtype: 'splitbutton',
                    text: 'Cabang',
                    ref: '../btnCabang',
                    class: '',
                    disabled: true,
                    hidden: true,
                    menu: new Ext.menu.Menu()
                }
            ]
        };
        jun.TreeUi.superclass.initComponent.call(this);

        this.on('beforeload', function () {
            if (jun.bu_id == '') {
                return false;
            }
        }, this);

        this.on("click", function (a, b) {
            b.stopEvent();
            if (a.isLeaf()) {
                if (a.id == "logout") {
                    Ext.MessageBox.confirm("Logout", "Are sure want logout?", function (a) {
                        if (a === "no") return;
                        LOGOUT = true;
                        window.location.href = "site/logout";
                    }, this);
                } else {
                    var tabpanel = Ext.getCmp('mainpanel');
                    var id = "docs-" + a.id;
                    var tab = tabpanel.getComponent(id);
                    if (tab) {
                        tabpanel.setActiveTab(tab);
                    }
                    else {
                        var obj = eval(a.id);
                        var object = new obj({id: id, closable: !0});
                        if (object.iswin) {
                            object.show();
                        } else {
                            var p = tabpanel.add(object);
                            tabpanel.setActiveTab(p)
                        }
                    }
                }
            }
        });
        Ext.Ajax.request({
            url: 'Bu/UserBu',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.btnbu.menu.removeAll();
                Ext.each(response.results, function (it, id, all) {
                    var item = new Ext.menu.Item({
                        text: it.bu_name,
                        id: it.bu_id,
                        kode: it.bu_kode
                    });
                    item.on('click', this.btnbuItemOnclick, this);
                    this.btnbu.menu.addItem(item);
                }, this);
                this.btnbu.menu.doLayout();

                Ext.Ajax.request({
                    url: 'Cabang/GetUserCabang',
                    method: 'POST',
                    scope: this,
                    success: function (f, a) {
                        var response = Ext.decode(f.responseText);
                        jun.usercabang = response.msg;
                        jun.securityroles = response.sec;
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnbuItemOnclick: function (t, e) {
        this.btnbu.setText(t.kode);
        this.btnbu.class = t.id;
        jun.bu_id = t.id;

        // this.resetBtnCabang();
        this.reloadAllStore(jun.bu_id);

        this.separator.setVisible(true);
        this.btnCabang.setVisible(true);
        this.btnCabang.setText(t.text);
        // this.btnbuOnclick();

        jun.mainTreeUi.getRootNode().reload();
        // jun.mainPanel.removeAll(true);
    },
    reloadAllStore: function (bu_id) {
        jun.rztCabang.reload();
        jun.rztCabangCmp.reload();
        jun.rztCabangUser.reload();

        jun.rztShift.reload();
        jun.rztShiftCmp.reload();

        jun.rztPegawai.reload();
        jun.rztPegawaiCmp.reload();
        jun.rztPegawaiLib.reload();
    }
});
/*
    Main panel
 */
jun.mainTreeUi = new jun.TreeUi({
    region: "west",
    split: !0,
    width: 240
});
jun.ViewportUi = Ext.extend(Ext.Viewport, {
    layout: "border",
    initComponent: function () {
        this.items = [
            {
                xtype: "box",
                region: "north",
                id: "app-header",
                html: SYSTEM_TITLE,
                height: 60
            },
            jun.mainTreeUi,
            {
                xtype: 'tabpanel',
                activeTab: 0,
                region: "center",
                frame: !0,
                id: "mainpanel",
                enableTabScroll: !0
            }
        ];
        jun.ViewportUi.superclass.initComponent.call(this);
    }
});
jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
Ext.onReady(function () {
    var a = function () {
        Ext.get("loading").remove(), Ext.fly("loading-mask").fadeOut({
            remove: !0
        });
    };
    Ext.Ajax.timeout = 10600000;
    Ext.QuickTips.init();
    Ext.Ajax.on("beforerequest", function (conn, opts) {
        jun.myMask.show();
        jun.ajaxCounter++;
    });
    Ext.Ajax.on("requestcomplete", function (conn, response, opts) {
        if (jun.ajaxCounter > 1) {
            jun.ajaxCounter--;
        } else {
            jun.ajaxCounter = 0;
            jun.myMask.hide();
        }
    });
    Ext.Ajax.on("requestexception", function (conn, response, opts) {
        jun.ajaxCounter = 0;
        jun.myMask.hide();
        switch (response.status) {
            case 403:
                window.location.href = 'site/logout';
                break;
            case 500:
                Ext.Msg.alert('Internal Server Error', response.responseText);
                break;
            default :
                Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                break;
        }
    });
    var b = new jun.ViewportUi({});
});