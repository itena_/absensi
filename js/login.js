jun.login = new Ext.extend(Ext.Window, {
    width: 390,
    height: 200,
    layout: "form",
    modal: !0,
    closable: !1,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Login " + "( <p style=\"color:mediumblue; display:inline-block\">AMOS</p> )",
    padding: 5,
    initComponent: function () {
        this.items = [{
            xtype: "box",
            style: "margin:5px",
            html: "Please, make sure that your computer's date and time are correct."
        }, {
            xtype: "form",
            frame: !1,
            bodyStyle: "background-color: #E4E4E4; padding: 10px",
            id: "form-Login",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            plain: !0,
            items: [{
                xtype: "xdatefield", ref: "../tgl", fieldLabel: "Date & Time",
                name: "tgl", id: "tglid", format: "d M Y H:i:s", readOnly: !0, anchor: "100%"
            }, {
                xtype: "textfield",
                hideLabel: !1,
                id: "usernameid",
                ref: "../username",
                maxLength: 128,
                anchor: "100%",
                fieldLabel: "Username",
                name: "loginUsername",
                allowBlank: !1
            }, {
                xtype: "textfield",
                hideLabel: !1,
                id: "passwordid",
                ref: "../password",
                maxLength: 128,
                anchor: "100%",
                fieldLabel: "Password",
                name: "loginPassword",
                inputType: "password",
                allowBlank: !1
            }]
        }];
        this.fbar = {xtype: "toolbar", items: [{xtype: "button", text: "Login", hidden: !1, ref: "../btnLogin"}]};
        jun.login.superclass.initComponent.call(this);
        this.btnLogin.on("click", this.onbtnLoginClick, this);
        this.setDateTime()
    },
    setDateTime: function () {
        Ext.Ajax.request({
            url: "GetDateTime", method: "POST", scope: this, success: function (a, c) {
                var b = Ext.decode(a.responseText);
                this.tgl.setValue(Date.parseDate(b.datetime, "Y-m-d H:i:s"))
            }, failure: function (a, c) {
                switch (c.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert("Failure", "Form fields may not be submitted with invalid values");
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert("Failure", "Ajax communication failed");
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert("Failure", c.result.msg)
                }
            }
        })
    },
    onbtnLoginClick: function () {
        var a = this.username.getValue(), c = this.password.getValue();
        "" == a.trim() || "" == c.trim() ? Ext.Msg.alert("Warning!", "Login Failed") : (a = Ext.getCmp("passwordid").getValue(), a = jun.EncryptPass(a), Ext.getCmp("passwordid").setValue(a), Ext.getCmp("form-Login").getForm().submit({
            scope: this,
            url: "login",
            params: {ip_local: IP_LOCAL},
            waitTitle: "Connecting",
            waitMsg: "Sending data...",
            success: function () {
                window.location =
                    BASE_URL
            },
            failure: function (a, c) {
                Ext.getCmp("form-Login").getForm().reset();
                this.setDateTime();
                switch (c.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert("Failure", "Form fields may not be submitted with invalid values");
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert("Failure", "Ajax communication failed");
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert("Failure", c.result.errors.reason)
                }
            }
        }))
    }
});
jun.ViewportUi = Ext.extend(Ext.Viewport, {
    layout: "border", initComponent: function () {
        this.items = [{xtype: "box", region: "north", applyTo: "header", height: 30}, {
            xtype: "container",
            autoEl: "div",
            region: "west",
            height: 20
        }, {xtype: "container", autoEl: "div", region: "east", height: 20}, {
            xtype: "container",
            autoEl: "div",
            region: "south",
            height: 20
        }];
        jun.ViewportUi.superclass.initComponent.call(this)
    }
});
function round(a, c) {
    return Number(Math.round(a + "e" + c) + "e-" + c)
}

function renderDate(a) {
    var c = new Date, c = Date.parseDate(a, "Y-m-d");
    return c.format("d M Y")
}

function renderDateYMD(a) {
    return (new Date(new Date((new Date(a)).getTime() + 864E5))).toISOString().slice(0, 10)
}

Ext.namespace("jun");
jun.StringGenerator = function (a, c) {
    var b = "";
    -1 < c.indexOf("a") && (b += "abcdefghijklmnopqrstuvwxyz");
    -1 < c.indexOf("A") && (b += "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    -1 < c.indexOf("#") && (b += "0123456789");
    -1 < c.indexOf("!") && (b += "~`!@#$%^&*()_+-={}[]:\";'<>?,./|\\");
    for (var d = "", f = a; 0 < f; --f) d += b[Math.round(Math.random() * (b.length - 1))];
    return d
};
jun.EncryptPass = function (a) {
    for (var c = 0; 13 > c; c++) a = b64_sha512(a);
    return a
};
Ext.ux.NumericField = function (a) {
    Ext.ux.NumericField.superclass.constructor.call(this, Ext.apply({style: "text-align:right;"}, a));
    this.useThousandSeparator && "," == this.decimalSeparator && Ext.isEmpty(a.thousandSeparator) ? this.thousandSeparator = "." : this.allowDecimals && "." == this.thousandSeparator && Ext.isEmpty(a.decimalSeparator) && (this.decimalSeparator = ",");
    this.onFocus = this.onFocus.createSequence(this.onFocus)
};
Ext.extend(Ext.ux.NumericField, Ext.form.NumberField, {
    currencySymbol: null,
    useThousandSeparator: !0,
    thousandSeparator: ",",
    alwaysDisplayDecimals: !1,
    setValue: function (a) {
        Ext.ux.NumericField.superclass.setValue.call(this, a);
        this.setRawValue(this.getFormattedValue(this.getValue()))
    },
    getFormattedValue: function (a) {
        if (Ext.isEmpty(a) || !this.hasFormat()) return a;
        var c = null;
        a = (c = 0 > a) ? -1 * a : a;
        a = this.allowDecimals && this.alwaysDisplayDecimals ? a.toFixed(this.decimalPrecision) : a;
        if (this.useThousandSeparator) {
            if (this.useThousandSeparator &&
                Ext.isEmpty(this.thousandSeparator)) throw"NumberFormatException: invalid thousandSeparator, property must has a valid character.";
            if (this.thousandSeparator == this.decimalSeparator) throw"NumberFormatException: invalid thousandSeparator, thousand separator must be different from decimalSeparator.";
            a = String(a);
            a = a.split(".");
            a[1] = a[1] ? a[1] : null;
            for (var b = a[0], d = /(\d+)(\d{3})/, f = this.thousandSeparator; d.test(b);) b = b.replace(d, "$1" + f + "$2");
            a = b + (a[1] ? this.decimalSeparator + a[1] : "")
        }
        return String.format("{0}{1}{2}",
            c ? "-" : "", Ext.isEmpty(this.currencySymbol) ? "" : this.currencySymbol + " ", a)
    },
    parseValue: function (a) {
        return Ext.ux.NumericField.superclass.parseValue.call(this, this.removeFormat(a))
    },
    removeFormat: function (a) {
        return Ext.isEmpty(a) || !this.hasFormat() ? a : (a = a.replace(this.currencySymbol + " ", ""), a = this.useThousandSeparator ? a.replace(new RegExp("[" + this.thousandSeparator + "]", "g"), "") : a, a)
    },
    getErrors: function (a) {
        return Ext.ux.NumericField.superclass.getErrors.call(this, this.removeFormat(a))
    },
    hasFormat: function () {
        return "." !=
            this.decimalSeparator || 1 == this.useThousandSeparator || !Ext.isEmpty(this.currencySymbol) || this.alwaysDisplayDecimals
    },
    onFocus: function () {
        this.setRawValue(this.removeFormat(this.getRawValue()))
    }
});
Ext.reg("numericfield", Ext.ux.NumericField);
Ext.ns("Ext.ux.form");
Ext.ux.form.XDateField = Ext.extend(Ext.form.DateField, {
    submitFormat: "Y-m-d", onRender: function () {
        Ext.ux.form.XDateField.superclass.onRender.apply(this, arguments);
        var a = this.name || this.el.dom.name;
        this.hiddenField = this.el.insertSibling({
            tag: "input",
            type: "hidden",
            name: a,
            value: this.formatHiddenDate(this.parseDate(this.value))
        });
        this.hiddenName = a;
        this.el.dom.removeAttribute("name");
        this.el.on({
            keyup: {scope: this, fn: this.updateHidden},
            blur: {scope: this, fn: this.updateHidden}
        }, Ext.isIE ? "after" : "before");
        this.setValue =
            this.setValue.createSequence(this.updateHidden)
    }, onDisable: function () {
        Ext.ux.form.XDateField.superclass.onDisable.apply(this, arguments);
        this.hiddenField && this.hiddenField.dom.setAttribute("disabled", "disabled")
    }, onEnable: function () {
        Ext.ux.form.XDateField.superclass.onEnable.apply(this, arguments);
        this.hiddenField && this.hiddenField.dom.removeAttribute("disabled")
    }, formatHiddenDate: function (a) {
        return Ext.isDate(a) ? "timestamp" === this.submitFormat ? a.getTime() / 1E3 : Ext.util.Format.date(a, this.submitFormat) :
            a
    }, updateHidden: function () {
        this.hiddenField.dom.value = this.formatHiddenDate(this.getValue())
    }
});
Ext.reg("xdatefield", Ext.ux.form.XDateField);
Ext.ux.Spinner = Ext.extend(Ext.util.Observable, {
    incrementValue: 1,
    alternateIncrementValue: 5,
    triggerClass: "x-form-spinner-trigger",
    splitterClass: "x-form-spinner-splitter",
    alternateKey: Ext.EventObject.shiftKey,
    defaultValue: 0,
    accelerate: !1,
    constructor: function (a) {
        Ext.ux.Spinner.superclass.constructor.call(this, a);
        Ext.apply(this, a);
        this.mimicing = !1
    },
    init: function (a) {
        this.field = a;
        a.afterMethod("onRender", this.doRender, this);
        a.afterMethod("onEnable", this.doEnable, this);
        a.afterMethod("onDisable", this.doDisable,
            this);
        a.afterMethod("afterRender", this.doAfterRender, this);
        a.afterMethod("onResize", this.doResize, this);
        a.afterMethod("onFocus", this.doFocus, this);
        a.beforeMethod("onDestroy", this.doDestroy, this)
    },
    doRender: function (a, c) {
        var b = this.el = this.field.getEl(), d = this.field;
        d.wrap ? this.wrap = d.wrap.addClass("x-form-field-wrap") : d.wrap = this.wrap = b.wrap({cls: "x-form-field-wrap"});
        this.trigger = this.wrap.createChild({
            tag: "img",
            src: Ext.BLANK_IMAGE_URL,
            cls: "x-form-trigger " + this.triggerClass
        });
        d.width || this.wrap.setWidth(b.getWidth() +
            this.trigger.getWidth());
        this.splitter = this.wrap.createChild({tag: "div", cls: this.splitterClass, style: "width:13px; height:2px;"});
        this.splitter.setRight(Ext.isIE ? 1 : 2).setTop(10).show();
        this.proxy = this.trigger.createProxy("", this.splitter, !0);
        this.proxy.addClass("x-form-spinner-proxy");
        this.proxy.setStyle("left", "0px");
        this.proxy.setSize(14, 1);
        this.proxy.hide();
        this.dd = new Ext.dd.DDProxy(this.splitter.dom.id, "SpinnerDrag", {dragElId: this.proxy.id});
        this.initTrigger();
        this.initSpinner()
    },
    doAfterRender: function () {
        var a;
        Ext.isIE && this.el.getY() != (a = this.trigger.getY()) && (this.el.position(), this.el.setY(a))
    },
    doEnable: function () {
        this.wrap && (this.disabled = !1, this.wrap.removeClass(this.field.disabledClass))
    },
    doDisable: function () {
        this.wrap && (this.disabled = !0, this.wrap.addClass(this.field.disabledClass), this.el.removeClass(this.field.disabledClass))
    },
    doResize: function (a, c) {
        "number" == typeof a && this.el.setWidth(a - this.trigger.getWidth());
        this.wrap.setWidth(this.el.getWidth() + this.trigger.getWidth())
    },
    doFocus: function () {
        this.mimicing ||
        (this.wrap.addClass("x-trigger-wrap-focus"), this.mimicing = !0, Ext.get(Ext.isIE ? document.body : document).on("mousedown", this.mimicBlur, this, {delay: 10}), this.el.on("keydown", this.checkTab, this))
    },
    checkTab: function (a) {
        a.getKey() == a.TAB && this.triggerBlur()
    },
    mimicBlur: function (a) {
        !this.wrap.contains(a.target) && this.field.validateBlur(a) && this.triggerBlur()
    },
    triggerBlur: function () {
        this.mimicing = !1;
        Ext.get(Ext.isIE ? document.body : document).un("mousedown", this.mimicBlur, this);
        this.el.un("keydown", this.checkTab,
            this);
        this.field.beforeBlur();
        this.wrap.removeClass("x-trigger-wrap-focus");
        this.field.onBlur.call(this.field)
    },
    initTrigger: function () {
        this.trigger.addClassOnOver("x-form-trigger-over");
        this.trigger.addClassOnClick("x-form-trigger-click")
    },
    initSpinner: function () {
        this.field.addEvents({spin: !0, spinup: !0, spindown: !0});
        this.keyNav = new Ext.KeyNav(this.el, {
            up: function (a) {
                a.preventDefault();
                this.onSpinUp()
            }, down: function (a) {
                a.preventDefault();
                this.onSpinDown()
            }, pageUp: function (a) {
                a.preventDefault();
                this.onSpinUpAlternate()
            },
            pageDown: function (a) {
                a.preventDefault();
                this.onSpinDownAlternate()
            }, scope: this
        });
        this.repeater = new Ext.util.ClickRepeater(this.trigger, {accelerate: this.accelerate});
        this.field.mon(this.repeater, "click", this.onTriggerClick, this, {preventDefault: !0});
        this.field.mon(this.trigger, {
            mouseover: this.onMouseOver,
            mouseout: this.onMouseOut,
            mousemove: this.onMouseMove,
            mousedown: this.onMouseDown,
            mouseup: this.onMouseUp,
            scope: this,
            preventDefault: !0
        });
        this.field.mon(this.wrap, "mousewheel", this.handleMouseWheel, this);
        this.dd.setXConstraint(0, 0, 10);
        this.dd.setYConstraint(1500, 1500, 10);
        this.dd.endDrag = this.endDrag.createDelegate(this);
        this.dd.startDrag = this.startDrag.createDelegate(this);
        this.dd.onDrag = this.onDrag.createDelegate(this)
    },
    onMouseOver: function () {
        if (!this.disabled) {
            var a = this.getMiddle();
            this.tmpHoverClass = Ext.EventObject.getPageY() < a ? "x-form-spinner-overup" : "x-form-spinner-overdown";
            this.trigger.addClass(this.tmpHoverClass)
        }
    },
    onMouseOut: function () {
        this.trigger.removeClass(this.tmpHoverClass)
    },
    onMouseMove: function () {
        if (!this.disabled) {
            var a =
                this.getMiddle();
            !(Ext.EventObject.getPageY() > a && "x-form-spinner-overup" == this.tmpHoverClass || Ext.EventObject.getPageY() < a && "x-form-spinner-overdown" == this.tmpHoverClass)
        }
    },
    onMouseDown: function () {
        if (!this.disabled) {
            var a = this.getMiddle();
            this.tmpClickClass = Ext.EventObject.getPageY() < a ? "x-form-spinner-clickup" : "x-form-spinner-clickdown";
            this.trigger.addClass(this.tmpClickClass)
        }
    },
    onMouseUp: function () {
        this.trigger.removeClass(this.tmpClickClass)
    },
    onTriggerClick: function () {
        if (!this.disabled && !this.el.dom.readOnly) {
            var a =
                this.getMiddle();
            this["onSpin" + (Ext.EventObject.getPageY() < a ? "Up" : "Down")]()
        }
    },
    getMiddle: function () {
        var a = this.trigger.getTop(), c = this.trigger.getHeight();
        return a + c / 2
    },
    isSpinnable: function () {
        return this.disabled || this.el.dom.readOnly ? (Ext.EventObject.preventDefault(), !1) : !0
    },
    handleMouseWheel: function (a) {
        if (0 != this.wrap.hasClass("x-trigger-wrap-focus")) {
            var c = a.getWheelDelta();
            0 < c ? (this.onSpinUp(), a.stopEvent()) : 0 > c && (this.onSpinDown(), a.stopEvent())
        }
    },
    startDrag: function () {
        this.proxy.show();
        this._previousY =
            Ext.fly(this.dd.getDragEl()).getTop()
    },
    endDrag: function () {
        this.proxy.hide()
    },
    onDrag: function () {
        if (!this.disabled) {
            var a = Ext.fly(this.dd.getDragEl()).getTop(), c = "";
            this._previousY > a && (c = "Up");
            this._previousY < a && (c = "Down");
            "" != c && this["onSpin" + c]();
            this._previousY = a
        }
    },
    onSpinUp: function () {
        if (0 != this.isSpinnable()) if (1 == Ext.EventObject.shiftKey) this.onSpinUpAlternate(); else this.spin(!1, !1), this.field.fireEvent("spin", this), this.field.fireEvent("spinup", this)
    },
    onSpinDown: function () {
        if (0 != this.isSpinnable()) if (1 ==
            Ext.EventObject.shiftKey) this.onSpinDownAlternate(); else this.spin(!0, !1), this.field.fireEvent("spin", this), this.field.fireEvent("spindown", this)
    },
    onSpinUpAlternate: function () {
        0 != this.isSpinnable() && (this.spin(!1, !0), this.field.fireEvent("spin", this), this.field.fireEvent("spinup", this))
    },
    onSpinDownAlternate: function () {
        0 != this.isSpinnable() && (this.spin(!0, !0), this.field.fireEvent("spin", this), this.field.fireEvent("spindown", this))
    },
    spin: function (a, c) {
        var b = parseFloat(this.field.getValue()), d = 1 == c ?
            this.alternateIncrementValue : this.incrementValue;
        1 == a ? b -= d : b += d;
        b = isNaN(b) ? this.defaultValue : b;
        b = this.fixBoundries(b);
        this.field.setRawValue(b)
    },
    fixBoundries: function (a) {
        return void 0 != this.field.minValue && a < this.field.minValue && (a = this.field.minValue), void 0 != this.field.maxValue && a > this.field.maxValue && (a = this.field.maxValue), this.fixPrecision(a)
    },
    fixPrecision: function (a) {
        var c = isNaN(a);
        return this.field.allowDecimals && -1 != this.field.decimalPrecision && !c && a ? parseFloat(parseFloat(a).toFixed(this.field.decimalPrecision)) :
            c ? "" : a
    },
    doDestroy: function () {
        this.trigger && this.trigger.remove();
        this.wrap && (this.wrap.remove(), delete this.field.wrap);
        this.splitter && this.splitter.remove();
        this.dd && (this.dd.unreg(), this.dd = null);
        this.proxy && this.proxy.remove();
        this.repeater && this.repeater.purgeListeners();
        this.mimicing && Ext.get(Ext.isIE ? document.body : document).un("mousedown", this.mimicBlur, this)
    }
});
Ext.form.Spinner = Ext.ux.Spinner;
Ext.ux.form.SpinnerField = Ext.extend(Ext.form.NumberField, {
    actionMode: "wrap",
    deferHeight: !0,
    autoSize: Ext.emptyFn,
    onBlur: Ext.emptyFn,
    adjustSize: Ext.BoxComponent.prototype.adjustSize,
    constructor: function (a) {
        var c = Ext.copyTo({}, a, "incrementValue,alternateIncrementValue,accelerate,defaultValue,triggerClass,splitterClass"),
            c = this.spinner = new Ext.ux.Spinner(c),
            c = a.plugins ? Ext.isArray(a.plugins) ? a.plugins.push(c) : [a.plugins, c] : c;
        Ext.ux.form.SpinnerField.superclass.constructor.call(this, Ext.apply(a, {plugins: c}))
    },
    getResizeEl: function () {
        return this.wrap
    },
    getPositionEl: function () {
        return this.wrap
    },
    alignErrorIcon: function () {
        this.wrap && this.errorIcon.alignTo(this.wrap, "tl-tr", [2, 0])
    },
    validateBlur: function () {
        return !0
    }
});
Ext.reg("spinnerfield", Ext.ux.form.SpinnerField);
Ext.form.SpinnerField = Ext.ux.form.SpinnerField;
Ext.namespace("Ext.ux.grid");
Ext.ux.grid.GridHeaderFilters = function (a) {
    a && Ext.apply(this, a)
};
Ext.extend(Ext.ux.grid.GridHeaderFilters, Ext.util.Observable, {
    fieldHeight: 22,
    fieldPadding: 1,
    highlightOnFilter: !0,
    highlightCls: null,
    stateful: !0,
    applyMode: "auto",
    filters: null,
    filtersInitMode: "replace",
    ensureFilteredVisible: !0,
    cfgFilterInit: !1,
    containerConfig: null,
    labelWidth: 50,
    fcc: null,
    filterFields: null,
    filterContainers: null,
    filterContainerCls: "x-ghf-filter-container",
    init: function (a) {
        this.grid = a;
        a = this.grid.getView();
        a.updateHeaders = a.updateHeaders.createSequence(function () {
                this.renderFilters.call(this)
            },
            this).createInterceptor(function () {
            return this.destroyFilters.call(this), !0
        }, this);
        this.grid.on({
            scope: this,
            render: this.onRender,
            resize: this.onResize,
            columnresize: this.onColResize,
            reconfigure: this.onReconfigure,
            beforedestroy: this.destroyFilters
        });
        this.stateful && (this.grid.on("beforestatesave", this.saveFilters, this), this.grid.on("beforestaterestore", this.loadFilters, this));
        this.grid.getColumnModel().on("hiddenchange", this.onColHidden, this);
        this.grid.addEvents("filterupdate");
        this.addEvents({render: !0});
        this.cfgFilterInit = Ext.isDefined(this.filters) && null !== this.filters;
        this.filters || (this.filters = {});
        this.configure(this.grid.getColumnModel());
        Ext.ux.grid.GridHeaderFilters.superclass.constructor.call(this);
        this.stateful && (Ext.isArray(this.grid.stateEvents) || (this.grid.stateEvents = []), this.grid.stateEvents.push("filterupdate"));
        Ext.apply(this.grid, {
            headerFilters: this, getHeaderFilter: function (a) {
                return this.headerFilters ? this.headerFilters.filters[a] : null
            }, setHeaderFilter: function (a, b) {
                if (this.headerFilters) {
                    var d =
                        {};
                    d[a] = b;
                    this.setHeaderFilters(d)
                }
            }, setHeaderFilters: function (a, b, d) {
                if (this.headerFilters) {
                    b && this.resetHeaderFilters(!1);
                    3 > arguments.length && (d = !0);
                    var f = !1, e;
                    for (e in a) this.headerFilters.filterFields[e] && (f = this.headerFilters.filterFields[e], this.headerFilters.setFieldValue(f, a[e]), this.headerFilters.applyFilter(f, !1), f = !0);
                    f && d && this.headerFilters.storeReload()
                }
            }, getHeaderFilterField: function (a) {
                if (this.headerFilters) return this.headerFilters.filterFields[a] ? this.headerFilters.filterFields[a] :
                    null
            }, resetHeaderFilters: function (a) {
                if (this.headerFilters) {
                    0 == arguments.length && (a = !0);
                    for (var b in this.headerFilters.filterFields) {
                        var d = this.headerFilters.filterFields[b];
                        Ext.isFunction(d.clearValue) ? d.clearValue() : this.headerFilters.setFieldValue(d, "");
                        this.headerFilters.applyFilter(d, !1)
                    }
                    a && this.headerFilters.storeReload()
                }
            }, applyHeaderFilters: function (a) {
                0 == arguments.length && (a = !0);
                this.headerFilters.applyFilters(a)
            }
        })
    },
    configure: function (a) {
        a = a.getColumnsBy(function (a) {
            return Ext.isObject(a.filter) ||
            Ext.isArray(a.filter) ? !0 : !1
        });
        this.fcc = {};
        for (var c = 0; c < a.length; c++) {
            var b = a[c], d = b.filter;
            Ext.isArray(d) || (d = [d]);
            for (var f = 0; f < d.length; f++) {
                var e = Ext.apply({filterName: 0 < f ? b.dataIndex + f : b.dataIndex}, d[f]);
                Ext.apply(e, {
                    columnId: b.id,
                    dataIndex: b.dataIndex,
                    hideLabel: Ext.isEmpty(e.fieldLabel),
                    anchor: "100%"
                });
                this.cfgFilterInit || Ext.isEmpty(e.value) || (this.filters[e.filterName] = Ext.isFunction(e.filterEncoder) ? e.filterEncoder.call(this, e.value) : e.value);
                delete e.value;
                e.applyFilterEvent ? (e.listeners =
                    {scope: this}, e.listeners[e.applyFilterEvent] = function (a) {
                    this.applyFilter(a)
                }, delete e.applyFilterEvent) : "auto" === this.applyMode || "change" === this.applyMode || Ext.isEmpty(this.applyMode) ? e.listeners = {
                    change: function (a) {
                        var b = a.getXType();
                        "combo" != b && "datefield" != b && this.applyFilter(a)
                    }, specialkey: function (a, b) {
                        b.stopPropagation();
                        b.getKey() == b.ENTER && a.el.dom.blur()
                    }, select: function (a) {
                        this.applyFilter(a)
                    }, scope: this
                } : "enter" === this.applyMode && (e.listeners = {
                    specialkey: function (a, b) {
                        b.stopPropagation();
                        b.getKey() == b.ENTER && this.applyFilters()
                    }, scope: this
                });
                var g = this.fcc[e.columnId];
                g || (g = {
                    cls: this.filterContainerCls,
                    border: !1,
                    bodyBorder: !1,
                    labelSeparator: "",
                    labelWidth: this.labelWidth,
                    layout: "form",
                    style: {},
                    items: []
                }, this.containerConfig && Ext.apply(g, this.containerConfig), this.fcc[e.columnId] = g);
                g.items.push(e)
            }
        }
    },
    renderFilterContainer: function (a, c) {
        this.filterContainers || (this.filterContainers = {});
        var b = this.grid.getColumnModel().getIndexById(a), d = this.grid.getView().getHeaderCell(b), d = Ext.get(d);
        Ext.isGecko && (d.dom.style.MozUserSelect = "text");
        d.dom.style.verticalAlign = "top";
        c.width = d.getWidth() - 3;
        b = new Ext.Container(c);
        b.render(d);
        this.filterContainers[a] = b;
        d = 0;
        this.filterFields || (this.filterFields = {});
        var f = b.findBy(function (a) {
            return !Ext.isEmpty(a.filterName)
        });
        if (!Ext.isEmpty(f)) for (var e = 0; e < f.length; e++) this.filterFields[f[e].filterName] = f[e], d += f[e].getHeight();
        return b
    },
    renderFilters: function () {
        if (this.fcc) {
            for (var a in this.fcc) this.renderFilterContainer(a, this.fcc[a]);
            this.setFilters(this.filters);
            this.highlightFilters(this.isFiltered())
        }
    },
    onRender: function () {
        this.renderFilters();
        this.isFiltered() && this.applyFilters(!1);
        this.fireEvent("render", this)
    },
    getFilterField: function (a) {
        return this.filterFields ? this.filterFields[a] : null
    },
    setFilters: function (a, c) {
        if ((this.filters = a) && this.filterFields) {
            for (var b in this.filters) this.filterFields[b] || delete this.filters[b];
            for (b in this.filterFields) {
                var d = this.filterFields[b], f = this.filters[d.filterName];
                Ext.isEmpty(f) ? c && this.setFieldValue(d, "") : this.setFieldValue(d,
                    f)
            }
        }
    },
    onColResize: function (a, c) {
        if (this.filterContainers) {
            var b = this.grid.getColumnModel().getColumnId(a);
            if (b = this.filterContainers[b]) isNaN(c) && (c = 0), b.setWidth(3 > c ? 0 : c - 3), b.doLayout(!1, !0)
        }
    },
    onResize: function () {
        for (var a = this.grid.getColumnModel().getColumnCount(), c = 0; c < a; c++) {
            var b = this.grid.getView().getHeaderCell(c), b = Ext.get(b);
            this.onColResize(c, b.getWidth())
        }
    },
    onColHidden: function (a, c, b) {
        b || (a = this.grid.getColumnModel().getColumnWidth(c), this.onColResize(c, a))
    },
    onReconfigure: function (a, c,
                             b) {
        this.destroyFilters();
        this.configure(b);
        this.renderFilters()
    },
    saveFilters: function (a, c) {
        var b = {}, d;
        for (d in this.filters) b[d] = this.filters[d];
        return c.gridHeaderFilters = b, !0
    },
    loadFilters: function (a, c) {
        var b = c.gridHeaderFilters;
        b && (this.cfgFilterInit ? "merge" === this.filtersInitMode && Ext.apply(b, this.filters) : this.filters = b)
    },
    isFiltered: function () {
        for (var a in this.filters) if (!Ext.isEmpty(this.filters[a])) return !0;
        return !1
    },
    highlightFilters: function (a) {
        if (this.highlightOnFilter && this.filterContainers &&
            this.grid.getView().mainHd) {
            var c = this.grid.getView().mainHd.child(".x-grid3-hd-row");
            Ext.isEmpty(this.highlightCls) ? c.setStyle("background-color", a ? this.highlightColor : "") : a ? c.addClass(this.highlightCls) : c.removeClass(this.highlightCls)
        }
    },
    getFieldValue: function (a) {
        return Ext.isFunction(a.filterEncoder) ? a.filterEncoder.call(a, a.getValue()) : a.getValue()
    },
    setFieldValue: function (a, c) {
        Ext.isFunction(a.filterDecoder) && (c = a.filterDecoder.call(a, c));
        a.setValue(c)
    },
    applyFilter: function (a, c) {
        2 > arguments.length &&
        (c = !0);
        if (a && a.isValid() && (!a.disabled || Ext.isDefined(this.grid.store.baseParams[a.filterName]))) {
            var b = this.getFieldValue(a);
            if (a.disabled || Ext.isEmpty(b)) delete this.grid.store.baseParams[a.filterName], delete this.filters[a.filterName]; else if (this.grid.store.baseParams[a.filterName] = b, this.filters[a.filterName] = b, this.ensureFilteredVisible) {
                var d = this.grid.getColumnModel().getIndexById(a.columnId);
                0 <= d && this.grid.getColumnModel().isHidden(d) && this.grid.getColumnModel().setHidden(d, !1)
            }
            this.highlightFilters(this.isFiltered());
            this.grid.fireEvent("filterupdate", a.filterName, b, a);
            c && this.storeReload()
        }
    },
    applyFilters: function (a) {
        1 > arguments.length && (a = !0);
        for (var c in this.filterFields) this.applyFilter(this.filterFields[c], !1);
        a && this.storeReload()
    },
    storeReload: function () {
        if (this.grid.store.lastOptions) {
            var a = {start: 0};
            this.grid.store.lastOptions.params && this.grid.store.lastOptions.params.limit && (a.limit = this.grid.store.lastOptions.params.limit);
            this.grid.store.load({params: a})
        }
    },
    getFilterContainer: function (a) {
        return this.filterContainers ?
            this.filterContainers[a] : null
    },
    destroyFilters: function () {
        if (this.filterFields) for (var a in this.filterFields) Ext.destroy(this.filterFields[a]), delete this.filterFields[a];
        if (this.filterContainers) for (a in this.filterContainers) Ext.destroy(this.filterContainers[a]), delete this.filterContainers[a]
    }
});
Ext.ux.grid.CheckColumn = Ext.extend(Ext.grid.Column, {
    processEvent: function (a, c, b, d, f) {
        if ("mousedown" == a) {
            var e = b.store.getAt(d);
            e.set(this.dataIndex, !e.data[this.dataIndex]);
            return !1
        }
        return Ext.grid.ActionColumn.superclass.processEvent.apply(this, arguments)
    }, renderer: function (a, c, b) {
        c.css += " x-grid3-check-col-td";
        return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', a ? "-on" : "")
    }, init: Ext.emptyFn
});
Ext.preg("checkcolumn", Ext.ux.grid.CheckColumn);
Ext.grid.CheckColumn = Ext.ux.grid.CheckColumn;
Ext.grid.Column.types.checkcolumn = Ext.ux.grid.CheckColumn;
Ext.namespace("Ext.ux.form");
Ext.ux.form.UCTextField = Ext.extend(Ext.form.TextField, {
    onRender: function () {
        Ext.ux.form.UCTextField.superclass.onRender.apply(this, arguments);
        var a = this, c = this.el;
        c.applyStyles({textTransform: "uppercase"});
        this.mon(c, "blur", function () {
            a.setValue(a.getValue().toUpperCase())
        })
    }
});
Ext.ux.form.CodeMirror = Ext.extend(Ext.form.TextArea, {
    mode: null, modeJs: null, codeMirrorPath: null, codeMirrorConfig: {}, initComponent: function () {
        function a(a) {
            return /\bCodeMirror-fullscreen\b/.test(a.getWrapperElement().className)
        }

        function c(a, c) {
            var f = a.getWrapperElement();
            c ? (f.className += " CodeMirror-fullscreen", f.style.height = (window.innerHeight || (document.documentElement || document.body).clientHeight) + "px", document.documentElement.style.overflow = "hidden") : (f.className = f.className.replace(" CodeMirror-fullscreen",
                ""), f.style.height = "", document.documentElement.style.overflow = "");
            a.refresh()
        }

        Ext.ux.form.CodeMirror.superclass.initComponent.apply(this, arguments);
        CodeMirror.on(window, "resize", function () {
            var a = document.body.getElementsByClassName("CodeMirror-fullscreen")[0];
            a && (a.CodeMirror.getWrapperElement().style.height = (window.innerHeight || (document.documentElement || document.body).clientHeight) + "px")
        });
        if (!this.codeMirrorPath) throw"Ext.ux.form.CodeMirror: codeMirrorPath required";
        if (!this.mode) throw"Ext.ux.form.CodeMirror: mode required";
        this.initialized = !1;
        this.on({
            resize: function (a, c, f) {
                if (a.initialized) {
                    var e = this.codeEditor.getScrollerElement().style;
                    e.width = c + "px";
                    a.grow || (e.height = f + "px");
                    this.codeEditor.refresh()
                }
            }, afterrender: function () {
                var b = this;
                CodeMirror.modeURL = b.codeMirrorPath + "/mode/%N/%N.js";
                Ext.applyIf(b.codeMirrorConfig, {
                    mode: b.mode,
                    lineWrapping: !0,
                    matchBrackets: !0,
                    lineNumbers: !0,
                    viewportMargin: Infinity,
                    extraKeys: {
                        F11: function (b) {
                            c(b, !a(b))
                        }, Esc: function (b) {
                            a(b) && c(b, !1)
                        }
                    }
                });
                b.codeEditor = CodeMirror.fromTextArea(b.el.dom,
                    b.codeMirrorConfig);
                b.codeEditor.on("change", function () {
                    b.fireEvent("change", b, b.codeEditor.getValue())
                });
                b.codeEditor.on("blur", function () {
                    b.fireEvent("blur", b)
                });
                (function () {
                    b.codeEditor.setOption("mode", b.mode);
                    CodeMirror.autoLoadMode(b.codeEditor, b.modeJs || b.mode)
                }).defer(201);
                b.initialized = !0;
                b.codeEditor.getWrapperElement().style.backgroundColor = "#fff";
                b.codeEditor.getWrapperElement().style.border = "1px solid #B5B8C8";
                (function () {
                    b.codeEditor.refresh()
                }).defer(1)
            }
        }, this)
    }, getValue: function () {
        this.initialized &&
        this.codeEditor.save();
        return Ext.ux.form.CodeMirror.superclass.getValue.apply(this, arguments)
    }, setValue: function (a) {
        var c = this;
        this.initialized && (this.codeEditor.setValue(a || ""), function () {
            c.codeEditor.refresh()
        }.defer(1));
        return Ext.ux.form.CodeMirror.superclass.setValue.apply(this, arguments)
    }, validate: function () {
        this.getValue();
        Ext.ux.form.CodeMirror.superclass.validate.apply(this, arguments)
    }
});
Ext.reg("codemirror", Ext.ux.form.CodeMirror);

function getIPs(a) {
    function c(c) {
        c = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(c)[1];
        void 0 === b[c] && a(c);
        b[c] = !0
    }

    var b = {}, d = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
    d || (d = iframe.contentWindow, d = d.RTCPeerConnection || d.mozRTCPeerConnection || d.webkitRTCPeerConnection);
    var f = new d({iceServers: [{urls: "stun:stun.services.mozilla.com"}]}, {optional: [{RtpDataChannels: !0}]});
    f.onicecandidate = function (a) {
        a.candidate && c(a.candidate.candidate)
    };
    f.createDataChannel("");
    f.createOffer(function (a) {
        f.setLocalDescription(a, function () {
        }, function () {
        })
    }, function () {
    });
    setTimeout(function () {
        f.localDescription.sdp.split("\n").forEach(function (a) {
            0 === a.indexOf("a=candidate:") && c(a)
        })
    }, 1E3)
};
jun.win = new Ext.extend(Ext.Window, {
    layout: "fit",
    width: 300,
    height: 150,
    closable: !1,
    resizable: !1,
    plain: !0,
    border: !1,
    initComponent: function () {
        this.items = [jun.login];
        jun.win.superclass.initComponent.call(this)
    }
});
Ext.onReady(function () {
    Ext.QuickTips.init();
    (new jun.login({})).show()
});
var hexcase = 0, b64pad = "";

function hex_sha512(a) {
    return rstr2hex(rstr_sha512(str2rstr_utf8(a)))
}

function b64_sha512(a) {
    return rstr2b64(rstr_sha512(str2rstr_utf8(a)))
}

function any_sha512(a, c) {
    return rstr2any(rstr_sha512(str2rstr_utf8(a)), c)
}

function hex_hmac_sha512(a, c) {
    return rstr2hex(rstr_hmac_sha512(str2rstr_utf8(a), str2rstr_utf8(c)))
}

function b64_hmac_sha512(a, c) {
    return rstr2b64(rstr_hmac_sha512(str2rstr_utf8(a), str2rstr_utf8(c)))
}

function any_hmac_sha512(a, c, b) {
    return rstr2any(rstr_hmac_sha512(str2rstr_utf8(a), str2rstr_utf8(c)), b)
}

function sha512_vm_test() {
    return "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f" == hex_sha512("abc").toLowerCase()
}

function rstr_sha512(a) {
    return binb2rstr(binb_sha512(rstr2binb(a), 8 * a.length))
}

function rstr_hmac_sha512(a, c) {
    var b = rstr2binb(a);
    32 < b.length && (b = binb_sha512(b, 8 * a.length));
    for (var d = Array(32), f = Array(32), e = 0; 32 > e; e++) d[e] = b[e] ^ 909522486, f[e] = b[e] ^ 1549556828;
    b = binb_sha512(d.concat(rstr2binb(c)), 1024 + 8 * c.length);
    return binb2rstr(binb_sha512(f.concat(b), 1536))
}

function rstr2hex(a) {
    try {
        hexcase
    } catch (e) {
        hexcase = 0
    }
    for (var c = hexcase ? "0123456789ABCDEF" : "0123456789abcdef", b = "", d, f = 0; f < a.length; f++) d = a.charCodeAt(f), b += c.charAt(d >>> 4 & 15) + c.charAt(d & 15);
    return b
}

function rstr2b64(a) {
    try {
        b64pad
    } catch (g) {
        b64pad = ""
    }
    for (var c = "", b = a.length, d = 0; d < b; d += 3) for (var f = a.charCodeAt(d) << 16 | (d + 1 < b ? a.charCodeAt(d + 1) << 8 : 0) | (d + 2 < b ? a.charCodeAt(d + 2) : 0), e = 0; 4 > e; e++) c = 8 * d + 6 * e > 8 * a.length ? c + b64pad : c + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(f >>> 6 * (3 - e) & 63);
    return c
}

function rstr2any(a, c) {
    var b = c.length, d, f, e, g, k, n = Array(Math.ceil(a.length / 2));
    for (d = 0; d < n.length; d++) n[d] = a.charCodeAt(2 * d) << 8 | a.charCodeAt(2 * d + 1);
    var m = Math.ceil(8 * a.length / (Math.log(c.length) / Math.log(2))), u = Array(m);
    for (f = 0; f < m; f++) {
        k = [];
        for (d = g = 0; d < n.length; d++) if (g = (g << 16) + n[d], e = Math.floor(g / b), g -= e * b, 0 < k.length || 0 < e) k[k.length] = e;
        u[f] = g;
        n = k
    }
    b = "";
    for (d = u.length - 1; 0 <= d; d--) b += c.charAt(u[d]);
    return b
}

function str2rstr_utf8(a) {
    for (var c = "", b = -1, d, f; ++b < a.length;) d = a.charCodeAt(b), f = b + 1 < a.length ? a.charCodeAt(b + 1) : 0, 55296 <= d && 56319 >= d && 56320 <= f && 57343 >= f && (d = 65536 + ((d & 1023) << 10) + (f & 1023), b++), 127 >= d ? c += String.fromCharCode(d) : 2047 >= d ? c += String.fromCharCode(192 | d >>> 6 & 31, 128 | d & 63) : 65535 >= d ? c += String.fromCharCode(224 | d >>> 12 & 15, 128 | d >>> 6 & 63, 128 | d & 63) : 2097151 >= d && (c += String.fromCharCode(240 | d >>> 18 & 7, 128 | d >>> 12 & 63, 128 | d >>> 6 & 63, 128 | d & 63));
    return c
}

function str2rstr_utf16le(a) {
    for (var c = "", b = 0; b < a.length; b++) c += String.fromCharCode(a.charCodeAt(b) & 255, a.charCodeAt(b) >>> 8 & 255);
    return c
}

function str2rstr_utf16be(a) {
    for (var c = "", b = 0; b < a.length; b++) c += String.fromCharCode(a.charCodeAt(b) >>> 8 & 255, a.charCodeAt(b) & 255);
    return c
}

function rstr2binb(a) {
    for (var c = Array(a.length >> 2), b = 0; b < c.length; b++) c[b] = 0;
    for (b = 0; b < 8 * a.length; b += 8) c[b >> 5] |= (a.charCodeAt(b / 8) & 255) << 24 - b % 32;
    return c
}

function binb2rstr(a) {
    for (var c = "", b = 0; b < 32 * a.length; b += 8) c += String.fromCharCode(a[b >> 5] >>> 24 - b % 32 & 255);
    return c
}

var sha512_k;

function binb_sha512(a, c) {
    void 0 == sha512_k && (sha512_k = [new int64(1116352408, -685199838), new int64(1899447441, 602891725), new int64(-1245643825, -330482897), new int64(-373957723, -2121671748), new int64(961987163, -213338824), new int64(1508970993, -1241133031), new int64(-1841331548, -1357295717), new int64(-1424204075, -630357736), new int64(-670586216, -1560083902), new int64(310598401, 1164996542), new int64(607225278, 1323610764), new int64(1426881987, -704662302), new int64(1925078388, -226784913), new int64(-2132889090,
        991336113), new int64(-1680079193, 633803317), new int64(-1046744716, -815192428), new int64(-459576895, -1628353838), new int64(-272742522, 944711139), new int64(264347078, -1953704523), new int64(604807628, 2007800933), new int64(770255983, 1495990901), new int64(1249150122, 1856431235), new int64(1555081692, -1119749164), new int64(1996064986, -2096016459), new int64(-1740746414, -295247957), new int64(-1473132947, 766784016), new int64(-1341970488, -1728372417), new int64(-1084653625, -1091629340), new int64(-958395405, 1034457026),
        new int64(-710438585, -1828018395), new int64(113926993, -536640913), new int64(338241895, 168717936), new int64(666307205, 1188179964), new int64(773529912, 1546045734), new int64(1294757372, 1522805485), new int64(1396182291, -1651133473), new int64(1695183700, -1951439906), new int64(1986661051, 1014477480), new int64(-2117940946, 1206759142), new int64(-1838011259, 344077627), new int64(-1564481375, 1290863460), new int64(-1474664885, -1136513023), new int64(-1035236496, -789014639), new int64(-949202525, 106217008), new int64(-778901479,
            -688958952), new int64(-694614492, 1432725776), new int64(-200395387, 1467031594), new int64(275423344, 851169720), new int64(430227734, -1194143544), new int64(506948616, 1363258195), new int64(659060556, -544281703), new int64(883997877, -509917016), new int64(958139571, -976659869), new int64(1322822218, -482243893), new int64(1537002063, 2003034995), new int64(1747873779, -692930397), new int64(1955562222, 1575990012), new int64(2024104815, 1125592928), new int64(-2067236844, -1578062990), new int64(-1933114872, 442776044),
        new int64(-1866530822, 593698344), new int64(-1538233109, -561857047), new int64(-1090935817, -1295615723), new int64(-965641998, -479046869), new int64(-903397682, -366583396), new int64(-779700025, 566280711), new int64(-354779690, -840897762), new int64(-176337025, -294727304), new int64(116418474, 1914138554), new int64(174292421, -1563912026), new int64(289380356, -1090974290), new int64(460393269, 320620315), new int64(685471733, 587496836), new int64(852142971, 1086792851), new int64(1017036298, 365543100), new int64(1126000580,
            -1676669620), new int64(1288033470, -885112138), new int64(1501505948, -60457430), new int64(1607167915, 987167468), new int64(1816402316, 1246189591)]);
    var b = [new int64(1779033703, -205731576), new int64(-1150833019, -2067093701), new int64(1013904242, -23791573), new int64(-1521486534, 1595750129), new int64(1359893119, -1377402159), new int64(-1694144372, 725511199), new int64(528734635, -79577749), new int64(1541459225, 327033209)],
        d = new int64(0, 0), f = new int64(0, 0), e = new int64(0, 0), g = new int64(0, 0), k = new int64(0, 0),
        n = new int64(0, 0), m = new int64(0, 0), u = new int64(0, 0), v = new int64(0, 0), y = new int64(0, 0),
        w = new int64(0, 0), x = new int64(0, 0), z = new int64(0, 0), A = new int64(0, 0), q = new int64(0, 0),
        r = new int64(0, 0), t = new int64(0, 0), h, l, p = Array(80);
    for (l = 0; 80 > l; l++) p[l] = new int64(0, 0);
    a[c >> 5] |= 128 << 24 - (c & 31);
    a[(c + 128 >> 10 << 5) + 31] = c;
    for (l = 0; l < a.length; l += 32) {
        int64copy(e, b[0]);
        int64copy(g, b[1]);
        int64copy(k, b[2]);
        int64copy(n, b[3]);
        int64copy(m, b[4]);
        int64copy(u, b[5]);
        int64copy(v, b[6]);
        int64copy(y, b[7]);
        for (h = 0; 16 > h; h++) p[h].h = a[l +
        2 * h], p[h].l = a[l + 2 * h + 1];
        for (h = 16; 80 > h; h++) int64rrot(q, p[h - 2], 19), int64revrrot(r, p[h - 2], 29), int64shr(t, p[h - 2], 6), x.l = q.l ^ r.l ^ t.l, x.h = q.h ^ r.h ^ t.h, int64rrot(q, p[h - 15], 1), int64rrot(r, p[h - 15], 8), int64shr(t, p[h - 15], 7), w.l = q.l ^ r.l ^ t.l, w.h = q.h ^ r.h ^ t.h, int64add4(p[h], x, p[h - 7], w, p[h - 16]);
        for (h = 0; 80 > h; h++) z.l = m.l & u.l ^ ~m.l & v.l, z.h = m.h & u.h ^ ~m.h & v.h, int64rrot(q, m, 14), int64rrot(r, m, 18), int64revrrot(t, m, 9), x.l = q.l ^ r.l ^ t.l, x.h = q.h ^ r.h ^ t.h, int64rrot(q, e, 28), int64revrrot(r, e, 2), int64revrrot(t, e, 7), w.l = q.l ^ r.l ^ t.l, w.h =
            q.h ^ r.h ^ t.h, A.l = e.l & g.l ^ e.l & k.l ^ g.l & k.l, A.h = e.h & g.h ^ e.h & k.h ^ g.h & k.h, int64add5(d, y, x, z, sha512_k[h], p[h]), int64add(f, w, A), int64copy(y, v), int64copy(v, u), int64copy(u, m), int64add(m, n, d), int64copy(n, k), int64copy(k, g), int64copy(g, e), int64add(e, d, f);
        int64add(b[0], b[0], e);
        int64add(b[1], b[1], g);
        int64add(b[2], b[2], k);
        int64add(b[3], b[3], n);
        int64add(b[4], b[4], m);
        int64add(b[5], b[5], u);
        int64add(b[6], b[6], v);
        int64add(b[7], b[7], y)
    }
    d = Array(16);
    for (l = 0; 8 > l; l++) d[2 * l] = b[l].h, d[2 * l + 1] = b[l].l;
    return d
}

function int64(a, c) {
    this.h = a;
    this.l = c
}

function int64copy(a, c) {
    a.h = c.h;
    a.l = c.l
}

function int64rrot(a, c, b) {
    a.l = c.l >>> b | c.h << 32 - b;
    a.h = c.h >>> b | c.l << 32 - b
}

function int64revrrot(a, c, b) {
    a.l = c.h >>> b | c.l << 32 - b;
    a.h = c.l >>> b | c.h << 32 - b
}

function int64shr(a, c, b) {
    a.l = c.l >>> b | c.h << 32 - b;
    a.h = c.h >>> b
}

function int64add(a, c, b) {
    var d = (c.l & 65535) + (b.l & 65535), f = (c.l >>> 16) + (b.l >>> 16) + (d >>> 16),
        e = (c.h & 65535) + (b.h & 65535) + (f >>> 16);
    c = (c.h >>> 16) + (b.h >>> 16) + (e >>> 16);
    a.l = d & 65535 | f << 16;
    a.h = e & 65535 | c << 16
}

function int64add4(a, c, b, d, f) {
    var e = (c.l & 65535) + (b.l & 65535) + (d.l & 65535) + (f.l & 65535),
        g = (c.l >>> 16) + (b.l >>> 16) + (d.l >>> 16) + (f.l >>> 16) + (e >>> 16),
        k = (c.h & 65535) + (b.h & 65535) + (d.h & 65535) + (f.h & 65535) + (g >>> 16);
    c = (c.h >>> 16) + (b.h >>> 16) + (d.h >>> 16) + (f.h >>> 16) + (k >>> 16);
    a.l = e & 65535 | g << 16;
    a.h = k & 65535 | c << 16
}

function int64add5(a, c, b, d, f, e) {
    var g = (c.l & 65535) + (b.l & 65535) + (d.l & 65535) + (f.l & 65535) + (e.l & 65535),
        k = (c.l >>> 16) + (b.l >>> 16) + (d.l >>> 16) + (f.l >>> 16) + (e.l >>> 16) + (g >>> 16),
        n = (c.h & 65535) + (b.h & 65535) + (d.h & 65535) + (f.h & 65535) + (e.h & 65535) + (k >>> 16);
    c = (c.h >>> 16) + (b.h >>> 16) + (d.h >>> 16) + (f.h >>> 16) + (e.h >>> 16) + (n >>> 16);
    a.l = g & 65535 | k << 16;
    a.h = n & 65535 | c << 16
};
