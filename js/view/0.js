Ext.reg('uctextfield', Ext.ux.form.UCTextField);
jun.bulan = new Ext.data.ArrayStore({
    fields: ["noBulan", "namaBulan"],
    data: [
        ["01", "Januari"],
        ["02", "Februari"],
        ["03", "Maret"],
        ["04", "April"],
        ["05", "Mei"],
        ["06", "Juni"],
        ["07", "Juli"],
        ["08", "Agustus"],
        ["09", "September"],
        ["10", "Oktober"],
        ["11", "November"],
        ["12", "Desember"]
    ]
});
jun.comboBulan = Ext.extend(Ext.form.ComboBox, {
    displayField: "namaBulan",
    valueField: "noBulan",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    emptyText: "Pilih Bulan",
    selectOnFocus: !0,
    lastQuery: "",
    initComponent: function () {
        this.store = jun.bulan, jun.comboBulan.superclass.initComponent.call(this)
    }
});

jun.active = new Ext.data.ArrayStore({
    fields: ["activeVal", "activeName"],
    data: [
        [0, "Non Aktif"],
        [1, "Aktif"]
    ]
});
jun.comboActive = Ext.extend(Ext.form.ComboBox, {
    displayField: "activeName",
    valueField: "activeVal",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    hiddenValue: 1,
    value: 1,
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.active, jun.comboActive.superclass.initComponent.call(this)
    }
});
jun.renderActive = function (a, b, c) {
    return a == 1 ? "Aktif" : "Non Aktif"
};

jun.getShift = function (a) {
    var b = jun.rztShiftLib, c = b.findExact("shift_id", a);
    return b.getAt(c);
};
jun.getDays = function (a) {
    var b = jun.rztDaysLib, c = b.findExact("day_id", a);
    return b.getAt(c);
};
jun.renderKodeShift = function (a, b, c) {
    var jb = jun.getShift(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode_shift;
};
jun.renderNamaDays = function (a, b, c) {
    var jb = jun.getDays(a);
    if (jb == null) {
        return '';
    }
    return jb.data.day_name;
};
jun.getPin = function (a) {
    var b = jun.rztPinLib, c = b.findExact("pin_id", a);
    return b.getAt(c);
};
jun.getNama = function () {
    var b = jun.rztPinLib, c = b.findExact("pin_id", a);
    return b.getAt(c);
};
jun.renderPIN = function (a, b, c) {
    var jb = jun.getPin(a);
    if (jb == null) {
        return '';
    }
    return jb.data.PIN;
};
jun.renderNamaLengkap = function (a, b, c) {
    var jb = jun.getPin(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_lengkap;
};
jun.getStatus = function (a) {
    var b = jun.rztStatusCmp;
    c = b.findExact("status_id", a);
    return b.getAt(c);
};
jun.renderStatus = function (a, b, c) {
    var jb = jun.getStatus(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama;
};
jun.getLevel = function (a) {
    var b = jun.rztLevelingLib;
    c = b.findExact("leveling_id", a);
    return b.getAt(c);
};
jun.renderLevelKode = function (a, b, c) {
    var jb = jun.getLevel(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode;
};
jun.renderLeveling = function (a, b, c) {
    var test = jun.rztLevelingLib.findExact("leveling_id", a);
    var data = jun.rztLevelingLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama;
};
jun.renderLevelNama = function (a, b, c) {
    var jb = jun.getLevel(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama;
};
jun.getGolongan = function (a) {
    var b = jun.rztGolonganLib;
    c = b.findExact("golongan_id", a);
    return b.getAt(c);
};
jun.renderGolongan = function (a, b, c) {
    var test = jun.rztGolonganLib.findExact("golongan_id", a);
    var data = jun.rztGolonganLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama;
};
jun.renderGolonganKode = function (a, b, c) {
    var jb = jun.getGolongan(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode;
};
jun.renderGolonganNama = function (a, b, c) {
    var jb = jun.getGolongan(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama;
};

jun.renderNIK = function (a, b, c) {
    var test = jun.rztPegawaiCmp.findExact("pegawai_id", a);
    var data = jun.rztPegawaiCmp.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nik;
};
jun.renderPegawai = function (a, b, c) {
    var test = jun.rztPegawaiCmp.findExact("pegawai_id", a);
    var data = jun.rztPegawaiCmp.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama_lengkap;
};

jun.renderPIN = function (a, b, c) {
    var test = jun.rztNikAllGrid.findExact("pegawai_id", a);
    var data = jun.rztNikAllGrid.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.PIN;
};
jun.renderPegawaiAll = function (a, b, c) {
    var test = jun.rztNikAllGrid.findExact("pegawai_id", a);
    var data = jun.rztNikAllGrid.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama_lengkap;
};

jun.renderPeriode = function (a, b, c) {
    var test = jun.rztPeriodeLib.findExact("periode_id", a);
    var data = jun.rztPeriodeLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.kode_periode;
};

jun.getShift = function (a) {
    var b = jun.rztShiftLib, c = b.findExact("shift_id", a);
    return b.getAt(c)
};
jun.renderKodeShift = function (a, b, c) {
    var jb = jun.getShift(a);
    return jb.data.kode_shift;
};
jun.renderInShift = function (a, b, c) {
    var jb = jun.getShift(a);
    return jb.data.in_time;
};
jun.renderOutShift = function (a, b, c) {
    var jb = jun.getShift(a);
    return jb.data.out_time;
};
jun.getShift2 = function (a) {
    var b = jun.rztAssignShift, c = b.findExact("shift_id", a);
    return b.getAt(c)
};
jun.renderKodeShift2 = function (a, b, c) {
    var jb = jun.getShift2(a);
    return jb.data.kode_shift;
};
jun.getCabang = function (a) {
    var b = jun.rztCabangCmp, c = b.findExact("cabang_id", a);
    return b.getAt(c)
};
jun.renderCabang = function (a, b, c) {
    var jb = jun.getCabang(a);
    return jb.data.kode_cabang;
};

//jun.getPin = function (a) {
//    var b = jun.rztPinLib, c = b.findExact("pin_id", a);
//    return b.getAt(c)
//};
jun.BackupRestoreWin = Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Backup / Restore",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4; padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file restore (*.pbu.gz)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Delete All Transaction",
                    hidden: !1,
                    ref: "../btnDelete"
                },
                {
                    xtype: "button",
                    text: "Download Backup",
                    hidden: !1,
                    ref: "../btnBackup"
                },
                {
                    xtype: "button",
                    text: "Upload Restore",
                    hidden: !1,
                    ref: "../btnRestore"
                }
            ]
        };
        jun.BackupRestoreWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
        this.btnRestore.on("click", this.btnRestoreClick, this);
        this.btnDelete.on("click", this.deleteRec, this);
    },
    onbtnBackupClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().reset();
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "Site/BackupAll";
        var form = Ext.getCmp('form-BackupRestoreWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnRestoreClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = false;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "site/RestoreAll";
        if (Ext.getCmp("form-BackupRestoreWin").getForm().isValid()) {
            Ext.getCmp("form-BackupRestoreWin").getForm().submit({
                url: 'site/RestoreAll',
                waitMsg: 'Uploading your restore...',
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.Msg.alert('Successfully', response.msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Confirm', 'Are you sure delete all transaction?', this.btnDeleteClick, this);
    },
    btnDeleteClick: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: 'site/DeleteTransAll',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
//======================================= Untuk Baca File Excel===============================
jun.dataStockOpname = '';
jun.dataSales = '';
jun.namasheet = '';
function readFileExcel(e) {
    dataStockOpname = "";
    if (itemFile.files.length == 0)
        return;
    var files = itemFile.files;
    var f = files[0];
    {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function (e) {
            var data = e.target.result;
            var wb;
            var arr = fixdata(data);
            wb = XLS.read(btoa(arr), {type: 'base64'});
            jun.dataStockOpname = to_json(wb);
            jun.namasheet = wb.SheetNames[0];
        };
        reader.readAsArrayBuffer(f);
    }
}
function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLS.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}
function fixdata(data) {
    var o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l)
        o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
}
//======================================= END OF : Untuk Baca File Excel=============================

jun.StatusKota = new Ext.data.ArrayStore({
    fields: ["status_kota", "status"],
    data: [
        [0, "Dalam Kota"],
        [1, "Luar Kota"]
    ]
});
jun.comboStatusKota = Ext.extend(Ext.form.ComboBox, {
    displayField: "status",
    valueField: "status_kota",
    typeAhead: !0,
    mode: "local",
    forceSelection: true,
    triggerAction: "all",
    name: "status_kota",
    hiddenName: "status_kota",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusKota;
        jun.comboStatusKota.superclass.initComponent.call(this);
    }
});


jun.ResetAbsensiWin = Ext.extend(Ext.Window, {
    title: "Reset Absensi",
    iconCls: "silk13-report",
    modez: 1,
    width: 420,
    height: 140,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ResetAbsensiWin",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: 'Me-RESET data absensi berarti menghapus semua data di RESULT, REVIEW FASE 2 dan TRANSFER PEGAWAI, serta menjadikan REVIEW FASE 1 menjadi belum valid. Apa Anda yakin?'
                    }
                ]
            }   
                    
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_excel",
//                    text: "Save to Excel",
//                    ref: "../btnSave"
//                },
                {
                    xtype: "button",
//                    iconCls: "silk13-html",
                    text: "YA RESET",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ResetAbsensiWin.superclass.initComponent.call(this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp('form-ResetAbsensiWin').getForm().submit({
            url: 'site/ResetAbsensi',
//            timeout: 1000,
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    }
});
jun.CreateSimulasiFase1Win = Ext.extend(Ext.Window, {
    title: "Buat Simulasi Fase 1",
    iconCls: "silk13-report",
    modez: 1,
    width: 420,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-CreateSimulasiFase1Win",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: false,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: false,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    }
                ]
            }

        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CreateSimulasiFase1Win.superclass.initComponent.call(this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    saveForm: function ()
    {
       var urlz;
       urlz = 'Fp/CreateSimulasiFase1';
       Ext.getCmp('form-CreateSimulasiFase1Win').getForm().submit({
           url: urlz,
           // timeout: 1000,
           scope: this,
           params: {
               mode: this.modez,
               cabang: this.cabang.getRawValue()
           },
           success: function (f, a) {
               var response = Ext.decode(a.response.responseText);
               Ext.MessageBox.show({
                   title: 'Info',
                   msg: response.msg,
                   buttons: Ext.MessageBox.OK,
                   icon: Ext.MessageBox.INFO
               });
               if (this.closeForm) {
                   this.close();
               }
           },
           failure: function (f, a) {
               switch (a.failureType) {
                   case Ext.form.Action.CLIENT_INVALID:
                       Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                       break;
                   case Ext.form.Action.CONNECT_FAILURE:
                       Ext.Msg.alert('Failure', 'Ajax communication failed ' + a.result.msg);
                       break;
                   case Ext.form.Action.SERVER_INVALID:
                       Ext.Msg.alert('Failure', a.result.msg);
               }
           }

       });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});


isTodayBeforeDate = function (date) {
    return new Date(new Date().toDateString()) < new Date(date.toDateString());
}

jun.StatusKota = new Ext.data.ArrayStore({
    fields: ["status_kota", "status"],
    data: [
        [0, "Dalam Kota"],
        [1, "Luar Kota"]
    ]
});
jun.comboStatusKota = Ext.extend(Ext.form.ComboBox, {
    displayField: "status",
    valueField: "status_kota",
    typeAhead: !0,
    mode: "local",
    forceSelection: true,
    triggerAction: "all",
    name: "status_kota",
    hiddenName: "status_kota",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusKota;
        jun.comboStatusKota.superclass.initComponent.call(this);
    }
});