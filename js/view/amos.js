jun.AmosSap = Ext.extend(Ext.Window, {
    title: "AMOS Smart Attendance Prediction",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 130,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #ffffff;padding: 10px",
                id: "form-AmosSap",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: 'AMOS Smart Attendance Prediction digunakan untuk meneyelsaikan pengerjaan ' +
                            'Review Fase 1 dan Review Fase 2 lebih cepat dengan memanfaatkan algoritma ' +
                            'yang lebih efektif dan efisien.'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-accept",
                    text: "Oke",
                    ref: "../btnOk"
                }
            ]
        };
        jun.AmosSap.superclass.initComponent.call(this);
        this.btnOk.on("click", this.onbtnOkclick, this);
    },
    onbtnOkclick: function () {
        this.close();
        var heightplus = 0;
        if(SHOW_AMOS_PHASE_2 == false)
            heightplus = 30;

        var form = new jun.AmosSapSelection({
            height: 150 + heightplus
        });
        form.show();
        form.fase2.setDisabled(true);
    }
});

jun.AmosSapSelection = Ext.extend(Ext.Window, {
    title: "AMOS Selection",
    iconCls: "silk13-script_add",
    modez: 1,
    width: 400,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-AmosSelection",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "label",
                        text: 'Pilih pengerjaan:'
                    },
                    {
                        xtype: 'checkbox',
                        boxLabel: 'Review Fase 1',
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: 'fase1',
                        ref: '../fase1'
                    },
                    {
                        xtype: 'checkbox',
                        boxLabel: 'Review Fase 2',
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: 'fase2',
                        ref: '../fase2'
                    },
                    {
                        xtype: "label",
                        style: {
                            color: 'red',
                            fontWeight: 'bold'
                        },
                        text: 'AMOS SAP untuk Review Fase 2 belum bisa digunakan.',
                        hidden: SHOW_AMOS_PHASE_2,
                        ref: '../warning'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-accept",
                    text: "Oke",
                    ref: "../btnOk"
                }
            ]
        };
        jun.AmosSapSelection.superclass.initComponent.call(this);
        this.fase1.on("check", this.onfase1check, this);
        this.fase2.on("check", this.onfase2check, this);
        this.btnOk.on("click", this.onbtnOkclick, this);
    },
    onfase1check: function () {
        if(this.fase1.getValue() == 1) {
            if (SHOW_AMOS_PHASE_2) {
                this.fase2.setDisabled(false);
            } else {
                this.fase2.setDisabled(true);
            }
        }
    },
    onbtnOkclick: function () {
        var form = new jun.AmosSapPhase();

        if(jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD){
            Ext.Msg.alert('Failure', 'Hanya bisa dilakukan oleh cabang.');
            return;
        } else {
            form.cabang_id.setValue(jun.usercabang);
            form.cabang_id.setReadOnly(true);
        }

        if(this.fase1.getValue() == 0 && this.fase2.getValue() == 0) {
            Ext.Msg.alert('Failure', 'Minimal centang 1 pengerjaan.');
            return;
        }
        form.show();
        form.fase1.setValue(this.fase1.getValue());
        form.fase2.setValue(this.fase2.getValue());
    }
});

jun.AmosSapPhase = Ext.extend(Ext.Window, {
    title: "AMOS Smart Attendance Prediction",
    iconCls: "silk13-script_add",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-AmosSelection",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Periode',
                        ref: '../periode_id',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Store',
                        ref: '../cabang_id',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztCabangUser,
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: 'hidden',
                        value: 0,
                        ref: '../fase1'
                    },
                    {
                        xtype: 'hidden',
                        value: 0,
                        ref: '../fase2'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-accept",
                    text: "Process",
                    ref: "../btnProcess"
                }
            ]
        };
        jun.AmosSapSelection.superclass.initComponent.call(this);
        this.btnProcess.on("click", this.btnProcessClick, this);
    },
    btnProcessClick: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menjalankan proses ini?', this.saveValidation, this);
    },
    saveValidation: function (btn, limitdate) {
        if (btn == 'no') {
            return;
        } else {
            var periode_id  = this.periode_id.getValue();
            var cabang_id   = this.cabang_id.getValue();
            var fase1       = this.fase1.getValue();
            var fase2       = this.fase2.getValue();

            this.btnProcess.setDisabled(true);

            Ext.Ajax.request({
                url: 'Amos/ValidasiPhaseAll',
                method: 'POST',
                scope: this,
                params: {
                    periode_id  : periode_id,
                    cabang_id   : cabang_id,
                    bu_id       : jun.bu_id,
                    fase1       : fase1,
                    fase2       : fase2
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });

                    this.btnProcess.setDisabled(false);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    }
});
