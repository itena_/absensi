jun.AmosAddressWin = Ext.extend(Ext.Window, {
    title: 'Alamat AMOS',
    modez: 1,
    width: 400,
    height: 190,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-AmosAddress',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'BU Kode',
                        name: 'bu_kode',
                        ref: '../bu_kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode Cabang',
                        name: 'kode_cabang',
                        ref: '../kode_cabang',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Alamat AMOS',
                        name: 'amos_address',
                        ref: '../amos_address',
                        anchor: '100%'
                    },
                    {
                        xtype: 'label',
                        text: 'Mis ANI: http://ani.absensi NGI: http://abasensi.naava'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AmosAddressWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'AmosAddress/update/id/' + this.id;
        } else {
            urlz = 'AmosAddress/create/';
        }
        Ext.getCmp('form-AmosAddress').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztAmosAddress.reload();
                jun.rztAmosAddressCmp.reload();
                jun.rztAmosAddressLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-AmosAddress').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});