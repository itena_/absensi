jun.AmosAddressstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AmosAddressstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AmosAddressStoreId',
            url: 'AmosAddress',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'amos_address_id'},
                {name: 'bu_kode'},
                {name: 'kode_cabang'},
                {name: 'amos_address'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        
    }
});
jun.rztAmosAddress = new jun.AmosAddressstore();
jun.rztAmosAddressLib = new jun.AmosAddressstore();
jun.rztAmosAddressCmp = new jun.AmosAddressstore();
