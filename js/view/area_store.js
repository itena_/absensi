jun.Areastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Areastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AreaStoreId',
            url: 'Area',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'area_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'bu_id'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if(jun.bu_id == ''){
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztArea = new jun.Areastore();
jun.rztAreaLib = new jun.Areastore();
jun.rztAreaCmp = new jun.Areastore();
