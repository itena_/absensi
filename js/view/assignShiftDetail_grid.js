jun.AssignShiftDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Assign Shift Detail",
    id: 'docs-jun.AssignShiftDetail',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Shift',
            sortable: true,
            resizable: true,
            dataIndex: 'shift_id',
            width: 200,
            renderer: jun.renderKodeShift
        },
        {
            header: 'In Time',
            sortable: true,
            resizable: true,
            dataIndex: 'shift_id',
            width: 100,
            renderer: jun.renderInShift
        },
        {
            header: 'Out Time',
            sortable: true,
            resizable: true,
            dataIndex: 'shift_id',
            width: 100,
            renderer: jun.renderOutShift
        },
        {
            header: 'Sunday',
            sortable: true,
            resizable: true,
            dataIndex: 'sunday',
            width: 90,
            renderer: function (v, m, r) {
                if (r.get('sunday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Monday',
            sortable: true,
            resizable: true,
            dataIndex: 'monday',
            width: 90,
            renderer: function (v, m, r) {
                if (r.get('monday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Tuesday',
            sortable: true,
            resizable: true,
            dataIndex: 'tuesday',
            width: 90,
            renderer: function (v, m, r) {
                if (r.get('tuesday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Wednesday',
            sortable: true,
            resizable: true,
            dataIndex: 'wednesday',
            width: 90,
            renderer: function (v, m, r) {
                if (r.get('wednesday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Thursday',
            sortable: true,
            resizable: true,
            dataIndex: 'thursday',
            width: 90,
            renderer: function (v, m, r) {
                if (r.get('thursday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Friday',
            sortable: true,
            resizable: true,
            dataIndex: 'friday',
            width: 90,
            renderer: function (v, m, r) {
                if (r.get('friday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Saturday',
            sortable: true,
            resizable: true,
            dataIndex: 'saturday',
            width: 90,
            renderer: function (v, m, r) {
                if (r.get('saturday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        }
    ],
    initComponent: function () {
        this.store = jun.rztAssignShiftDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Shift :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztDayShiftHari, //RUMUS deklarasi store langsung di combo
                            hiddenName: 'shift_id',
                            valueField: 'shift_id',
                            ref: '../../shift_id',
                            displayField: 'kode_shift',
                            width: 230,
//                          sortInfo: {field: 'shift_id', direction: 'ASC'},
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<div style="width:100%;display:inline-block;">\n\
                                    <span style="font-weight: bold">{kode_shift}</span>\n\
                                    <br>\n\{in_time} - {out_time}\n\
                                    </div>',
                                    "</div></tpl>")
                        },
                        {
                            xtype: 'hidden',
                            name: 'pin_id',
                            ref: '../../pin'
                        },
                        {
                            xtype: 'hidden',
                            name: 'sunday',
                            ref: '../../sunday'
                        },
                        {
                            xtype: 'hidden',
                            name: 'monday',
                            ref: '../../monday'
                        },
                        {
                            xtype: 'hidden',
                            name: 'tuesday',
                            ref: '../../tuesday'
                        },
                        {
                            xtype: 'hidden',
                            name: 'wednesday',
                            ref: '../../wednesday'
                        },
                        {
                            xtype: 'hidden',
                            name: 'thursday',
                            ref: '../../thursday'
                        },
                        {
                            xtype: 'hidden',
                            name: 'friday',
                            ref: '../../friday'
                        },
                        {
                            xtype: 'hidden',
                            name: 'saturday',
                            ref: '../../saturday'
                        }

                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            width: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            width: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            width: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.AssignShiftDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.shift_id.on('select', this.shiftOnSelect, this);
//        this.bayar.on('keyup', this.bayarOnkeyPress, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    shiftOnSelect: function (combo, record, index) {
        var as = combo.store.findExact('shift_id', combo.getValue());
        var b = combo.store.getAt(as);
        this.sunday.setValue(b.get('sunday'));
        this.monday.setValue(b.get('monday'));
        this.tuesday.setValue(b.get('tuesday'));
        this.wednesday.setValue(b.get('wednesday'));
        this.thursday.setValue(b.get('thursday'));
        this.friday.setValue(b.get('friday'));
        this.saturday.setValue(b.get('saturday'));
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.shift_id.setValue(record.data.shift_id);
            this.sunday.setValue(record.data.sunday);
            this.monday.setValue(record.data.monday);
            this.tuesday.setValue(record.data.tuesday);
            this.wednesday.setValue(record.data.wednesday);
            this.thursday.setValue(record.data.thursday);
            this.friday.setValue(record.data.friday);
            this.saturday.setValue(record.data.saturday);
//            this.kurang.setValue(record.data.kurang);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        var shift_id = this.shift_id.getValue();
        var pin = this.pin.getValue();

        if (this.pin_id == "") {
            Ext.MessageBox.alert("Error", "Pilih NIK terlebih dahulu.");
            return
        }
        if (shift_id == "") {
            Ext.MessageBox.alert("Error", "Shift harus dipilih.");
            return
        }
//set ngecek kedobelan------------------------        
        var as = this.store.findExact('shift_id', this.shift_id.getValue());
        if (as != -1) {
            Ext.MessageBox.alert("Error", "Shift sudah terdaftar.");
            return
        }       
//--------------------------------------------
        
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('shift_id', shift_id);
            record.set('pin_id', this.pin_id);
            record.set('sunday', this.sunday.getValue());
            record.set('monday', this.monday.getValue());
            record.set('tuesday', this.tuesday.getValue());
            record.set('wednesday', this.wednesday.getValue());
            record.set('thursday', this.thursday.getValue());
            record.set('friday', this.friday.getValue());
            record.set('saturday', this.saturday.getValue());
            record.commit();
        } else {
            var c = this.store.recordType,
                    d = new c({
                        shift_id: shift_id,
                        pin_id: this.pin_id,
                        sunday: this.sunday.getValue(),
                        monday: this.monday.getValue(),
                        tuesday: this.tuesday.getValue(),
                        wednesday: this.wednesday.getValue(),
                        thursday: this.thursday.getValue(),
                        friday: this.friday.getValue(),
                        saturday: this.saturday.getValue()
                    });
            this.store.add(d);
        }
        this.shift_id.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
        this.commitChanges();
    }
});
