jun.AssignShiftWin = Ext.extend(Ext.Window, {
    title: 'Assign Shift',
    modez: 1,
    width: 830,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-AssignShift',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "NIK:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPinLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'pegawai_id',
                        valueField: 'pegawai_id',
                        ref: '../pegawai_id',
                        displayField: 'pin_nama',
                        width: 175,
                        x: 45,
                        y: 2,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<div style="width:100%;display:inline-block;">\n\
                                    <span style="font-weight: bold">{PIN}</span>\n\
                                    <br>\n\{nama_lengkap}\n\
                                    </div>',
                                "</div></tpl>")
                    },
                    {
                        xtype: "label",
                        text: "Template:",
                        x: 235,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztTemplateLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'template_id',
                        valueField: 'template_id',
                        ref: '../template_id',
                        displayField: 'name',
                        width: 175,
                        x: 300,
                        y: 2,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<div style="width:100%;display:inline-block;">\n\
                                    <span style="font-weight: bold">{name}</span>\n\
                                    </div>',
                                "</div></tpl>")
                    },
                    new jun.AssignShiftDetailGrid({
                        x: 5,
                        y: 25,
                        height: 220,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    })

                            //-------------------------------------
//                    
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AssignShiftWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on('close', this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.pegawai_id.on('select', this.pinOnSelect, this);
        this.template_id.on('select', this.templateOnSelect, this);
        if (this.modez == 1) {
            this.btnSave.setVisible(false);
        } else if (this.modez == 0) {
            this.btnSave.setVisible(true);
        } else if (this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.griddetils.btnAdd.setDisabled(true);
        }
    },
    onWinClose: function () {
        jun.rztAssignShiftDetail.removeAll();
    },
    pinOnSelect: function (combo, record, index) {
        var as = combo.store.findExact('pegawai_id', combo.getValue());
        var b = combo.store.getAt(as);
        if (b.get('pegawai_id') == combo.getValue()) {
            this.griddetils.store.baseParams = {
                pegawai_id: combo.getValue()
            };
            this.griddetils.store.load();
            this.griddetils.store.baseParams = {};
        }
        this.griddetils.pegawai_id = combo.getValue();
        this.griddetils.pin.setValue(b.get('pin_id'));
    },
    templateOnSelect: function (combo, record, index) {
//        var as = jun.rztTemplateDetailLib.findExact('template_id', combo.getValue());
//        var b = jun.rztTemplateDetailLib.getAt(as);
//        var c = b.get('shift_id');

//        this.griddetils.store.baseParams = {
//            template_id: combo.getValue()
//        };
        this.griddetils.store.load({
            params: {
                template_id: combo.getValue()
            }
        });
//        this.griddetils.store.baseParams = {};
//        console.log(as);
//        console.log(b);
//        console.log(c);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
//        btnDisabled(true);
        var urlz;
        if (this.griddetils.store.data.length == 0) {
            Ext.Msg.alert('Error', "Template belum dipilih!");
            this.btnDisabled(false);
            return;
        }        
//        if (this.modez == 1 || this.modez == 2) {
//            urlz = 'ShiftPin/update/id/' + this.id;
//        } else {
            urlz = 'ShiftPin/create/';
//        }
        
        Ext.getCmp('form-AssignShift').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            params: {
                mode: this.modez,
                detil: Ext.encode(Ext.pluck(
                        this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztAssignShift.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-AssignShift').getForm().reset();
                    jun.rztAssignShiftDetail.removeAll();
                    this.formz.getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
//                this.onWinClose();
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});