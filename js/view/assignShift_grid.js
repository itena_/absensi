jun.AssignShiftGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Assign Shift",
    id: 'docs-jun.AssignShiftGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'pin',
            width: 30
        },
        {
            header: 'Nama Lengkap',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 30
        },
        {
            header: 'Yang Mendaftarkan',
            sortable: true,
            resizable: true,
            dataIndex: 'user_assign',
            width: 30
        }
//        {
//            header: 'In Time',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'in_time',
//            width: 100    
//        },
//        {
//            header: 'Out Time',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'out_time',
//            width: 100
//        }
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        if (jun.rztShiftLib.getTotalCount() === 0) {
            jun.rztShiftLib.load();
        }
        if (jun.rztTemplateLib.getTotalCount() === 0) {
            jun.rztTemplateLib.load();
        }
        if (jun.rztPinLib.getTotalCount() === 0) {
            jun.rztPinLib.load();
        }
        if (jun.rztDayShiftHari.getTotalCount() === 0) {
            jun.rztDayShiftHari.load();
        }

        this.store = jun.rztAssignShift;
        this.userPermission(1, 1, 1);
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Daftarkan Shift Pegawai',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah Shift Pegawai' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: true
                }
            ]
        };
        jun.AssignShiftGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.on('rowdblclick', this.loadEditForm, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.AssignShiftWin({modez: 0});
        form.griddetils.store.removeAll();
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pegawai_id;
        var form = new jun.AssignShiftWin({modez: 1, id: idz});
        form.pegawai_id.setValue(selectedz.json.pegawai_id);
        form.griddetils.pin.setValue(selectedz.json.pin_id);
        form.show(this);
//        form.formz.getForm().loadRecord(this.record);
        form.griddetils.store.baseParams = {
            pegawai_id: idz
        };
        form.griddetils.store.load();
        form.griddetils.store.baseParams = {};
    },
    deleteRec: function () {
        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        Ext.MessageBox.confirm(
                "Hapus Data Shift Per Hari",
                "Apakah anda yakin ingin menghapus data ini?",
                this.deleteRecYes,
                this
                );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ShiftPin/delete/id/' + record.json.pin_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztAssignShift.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
});
