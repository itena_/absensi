jun.AssignShiftstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AssignShiftstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssignShiftStoreId',
            url: 'ShiftPin',
            root: 'results',
            totalProperty: 'total',
            // autoLoad: true,
            fields: [
                {name: 'shiftpin_id'},
                {name: 'shift_id'},
                {name: 'pin_id'},
                {name: 'pegawai_id'},
                {name: 'user_assign'},
                {name: 'pin'},
                {name: 'kode_shift'},
                {name: 'in_time'},
                {name: 'out_time'},
                {name: 'sunday'},
                {name: 'monday'},
                {name: 'tuesday'},
                {name: 'wednesday'},
                {name: 'thursday'},
                {name: 'friday'},
                {name: 'saturday'},
                {name: 'nama_lengkap'}
            ]
        }, cfg));
    }
});
jun.rztAssignShift = new jun.AssignShiftstore();
jun.rztAssignShiftDetail = new jun.AssignShiftstore();
//jun.rztPelunasanHutang.load
jun.rztAssignShiftFilter = new jun.AssignShiftstore();
