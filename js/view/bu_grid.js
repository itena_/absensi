jun.BuGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Bisnis Unit",
    id: 'docs-jun.BuGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nama Bisnis Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_name',
            width: 100
        },
        {
            header: 'Nama Alias',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_nama_alias',
            width: 100
        },
        {
            header: 'Kode BU',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_kode',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztBu;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add/Update Bisnis Unit',
                    ref: '../btnAddUpdate'
                },
                {
                    xtype: 'button',
                    text: 'Buat Bisnis Unit',
                    ref: '../btnAdd',
                    hidden: true
                },
                {
                    xtype: 'tbseparator',
                    hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Ubah Bisnis Unit',
                    ref: '../btnEdit',
                    hidden: true
                },
                {
                    xtype: 'tbseparator',
                    hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Sinkron',
                    ref: '../btnUpdateKaryawan',
                    hidden: true
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.BuGrid.superclass.initComponent.call(this);
        this.btnAddUpdate.on('Click', this.btnAddUpdateOnClick, this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnUpdateKaryawan.on('Click', this.updateKaryawanClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    updateKaryawanClick: function () {
        Ext.Ajax.request({
            url: 'Bu/updateDataKaryawan',
            method: 'POST',
            success: function (f, a) {
                jun.rztBu.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: (response.msg == '' ? 'Sinkronisasi berhasil' : response.msg),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnAddUpdateOnClick: function () {
        var form = new jun.BuAddUpdateWin({modez: 0});
        form.show();
    },
    loadForm: function () {
        var form = new jun.BuWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.bu_id;
        var form = new jun.BuWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});
