jun.CabangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Cabang",
    id: 'docs-jun.CabangGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_cabang',
            width: 100
        },
        {
            header: 'Nama Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_cabang',
            width: 100
        },
        {
            header: 'Alamat',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat_cabang',
            width: 100
        },
        {
            header: 'Status Cabang Pusat',
            sortable: true,
            resizable: true,
            dataIndex: 'kepala_cabang_stat',
            width: 100,
            renderer: function (v, m, r) {
                return v == 1 ? 'YA' : '';
            }
        }
    ],
    initComponent: function () {
        if (jun.rztAreaCmp.getTotalCount() === 0)
            jun.rztAreaCmp.load();
        if (jun.rztBuCmp.getTotalCount() === 0)
            jun.rztBuCmp.load();

        this.store = jun.rztCabang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Cabang',
                    ref: '../btnAdd',
                    hidden: true
                },
                {
                    xtype: 'tbseparator',
                    hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Ubah Cabang',
                    ref: '../btnEdit',
                    hidden: true
                },
                {
                    xtype: 'tbseparator',
                    hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Sinkron Cabang',
                    ref: '../btnSoap'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Daftarkan sebagai Cabang Pusat',
                    ref: '../btnCabangPusat'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        this.store.reload();

        jun.CabangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnSoap.on('Click', this.soapClick, this);
        this.btnCabangPusat.on('Click', this.btnCabangPusatonClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    soapClick: function () {
        Ext.Ajax.request({
            url: 'Cabang/sinkron/',
            method: 'POST',
            scope: this,
            params: {
                bu_id: jun.bu_id
            },
            success: function (f, a) {
                jun.rztCabang.reload();
                jun.rztCabangCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: (response.msg == '' ? 'Sinkronisasi berhasil' : response.msg),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnCabangPusatonClick: function () {
        var cabang_id = this.record.data.cabang_id;
        Ext.Ajax.request({
            url: 'Cabang/AssignCabangPusat/',
            method: 'POST',
            scope: this,
            params: {
                cabang_id: cabang_id
            },
            success: function (f, a) {
                jun.rztCabang.reload();
                jun.rztCabangCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CabangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.cabang_id;
        var form = new jun.CabangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});
// jun.smCabang = new Ext.grid.CheckboxSelectionModel({
//     dataIndex: 'checked'
// });
jun.filterCabang = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: false, // json encode the filter query
    local: true,   // defaults to false (remote filtering)
    filters: [
        {
            type: 'string',
            dataIndex: 'kode_cabang'
        },
        {
            type: 'string',
            dataIndex: 'bu_kode'
        }
    ]
});
jun.checkAll = function () {
    var t = Ext.get('hCheckAll');
    if (t.dom.text === 'Check All') {
        jun.rztCabangBu.checkAll();
        t.dom.text = 'UnCheck All';
    } else {
        jun.rztCabangBu.uncheckAll();
        t.dom.text = 'Check All';
    }
};
jun.CabangBuGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Cabang",
    loadMask: true,
    id: 'docs-jun.CabangBuGrid',
    modez: 0,
    // iconCls: "silk-grid",
    // viewConfig: {
    //     forceFit: true
    // },
    // sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    sm: new Ext.grid.CheckboxSelectionModel(),
    plugins: [jun.filterCabang],
    columns: [
        new Ext.ux.grid.CheckColumn({
            header: '<a href="#" id="hCheckAll" onclick="jun.checkAll();" style="text-align: center;">Check All</a>',
            // header: '<div ext-xtype="checkbox" colindex=0></div>',
            width: 100,
            inputValue: 1,
            uncheckedValue: 0,
            align: 'center',
            dataIndex: 'checked',
            filterable: false
        }),
        {
            header: 'Kode Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_cabang',
            width: 200,
            filterable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            }
        },
        {
            header: 'Bisnis Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_name',
            width: 200,
            filterable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            }
        }
    ],
    initComponent: function () {
        this.store = jun.rztCabangBu;
        if (jun.rztAreaCmp.getTotalCount() === 0)
            jun.rztAreaCmp.load();
        if (jun.rztBuCmp.getTotalCount() === 0)
            jun.rztBuCmp.load();
        jun.CabangBuGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        if (this.modez == 0) {
            this.store.baseParams = {
                modez: 0
            };
        } else {
            this.store.baseParams = {
                modez: this.modez
            };
        }
        this.store.reload();
        this.store.baseParams = {};
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CabangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.cabang_id;
        var form = new jun.CabangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});