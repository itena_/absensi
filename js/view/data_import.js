jun.ListImportData = Ext.extend(Ext.Window, {
    title: "Pilih salah satu dari List Import Data",
    iconCls : "silk13-transmit_go",
    modez: 1,
    width: 270,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ListImportData",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'button',
                        text: 'List Import Data',
                        style: {
                            marginLeft: '5px'
                        },
                        width: 100,
                        menu: {
                            xtype: 'menu',
                            items: [
                                {
                                    text: 'Import Timesheet',
                                    ref: '../../../timesheet'
                                },
                                {
                                    text: 'Import Timesheet LARK',
                                    ref: '../../../lark'
                                },
                                {
                                    text: 'Import attlog.dat',
                                    ref: '../../../attlog'
                                }
                            ]
                        }
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Tutup",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.ListImportData.superclass.initComponent.call(this);
        this.timesheet.on('click', this.onbtnTimesheet, this);
        this.lark.on('click', this.onbtnLark, this);
        this.attlog.on('click', this.onbtnAttlog, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    onbtnTimesheet: function (t) {
        var form = new jun.ImportData();
        form.show();
    },
    onbtnAttlog: function (t) {
        var form = new jun.ImportAttlog();
        form.show();
    },
    onbtnLark: function (t) {
        var form = new jun.ImportAttlog({
            tipe: 'LARK',
            title: 'Import Timesheet LARK'
        });
        form.show();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});

jun.ImportData = Ext.extend(Ext.Window, {
    title: "Import TimeSheet",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 290,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "panel",
                bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
                html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            },
            {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeLib,
                    fieldLabel: 'Periode',
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                            <br>\n\<b>Start: </b>{periode_start}\n\\n\
                            <br>\n\<b>End: </b>{periode_end}\n\</span>',
                            "</div></tpl>"),
                    ref: 'periode',
                    anchor: '100%' 
                },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xls)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 100,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg'
                }
            },
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportData",
                border: !1
            },  
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-DownloadFormatTimeSheet",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                hidden: true,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Download Format TimeSheet",
                    ref: "../btnDownload"
                },
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportData.superclass.initComponent.call(this);
        this.btnDownload.on("click", this.onbtnDownloadclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },    
    onbtnDownloadclick: function () {
        window.open(FORMAT_UPLOAD_TIMESHEET);        
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        // this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI IMPORT TIMESHEET -----');
        jun.submitKasCounter = 0;
        this.loopPost();
    },
    loopPost: function () {
        var arrData = jun.dataStockOpname[jun.namasheet];
        var periode = this.periode.getValue();
        if (jun.submitKasCounter >= arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT TIMESHEET SELESAI -----\n'
                + 'Jumlah data : ' + (jun.submitKasCounter));
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            jun.dataStockOpname = null;
            var pegawai_id = Ext.getCmp('pinIdOnValidasiGrid').getValue();
            if(pegawai_id) {
                jun.rztValidasi.baseParams = {
                    pegawai_id: pegawai_id,
                    periode_id: periode
                };
                jun.rztValidasi.reload();
            }
            return;
        } 
        var d = arrData[jun.submitKasCounter];

        if (this.isemptyorwhitespace(d.NIK) || this.isemptyorwhitespace(d.TANGGAL_MASUK))
        {
            this.writeLog('DATA TIDAK LENGKAP PADA BARIS KE-'+(jun.submitKasCounter+1));
            return;
        }

        Ext.getCmp('form-ImportData').getForm().submit({
            url: 'Validasi/Create',
            timeout: 30000,
            scope: this,
            params: {
                mode: 'import',
                PIN: d.NIK,
                jam_in: d.TANGGAL_MASUK,
                jam_out: !d.TANGGAL_KELUAR ? d.TANGGAL_MASUK : d.TANGGAL_KELUAR,
                in_time: d.JAM_MASUK,
                out_time: d.JAM_KELUAR,
                ket: !d.KODE_KETERANGAN  ? 3 : d.KODE_KETERANGAN,
                nosurat: d.ALASAN,
                approval_lembur: !d.APPROVE_LEMBUR ? 0 : d.APPROVE_LEMBUR,
                periode_id: periode,
                bu_id: jun.bu_id,
                kode_cabang: d.CABANG
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                this.loopLog(response.msg);
                this.nextPost();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    nextPost: function () {
        jun.submitKasCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('\n----- ' + (jun.submitKasCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --------------\n' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    },
    isemptyorwhitespace: function isEmptyOrSpaces(str){
        return str === null || str.match(/^ *$/) !== null;
    }
});

jun.ImportAttlog = Ext.extend(Ext.Window, {
    iconCls : "silk13-report",
    title   : "Import Attlog",
    tipe    : 'Attlog',
    modez   : 1,
    width   : 450,
    height  : 290,
    layout  : "form",
    modal   : !0,
    padding : 5,
    closeForm: !1,
    resizable: !1,
    iswin   : !0,
    initComponent: function () {
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "panel",
                bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
                html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br>' +
                    '<span style="color:red;">Gunakan format yang telah disediakan di tombol ' +
                    '<span style="font-weight:bold">Download Format</span>' +
                    ' di bawah.</span><br>' +
                    '<span style="color:red;">Jika bingung, konsultasi dengan IT sebelum melakukan import.</span>'
            },
            {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeLib,
                    fieldLabel: 'Periode',
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                            <br>\n\<b>Start: </b>{periode_start}\n\\n\
                            <br>\n\<b>End: </b>{periode_end}\n\</span>',
                            "</div></tpl>"),
                    ref: 'periode',
                    anchor: '100%'
                },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xls)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 100,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    // readOnly: true,
                    ref: '../log_msg'
                }
            },
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportData",
                border: !1
            },
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-DownloadFormatTimeSheet",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                hidden: true,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Download Format " + this.tipe,
                    ref: "../btnDownload"
                },
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportAttlog.superclass.initComponent.call(this);
        this.btnDownload.on("click", this.onbtnDownloadclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnDownloadclick: function () {
        if(this.tipe === 'LARK')
            window.open(TEMPLATE_UPLOAD_LARK);
        else
            window.open(TEMPLATE_UPLOAD);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        this.btnSave.setDisabled(true);

        if (this.tipe === 'LARK')
            {
                this.importForm();
            }
        else {
            this.log_msg.setValue('');
            this.writeLog('\n----- MULAI IMPORT TIMESHEET -----');
            jun.submitKasCounter = 0;
            this.loopPost();
        }
    },
    loopPost: function () {
        var arrData = jun.dataStockOpname[jun.namasheet];
        var periode = this.periode.getValue();
        if (jun.submitKasCounter >= arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT ATTLOG SELESAI -----\n'
                + 'Jumlah data : ' + (jun.submitKasCounter));
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            jun.dataStockOpname = null;
            return;
        }
        var d = arrData[jun.submitKasCounter];

        // console.log(d);

        if (this.tipe === 'LARK') {
            var url     = 'Fp/ImportLark';
            var params  = {
                name    : d.name,
                dept    : d.dept,
                group   : d.group,
                date    : d.date,
                in_time : d.in_time,
                out_time: d.out_time,
                periode : periode
            };
        } else {
            if (this.isemptyorwhitespace(d.date) ||
                this.isemptyorwhitespace(d.nama) ||
                this.isemptyorwhitespace(d.nik)) {
                this.writeLog('DATA TIDAK LENGKAP PADA BARIS KE-'+(jun.submitKasCounter+1));
                return;
            }

            var url     = 'Fp/ImportAttlog';
            var params  = {
                date    : d.date,
                name    : d.nama,
                nik     : d.nik,
                periode : periode
            };
        }

        Ext.getCmp('form-ImportData').getForm().submit({
            url     : url,
            timeout : 30000,
            scope   : this,
            params  : params,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                var msg      = response.msg;
                if(msg == "Hanya cabang yang bisa menggunakan fungsi ini!") {
                    Ext.MessageBox.show({
                        title   : 'Info',
                        msg     : msg,
                        buttons : Ext.MessageBox.OK,
                        icon    : Ext.MessageBox.INFO
                    });
                } else {
                    this.loopLog(msg);
                    this.nextPost();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    nextPost: function () {
        jun.submitKasCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('\n----- ' + (jun.submitKasCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --------------\n' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    },
    isemptyorwhitespace: function isEmptyOrSpaces(str){
        return str === null || str.match(/^ *$/) !== null;
    },

    importForm: function () {
        var arrData = jun.dataStockOpname[jun.namasheet];
        var periode = this.periode.getValue();
        Ext.Ajax.request({
            url: 'Fp/ImportLark',
            timeout: 10600000,
            scope: this,
            params: {
                periode_id  : periode,
                detil       : Ext.encode(arrData)
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var msg = response.msg;
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.btnSave.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
