jun.DayShiftDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Day Shift Detail",
    id: 'docs-jun.AssignShiftDetail',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Day',
            sortable: true,
            resizable: true,
            dataIndex: 'day_id',
            width: 100,
            renderer: jun.renderNamaDays
        }
    ],
    initComponent: function () {
        this.store = jun.rztDayShiftDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Day :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztDaysLib, //RUMUS deklarasi store langsung di combo
                            hiddenName: 'day_id',
                            valueField: 'day_id',
                            ref: '../../day_id',
                            displayField: 'day_name',
                            width: 130
                        },
                        {
                            xtype: 'hidden',
                            name: 'shift_id',
                            ref: '../../shift'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            width: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            width: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.DayShiftDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
//        this.bayar.on('keyup', this.bayarOnkeyPress, this);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.day_id.setValue(record.data.day_id);
//            this.kurang.setValue(record.data.kurang);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadForm: function () {
        var day_id = this.day_id.getValue();
        var shift = this.shift.getValue();

        if (this.shift_id == "") {
            Ext.MessageBox.alert("Error", "Pilih Shift terlebih dahulu.");
            return
        }
        if (day_id == "") {
            Ext.MessageBox.alert("Error", "Day harus dipilih.");
            return
        }
//set ngecek kedobelan------------------------        
        var as = this.store.findExact('day_id', this.day_id.getValue());
        if (as != -1) {
            Ext.MessageBox.alert("Error", "Hari sudah terdaftar.");
            return
        }
//--------------------------------------------
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('day_id', day_id);
            record.set('shift_id', this.shift_id);
            record.commit();
        } else {
            var c = jun.rztDayShiftDetail.recordType,
                    d = new c({
                        day_id: day_id,
                        shift_id: this.shift_id
                    });
            jun.rztDayShiftDetail.add(d);
        }
        this.day_id.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
