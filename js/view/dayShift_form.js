jun.DayShiftWin = Ext.extend(Ext.Window, {
    title: 'DayShift',
    modez: 1,
    width: 450,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background: none; padding: 10px',
                id: 'form-DayShift',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Shift:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztShift,
                        hiddenName: 'shift_id',
                        valueField: 'shift_id',
                        emptyText: 'Pilih Shift',
                        ref: '../shift_id',
                        displayField: 'kode_shift',
                        width: 175,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<div style="width:100%;display:inline-block;">\n\
                                    <span style="font-weight: bold">{kode_shift}</span>\n\
                                    <br>\n\{in_time} - {out_time}\n\
                                    </div>',
                                "</div></tpl>")
                    },
                    new jun.DayShiftDetailGrid({
                        x: 5,
                        y: 25,
                        height: 250,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.DayShiftWin.superclass.initComponent.call(this);
        this.on('close', this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.shift_id.on('select', this.shiftOnSelect, this);

        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    
    onWinClose: function () {
        jun.rztDayShiftDetail.removeAll();
    },
    shiftOnSelect: function (combo, record, index) {
        var as = combo.store.findExact('shift_id', combo.getValue());
        var b = combo.store.getAt(as);
        if (b.get('shift_id') == combo.getValue()) {
            this.griddetils.store.baseParams = {
                shift_id: combo.getValue()
            };
            this.griddetils.store.load();
            this.griddetils.store.baseParams = {};
        }
        this.griddetils.shift_id = combo.getValue();
        this.griddetils.shift.setValue(combo.getValue());
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 0) {
            urlz = 'DayShift/Create/';
        } else if (this.modez == 1) {
            urlz = 'DayShift/Update/id/' + this.id;
        }
        this.formz.getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            params: {
                id : this.dayshift_id,
                idshift : this.shift_id.getValue(),
                mode: this.modez,
                detil: Ext.encode(Ext.pluck(
                        this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztDayShift.reload();
                jun.rztShiftLib.reload();
                jun.rztDayShiftHari.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});