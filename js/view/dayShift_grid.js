jun.DayShiftGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Shift Per Hari",
    id: 'docs-jun.DayShiftGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Shift',
            sortable: true,
            resizable: true,
            dataIndex: 'shift_id',
            width: 300,
            renderer: jun.renderKodeShift
        },
//        {
//            header: 'Hari',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'day_id',
//            width: 100,
//            renderer: jun.renderNamaDays
//        },
        {
            header: 'Sunday',
            sortable: true,
            resizable: true,
            dataIndex: 'sunday',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('sunday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Monday',
            sortable: true,
            resizable: true,
            dataIndex: 'monday',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('monday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Tuesday',
            sortable: true,
            resizable: true,
            dataIndex: 'tuesday',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('tuesday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Wednesday',
            sortable: true,
            resizable: true,
            dataIndex: 'wednesday',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('wednesday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Thursday',
            sortable: true,
            resizable: true,
            dataIndex: 'thursday',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('thursday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Friday',
            sortable: true,
            resizable: true,
            dataIndex: 'friday',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('friday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        },
        {
            header: 'Saturday',
            sortable: true,
            resizable: true,
            dataIndex: 'saturday',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('saturday') == 1) {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #ff704d; border: solid 1px #A5A5A5";
                }
            }
        }
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        if (jun.rztShiftLib.getTotalCount() === 0) {
            jun.rztShiftLib.load();
        };
        if (jun.rztDaysLib.getTotalCount() === 0) {
            jun.rztDaysLib.load();
        };
        if (jun.rztDayShiftHari.getTotalCount() === 0) {
            jun.rztDayShiftHari.load();
        };

        this.store = jun.rztDayShiftHari;

        //switch(UROLE){
        //    default:
        this.userPermission(1, 1, 1);
        //}

        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete',
                    hidden: this.actDelete ? false : true
                },
                '->',
                {
                    xtype: 'button',
                    text: '<b>Kode Shift</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'kode_shift',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Kode Shift',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_shift';
                                        Ext.getCmp('docs-jun.DayShiftGrid').txtFind.focus().setValue("");
                                    }
                                }
                            },
                            {
                                text: 'Hari',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'day_name';
                                        Ext.getCmp('docs-jun.DayShiftGrid').txtFind.focus().setValue("");
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                }
            ]
        };

        jun.DayShiftGrid.superclass.initComponent.call(this);
        this.btnAdd.on('click', this.loadForm, this);
        this.btnEdit.on('click', this.loadEditForm, this);
        this.btnDelete.on('click', this.deleteRec, this);
        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);

        this.fieldsearch = "";
        this.valuesearch = "";
    },
    setFilterData: function (n) {
        if (parseInt(this.store.baseParams.tipe_shift_id) == n) {
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.moveFirst();
        }
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.fieldsearch = this.btnFind.fieldsearch;
            this.valuesearch = c.getValue();
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        jun.rztDayShiftDetail.removeAll();
        var form = new jun.DayShiftWin({
            modez: 0,
            title: "Buat Data - Shift Per Hari",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        var idz = selectedz.json.dayshift_id;
        var idz_shift = selectedz.json.shift_id;


        var form = new jun.DayShiftWin({
            modez: this.actEdit ? 1 : 2,
            id: idz,
            title: (this.actEdit ? "Ubah" : "Lihat") + " Data - Shift Per Hari",
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztDayShiftDetail.baseParams = {
            dayshift_id: idz,
            shift_id: idz_shift
        };
        jun.rztDayShiftDetail.load();
        jun.rztDayShiftDetail.baseParams = {};

    },
    deleteRec: function () {
        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        Ext.MessageBox.confirm(
                "Hapus Data Shift Per Hari",
                "Apakah anda yakin ingin menghapus data ini?",
                this.deleteRecYes,
                this
                );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        Ext.Ajax.request({
            url: 'DayShift/delete/id/' + record.json.shift_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztDayShift.reload();
                jun.rztDayShiftHari.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
