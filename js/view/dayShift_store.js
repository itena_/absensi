jun.DayShiftStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DayShiftStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DayShiftStoreId',
            url: 'DayShift',
            root: 'results',
            totalProperty: 'total',
            // autoLoad: true,
            fields: [
                {name: 'dayshift_id'},
                {name: 'shift_id'},
                {name: 'day_id'},
                {name: 'sunday'},
                {name: 'monday'},
                {name: 'tuesday'},
                {name: 'wednesday'},
                {name: 'thursday'},
                {name: 'friday'},
                {name: 'saturday'},
                {name: 'kode_shift'},
                {name: 'in_time'},
                {name: 'out_time'}
            ]
        }, cfg));
    }
});
jun.rztDayShift = new jun.DayShiftStore();
jun.rztDayShiftLib = new jun.DayShiftStore();
jun.rztDayShiftDetail = new jun.DayShiftStore();
jun.rztDayShiftHari = new jun.DayShiftStore({url : 'DayShift/DS'});