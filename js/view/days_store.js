jun.DaysStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DaysStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DaysStoreId',
            url: 'DayShift/GetDays',
            root: 'results',
            totalProperty: 'total',
            autoLoad: true,
            fields: [
                {name: 'day_id'},
                {name: 'day_name'},
                {name: 'kode_day', type: 'int'}
            ]
        }, cfg));
    }
});

jun.rztDaysLib = new jun.DaysStore();