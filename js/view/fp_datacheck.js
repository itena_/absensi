jun.FpDataCheckWin = Ext.extend(Ext.Window, {
    title: 'Data Check',
    modez: 1,
    width: 400,
    height: 220,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-FpDataCheckWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'label',
                        text: 'Centang untuk memilih shift pada tanggal '+ this.hariini
                    },
                    new jun.FpDataCheckGrid({
                        height: 220,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        anchor: '100%'
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.FpDataCheckWin.superclass.initComponent.call(this);
        this.on('close', this.onWinClose, this);
        this.btnSaveClose.on('click', this.saveForm, this);
        this.btnCancel.on('click', this.onbtnCancel, this);
    },
    onbtnCancel: function () {
        this.close();
    },
    onWinClose: function () {
        jun.rztFpDataCheckStore.removeAll();
        jun.rztFp.reload();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        var a = this.griddetils.store.findExact('count', true);
        if (a == -1) {
            Ext.Msg.alert('Error', "Pilih salah satu shift!");
            return;
        }
        var data  = this.griddetils.store.data.items;
        var count = 0;
        for(var i = 0; i < data.length; i++) {
            if(data[i].data.count == true){
                count++;
            }
        }
        if(count > 1) {
            Ext.Msg.alert('Error', '<p class="redInline">Hanya pilih 1 Shift!</p>');
            return;
        }
        this.closeForm = true;
        this.btnDisabled(true);
        var urlz = 'Fp/saveOneDay';
        Ext.getCmp('form-FpDataCheckWin').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            params: {
                periode_id      : this.periode_id,
                PIN             : this.PIN,
                pin_id          : this.pin_id,
                kode_ket        : this.kode_ket,
                cabang_id       : this.cabang_id,
                no_surat_tugas  : this.no_surat_tugas,
                detil           : Ext.encode(Ext.pluck(
                                this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                // jun.rztFp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.getCmp('docs-jun.FpGrid').saveValidation('yes', response.msg);
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    }
});

jun.FpDataCheckGrid = Ext.extend(Ext.grid.EditorGridPanel, {
    title: "FpDataCheckGrid",
    id: 'docs-jun.FpDataCheckGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    clicksToEdit: 1,
    columns: [
        {
            header: 'Shift',
            sortable: true,
            resizable: true,
            dataIndex: 'shift_id',
            width: 100,
            hidden: true
        },
        {
            header: 'Shift',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_shift',
            width: 100
        },
        {
            xtype: 'checkcolumn',
            header: 'Choose',
            dataIndex: 'count',
            width: 55
        }
    ],
    initComponent: function () {
        if (jun.rztShiftLib.getTotalCount() == 0){
            jun.rztShiftLib.reload();
        }
        this.store = jun.rztFpDataCheckStore;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [

                    ]
                }
            ]
        };
        jun.FpDataCheckGrid.superclass.initComponent.call(this);
    }
});

jun.FpDataCheckStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FpDataCheckStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FpDataCheckStoreId',
            url: '',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'shift_id'},
                {name: 'jam_in'},
                {name: 'jam_out'},
                {name: 'kode_shift'}
            ]
        }, cfg));
    }
});
jun.rztFpDataCheckStore = new jun.FpDataCheckStore();
