jun.FpWin = Ext.extend(Ext.Window, {
    title: 'Fp',
    modez: 1,
    width: 350,
    height: 140,
    layout: 'form',
    modal: true,
    padding: 5,
    pin: '',
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background: none; padding: 10px',
                id: 'form-Fp',
                labelWidth: 80,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'datefield',
                        fieldLabel: 'In',
                        name: 'jam_in',
//                        value: 'jam_in',
                        ref: '../jam_in',
                        format: 'Y-m-d H:i:s',
                        anchor: '100%',
//                        allowBlank: false
                        readOnly: true,
                        value: this.jam_in
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Out',
                        name: 'jam_out',
//                        value: 'jam_out',
                        ref: '../jam_out',
                        format: 'Y-m-d H:i:s',
                        anchor: '100%',
//                        allowBlank: false
                        readOnly: true,
                        value: this.jam_out
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Periode',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib,
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                            <br>\n\<b>Start: </b>{periode_start}\n\\n\
                            <br>\n\<b>End: </b>{periode_end}\n\</span>',
                                "</div></tpl>"),
                        ref: '../periode',
                        hidden: true,
                        width: 200
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
//                    {
//                        xtype: 'checkbox',
//                        fieldLabel: "LK",
//                        value: this.kode_ket_in,
//                        inputValue: 1,
////                        uncheckedValue: 0,
//                        name: "kode_ket_in",
//                        ref: "../kode_ket_in"
//                    },
                    {
                        xtype: 'hidden',
                        name: 'PIN',
//                        ref:'../pin',
                        value: this.pin
                    },
                    {
                        xtype: 'hidden',
                        name: 'fpin',
//                        ref:'../pin',
                        value: this.fp_id_in
                    },
                    {
                        xtype: 'hidden',
                        name: 'fpout',
//                        ref:'../pin',
                        value: this.fp_id_out
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Validasi',
                    hidden: true,
                    ref: '../btnValid'
                },
                {
                    xtype: 'button',
                    text: 'Invalidasi',
                    hidden: true,
                    ref: '../btnInvalid'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Verifikasi All',
                    hidden: true,
                    ref: '../btnVerif'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.FpWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnValid.on('click', this.onbtnValidclick, this);
        this.btnVerif.on('click', this.onbtnVerifclick, this);
        this.btnInvalid.on('click', this.onbtnInvalidclick, this);

        this.btnCancel.setText(this.modez < 2 ? 'Batal' : 'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            this.jam_in.setReadOnly(false);
            this.jam_out.setReadOnly(false);
        } else if (this.modez == 1) { //edit
            this.btnSaveClose.setVisible(false);
            this.btnSave.setVisible(false);
            this.btnValid.setVisible(true);
            this.btnInvalid.setVisible(true);
            this.kode_ket_in.setDisabled(true);
        } else if (this.modez == 5) { //edit
            this.btnSaveClose.setVisible(false);
            this.btnSave.setVisible(false);
            this.btnVerif.setVisible(true);
            this.periode.setVisible(true);
            this.jam_in.setVisible(false);
            this.jam_out.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({readOnly: true});
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onbtnVerifclick: function () {
        if (this.periode.getValue() == '') {
            Ext.MessageBox.alert("Warning", "Anda belum memilih periode!");
            return;
        }
        this.formz.getForm().submit({
            url: 'Fp/verif',
            timeout: 10600000,
            scope: this,
            params: {
                periode_id: this.periode.getValue()
            },
            success: function () {
                jun.rztFp.reload();
                jun.rztResultLib.reload();
                Ext.Msg.alert('Sukses', 'Data sudah diverifikasi.');
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        jun.rztFp.reload();
                        Ext.Msg.alert('Sukses', 'Data sudah diverifikasi.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 0) {
            urlz = 'Fp/Create/';
        } else if (this.modez == 1) {
            urlz = 'Fp/Update/id/' + this.id;
        }
        this.formz.getForm().submit({
            url: urlz,
            timeout: 10600000,
            scope: this,
            params: {
                periode_id: this.periode_id
            },
            success: function (f, a) {
                jun.rztFp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnValidclick: function () {
        var pin = this.PIN;
        this.close();
        Ext.Ajax.request({
            url: 'Fp/assignValidasi',
            method: 'POST',
            params: {
                PIN: pin,
                jamin: this.jamin,
                jamout: this.jamout,
                fp_id_in: this.fp_id_in,
                fp_id_out: this.fp_id_out,
                periode_id: this.periode_id
            },
            success: function (f, a) {
                jun.rztFp.reload();
//                var response = Ext.decode(a.response.responseText);
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
//                if (this.modez == 0) {
//                    this.formz.getForm().reset();
//                    this.btnDisabled(false);
//                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnInvalidclick: function () {
        var pin = this.PIN;
        this.close();
        Ext.Ajax.request({
            url: 'Fp/assignValidasi',
            method: 'POST',
            params: {
                PIN: pin,
                jamin: this.jamin,
                jamout: this.jamout,
                valid: 2,
                fp_id_in: this.fp_id_in,
                fp_id_out: this.fp_id_out,
                periode_id: this.periode_id
            },
            success: function (f, a) {
                jun.rztFp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
//                if (this.modez == 0) {
//                    this.formz.getForm().reset();
//                    this.btnDisabled(false);
//                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});