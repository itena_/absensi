jun.FpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Review Presensi Fase 1",
    id: 'docs-jun.FpGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 95px;';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Sunday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return getFpColumn(v,m,r, 'Sunday');
            }
        },
        {
            header: 'Monday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return getFpColumn(v,m,r, 'Monday');
            }
        },
        {
            header: 'Tuesday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return getFpColumn(v,m,r, 'Tuesday');
            }
        },
        {
            header: 'Wednesday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return getFpColumn(v,m,r, 'Wednesday');
            }
        },
        {
            header: 'Thursday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return getFpColumn(v,m,r, 'Thursday');
            }
        },
        {
            header: 'Friday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return getFpColumn(v,m,r, 'Friday');
            }
        },
        {
            header: 'Saturday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return getFpColumn(v,m,r, 'Saturday');
            }
        }
    ],
    initComponent: function () {
        // if (jun.rztPinLib.getTotalCount() === 0) jun.rztPinLib.load();
        // if (jun.rztPegawaiLib.getTotalCount() === 0) jun.rztPegawaiLib.load();
        // if (jun.rztShiftLib.getTotalCount() === 0) jun.rztShiftLib.load();

        if (jun.rztIpCmp.getTotalCount() === 0) jun.rztIpCmp.load({params: {grup: true}});
        if (jun.rztCabangUser.getTotalCount() === 0) jun.rztCabangUser.load();
        if (jun.rztPeriodeLib.getTotalCount() === 0) jun.rztPeriodeLib.load();
        if (jun.rztPegawaiLib.getTotalCount() === 0) jun.rztPegawaiLib.load();
        if (jun.rztDaysLib.getTotalCount() === 0) jun.rztDaysLib.load();

        this.store = jun.rztFp;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: 'Periode:'
                },
                {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeLib,
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                        <br>\n\<b>Start: </b>{periode_start}\n\\n\
                        <br>\n\<b>End: </b>{periode_end}\n\</span>',
                        "</div></tpl>"),
                    ref: '../periode',
                    width: 200
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    text: 'NIK:'
                },
                {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    mode: 'remote',
                    triggerAction: 'query',
                    autoSelect: false,
                    minChars: 3,
                    matchFieldWidth: !1,
                    // pageSize: 20,
                    listWidth: 450,
                    lastQuery: "",
                    lazyRender: true,
                    forceSelection: true,
                    store: jun.rztPegawaiLib,
                    valueField: 'pegawai_id',
                    displayField: 'nik',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<span style="width:100%;display:inline-block;"><b>{nik}</b><br>{nama_lengkap}</span>',
                        "</div></tpl>"),
                    ref: '../pin',
                    width: 200
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Tarik Presensi Manual',
                    iconCls: 'silk13-star',
                    ref: '../btnAttLog'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Reset Fase 1',
                    iconCls: 'silk13-ruby',
                    ref: '../btnResetFase1'
                },
                '->',
                {
                    xtype: 'button',
                    text: 'Validasi dan Simpan',
                    iconCls: 'silk13-shape_square_go',
                    ref: '../btnSimpanValid'
                }
            ]
        };

        jun.FpGrid.superclass.initComponent.call(this);
        this.pin.on('select', this.loadFP, this);
        this.btnSimpanValid.on('click', this.simpanValidOnClick, this);
        this.btnAttLog.on('click', this.tarikAttLog, this);
        this.btnResetFase1.on('click', this.btnResetFase1onClick, this);
        this.on('beforedestroy', this.beforeDestroy, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnResetFase1onClick: function () {
        if (!this.pin.getValue()) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih NIK!");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin me-reset Review Fase 1 untuk karyawan ini ' +
            'pada periode ' + this.periode.getRawValue(), this.resetFase1, this);
    },
    resetFase1: function (btn) {
        if (btn == 'no') {
            return;
        } else {
            Ext.Ajax.request({
                url: 'Fp/ResetFase1',
                method: 'POST',
                scope: this,
                params: {
                    periode_id: this.periode.getValue(),
                    PIN: this.pin.getRawValue(),
                    data: Ext.encode(Ext.pluck(
                        this.store.data.items, "data"))
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    this.store.reload({
                        callback: function() {
                            Ext.MessageBox.show({
                                title: 'Info',
                                msg: response.msg,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        }
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    loadFP: function (c, r, i) {
        if (this.periode.getValue() == '') {
            Ext.MessageBox.alert("Warning", "Anda belum memilih periode!");
            this.pin.setValue("");
            return;
        } else {
            this.pin_number = c.getValue();
            this.store.baseParams = {
                pegawai_id: this.pin_number,
                periode_id: this.periode.getValue(),
                PIN: this.pin.getRawValue(),
                bu_id: jun.bu_id
            };
            this.store.reload();
        }

    },
    beforeDestroy: function () {
        this.store.removeAll();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    tarikAttLog: function () {
        var form = new jun.IpFpWin({
            modez: 6,
            title: 'Masukkan Tanggal dan Cabang'
        });
        form.show();
    },
    simpanValidOnClick: function () {
        if (!this.pin.getValue()) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih NIK!");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin memvalidasi dan menyimpan semua data ini?', this.saveValidationConfirm, this);
    },
    saveValidationConfirm: function (btn) {
        var a = jun.rztValidasi.findBy(
            function (t) {
                return t.get('pegawai_id') === this.pin.getValue() && t.get('periode_id') === this.periode.getValue();
            }, this);
        if (btn == 'no') {
            return;
        } else {
            if (a != -1) {
                Ext.MessageBox.confirm('Pertanyaan', 'Sudah terdapat data yang tersimpan. Jika Anda pilih Yes maka akan menimpa <b> SEMUA </b> data yang ada?', this.saveValidation, this);
            } else {
                this.saveValidation('yes');
            }

        }
    },
    saveValidation: function (btn, limitdate) {
        var status = '';
        var periode_id = this.periode.getValue();
        var PIN = this.pin.getRawValue();
        var pin_id = this.pin.getValue();
        if (btn == 'no') {
            return;
        } else {
            Ext.Ajax.request({
                url: 'Fp/saveValid',
                method: 'POST',
                params: {
                    periode_id: periode_id,
                    PIN: PIN,
                    pin_id: pin_id,
                    limitdate: limitdate,
                    data: Ext.encode(Ext.pluck(
                        this.store.data.items, "data"))
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    if (response.msg == 'checked') {
                        var data = Ext.decode(response.datacheck);
                        var c = jun.rztFpDataCheckStore.recordType;
                        for (var i = 0; i < data.length; i++) {
                            var d = new c({
                                shift_id    : data[i].shift_id,
                                jam_in      : data[i].jam_in,
                                jam_out     : data[i].jam_out,
                                fp_id_in    : data[i].fp_id_in,
                                fp_id_out   : data[i].fp_id_out,
                                kode_shift  : data[i].kode_shift + " (" + data[i].in_time + " - " + data[i].out_time + ") "
                            });
                            jun.rztFpDataCheckStore.add(d);
                        }
                        var form = new jun.FpDataCheckWin({
                            title           : '<p class="redInline">Pilih Shift untuk ' + response.nik +'</p>',
                            periode_id      : periode_id,
                            PIN             : PIN,
                            pin_id          : pin_id,
                            hariini         : response.hariini,
                            kode_ket        : response.kode_ket,
                            cabang_id       : response.cabang_id,
                            no_surat_tugas  : response.no_surat_tugas
                        });
                        form.show();
                    } else {
                        if(response.success != false) {
                            jun.rztFp.reload({
                                callback: function() {
                                    Ext.MessageBox.show({
                                        title   : 'Info',
                                        msg     : response.msg,
                                        buttons : Ext.MessageBox.OK,
                                        icon    : Ext.MessageBox.INFO
                                    });
                                }
                            });
                        } else {
                            Ext.MessageBox.show({
                                title   : 'Info',
                                msg     : response.msg,
                                buttons : Ext.MessageBox.OK,
                                icon    : Ext.MessageBox.INFO
                            });
                        }
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    }
});
function getFpColumn (v,m,r, day) {
    var x = r.get(day), y = x.tgl_hari ? '<b>' + x.tgl_hari + '</b>' : '';
    if (x.PIN) {
        y += '</br>In : <span style="color: blue;">' + x.jam_in +
            '</span></br>Out : <span style="color: blue;">' + x.jam_out +
            '</span></br>Validasi :' + (x.status_int_in == 1 && (x.Status_in == 9 && x.Status_out == 9) ? ' Valid' : ' Belum Valid') +
            '</span></br>Status : ' + (x.tipe_data_in == 0 ? ' Hadir' : ' LK') +
            '</span></br>Simpan : ' + (x.log_in == 0 && (x.Status_in == 9 && x.Status_out == 9) ? ' Belum disimpan' : ' Sudah disimpan');
    }
    if ((x.status_int_in == 1 || x.status_int_out == 1) && (x.Status_in == 9 && x.Status_out == 9) && x.kode_ket_in == 0) {
        m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
    } else if ((x.status_int_in == 1 || x.status_int_out == 1) && (x.Status_in == 9 && x.Status_out == 9) && x.kode_ket_in == 1) {
        m.style = "background-color: #f6ff99; border: solid 1px #A5A5A5";
    }
    return y;
}
