jun.FpStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FpStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FpStoreId',
            url: 'Fp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'Sunday'},
                {name: 'Monday'},
                {name: 'Tuesday'},
                {name: 'Wednesday'},
                {name: 'Thursday'},
                {name: 'Friday'},
                {name: 'Saturday'},
                {name: 'cabang_id'}
            ]
        }, cfg));
    }
});
jun.rztFp = new jun.FpStore();
jun.rztFpLib = new jun.FpStore();