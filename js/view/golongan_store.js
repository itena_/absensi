jun.Golonganstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Golonganstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GolonganStoreId',
            url: 'Golongan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'golongan_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'bu_id'}
            ]
        }, cfg));
        this.on('beforeload', function (a, b) {
            if (jun.bu_id == '')
                return false;

            b.params.bu_id = jun.bu_id;
        }, this);
    }
});
jun.rztGolongan = new jun.Golonganstore();
jun.rztGolonganLib = new jun.Golonganstore();
jun.rztGolonganCmp = new jun.Golonganstore();
jun.rztGolonganLib.load();
