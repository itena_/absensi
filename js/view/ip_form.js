jun.IpWin = Ext.extend(Ext.Window, {
    title: 'IP',
    modez: 1,
    width: 400,
    height: 290-100+50,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Ip',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bisnis Unit',
                        store: jun.rztBuLib,
                        hiddenName: 'bu_id',
                        valueField: 'bu_id',
                        displayField: 'bu_name',
                        ref: '../bu_id',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Cabang',
                        store: jun.rztCabangCmp,
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'nama_cabang',
                        ref: '../cabang_id',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Kode Cabang',
                        hideLabel: false,
                        readOnly: true,
                        name: 'kode_cabang',
                        id: 'kode_cabangid',
                        ref: '../kode_cabang',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'IP',
                        hideLabel: false,
                        name: 'kode_ip',
                        id: 'ipid',
                        ref: '../ip',
                        maxLength: 100,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'COM KEY',
                        hideLabel: false,
                        name: 'com_key',
                        id: 'com_keyid',
                        ref: '../com_key',
                        maxLength: 100,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Force UDP',
                        name: 'force_udp',
                        ref: '../force_udp'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.IpWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.bu_id.on('select', this.buOnSelect, this);
        this.cabang_id.on('select', this.cabangOnSelect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    buOnSelect: function (combo, record, index) {
        jun.rztCabangCmp.baseParams = {
            bu_id: record.data.bu_id
        };
        jun.rztCabangCmp.reload({
            scope: this,
            callback: function(){
                this.cabang_id.setValue('');
                this.kode_cabang.setValue('');
                this.ip.setValue('');
                this.com_key.setValue('');
            }
        });
        jun.rztCabangCmp.baseParams = {};
    },
    cabangOnSelect: function (combo, record, index) {
        this.kode_cabang.setValue(record.data.kode_cabang);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Ip/update/id/' + this.id;
        } else {
            urlz = 'Ip/create/';
        }
        Ext.getCmp('form-Ip').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztIp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Ip').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});

jun.IpFpWin = Ext.extend(Ext.Window, {
    title: 'Ip',
    modez: 1,
    width: 350,
    height: 195,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Ip',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Periode',
                        name:'periodeabsen',
                        id:'periodeabsen',
                        ref:'../periodeabsen',
                        hiddenName: 'periodeabsen',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', '6 Bulan Terakhir'],
                                ['1', '3 Bulan Terakhir'],
                                ['2', '2 Bulan Terakhir'],
                                ['3', '1 Bulan Terakhir'],
                                ['4', '1 Minggu Terakhir'],
                                ['5', 'Define']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Pilih Periode...",
                        mode : 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tglmulai',
                        fieldLabel: 'Tanggal Mulai',
                        name: 'tglmulai',
                        id: 'tglmulaiid',
                        anchor: "100%",
                        allowBlank: false,
                        hidden: true,
                        format: 'Y-m-d'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tglsampai',
                        fieldLabel: 'Tanggal Sampai',
                        name: 'tglsampai',
                        id: 'tglsampaiid',
                        anchor: "100%",
                        hidden: true,
                        allowBlank: false,
                        format: 'Y-m-d'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Cabang',
                        anchor: "100%",
                        name:'cabangabsen',
                        hiddenName: 'cabangabsen',
                        style: 'margin-bottom:2px',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztIpCmp,
                        allowBlank: false,
                        valueField: 'ip_id',
                        ref: '../cabang',
                        displayField: 'cabang_ip',
                        // sortInfo: {field: 'kode_cabang', direction: 'ASC'},
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;"><span style="font-weight: bold">{kode_ip}</span><br>\n\
                             {cabang} - {nama_cabang}</div>',
                            "</div></tpl>")
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.IpFpWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.periodeabsen.on('Select', this.periodeabsenonselect, this);
    },

    periodeabsenonselect: function(c,r,i)
    {
        var per = Ext.getCmp('periodeabsen').getValue();

        if(per == '5')
        {
            this.tglmulai.setVisible(true);
            this.tglsampai.setVisible(true);

            this.tglmulai.allowBlank = false;
            this.tglsampai.allowBlank = false;
        }
        else {
            this.tglmulai.setVisible(false);
            this.tglsampai.setVisible(false);

            this.tglmulai.allowBlank = true;
            this.tglsampai.allowBlank = true;
        }
    },


    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
        /*if (!this.tgl.getValue() || !this.cabang.getValue()) {
            Ext.Msg.alert('Gagal', 'Data belum lengkap.');
            return;
        }*/
        this.btnDisabled(true);
        this.formz.getForm().submit({
            url: 'Fp/GetAttLog',
            method: 'POST',
            scope: this,
            timeout:30000,
            /*params: {
                tgl: this.tgl.getValue(),
                cabang_id: this.cabang.getValue()
            },*/
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg + "<br>" + response.msg1,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.btnDisabled(false);
                // this.close();
            },
            failure: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', response.msg);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});