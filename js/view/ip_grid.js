jun.IpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Ip",
    id: 'docs-jun.IpGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nama BU',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_name',
            width: 100
        },
        {
            header: 'BU Alias',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_nama_alias',
            width: 100
        },
        {
            header: 'Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_cabang',
            width: 100
        },
        {
            header: 'IP',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_ip',
            width: 100
        },
        {
            header: 'COM KEY',
            sortable: true,
            resizable: true,
            dataIndex: 'com_key',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBuLib.getTotalCount() === 0)
            jun.rztBuLib.reload();

        this.store = jun.rztIp;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat IP',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah IP',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus IP',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        // this.store.baseParams = {};
        jun.IpGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    loadForm: function () {
        var form = new jun.IpWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.ip_id;
        var form = new jun.IpWin({modez: 1, id: idz});

        jun.rztCabangCmp.baseParams = {
            bu_id: selectedz.data.bu_id
        };
        jun.rztCabangCmp.reload({
            scope: this,
            callback: function(){
                form.show(this);
                form.formz.getForm().loadRecord(this.record);
            }
        });
        jun.rztCabangCmp.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Ip/delete/id/' + record.json.Ip_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztIp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})