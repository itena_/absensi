jun.Ipstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Ipstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'IpStoreId',
            url: 'Ip',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'ip_id'},
                {name: 'cabang_id'},
                {name: 'cabang'},
                {name: 'cabang_ip'},
                {name: 'kode_ip'},
                {name: 'com_key'},
                {name: 'bu_name'},
                {name: 'bu_nama_alias'},
                {name: 'bu_id'},
                {name: 'nama_cabang'},
                {name: 'kode_cabang'},
                {name: 'force_udp'}
            ]
        }, cfg));
    }
});
jun.rztIp = new jun.Ipstore();
jun.rztIpCmp = new jun.Ipstore();
//jun.rztIp.load();
