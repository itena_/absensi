jun.Jabatanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Jabatanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JabatanStoreId',
            url: 'Jabatan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jabatan_id'},
                {name: 'kode'},
                {name: 'nama_jabatan'}
            ]
        }, cfg));
        this.on('beforeload', function (a, b) {
            if (jun.bu_id == '')
                return false;

            b.params.bu_id = jun.bu_id;
        }, this);
    }
});
jun.rztJabatan = new jun.Jabatanstore();
jun.rztJabatanCmp = new jun.Jabatanstore();
jun.rztJabatanLib = new jun.Jabatanstore();
//jun.rztJabatan.load();
