jun.JenisPeriodestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.JenisPeriodestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JenisPeriodeStoreId',
            url: 'JenisPeriode',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'jenis_periode_id'},
                {name: 'bu_id'},
                {name: 'nama_jenis'}
            ]
        }, cfg));
    }
});
jun.rztJenisPeriode = new jun.JenisPeriodestore();
jun.rztJenisPeriodeCmp = new jun.JenisPeriodestore();
//jun.rztJenisPeriode.load();
