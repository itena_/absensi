jun.Keteranganstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Keteranganstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KeteranganStoreId',
            url: 'Keterangan',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'keterangan_id'},
                {name: 'nama_ket'}
            ]
        }, cfg));
    }
});
jun.rztKeterangan = new jun.Keteranganstore();
jun.rztKeteranganLib = new jun.Keteranganstore();
//jun.rztKeterangan.load();
