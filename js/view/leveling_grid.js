jun.LevelingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Leveling",
    id: 'docs-jun.LevelingGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode',
            width: 100
        },
        {
            header: 'Nama Leveling',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        }
    ],
    initComponent: function () {
        // if (jun.rztBuCmp.getTotalCount() === 0)
        jun.rztBuCmp.load();
        this.store = jun.rztLeveling;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat Leveling',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Leveling',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Leveling',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Sinkron',
                    ref: '../btnSoap',
                    hidden: true
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.LevelingGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnSoap.on('Click', this.soapClick, this);
    },
    soapClick: function () {
        Ext.Ajax.request({
            url: 'Leveling/sinkron/',
            method: 'POST',
            scope: this,
            params: {
                bu_id: jun.bu_id
            },
            success: function (f, a) {
                jun.rztLeveling.reload();
                jun.rztLevelingCmp.reload();
                jun.rztLevelingLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: (response.msg == '' ? 'Sinkronisasi berhasil' : response.msg),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.LevelingWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.leveling_id;
        var form = new jun.LevelingWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Leveling/delete/id/' + record.json.leveling_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztLeveling.reload();
                jun.rztLevelingLib.reload();
                jun.rztLevelingCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
// jun.smLeveling = new Ext.grid.CheckboxSelectionModel({
//     dataIndex: 'checked'
// });
jun.filterLeveling = new Ext.ux.grid.GridFilters({
    // encode and local configuration options defined previously for easier reuse
    encode: false, // json encode the filter query
    local: true,   // defaults to false (remote filtering)
    filters: [
        {
            type: 'string',
            dataIndex: 'kode'
        },
        {
            type: 'string',
            dataIndex: 'nama'
        },
        {
            type: 'string',
            dataIndex: 'bu_kode'
        }
    ]
});
jun.checkAll = function () {
    var t = Ext.get('hLevelingCheckAll');
    if (t.dom.text === 'Check All') {
        jun.rztLevelingSR.checkAll();
        t.dom.text = 'UnCheck All';
    } else {
        jun.rztLevelingSR.uncheckAll();
        t.dom.text = 'Check All';
    }
};
jun.LevelingSRGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Leveling",
    loadMask: true,
    modez: 0,
    id: 'docs-jun.LevelingSRGrid',
    // iconCls: "silk-grid",
    // viewConfig: {
    //     forceFit: true
    // },
    // sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    sm: new Ext.grid.CheckboxSelectionModel(),
    plugins: [jun.filterLeveling],
    columns: [
        new Ext.ux.grid.CheckColumn({
            header: '<a href="#" id="hLevelingCheckAll" onclick="jun.checkAll();" style="text-align: center;">Check All</a>',
            width: 100,
            align: 'center',
            inputValue: 1,
            uncheckedValue: 0,
            dataIndex: 'checked',
            filterable: false
        }),
        {
            header: 'Kode Leveling',
            sortable: true,
            resizable: true,
            dataIndex: 'kode',
            width: 200,
            filterable: true,
            filter: {
                type: 'string'
            }
        },
        {
            header: 'Nama Leveling',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 200,
            filterable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            }
        },
        {
            header: 'Bisnis Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_name',
            width: 200,
            filterable: true,
            filter: {
                type: 'string'
                // specify disabled to disable the filter menu
                //, disabled: true
            }
        }
    ],
    initComponent: function () {
        // if (jun.rztAreaCmp.getTotalCount() === 0)
        //     jun.rztAreaCmp.load();
        // if (jun.rztBuCmp.getTotalCount() === 0)
        // jun.rztBuCmp.load();
        this.store = jun.rztLevelingSR;
        jun.LevelingSRGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        if (this.modez == 0) {
            this.store.baseParams = {
                modez: 0
            };
        } else {
            this.store.baseParams = {
                modez: this.modez
            };
        }
        this.store.reload();
        this.store.baseParams = {};
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CabangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.cabang_id;
        var form = new jun.CabangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});
