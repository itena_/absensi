jun.Levelingstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Levelingstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LevelingStoreId',
            url: 'Leveling',
            root: 'results',
            totalProperty: 'total',
            // autoLoad: true,
            fields: [
                {name: 'leveling_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'bu_id'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if(jun.bu_id == ''){
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztLeveling = new jun.Levelingstore();
jun.rztLevelingLib = new jun.Levelingstore();
jun.rztLevelingCmp = new jun.Levelingstore();
jun.rztLevelingSRstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.rztLevelingSRstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LevelingSRstoreId',
            url: 'Leveling/IndexSr',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sr_leveling'},
                {name: 'security_roles_id'},
                {name: 'leveling_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'bu_id'},
                {name: 'bu_kode'},
                {name: 'bu_name'},
                {name: 'checked', type: 'boolean'}
            ]
        }, cfg));
    },
    checkAll: function () {
//      var dataIndex = column.dataIndex;
        for (var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('checked', 1);
            record.commit(true);
        }
    },
    uncheckAll: function () {
//      var dataIndex = column.dataIndex;
        for (var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('checked', 0);
            record.commit(true);
        }
    }
});
jun.rztLevelingSR = new jun.rztLevelingSRstore();
