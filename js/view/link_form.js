jun.LinkWin = Ext.extend(Ext.Window, {
    title: "Link Tutorial AMOS",
    modez: 1,
    width: 625,
    height: 520,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-LinkWin",
                labelWidth: 150,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    new jun.LinkGrid({
                        x: 5,
                        y: 25,
                        height: 450,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    })
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Tutup",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.LinkWin.superclass.initComponent.call(this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSave.setDisabled(a);
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});

jun.LinkFormWin = Ext.extend(Ext.Window, {
    title: 'Link',
    modez: 1,
    width: 400,
    height: 180,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-LinkFormWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Urutan',
                        hideLabel: false,
                        name: 'visible',
                        ref: '../visible',
                        maxLength: 30,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Judul',
                        hideLabel: false,
                        name: 'note',
                        ref: '../note',
                        maxLength: 255,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Link',
                        hideLabel: false,
                        name: 'link',
                        ref: '../link',
                        maxLength: 255,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.LinkFormWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Link/update/id/' + this.id;
        } else {
            urlz = 'Link/create/';
        }
        Ext.getCmp('form-LinkFormWin').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztLink.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-LinkFormWin').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});