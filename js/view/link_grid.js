jun.LinkGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Link",
    id: 'docs-jun.LinkGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No.',
            sortable: true,
            resizable: true,
            dataIndex: 'visible',
            width: 10
        },
        {
            header: 'Judul',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Link',
            sortable: true,
            resizable: true,
            dataIndex: 'link',
            width: 70
        }
    ],
    initComponent: function () {
        this.store = jun.rztLink;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    hidden: jun.securityroles == SECURITY_ROLE_ADMINISTRATOR ? false : true,
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator',
                    hidden: jun.securityroles == SECURITY_ROLE_ADMINISTRATOR ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    hidden: jun.securityroles == SECURITY_ROLE_ADMINISTRATOR ? false : true,
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator',
                    hidden: jun.securityroles == SECURITY_ROLE_ADMINISTRATOR ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    hidden: jun.securityroles == SECURITY_ROLE_ADMINISTRATOR ? false : true,
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator',
                    hidden: jun.securityroles == SECURITY_ROLE_ADMINISTRATOR ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Video tidak bisa dibuka?',
                    ref: '../btnError'
                },
                '->',
                {
                    xtype: 'label',
                    text: 'Double click Link untuk membuka video.'
                }
            ]
        };

        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};

        jun.LinkGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnError.on('Click', this.btnErrorClick, this);
        this.on('celldblclick', this.openLink);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    openLink: function (t) {
        var form = new jun.InfoWin({
            modez: 0,
            title: 'Link',
            note: t.record.data.link
        });
        form.show();
        form.btnCancel.setText('Copy & Tutup');
        window.open(t.record.data.link, '_blank').focus();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },

    btnErrorClick: function () {
        var form = new jun.InfoWin({
            modez: 0,
            title: 'Info',
            note: 'Jika video tidak bisa dibuka, silahkan copy link video-nya dan dibuka menggunakan Google Chrome.'
        });
        form.show();
    },

    loadForm: function () {
        var form = new jun.LinkFormWin({
            modez: 0,
            title: 'Add Link'
        });
        form.show();
    },

    loadEditForm: function () {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.link_id;
        var form = new jun.LinkFormWin({
            modez: 1, id: idz,
            title: 'Edit Link'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },

    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes: function (btn) {

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'Link/delete/id/' + record.json.link_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztLink.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
});
