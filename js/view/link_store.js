jun.Linkstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Linkstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LinkStoreId',
            url: 'Link',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'link_id'},
                {name: 'note'},
                {name: 'link'},
                {name: 'visible'}

            ]
        }, cfg));
    }
});
jun.rztLink = new jun.Linkstore();
//jun.rztLink.load();
