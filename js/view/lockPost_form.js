jun.LockPostWin = Ext.extend(Ext.Window, {
    title: 'Post & Lock Absensi',
    modez: 1,
    width: 600,
    height: 450,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-LockPost',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib,
                        fieldLabel: 'Periode',
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                                <br>\n\<b>Start: </b>{periode_start}\n\\n\
                                <br>\n\<b>End: </b>{periode_end}\n\</span>',
                                "</div></tpl>"),
                        ref: '../periode',
                        id: 'periodeOnLockPost',
                        width: 200
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBuCmp,
                        fieldLabel: 'Unit Usaha',
                        hiddenName: 'bu_id',
                        valueField: 'bu_id',
                        displayField: 'bu_name',
                        ref: '../bu',
                        id: 'buOnLockPost',
                        width: 200
                    },
                    {
                        xtype: 'label',
                        text: '(Double click untuk mengganti status.)'
                    },
                    new jun.LockPostDetailGrid({
                        height: 280,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        anchor: '100%'
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Lock All Cabang',
                    hidden: (jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD) ? false : true,
                    ref: '../btnLockAllCabang'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.LockPostWin.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnLockAllCabang.on('click', this.btnLockAllCabangOnClick, this);
        this.bu.on('select', this.onbuSelect, this);
        this.periode.on('select', this.onperiodeSelect, this);
        this.on('close', this.winClose, this);
    },
    btnLockAllCabangOnClick: function () {
        Ext.Ajax.request({
            url: 'LockPost/LockAllCabang',
            method: 'POST',
            scope: this,
            params: {
                bu_id: this.bu.getValue(),
                periode_id: this.periode.getValue()
            },
            success: function (f, a) {
                jun.rztLockPost.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (response.success == true) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    winClose: function () {
        this.griddetils.store.removeAll();
		jun.rztLockPost.reload();
    },
    onbuSelect: function (c, r, i) {
        if (!this.periode.getValue()) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Periode");
            return;
        }
        this.griddetils.store.baseParams = {mode: "grid", cmp: 'detail', bu_id: c.getValue(), periode_id: this.periode.getValue()};
        this.griddetils.store.reload();
    },
    onperiodeSelect: function (c, r, i) {
        this.bu.setValue('');
        this.griddetils.store.removeAll();
    },
    onbtnCancelclick: function () {
        this.close();
    }

});

jun.LockPostDetailGrid = Ext.extend(Ext.grid.EditorGridPanel, {
    title: "Lock Post Detail",
    id: 'docs-jun.LockPostDetail',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    clicksToEdit: 1,
    columns: [
        {
            header: 'Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_cabang',
            width: 100
        },
        {
            header: 'Status Posted',
            sortable: true,
            resizable: true,
            dataIndex: 'posted',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('posted') == 1) {
                    return 'POSTED';
                } else {
                    return 'UNPOSTED';
                }
            }
        },
        {
            header: 'Posted User',
            sortable: true,
            resizable: true,
            dataIndex: 'posted_user_name',
            width: 100
        },
        {
            header: 'Status Locked',
            sortable: true,
            resizable: true,
            dataIndex: 'locked',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get('locked') == 1) {
                    return 'LOCKED';
                } else {
                    return 'UNLOCKED';
                }
            }
        },
        {
            header: 'Locked User',
            sortable: true,
            resizable: true,
            dataIndex: 'locked_user_name',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztLockPostDetail;
        jun.LockPostDetailGrid.superclass.initComponent.call(this);
        this.on('celldblclick', this.editCol);
    },
    editCol: function (t, r, c, e) {
        var record = this.store.getAt(r);
        var col = t.getColumnModel().getColumnAt(c).dataIndex;
        if (col == 'posted') {          
            if (t.getSelectionModel().selection.record.data.locked == 1){
                Ext.MessageBox.alert("Warning", "Tidak bisa dirubah, karena periode ini sudah di <b>LOCK</b>.");
                return; 
            }
            this.create(record, t, col);
        } else if (col == 'locked') {
            if(jun.securityroles == SECURITY_ROLE_HRD || jun.securityroles == SECURITY_ROLE_ADMINISTRATOR ){
                this.create(record, t, col);
            } else {                
                Ext.MessageBox.alert("Warning", "Anda tidak punya akses untuk mengubah status Lock.");
                return;
            }            
        }
    },
    create: function (record, t, col) {
        Ext.Ajax.request({
            url: 'LockPost/Create',
            method: 'POST',
            params: {
                periode_id : Ext.getCmp('periodeOnLockPost').getValue(),
                cabang_id : t.getSelectionModel().selection.record.data.cabang_id,
                bu_id : Ext.getCmp('buOnLockPost').getValue(),
                col : col,
                status : col == 'posted' ? record.data.posted : record.data.locked
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.msg == true) {
                    col == 'posted' ? record.set('posted', response.postlock) : record.set('locked', response.postlock);
                    col == 'posted' ? record.set('posted_user_name', response.user_name) : record.set('locked_user_name', response.user_name);
                } else {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });                    
                }      
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
