jun.LockPostGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Post & Lock Absensi",
    id: 'docs-jun.LockPostGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [ 
        {
            header: 'Periode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_periode',
            width: 40
        },
        {
            header: 'Unit Usaha',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_name',
            width: 40
        },
        {
            header: 'Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_cabang',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztPeriodeLib.getTotalCount() === 0){
            jun.rztPeriodeLib.load();            
        }
        if (jun.rztCabangCmp.getTotalCount() === 0) {
            jun.rztCabangCmp.load();
        }
        if (jun.rztBuCmp.getTotalCount() === 0) {
            jun.rztBuCmp.load();
        }
        this.store = jun.rztLockPost;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat atau Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Check Presensi',
                    ref: '../btnCheckAbsen'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload(); 
        
        jun.LockPostGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnCheckAbsen.on('Click', this.onbtnCheckAbsenClick, this); 
        this.on('celldblclick', this.loadEditForm, this); 
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onbtnCheckAbsenClick: function () {
        var form = new jun.CheckAbsensi({modez: 0});
        form.show();
    },
    getrow: function (sm, idx, r) {
        this.record = r; 
        var selectedz = this.sm.getSelections();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        // console.log(selectedz);
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            var form = new jun.LockPostWin({modez: 0});
            form.show();
        } else {
            var periode_id = selectedz.json.periode_id;
            var bu_id = selectedz.json.bu_id;
            var form = new jun.LockPostWin({modez: 1, periode_id: periode_id, bu_id: bu_id});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
            jun.rztLockPostDetail.baseParams = {
                periode_id: periode_id,
                bu_id: bu_id,
                mode: "grid", cmp: 'detail'
            };
            jun.rztLockPostDetail.load();
            jun.rztLockPostDetail.baseParams = {};
        }
    }
});
