jun.LockPoststore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LockPoststore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LockPostStoreId',
            url: 'LockPost',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'lock_post_id'},
                {name: 'bu_id'},
                {name: 'periode_id'},
                {name: 'cabang_id'},
                {name: 'locked'},
                {name: 'locked_user_id'},
                {name: 'locked_tdate'},
                {name: 'posted'},
                {name: 'posted_user_id'},
                {name: 'posted_tdate'},
                {name: 'auto_lock'},
                
                {name: 'bu_name'},
                {name: 'kode_periode'},
                {name: 'kode_cabang'},
                {name: 'locked_user_name'},
                {name: 'posted_user_name'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztLockPost = new jun.LockPoststore();
jun.rztLockPostDetail = new jun.LockPoststore({baseParams : {mode: "grid", cmp: 'detail'}});
//jun.rztLockPost.load();
