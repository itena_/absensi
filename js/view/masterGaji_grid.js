jun.MasterGajiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Gaji",
    id: 'docs-jun.MasterGajiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Leveling',
            sortable: true,
            resizable: true,
            dataIndex: 'leveling_id',
            width: 100,
            renderer: jun.renderLeveling,
            filter: {xtype: "textfield", filterName: "level"}
        },
        {
            header: 'Golongan',
            sortable: true,
            resizable: true,
            dataIndex: 'golongan_id',
            width: 100,
            renderer: jun.renderGolongan,
            filter: {xtype: "textfield", filterName: "golongan"}
        },
        {
            header: 'Area',
            sortable: true,
            resizable: true,
            dataIndex: 'area_id',
            width: 100,
            renderer: jun.renderArea,
            filter: {xtype: "textfield", filterName: "area"}
        },
        {
            header: 'Master',
            sortable: true,
            resizable: true,
            dataIndex: 'master_id',
            width: 100,
            renderer: jun.renderMaster,
            filter: {xtype: "textfield", filterName: "master"}
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        jun.rztGolonganLib.load();
        jun.rztLevelingLib.load();
        jun.rztAreaLib.load();
        jun.rztMasterLib.load();
        jun.rztBuCmp.load();
        jun.rztGolonganCmp.load();
        jun.rztMasterCmp.load();
        jun.rztLevelingCmp.load();
        jun.rztAreaCmp.load();
        this.store = jun.rztMasterGaji;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    iconCls: 'silk13-pencil',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.MasterGajiGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.MasterGajiWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.master_gaji_id;
        var form = new jun.MasterGajiWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'MasterGaji/delete/id/' + record.json.master_gaji_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztMasterGaji.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
