jun.MasterGajistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MasterGajistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MasterGajiStoreId',
            url: 'MasterGaji',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'master_gaji_id'},
                {name: 'leveling_id'},
                {name: 'area_id'},
                {name: 'golongan_id'},
                {name: 'master_id'},
                {name: 'amount'},
                {name: 'bu_id'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztMasterGaji = new jun.MasterGajistore();
//jun.rztMasterGaji.load();
