jun.Masterstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Masterstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MasterStoreId',
            url: 'Master',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'master_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'periode_'},
                {name: 'bu_id'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztMaster = new jun.Masterstore();
jun.rztMasterLib = new jun.Masterstore();
jun.rztMasterCmp = new jun.Masterstore();
jun.rztMasterLib.load();
