jun.OffWin = Ext.extend(Ext.Window, {
    title: 'Off',
    modez: 1,
    width: 500,
    height: 445-100,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    periode_id: '',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Off',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Libur',
                        readOnly: true,
                        allowBlank: false,
                        name: 'libur',
                        ref: '../libur',
                        anchor: '100%'
                    },
                    {
                        xtype: 'button',
                        text: 'Check All',
                        hidden: false,
                        ref: '../btnAll'
                    },
                    new jun.PegawaiReportGrid({
                        height: 210,
                        frameHeader: !1,
                        header: !1,
                        ref: "../pegawaidetils"
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.OffWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnAll.on('click', this.onbtnCheckAll, this);

    },onbtnCheckAll: function (b, e)
    {
        if (b.getText() == 'Check All') {
            this.pegawaidetils.store.checkAll();
            b.setText('Uncheck All');
        } else if (b.getText() == 'Uncheck All') {
            this.pegawaidetils.store.uncheckAll();
            b.setText('Check All');
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
        this.btnDisabled(true);
        Ext.getCmp('form-Off').getForm().submit({
            url: 'Off/OffAll',
            timeout: 1000,
            scope: this,
            params: {
                pegawaidetils: Ext.encode(Ext.pluck(
                    this.pegawaidetils.store.data.items, "data")),
                periode_id: this.periode_id
            },
            success: function (f, a) {
                jun.rztOff.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Off').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});