jun.OffGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Off",
    id: 'docs-jun.OffGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'selected_date',
            width: 100
        },
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBuLib.getTotalCount() === 0)
            jun.rztBuLib.reload();

        this.store = jun.rztOff;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: 'Periode'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                    hiddenName: 'periode_id',
                    valueField: 'periode_id',
                    emptyText: 'Pilih Periode',
                    ref: '../periode_id',
                    displayField: 'kode_periode',
                    width: 200,
                    anchor: '100%',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<div style="width:100%;display:inline-block;">\n\
                            <span style="font-weight: bold">{kode_periode}</span>\n\
                            <br>\n\{periode_start} - {periode_end}\n\
                            </div>',
                        "</div></tpl>"),
                    id: 'periodeOnOffGrid'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    text: 'Double click untuk menandai OFF.'
                },
                '->',
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Multi Review Off',
                    ref: '../btnOff'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};

        jun.OffGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.periode_id.on('select', this.loadData, this);
        this.on('celldblclick', this.onCelldblclick);
        this.btnOff.on('click', this.onbtnOffClick, this);
    },
    onbtnOffClick: function () {
        if (this.periode_id.getValue() == '') {
            Ext.Msg.alert('Failure', 'Pilih periode terlebih dahulu!');
            return;
        }

        var libur = jun.rztPeriodeStore.data.items[0].data.libur;

        var form = new jun.OffWin({modez: 0, periode_id: this.periode_id.getValue()});
        form.show();
        form.libur.setValue(libur);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadData: function (c, r, i) {
        jun.rztPeriodeStore.load({params: {periode_id: c.getValue()}});

        if(jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD){
            Ext.Msg.alert('Failure', 'Hanya bisa dilakukan oleh cabang.');
            return;
        }
        this.store.baseParams = {
            cabang_id   : jun.usercabang,
            periode_id  : c.getValue()
        };
        this.store.reload();
    },
    onCelldblclick: function (t, r, c, e) {
        var record = this.store.getAt(r);
        var col = t.getColumnModel().getColumnAt(c).dataIndex;

        if (record.data.note == 'data double') {
            Ext.Msg.alert('Failure', 'Review Off di tanggal yang memiliki data double tidak diperbolehkan!');
            return;
        } else {
            if (record.data.note == 'belum diisi')
                this.createConfirmation(record);
            else
                this.invalid(record, t, col);
        }
    },
    createConfirmation: function (record) {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin meng-Off kan NIK <b>'+ record.data.nik + '</b> pada tanggal <b>'+ record.data.selected_date + '</b> ?', this.create, this);
    },
    create: function (btn) {
        if (btn == 'no') {
            return;
        }

        //CEK JATAH OFF

        var data        = this.record.data;
        var pegawai_id  = data.pegawai_id;
        var periode_id  = this.periode_id.getValue();

        jun.rztShiftCmp.reload({
            params: {'pegawai_id': data.pegawai_id},
            callback: function (recs) {
                var shift       = jun.rztShiftCmp.getAt(0).data;
                var shift_id    = shift.shift_id;
                var day         = new Date(data.selected_date);
                var nextDay     = new Date(day);
                                  nextDay.setDate(day.getDate() + 1);
                var tomorrow    = nextDay.dateFormat('Y-m-d');

                Ext.Ajax.request({
                    url: 'Validasi/Create',
                    method: 'POST',
                    scope: this,
                    params: {
                        pegawai_id  : pegawai_id,
                        periode_id  : periode_id,
                        shift_id    : shift_id,
                        jam_in      : data.selected_date,
                        jam_out     : shift.in_time < shift.out_time ?  data.selected_date : tomorrow,
                        PIN         : data.nik,
                        nosurat     : "",
                        in_time     : shift.in_time,
                        out_time    : shift.out_time,
                        ket         : 4
                    },
                    success: function (f, a) {
                        jun.rztOff.reload({
                            params: {'periode_id': periode_id}
                        });
                        var response = Ext.decode(f.responseText);
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: 'Off ditambahkan.',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        });
    },
    invalid: function (record, t, col) {
        Ext.Msg.alert('Failure', 'Fitur belum tersedia.');
        return;
    }
});