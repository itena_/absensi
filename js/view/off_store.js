jun.Offstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Offstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'OffStoreId',
            url: 'Off',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'selected_date'},
                {name: 'bu_name'},
                {name: 'nik'},
                {name: 'pegawai_id'},
                {name: 'note'},
                {name: 'nama_lengkap'}
            ]
        }, cfg));
    }
});
jun.rztOff = new jun.Offstore();
//jun.rztOff.load();
