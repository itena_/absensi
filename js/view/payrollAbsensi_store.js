jun.PayrollAbsensistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PayrollAbsensistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PayrollAbsensiStoreId',
            url: 'PayrollAbsensi',
            root: 'results',
            // autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'payroll_absensi_id'},
                {name: 'periode_id'},
                {name: 'pegawai_id'},                
                {name: 'total_hari_kerja'},
                {name: 'locked'},
                {name: 'tdate'},
                {name: 'total_lk'},
                {name: 'total_sakit'},
                {name: 'total_cuti_tahunan'},
                {name: 'total_cuti_menikah'},
                {name: 'total_cuti_bersalin'},
                {name: 'total_cuti_istimewa'},
                {name: 'total_cuti_non_aktif'},
                {name: 'total_off'},
                {name: 'total_cuti_menikah'},
                {name: 'total_cuti_bersalin'},
                {name: 'total_cuti_istimewa'},
                {name: 'total_cuti_non_aktif'},
                {name: 'total_lembur_1'},
                {name: 'total_lembur_next'},
                {name: 'nik'},
                {name: 'less_time'},
                {name: 'jatah_off'},
                {name: 'nik'},
                {name: 'pegawai_id'}
            ]
        }, cfg));
    }
});
jun.rztPayrollAbsensi = new jun.PayrollAbsensistore();
jun.rztPayrollAbsensiLib = new jun.PayrollAbsensistore();
//jun.rztPayrollAbsensi.load();
