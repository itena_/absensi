jun.PayrollDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PayrollDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PayrollDetailsStoreId',
            url: 'PayrollDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'payroll_detail_id'},
                {name: 'payroll_id'},
                {name: 'nama_skema'},
                {name: 'type_'},
                {name: 'amount'}
            ]
        }, cfg));
    }
});
jun.rztPayrollDetails = new jun.PayrollDetailsstore();
//jun.rztPayrollDetails.load();
