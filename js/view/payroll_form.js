jun.PayrollWin = Ext.extend(Ext.Window, {
    title: 'Payroll',
    modez: 1,
    width: 1100,
    height: 600,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Payroll',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'border',
                ref: 'formz',
                border: false,
                items: [
                    {
                        region: 'west',
                        height: 100,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        width: '30%',
                        layout: 'form',
                        labelWidth: 100,
                        labelAlign: 'left',
                        items: [
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Periode',
                                store: jun.rztPeriodeCmp,
                                hiddenName: 'periode_id',
                                valueField: 'periode_id',
                                displayField: 'kode_periode',
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'NIK',
                                hideLabel: false,
                                //hidden:true,
                                name: 'nik',
                                ref: '../nik',
                                maxLength: 36,
                                //allowBlank: ,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Lengkap',
                                hideLabel: false,
                                //hidden:true,
                                name: 'nama_lengkap',
                                ref: '../nama_lengkap',
                                maxLength: 36,
                                //allowBlank: ,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'E-Mail',
                                hideLabel: false,
                                //hidden:true,
                                name: 'email',
                                ref: '../email',
                                maxLength: 50,
                                //allowBlank: ,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Leveling',
                                store: jun.rztLevelingCmp,
                                hiddenName: 'leveling_id',
                                valueField: 'leveling_id',
                                displayField: 'kode',
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Golongan',
                                store: jun.rztGolonganCmp,
                                hiddenName: 'golongan_id',
                                valueField: 'golongan_id',
                                displayField: 'kode',
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                fieldLabel: 'Area',
                                store: jun.rztAreaCmp,
                                hiddenName: 'area_id',
                                valueField: 'area_id',
                                displayField: 'nama',
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Total Income',
                                //hidden:true,
                                name: 'total_income',
                                id: 'total_income_id',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Total Deduction',
                                //hidden:true,
                                name: 'total_deduction',
                                id: 'total_deduction_id',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Take Home Pay',
                                //hidden:true,
                                name: 'take_home_pay',
                                id: 'take_home_pay_id',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Transfer',
                                //hidden:true,
                                name: 'transfer',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        region: 'east',
                        height: 100,
                        bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                        layout: 'form',
                        labelWidth: 100,
                        width: '30%',
                        labelAlign: 'left',
                        items: [
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Hari Kerja',
                                //hidden:true,
                                name: 'total_hari_kerja',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Cuti Tahunan',
                                //hidden:true,
                                name: 'total_cuti_tahunan',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Cuti Menikah',
                                //hidden:true,
                                name: 'total_cuti_menikah',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Cuti Bersalin',
                                //hidden:true,
                                name: 'total_cuti_bersalin',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Cuti Istimewa',
                                //hidden:true,
                                name: 'total_cuti_istimewa',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Cuti Non Aktif',
                                //hidden:true,
                                name: 'total_cuti_non_aktif',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Sakit',
                                //hidden:true,
                                name: 'total_sakit',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Luar Kota',
                                //hidden:true,
                                name: 'total_lk',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Ganti Off',
                                //hidden:true,
                                name: 'total_off',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Lembur 1',
                                //hidden:true,
                                name: 'total_lembur_1',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Lembur 2',
                                //hidden:true,
                                name: 'total_lembur_next',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'numericfield',
                                hideLabel: false,
                                fieldLabel: 'Less Time',
                                //hidden:true,
                                name: 'less_time',
                                maxLength: 30,
                                readOnly: true,
                                anchor: '100%'
                            }
                        ]
                    },
                    // {
                    //     region: 'east',
                    //     height: 100,
                    //     bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                    //     layout: 'form',
                    //     width: '30%',
                    //     labelWidth: 100,
                    //     labelAlign: 'left',
                    //     items: [
                    //
                    //     ]
                    // },
                    new jun.PayrollDetailsGrid({
                        region: 'center',
                        width: '30%',
                        frameHeader: !1,
                        height: 100,
                        header: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PayrollWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Payroll/update/id/' + this.id;
        Ext.getCmp('form-Payroll').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.rztPayrollDetails.store.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Payroll').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.PayrollGenerateWin = Ext.extend(Ext.Window, {
    title: 'Generate Payroll',
    modez: 1,
    width: 400,
    height: 115,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PayrollGenerate',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PayrollGenerateWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        urlz = 'Payroll/PayrollGenerate/';
        Ext.getCmp('form-PayrollGenerate').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PayrollGenerate').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});