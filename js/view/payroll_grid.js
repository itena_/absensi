jun.PayrollGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Payroll",
    id: 'docs-jun.PayrollGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Periode',
            sortable: true,
            resizable: true,
            dataIndex: 'periode_id',
            width: 100,
            renderer: jun.renderPeriode
        },
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100
        },
        {
            header: 'Level',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_level',
            width: 100
        },
        {
            header: 'Golongan',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_gol',
            width: 100
        },
        {
            header: 'Jabatan',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_jabatan',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztSchemaCmp.getTotalCount() === 0)
            jun.rztSchemaCmp.load();
        if (jun.rztPeriodeCmp.getTotalCount() === 0)
            jun.rztPeriodeCmp.load();
        if (jun.rztLevelingCmp.getTotalCount() === 0)
            jun.rztLevelingCmp.load();
        if (jun.rztGolonganCmp.getTotalCount() === 0)
            jun.rztGolonganCmp.load();
        if (jun.rztAreaCmp.getTotalCount() === 0)
            jun.rztAreaCmp.load();
        this.store = jun.rztPayroll;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Generate All per Periode',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Payroll per Pegawai',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Recalculated per Pegawai',
                    ref: '../btnRecalculated'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Download Template Komponen Gaji',
                    ref: '../btnDownloadImport',
                    hidden: false
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Import Template Gaji',
                    ref: '../btnImport',
                    hidden: false
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Slip Gaji',
                    ref: '../btnCreateSlipGaji'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportPayroll",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "periode_id",
                            ref: "../../periode_id"
                        },
                        {
                            xtype: "hidden",
                            name: "payroll_id",
                            ref: "../../payroll_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
                //,
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PayrollGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnRecalculated.on('Click', this.recalculatedRec, this);
        this.btnCreateSlipGaji.on('Click', this.createSlipGaji, this);
        this.btnDownloadImport.on('Click', this.downloadTemplate, this);
        this.btnImport.on('Click', this.importData, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    downloadTemplate: function () {
        window.open(TEMPLATE_UPLOAD);
    },
    importData: function () {
        var form = new jun.ImportKomponenPayroll();
        form.show();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PayrollGenerateWin({modez: 0});
        form.show();
    },
    createSlipGaji: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih payroll");
            return;
        }
        if (jun.rztLock.findExact('periode_id', selectedz.data.periode_id) == -1) {
            Ext.MessageBox.alert("Warning", "Data Payroll belum di lock");
            return;
        }
        // if (selectedz.data.lock == 0) {
        //
        // }
        Ext.getCmp("form-ReportPayroll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPayroll").getForm().url = "Report/CetakSlip";
        this.payroll_id.setValue(selectedz.json.payroll_id);
        var form = Ext.getCmp('form-ReportPayroll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.payroll_id;
        var form = new jun.PayrollWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPayrollDetails.load({
            params: {
                payroll_id: idz
            }
        });
    },
    recalculatedRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin kalkulasi data ini?', this.recalculatedRecRecYes, this);
    },
    recalculatedRecRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Payroll/PayrollRecalculated',
            method: 'POST',
            params: {
                periode_id: record.data.periode_id,
                pegawai_id: record.data.pegawai_id
            },
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Payroll/delete/id/' + record.json.payroll_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ImportKomponenPayroll = Ext.extend(Ext.Window, {
    title: "Import Komponen Payroll",
    iconCls: "silk13-report",
    modez: 1,
    width: 500,
    height: 445,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportKomponenPayroll",
                bodyStyle: 'background-color: #E4E4E4; padding: 10px;margin-bottom: 5px;',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: true,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Schema',
                        store: jun.rztSchemaCmp,
                        hiddenName: 'schema_id',
                        valueField: 'schema_id',
                        displayField: 'nama_skema',
                        ref: '../schema',
                        anchor: '75%'
                    }
                ]
            },
            // {
            //     xtype: "panel",
            //     bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
            //     html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            // },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xls)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 250,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg',
                    style: {
                        'fontFamily': 'courier new',
                        'fontSize': '12px'
                    }
                }
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportKomponenPayroll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file importKas.");
            return;
        }
        jun.importKasStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.CheckImportKomponenPayrollWin({generateQuery: 0});
        win.show(this);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI IMPORT KOMPONEN GAJI -----');
        this.rowLoopCounter = 0;
        this.headerCounter = 0;
        this.dataSubmit = null;
        this.arrData = jun.dataStockOpname[jun.namasheet];
        this.loopPost();
    },
    headerCounter: 0,
    dataSubmit: null,
    arrData: null,
    loopPost: function () {
        if (this.headerCounter >= this.arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT KOMPONEN GAJI SELESAI -----'
                + '\nJumlah data : ' + this.headerCounter
                + '\n-------------------------------------------\n');
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            this.arrData = null;
            jun.dataStockOpname = null;
            return;
        }
        var d = this.arrData[this.headerCounter];
        // this.headerCounter++;
        this.dataSubmit = {};
        this.writeLog('\n============== Data ke- ' + this.headerCounter + ' ==============\n');
        this.loopLog('Data Header nomor : ' + this.headerCounter);
        this.dataSubmit['nik'] = d.NO_NIK;
        this.dataSubmit['amount'] = d.AMOUNT;
        this.writeLog('NIK : ' + this.dataSubmit['nik'] + '\nAMOUNT : ' + this.dataSubmit['amount']);
        this.writeLog('>>> Submit : Data nomor : ' + this.headerCounter);
        Ext.getCmp('form-ImportKomponenPayroll').getForm().submit({
            url: 'PayrollDetails/Upload',
            timeout: 1000,
            scope: this,
            params: this.dataSubmit,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                var msg = '';
                if (response.success) {
                    msg = response.msg;
                } else {
                    msg = '***** GAGAL ****\n' + response.msg;
                    this.btnSave.setDisabled(false);
                }
                this.writeLog('<<< Result : dari nomor : ' + this.headerCounter + '\n' + msg);
                if (response.success) {
                    this.nextRow();
                    // jun.rztPayroll.reload();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        // this.nextRow();
    },
    nextRow: function () {
        this.headerCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('Baris ke- ' + (this.headerCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --> ' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    }
});
