jun.PegawaiNonAktifGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pegawai Non Aktif",
    id: 'docs-jun.PegawaiNonAktifGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100,
            filter: {xtype: "textfield", filterName: "nik"}
        },
        {
            header: 'Nama Lengkap',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100,
            filter: {xtype: "textfield", filterName: "nama_lengkap"}
        },
        {
            header: 'Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_cabang',
            // renderer: jun.renderCabang,
            width: 100,
            filter: {xtype: "textfield", filterName: "kode_cabang"}
        },
        {
            header: 'Status Pegawai',
            sortable: true,
            resizable: true,
            dataIndex: 'kode',
            width: 100,
            filter: {xtype: "textfield", filterName: "kode"}
        },
        {
            header: 'Kelompok Pegawai',
            sortable: true,
            resizable: true,
            dataIndex: 'kelompok_pegawai',
            width: 100,
            renderer: function (v, m, r) {
                if(r.get('kelompok_pegawai') == 1){
                    return 'OG,OB atau Security';
                } else return 'Karyawan';
            }
        }
    ],
    initComponent: function () {
        if (jun.rztJabatanCmp.getTotalCount() === 0)
            jun.rztJabatanCmp.load();
        if (jun.rztStatusPegawaiCmp.getTotalCount() === 0)
            jun.rztStatusPegawaiCmp.load();
        if (jun.rztGolonganCmp.getTotalCount() === 0)
            jun.rztGolonganCmp.load();
        if (jun.rztLevelingCmp.getTotalCount() === 0)
            jun.rztLevelingCmp.load();
        if (jun.rztCabangCmp.getTotalCount() === 0)
            jun.rztCabangCmp.load();
        if (jun.rztStatusCmp.getTotalCount() === 0)
            jun.rztStatusCmp.load();
        if (jun.rztUsers.getTotalCount() === 0) {
            jun.rztUsers.load();
        }

        this.store = jun.rztPegawaiNonAktif;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [

            ]
        };
        this.store.baseParams = {mode: "grid", status_pegawai: "nonAktif"};
        this.store.reload();

        jun.PegawaiNonAktifGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});
