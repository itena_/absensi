jun.PegawaiSpesialWin = Ext.extend(Ext.Window, {
    title: 'Pegawai Case Khusus',
    modez: 1,
    width: 400,
    height: 190+30,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PegawaiSpesial',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Pegawai',
                        style: 'margin-bottom:2px',
                        mode: 'remote',
                        triggerAction: 'query',
                        autoSelect: false,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        listWidth: 450,
                        lastQuery: "",
                        lazyRender: true,
                        forceSelection: true,
                        store: jun.rztPegawaiAll,
                        hiddenName: 'pegawai_id',
                        valueField: 'pegawai_id',
                        ref: '../pegawai_id',
                        displayField: 'nik',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{nik}</b><br>{nama_lengkap}</span>',
                            "</div></tpl>"),
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Ambil dari BU',
                        hideLabel: false,
                        name: 'bu_kode',
                        id: 'bu_kodeid',
                        ref: '../bu_kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'label',
                        text: 'Pisahkan BU dengan semicolon (;) jika lebih terdapat dari 1 BU.'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nik Lain',
                        hideLabel: false,
                        name: 'nik_lain',
                        id: 'nik_lainid',
                        ref: '../nik_lain',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Cabang Lain',
                        hideLabel: false,
                        //hidden:true,
                        name: 'cabang',
                        ref: '../cabang',
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PegawaiSpesialWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'PegawaiSpesial/update/id/' + this.id;
        } else {
            urlz = 'PegawaiSpesial/create/';
        }
        Ext.getCmp('form-PegawaiSpesial').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPegawaiSpesial.reload();
                jun.rztPegawaiSpesialCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PegawaiSpesial').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});

jun.TarikDataPegawaiSpesialWin = Ext.extend(Ext.Window, {
    title: "Tarik Data Absensi Pegawai Case Khusus",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 180-30,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-TarikDataPegawaiSpesial",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 175,
                        anchor: "100%",
//                        x: 70,
//                        y: 2,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        mode: 'local',
                        triggerAction: 'all',
                        autoSelect: false,
                        typeAhead: true,
                        lazyRender: true,
                        forceSelection: true,
                        store: jun.rztPegawaiSpesialCmp,
                        hiddenName: 'pegawai_id',
                        valueField: 'pegawai_id',
                        fieldLabel: 'NIK',
                        displayField: 'pin_nama',
                        ref: '../pegawai',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Tarik Data Absensi",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Batal",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.TarikDataPegawaiSpesialWin.superclass.initComponent.call(this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },

    saveForm: function () {
        if (!this.periode_id.getValue()) {
            Ext.Msg.alert('Error', "Periode harus dipilih!");
            return;
        }
        this.btnDisabled(true);
        var urlz = 'pegawaiSpesial/TarikData';
        Ext.getCmp('form-TarikDataPegawaiSpesial').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TarikDataPegawaiSpesial').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});