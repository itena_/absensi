jun.PegawaiSpesialGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pegawai yg finger di mesin finger PT lain",
    id: 'docs-jun.PegawaiSpesialGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100
        },
        {
            header: 'Nama Pegawai',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100
        },
        {
            header: 'Ambil dari BU',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_kode',
            width: 100
        },
        {
            header: 'Nik Lain',
            sortable: true,
            resizable: true,
            dataIndex: 'nik_lain',
            width: 100
        },
        {
            header: 'Finger di luar cabang Pusat',
            sortable: true,
            resizable: true,
            dataIndex: 'cabang',
            width: 100
        }

    ],
    initComponent: function () {
        jun.rztPegawaiAll.load();
        if (jun.rztPegawaiSpesialCmp.getTotalCount() === 0) {
            jun.rztPegawaiSpesialCmp.load();
        }
        this.store = jun.rztPegawaiSpesial;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Tarik Data Absensi',
                    ref: '../btnTarikData'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PegawaiSpesialGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnTarikData.on('Click', this.loadFormTarikData, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    loadForm: function () {
        var form = new jun.PegawaiSpesialWin({modez: 0});
        form.show();
    },
    loadFormTarikData: function () {
        var form = new jun.TarikDataPegawaiSpesialWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pegawai_spesial_id;
        var form = new jun.PegawaiSpesialWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;

        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PegawaiSpesial/delete/id/' + record.json.pegawai_spesial_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPegawaiSpesial.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }}
});
