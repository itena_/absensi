jun.PegawaiSpesialstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PegawaiSpesialstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PegawaiSpesialStoreId',
            url: 'PegawaiSpesial',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pegawai_spesial_id'},
                {name: 'pegawai_id'},
                {name: 'nik'},
                {name: 'nama_lengkap'},
                {name: 'bu_kode'},
                {name: 'nik_lain'},
                {name: 'cabang'},
                {name: 'pin_nama'},
                {name: 'visible'}
            ]
        }, cfg));
    }
});
jun.rztPegawaiSpesial = new jun.PegawaiSpesialstore();
jun.rztPegawaiSpesialCmp = new jun.PegawaiSpesialstore();
//jun.rztPegawaiSpesial.load();
