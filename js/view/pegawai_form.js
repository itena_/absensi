jun.PegawaiWin = Ext.extend(Ext.Window, {
    title: 'Pegawai',
    modez: 1,
    width: 450,
    height: 535,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Pegawai',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "xdatefield",
                        fieldLabel: 'Tgl Masuk',
                        ref: "../tgl_masuk",
                        name: "tgl_masuk",
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        // format: "d M Y",
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Lengkap',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_lengkap',
                        id: 'nama_lengkapid',
                        ref: '../nama_lengkap',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Leveling',
                        store: jun.rztLevelingCmp,
                        hiddenName: 'leveling_id',
                        valueField: 'leveling_id',
                        displayField: 'kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Golongan',
                        store: jun.rztGolonganCmp,
                        hiddenName: 'golongan_id',
                        valueField: 'golongan_id',
                        displayField: 'kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Cabang',
                        store: jun.rztCabangCmp,
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        id: 'cabangidonformpegawai',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Jabatan',
                        store: jun.rztJabatanCmp,
                        hiddenName: 'jabatan_id',
                        valueField: 'jabatan_id',
                        displayField: 'nama_jabatan',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NIK',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nik',
                        id: 'nikid',
                        ref: '../nik',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nik Lokal',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nik_lokal',
                        ref: '../nik_lokal',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email',
                        ref: '../email',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status Pegawai',
                        store: jun.rztStatusPegawaiCmp,
                        hiddenName: 'status_pegawai_id',
                        valueField: 'status_pegawai_id',
                        displayField: 'kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Bank',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bank_nama',
                        ref: '../bank_nama',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kota Bank',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bank_kota',
                        ref: '../bank_kota',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Rekening',
                        hideLabel: false,
                        //hidden:true,
                        name: 'rekening',
                        ref: '../rekening',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        store: jun.rztStatusCmp,
                        hiddenName: 'status_id',
                        valueField: 'status_id',
                        displayField: 'nama',
                        anchor: '100%'
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Create',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tdate',
                        ref: '../tdate',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Last Update',
                        hideLabel: false,
                        //hidden:true,
                        name: 'last_update',
                        ref: '../last_update',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PegawaiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Pegawai/update/id/' + this.id;
        } else {
            urlz = 'Pegawai/create/';
        }
        Ext.getCmp('form-Pegawai').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPegawai.reload();
                jun.rztPegawaiLib.reload();
                jun.rztPegawaiCmp.reload();
                jun.rztPinLib.reload();
                jun.rztPegawaiLib.reload();
                jun.rztAssignShift.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Pegawai').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});

jun.CreateUserWin = Ext.extend(Ext.Window, {
    title: 'Create User',
    modez: 1,
    width: 400,
    height: 150,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-CreateUserWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Lengkap',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_lengkap',
                        id: 'nama_lengkapid',
                        ref: '../nama_lengkap',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Cabang',
                        store: jun.rztCabangCmp,
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        id: 'cabangidonformpegawai',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CreateUserWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        Ext.getCmp('form-CreateUserWin').getForm().submit({
            url: 'Pegawai/CreatePegawaiforUser',
            scope: this,
            params: {
                nama: this.nama_lengkap.getValue().toLowerCase()
            },
            success: function (f, a) {
                jun.rztPegawai.reload();
                jun.rztPegawaiLib.reload();
                jun.rztPegawaiCmp.reload();
                var response = Ext.decode(a.response.responseText);

                Ext.Ajax.request({
                    url: "Users/create/",
                    timeout: 1e3,
                    scope: this,
                    params: {
                        user_id: response.nik,
                        password: jun.EncryptPass(response.nik),
                        active: 1,
                        security_roles_id: '0f3d2a11-6383-11e7-98a7-aac0c493942d',
                        name_: response.nama_lengkap,
                        pegawai_id: response.pegawai_id
                    }
                });

                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-CreateUserWin').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});