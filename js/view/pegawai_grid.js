jun.PegawaiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pegawai",
    id: 'docs-jun.PegawaiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100,
            filter: {xtype: "textfield", filterName: "nik"}
        },
        {
            header: 'Nama Lengkap',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100,
            filter: {xtype: "textfield", filterName: "nama_lengkap"}
        },
        {
            header: 'Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_cabang',
            // renderer: jun.renderCabang,
            width: 100,
            filter: {xtype: "textfield", filterName: "kode_cabang"}
        },
        {
            header: 'Kelompok Pegawai',
            sortable: true,
            resizable: true,
            dataIndex: 'kelompok_pegawai',
            width: 100,
            renderer: function (v, m, r) {
                if(r.get('kelompok_pegawai') == 1){
                    return 'OG,OB atau Security';
                } else return 'Karyawan';
            }
        }
    ],
    initComponent: function () {
        if (jun.rztJabatanCmp.getTotalCount() === 0)
            jun.rztJabatanCmp.load();
        if (jun.rztStatusPegawaiCmp.getTotalCount() === 0)
            jun.rztStatusPegawaiCmp.load();
        if (jun.rztGolonganCmp.getTotalCount() === 0)
            jun.rztGolonganCmp.load();
        if (jun.rztLevelingCmp.getTotalCount() === 0)
            jun.rztLevelingCmp.load();
        if (jun.rztCabangCmp.getTotalCount() === 0)
            jun.rztCabangCmp.load();
        if (jun.rztStatusCmp.getTotalCount() === 0)
            jun.rztStatusCmp.load();
        if (jun.rztUsers.getTotalCount() === 0) {
            jun.rztUsers.load();
        }

        this.store = jun.rztPegawai;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    iconCls: 'silk13-pencil',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete', hidden: true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Toggle kelompok pegawai',
                    ref: '../btnKelompokPegawai'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Akun Cabang',
                    ref: '../btnCreateUser',
                    hidden: true
                },
                '->',
                {
                    xtype: 'button',
                    text: 'Sinkron Data Pegawai',
                    ref: '../btnSoap'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();

        jun.PegawaiGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnKelompokPegawai.on('Click', this.btnKelompokPegawaiClick, this);
        this.btnSoap.on('Click', this.soapClick, this);
        this.btnCreateUser.on('Click', this.loadFormUser, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);

        if(jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD){
            this.btnCreateUser.setVisible(true);
        }
    },
    btnKelompokPegawaiClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Karyawan");
            return;
        }
        Ext.Ajax.request({
            url: 'Pegawai/KelompokPegawai/',
            method: 'POST',
            scope: this,
            params: {
                pegawai_id: selectedz.json.pegawai_id
            },
            success: function (f, a) {
                jun.rztPegawai.reload();
                jun.rztPegawaiCmp.reload();
                jun.rztPegawaiLib.reload();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    soapClick: function () {
        Ext.Ajax.request({
            url: 'Pegawai/sinkron/',
            method: 'POST',
            scope: this,
            timeout: 3000000,
            params: {
                bu_id: jun.bu_id
            },
            success: function (f, a) {
                jun.rztPegawai.reload();
                jun.rztPegawaiCmp.reload();
                jun.rztPegawaiLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: (response.msg == '' ? 'Sinkronisasi berhasil' : response.msg),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PegawaiWin({modez: 0});
        form.show();
    },
    loadFormUser: function () {
        var form = new jun.CreateUserWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        Ext.Ajax.request({
            url: 'Pegawai/checkuser',
            method: 'POST',
            scope: this,
            params: {
                'pegawai_id' : selectedz.json.pegawai_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var idz = selectedz.json.pegawai_id;
                var form = new jun.PegawaiWin({modez: 1, id: idz});
                if (response.msg == true){
                    Ext.getCmp('cabangidonformpegawai').setReadOnly(true);
                }
                form.show(this);
                form.formz.getForm().loadRecord(this.record);
                form.tgl_masuk.setReadOnly(true);
                form.nama_lengkap.setReadOnly(true);
                form.nik.setReadOnly(true);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Pegawai/delete/id/' + record.json.pegawai_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPegawai.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

jun.PegawaiReportGrid = Ext.extend(Ext.grid.EditorGridPanel, {
    title: "PegawaiReport",
    id: 'docs-jun.PegawaiReportGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    clicksToEdit: 1,
    columns: [
        {
            header: 'NIK',
            sortable: false,
            resizable: true,
            dataIndex: 'nik',
            width: 50
        },
        {
            header: 'Nama',
            sortable: false,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100
        },
        {
            xtype: 'checkcolumn',
            header: 'Check',
            dataIndex: 'count',
            width: 55
        }
    ],
    initComponent: function () {
        this.store = jun.PegawaiReport;
        this.store.baseParams = {cabang_id: jun.usercabang};
        this.store.reload();
        this.store.baseParams = {};
        jun.PegawaiReportGrid.superclass.initComponent.call(this);
    }
});
