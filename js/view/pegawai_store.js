jun.Pegawaistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Pegawaistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PegawaiStoreId',
            url: 'Pegawai',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pegawai_id'},
                {name: 'nik'},
                {name: 'golongan_id'},
                {name: 'leveling_id'},
                {name: 'cabang_id'},
                {name: 'kode_cabang'},
                {name: 'status_id'},
                {name: 'nama_lengkap'},
                {name: 'email'},
                {name: 'tgl_masuk'},
                {name: 'nik_lokal'},
                {name: 'kode'},
                {name: 'bank_nama'},
                {name: 'bank_kota'},
                {name: 'rekening'},
                {name: 'status_pegawai_id'},
                {name: 'jabatan_id'},
                {name: 'tdate'},
                {name: 'tuser'},
                {name: 'last_update'},
                {name: 'last_update_id'},
                {name: 'kelompok_pegawai'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    },
    checkAll: function(store, column, checked){
        for(var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('count', true);
        }
    },
    uncheckAll: function(store, column, checked){
        for(var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('count', false);
        }
    }
});
jun.rztPegawai = new jun.Pegawaistore({
    baseParams: {mode: "grid"}
});
jun.rztPegawaiCmp = new jun.Pegawaistore();
jun.rztPegawaiLib = new jun.Pegawaistore();
jun.rztPegawaiAll = new jun.Pegawaistore({
    baseParams: {all_pegawai: true}
});
jun.rztPegawaiNonAktif = new jun.Pegawaistore({
    baseParams: {status_pegawai: "nonAktif"}
});
jun.PegawaiReport = new jun.Pegawaistore();
