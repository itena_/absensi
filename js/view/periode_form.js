jun.PeriodeWin = Ext.extend(Ext.Window, {
    title: 'Periode',
    modez: 1,
    width: 450,
    height: 230 + 60,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background: none; padding: 10px',
                id: 'form-Periode',
                labelWidth: 80,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Jenis Periode',
                        store: jun.rztJenisPeriodeCmp,
                        hiddenName: 'jenis_periode_id',
                        valueField: 'jenis_periode_id',
                        displayField: 'nama_jenis',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode',
                        allowBlank: false,
                        name: 'kode_periode',
                        maxLength: 45,
                        anchor: '100%',
                        emptyText: 'Bulan(spasi)Tahun ex: Januari 2017'
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Mulai',
                        name: 'periode_start',
                        ref: '../periode_start',
                        format: 'Y-m-d H:i:s',
                        allowBlank: false,
                        anchor: '100%',
                        value: this.modez == 1 ? this.periode_start : ''
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Berakhir',
                        name: 'periode_end',
                        ref: '../periode_end',
                        format: 'Y-m-d 23:59:59',
                        allowBlank: false,
                        anchor: '100%',
                        value: this.modez == 1 ? this.periode_end : ''
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tgl libur',
                        ref: '../tgl_libur',
                        readOnly: true,
                        format: 'Y-m-d',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Libur',
                        readOnly: true,
                        allowBlank: false,
                        name: 'libur',
                        ref: '../libur',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Jumlah Off',
                        name: 'jumlah_off',
                        ref: '../jumlah_off',
                        value: 0,
                        readOnly: true,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'button',
                        text: 'Reset Libur',
                        ref: '../btnResetLibur'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PeriodeWin.superclass.initComponent.call(this);
        this.periode_start.on('select', this.onPeriodeSelect, this);
        this.periode_end.on('select', this.onPeriodeSelect, this);
        this.tgl_libur.on('select', this.ontgl_liburSelect, this);
        this.btnResetLibur.on('click', this.onbtnResetLiburClick, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);

        this.btnCancel.setText(this.modez < 2? 'Batal':'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if(this.modez == 1) { //edit
            this.btnSave.setVisible(false);
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({ readOnly: true });
        }
    },
    onPeriodeSelect: function () {
        if(this.periode_start.getValue() !== '' && this.periode_end.getValue() != '') {
            this.tgl_libur.setReadOnly(false);
        }
    },
    ontgl_liburSelect: function (c) {
        var tgl_libur  = renderDateYMD(c.getValue());
        if(tgl_libur < renderDateYMD(this.periode_start.getValue()) || tgl_libur > renderDateYMD(this.periode_end.getValue())) {
            Ext.MessageBox.alert("Warning", "Tanggal libur harus berada di dalam periode!");
            return;
        }

        var jumlah_off = this.jumlah_off.getValue();

        if(this.libur.getValue() == '')
            this.libur.setValue(tgl_libur);
        else
            this.libur.setValue(tgl_libur + ';' + this.libur.getValue());

        jumlah_off++;
        this.jumlah_off.setValue(jumlah_off);
    },
    onbtnResetLiburClick: function () {
        this.libur.setValue('');
        this.tgl_libur.setValue('');
        this.jumlah_off.setValue(0);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 0) {
            urlz = 'Periode/create/';
        } else if (this.modez == 1) {
            urlz = 'Periode/update/id/' + this.id;
        }
        this.formz.getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPeriode.reload();
                jun.rztPeriodeLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});