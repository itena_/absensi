jun.PeriodeGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Periode",
    id: 'docs-jun.PeriodeGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Periode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_periode',
            width: 100
        },
        {
            header: 'Mulai',
            sortable: true,
            resizable: true,
            dataIndex: 'periode_start',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d-M-Y H:i:s')
        },
        {
            header: 'Berakhir',
            sortable: true,
            resizable: true,
            dataIndex: 'periode_end',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d-M-Y H:i:s')
        },
        {
            header: 'Jumlah Off',
            sortable: true,
            resizable: true,
            dataIndex: 'jumlah_off',
            width: 100
        }
    ],
    userPermission: function (actCreate, actEdit, actDelete) {
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        jun.rztJenisPeriodeCmp.load();
        this.store = jun.rztPeriode;
        //switch(UROLE){
        //    default:
        this.userPermission(1, 1, 1);
        //}
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: this.actEdit ? 'Ubah' : 'Lihat',
                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete',
                //     iconCls: 'silk13-delete',
                //     hidden: this.actDelete ? false : true
                // },
                // '->',
                {
                    xtype: 'button',
                    text: '<b>Kode Periode</b>',
                    ref: '../btnFind',
                    iconCls: 'silk13-find',
                    fieldsearch: 'kode_periode',
                    menu: {
                        xtype: 'menu',
                        items: [
                            {
                                text: 'Kode Periode',
                                listeners: {
                                    click: function (m, e) {
                                        m.parentMenu.ownerCt.setText('<b>' + m.text + '</b>');
                                        m.parentMenu.ownerCt.fieldsearch = 'kode_periode';
                                        Ext.getCmp('docs-jun.PeriodeGrid').txtFind.focus().setValue("");
                                    }
                                }
                            }
                        ]
                    }
                },
                {
                    xtype: 'textfield',
                    ref: '../txtFind',
                    emptyText: 'Teks pencarian ...',
                    enableKeyEvents: true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    text: 'Jika periode tidak tersedia, minta HRD untuk membuatnya.'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        jun.PeriodeGrid.superclass.initComponent.call(this);
        this.btnAdd.on('click', this.loadForm, this);
        this.btnEdit.on('click', this.loadEditForm, this);
        // this.btnDelete.on('click', this.deleteRec, this);
        this.txtFind.on('keyup', this.find, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    setFilterData: function (n) {
        if (parseInt(this.store.baseParams.tipe_periode_id) == n) {
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.doRefresh();
        } else {
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.moveFirst();
        }
    },
    find: function (c, e) {
        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
            this.fieldsearch = this.btnFind.fieldsearch;
            this.valuesearch = c.getValue();
            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
            this.botbar.moveFirst();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PeriodeWin({
            modez: 0,
            title: "Buat Data Periode",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Periode");
            return;
        }
        var idz = selectedz.json.periode_id;
        var form = new jun.PeriodeWin({
            modez: 1,
            id: idz,
            title: (this.actEdit ? "Ubah" : "Lihat") + " Data Periode : " + selectedz.json.kode_periode,
            iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
            periode_start: selectedz.json.periode_start,
            periode_end: selectedz.json.periode_end
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data Periode");
            return;
        }
        Ext.MessageBox.confirm(
            'Hapus Data Periode',
            "Apakah anda yakin ingin menghapus data Periode ini?<br><br>Kode periode : " + record.json.kode_periode,
            this.deleteRecYes,
            this
        );
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Periode/delete/id/' + record.json.periode_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPeriode.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
