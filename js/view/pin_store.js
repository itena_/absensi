jun.PinStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PinStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PinStoreId',
            url: 'ShiftPin/GetPin',
            root: 'results',
            // autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'pegawai_id'},
                {name: 'pin_id'},
                {name: 'cabang_id'},
                {name: 'PIN'},
                {name: 'nama_lengkap'},
                {name: 'pin_nama'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});

jun.rztPinLib = new jun.PinStore();
jun.rztNikAll = new jun.PinStore({baseParams: {allpegawai: true}});
jun.rztNikAllGrid = new jun.PinStore({baseParams: {allpegawai: true}});