jun.ReportFp = Ext.extend(Ext.Window, {
    title: "Report Finger Print",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
//    height: 140,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportFp",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 175,
                        anchor: "100%",
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        mode: 'remote',
                        triggerAction: 'query',
                        autoSelect: false,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        listWidth: 450,
                        lastQuery: "",
                        lazyRender: true,
                        forceSelection: true,
                        store: jun.rztPegawaiLib,
                        valueField: 'pegawai_id',
                        hiddenName: 'pegawai_id',
                        fieldLabel: 'NIK',
                        displayField: 'nik',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{nik}</b><br>{nama_lengkap}</span>',
                            "</div></tpl>"),
                        ref: '../pegawai',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: (jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD) ? jun.rztCabangCmp : jun.rztCabangUser,
                        emptyText: 'Tampilkan dari semua cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "bu_id",
                        value: jun.bu_id,
                        ref: "../bu_id"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportFp.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportFp").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFp").getForm().url = "Report/Fp";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportFp').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportFp").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFp").getForm().url = "Report/Fp";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportFp').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportFpReal = Ext.extend(Ext.Window, {
    title: "Report Finger Print Real",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportFpReal",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 175,
                        anchor: "100%",
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        mode: 'remote',
                        triggerAction: 'query',
                        autoSelect: false,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        listWidth: 450,
                        lastQuery: "",
                        lazyRender: true,
                        forceSelection: true,
                        store: jun.rztPegawaiLib,
                        valueField: 'pegawai_id',
                        hiddenName: 'pegawai_id',
                        displayField: 'nik',
                        fieldLabel: 'NIK',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{nik}</b><br>{nama_lengkap}</span>',
                            "</div></tpl>"),
                        ref: '../pegawai',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
                        ref: '../store',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportFpReal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        if (this.store.getValue() == '') {
            Ext.MessageBox.alert("Warning", "Cabang tidak boleh kosong.");
            return;
        }
        Ext.getCmp("form-ReportFpReal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFpReal").getForm().url = "Report/FpReal";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportFpReal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        if (this.store.getValue() == '') {
            Ext.MessageBox.alert("Warning", "Cabang tidak boleh kosong.");
            return;
        }
        Ext.getCmp("form-ReportFpReal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFpReal").getForm().url = "Report/FpReal";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportFpReal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportAbsen = Ext.extend(Ext.Window, {
    title: "Report Presensi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: (jun.securityroles == SECURITY_ROLE_HRD) ? 360 : 220,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAbsen",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../awal',
                        fieldLabel: 'Awal',
                        name: 'awal',
                        id: 'awalid',
                        anchor: "100%",
                        allowBlank: true,
                        hidden: (jun.securityroles == SECURITY_ROLE_HRD) ? false : true,
                        format: 'Y-m-d'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../akhir',
                        fieldLabel: 'Akhir',
                        name: 'akhir',
                        id: 'akhirid',
                        anchor: "100%",
                        allowBlank: true,
                        hidden: (jun.securityroles == SECURITY_ROLE_HRD) ? false : true,
                        format: 'Y-m-d'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: (jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD) ? jun.rztCabangCmp : jun.rztCabangUser,
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Per Lokasi Presensi',
                        name: 'show_detail',
                        checked: true,
                        ref: '../show_detail'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Urutkan Jabatan',
                        name: 'urut_jabatan',
                        ref: '../urut_jabatan'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Jatah Off',
                        name: 'jatah_off',
                        ref: '../jatah_off',
                        hidden: true
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAbsen.superclass.initComponent.call(this);
        this.periode_id.on('select', this.onPeriodeSelect, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onPeriodeSelect: function(combo, record, index) {
        var awal = record.data.periode_start.split(' ');
        var akhir = record.data.periode_end.split(' ');

        this.awal.setValue(awal[0]);
        this.akhir.setValue(akhir[0]);
    },
    onbtnPdfclick: function () {
        if (!this.cabang.getValue()) {
            Ext.Msg.alert('Error', "Cabang belum dipilih!");
            return;
        }
        Ext.getCmp("form-ReportAbsen").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsen").getForm().url = "Report/Absen";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAbsen').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        if (!this.cabang.getValue()) {
            Ext.Msg.alert('Error', "Cabang belum dipilih!");
            return;
        }
        Ext.getCmp("form-ReportAbsen").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsen").getForm().url = "Report/Absen";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportAbsen').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportFpMix = Ext.extend(Ext.Window, {
    title: "Report Finger Print Mix",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportFpMix",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 175,
                        anchor: "100%",
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        mode: 'remote',
                        triggerAction: 'query',
                        autoSelect: false,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        listWidth: 450,
                        lastQuery: "",
                        lazyRender: true,
                        forceSelection: true,
                        store: jun.rztPegawaiLib,
                        valueField: 'pegawai_id',
                        hiddenName: 'pegawai_id',
                        displayField: 'nik',
                        fieldLabel: 'NIK',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{nik}</b><br>{nama_lengkap}</span>',
                            "</div></tpl>"),
                        ref: '../pegawai',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
                        ref: '../store',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "bu_id",
                        value: jun.bu_id,
                        ref: "../bu_id"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportFpMix.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        if (this.store.getValue() == '') {
            Ext.MessageBox.alert("Warning", "Cabang tidak boleh kosong.");
            return;
        }
        Ext.getCmp("form-ReportFpMix").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFpMix").getForm().url = "Report/FpMix";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportFpMix').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        if (this.store.getValue() == '') {
            Ext.MessageBox.alert("Warning", "Cabang tidak boleh kosong.");
            return;
        }
        Ext.getCmp("form-ReportFpMix").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFpMix").getForm().url = "Report/FpMix";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportFpMix').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.ReportShift = Ext.extend(Ext.Window, {
    title: "Report Shift",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 120 + 30,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportShift",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: (jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD) ? jun.rztCabangCmp : jun.rztCabangUser,
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan yang aktif saja',
                        name: 'aktif',
                        ref: '../aktif'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportShift.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        if (!this.cabang.getValue()) {
            Ext.Msg.alert('Error', "Cabang belum dipilih!");
            return;
        }
        Ext.getCmp("form-ReportShift").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportShift").getForm().url = "Report/Shift";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportShift').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        if (!this.cabang.getValue()) {
            Ext.Msg.alert('Error', "Cabang belum dipilih!");
            return;
        }
        Ext.getCmp("form-ReportShift").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportShift").getForm().url = "Report/Shift";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportShift').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.ReportAbsenjam = Ext.extend(Ext.Window, {
    title: "Report Absen (Jam)",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAbsenjam",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
//                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: (jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD) ? jun.rztCabangCmp : jun.rztCabangUser,
//                        value: cabang_value == 1 ? 'cabang_id' : '',
//                        emptyText: 'Tampilkan dari semua cabang',
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Per Lokasi Absen',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Urutkan Jabatan',
                        name: 'urut_jabatan',
                        ref: '../urut_jabatan'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAbsenjam.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        if (!this.cabang.getValue()) {
            Ext.Msg.alert('Error', "Cabang belum dipilih!");
            return;
        }
        Ext.getCmp("form-ReportAbsenjam").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsenjam").getForm().url = "Report/Absenjam";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAbsenjam').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        if (!this.cabang.getValue()) {
            Ext.Msg.alert('Error', "Cabang belum dipilih!");
            return;
        }
        Ext.getCmp("form-ReportAbsenjam").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsenjam").getForm().url = "Report/Absenjam";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportAbsenjam').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportAbsenAll = Ext.extend(Ext.Window, {
    title: "Report Presensi All",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAbsenAll",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAbsenAll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        // if (!this.cabang.getValue()) {
        //     Ext.Msg.alert('Error', "Cabang belum dipilih!");
        //     return;
        // }
        Ext.getCmp("form-ReportAbsenAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsenAll").getForm().url = "Report/AbsenAll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAbsenAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        // if (!this.cabang.getValue()) {
        //     Ext.Msg.alert('Error', "Cabang belum dipilih!");
        //     return;
        // }
        Ext.getCmp("form-ReportAbsenAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsenAll").getForm().url = "Report/AbsenAll";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportAbsenAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapAbsensi = Ext.extend(Ext.Window, {
    title: "Rekap Presensi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapAbsensi",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeLib,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapAbsensi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapAbsensi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapAbsensi").getForm().url = "Report/RekapAbsensi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapAbsensi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportRekapAbsensi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapAbsensi").getForm().url = "Report/RekapAbsensi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportRekapAbsensi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.RecalculatedAllLemburGrid = Ext.extend(Ext.Window, {
    title: "Recalculate All Lembur & LessTime",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150 + 30,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-RecalculatedAllLemburGrid",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: 'button',
                        fieldLabel: 'Lepas Limit Lembur?',
                        text: 'TIDAK',
                        scale: 'medium',
                        name: 'limitlembur',
                        ref: '../btnToggleLimit'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
//                    iconCls: "silk13-page_white_excel",
                    text: "Recalculate",
                    ref: "../btnRecalculate"
                }
            ]
        };
        jun.RecalculatedAllLemburGrid.superclass.initComponent.call(this);
        this.btnRecalculate.on("click", this.onbtnRecalculateclick, this);
        this.btnToggleLimit.on('Click', this.btnToggleLimitClick, this);
    },
    btnToggleLimitClick: function (t) {
        if (t.getText() === 'TIDAK') {
            t.setText('YA');
        } else {
            t.setText('TIDAK');
        }
    },
    onbtnRecalculateclick: function () {
         Ext.Ajax.request({
            url: 'Validasi/RecalculateLemburAll',
            method: 'POST',
            scope: this,
            params: {
                periode_id: this.periode_id.getValue(),
                cabang: this.cabang.getValue(),
                limitLembur: this.btnToggleLimit.getText()
            },
            success: function (f, a) { 
                var response = Ext.decode(f.responseText); 
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    } ,
});

jun.ReportFpValidasi = Ext.extend(Ext.Window, {
    title: "Report Finger Print After Validasi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
//    height: 140,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportFpValidasi",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
//                    {
//                        xtype: "label",
//                        text: "Periode:",
//                        x: 5,
//                        y: 5
//                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 175,
                        anchor: "100%",
//                        x: 70,
//                        y: 2,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
//                    {
//                        xtype: 'combo',
//                        typeAhead: true,
//                        fieldLabel: 'Pegawai',
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'local',
//                        forceSelection: true,
//                        allowBlank: true,
//                        emptyText: 'ALL',
//                        store: jun.rztPegawaiLib,
//                        hiddenName: 'pegawai_id',
//                        valueField: 'pegawai_id',
//                        ref: '../pegawai',
//                        displayField: 'nik',
//                        anchor: '100%'
//                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        mode: 'remote',
                        triggerAction: 'query',
                        autoSelect: false,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        listWidth: 450,
                        lastQuery: "",
                        lazyRender: true,
                        forceSelection: true,
                        store: jun.rztPegawaiLib,
                        emptyText: 'All',
                        hiddenName: 'pegawai_id',
                        valueField: 'pegawai_id',
                        fieldLabel: 'NIK',
                        displayField: 'pin_nama',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{PIN}</b><br>{nama_lengkap}</span>',
                            "</div></tpl>"),
                        ref: '../pegawai',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
//                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        emptyText: 'All Cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportFpValidasi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportFpValidasi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFpValidasi").getForm().url = "Report/FpValidasi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportFpValidasi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportFpValidasi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFpValidasi").getForm().url = "Report/FpValidasi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportFpValidasi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.ReportKpi  = Ext.extend(Ext.Window, {
    title: "Report KPI",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
//    height: 140,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportKpi",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
//                    {
//                        xtype: "label",
//                        text: "Periode:",
//                        x: 5,
//                        y: 5
//                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },


                    //--menggunakan periode cut off--
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        hidden: true, // untuk menyembunyikan periode cut off
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 175,
                        anchor: "100%",
//                        x: 70,
//                        y: 2,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")

                    },
//                    {
//                        xtype: 'combo',
//                        typeAhead: true,
//                        fieldLabel: 'Pegawai',
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'local',
//                        forceSelection: true,
//                        allowBlank: true,
//                        emptyText: 'ALL',
//                        store: jun.rztPegawaiLib,
//                        hiddenName: 'pegawai_id',
//                        valueField: 'pegawai_id',
//                        ref: '../pegawai',
//                        displayField: 'nik',
//                        anchor: '100%'
//                    },
//                     {
//                         xtype: 'combo',
//                         style: 'margin-bottom:2px',
//                         mode: 'remote',
//                         triggerAction: 'query',
//                         autoSelect: false,
//                         minChars: 3,
//                         matchFieldWidth: !1,
//                         pageSize: 20,
//                         listWidth: 450,
//                         lastQuery: "",
//                         lazyRender: true,
//                         forceSelection: true,
//                         store: jun.rztPegawaiLib,
//                         hiddenName: 'pegawai_id',
//                         valueField: 'pegawai_id',
//                         fieldLabel: 'NIK',
//                         displayField: 'pin_nama',
//                         itemSelector: "div.search-item",
//                         tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
//                             '<span style="width:100%;display:inline-block;"><b>{PIN}</b><br>{nama_lengkap}</span>',
//                             "</div></tpl>"),
//                         ref: '../pegawai',
//                         anchor: '100%'
//                     },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
//                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        emptyText: 'Tampilkan dari semua cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportKpi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportKpi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKpi").getForm().url = "Report/Kpi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportKpi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportKpi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKpi").getForm().url = "Report/Kpi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportKpi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapPegawai = Ext.extend(Ext.Window, {
    title: "Report Rekap Pegawai",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
    height: 110,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapPegawai",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapPegawai.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapPegawai").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPegawai").getForm().url = "Report/PrintRekapPegawai";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapPegawai').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportRekapPegawai").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPegawai").getForm().url = "Report/PrintRekapPegawai";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportRekapPegawai').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LockPayroll = Ext.extend(Ext.Window, {
    title: "Lock Payroll",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-LockPayroll",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    // iconCls: "silk13-page_white_excel",
                    text: "Lock Payroll",
                    ref: "../btnSave"
                }
            ]
        };
        jun.LockPayroll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        // this.btnDisabled(true);
        var urlz = 'Payroll/lock';
        Ext.getCmp('form-LockPayroll').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                // this.btnDisabled(false);
            }
        });
    }
});
jun.ExportShift = Ext.extend(Ext.Window, {
    title: "Export Presensi Shift",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ExportShift",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeLib,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ExportShift.superclass.initComponent.call(this);
        // this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ExportShift").getForm().standardSubmit = !0;
        Ext.getCmp("form-ExportShift").getForm().url = "Report/ExportShift";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ExportShift').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ExportShift").getForm().standardSubmit = !0;
        Ext.getCmp("form-ExportShift").getForm().url = "Report/ExportShift";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ExportShift').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.ReportMasterGaji = Ext.extend(Ext.Window, {
    title: "Master Gaji",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
    height: 110,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportMasterGaji",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportMasterGaji.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportMasterGaji").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportMasterGaji").getForm().url = "Report/PrintMasterGaji";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportMasterGaji').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportMasterGaji").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportMasterGaji").getForm().url = "Report/PrintMasterGaji";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportMasterGaji').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapPayroll = Ext.extend(Ext.Window, {
    title: "Rekap Payroll",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapPayroll",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    },
                    // {
                    //     xtype: 'combo',
                    //     style: 'margin-bottom:2px',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     fieldLabel: 'Periode',
                    //     forceSelection: true,
                    //     store: jun.rztPeriodeCmp, //RUMUS deklarasi store langsung di combo
                    //     hiddenName: 'periode_id',
                    //     valueField: 'periode_id',
                    //     emptyText: 'Pilih Periode',
                    //     ref: '../periode_id',
                    //     displayField: 'kode_periode',
                    //     // width: 175,
                    //     itemSelector: "div.search-item",
                    //     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                    //         '<div style="width:100%;display:inline-block;">\n\
                    //             <span style="font-weight: bold">{kode_periode}</span>\n\
                    //             <br>\n\{periode_start} - {periode_end}\n\
                    //             </div>',
                    //         "</div></tpl>")
                    // },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapPayroll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapPayroll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPayroll").getForm().url = "Report/PrintRekapPayroll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapPayroll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportRekapPayroll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPayroll").getForm().url = "Report/PrintRekapPayroll";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportRekapPayroll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.TimeSheetAX = Ext.extend(Ext.Window, {
    title: "Time Sheet AX",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-TimeSheetAX",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeLib,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: (jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD) ? jun.rztCabangCmp : jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapAbsensi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-TimeSheetAX").getForm().standardSubmit = !0;
        Ext.getCmp("form-TimeSheetAX").getForm().url = "Report/TimeSheetAX";
        this.format.setValue("html");
        var form = Ext.getCmp('form-TimeSheetAX').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-TimeSheetAX").getForm().standardSubmit = !0;
        Ext.getCmp("form-TimeSheetAX").getForm().url = "Report/TimeSheetAX";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-TimeSheetAX').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.ReportLessTime = Ext.extend(Ext.Window, {
    title: "Rekap Less Time",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    namafungsi: "ReportLessTime",
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-" + this.namafungsi,
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Hanya tampilkan data yang lebih dari 0',
                        name: 'show_active',
                        ref: '../show_active'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportLessTime.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-" + this.namafungsi).getForm().standardSubmit = !0;
        Ext.getCmp("form-" + this.namafungsi).getForm().url = "Report/" + this.namafungsi;
        this.format.setValue("html");
        var form = Ext.getCmp("form-" + this.namafungsi).getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-" + this.namafungsi).getForm().standardSubmit = !0;
        Ext.getCmp("form-" + this.namafungsi).getForm().url = "Report/" + this.namafungsi;
        this.format.setValue("excel");
        var form = Ext.getCmp("form-" + this.namafungsi).getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportLupaAbsen = Ext.extend(Ext.Window, {
    title: "Rekap Lupa Absen",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    namafungsi: "ReportLupaAbsen",
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
//        this.namafungsi = 'ReportLessTime';
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-" + this.namafungsi,
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportLupaAbsen.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-" + this.namafungsi).getForm().standardSubmit = !0;
        Ext.getCmp("form-" + this.namafungsi).getForm().url = "Report/" + this.namafungsi;
        this.format.setValue("html");
        var form = Ext.getCmp("form-" + this.namafungsi).getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-" + this.namafungsi).getForm().standardSubmit = !0;
        Ext.getCmp("form-" + this.namafungsi).getForm().url = "Report/" + this.namafungsi;
        this.format.setValue("excel");
        var form = Ext.getCmp("form-" + this.namafungsi).getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapCuti = Ext.extend(Ext.Window, {
    title: "Rekap Presensi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapCuti",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Tahun',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tahun_cuti',
                        id: 'tahun_cutiid',
                        ref: '../tahun_cuti',
                        maxLength: 45,
                        allowBlank: false,
                        emptyText: 'ex: 2020',
                        regex: /^\d{4}$/i,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapCuti.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapCuti").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapCuti").getForm().url = "Report/Cuti";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapCuti').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportRekapCuti").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapCuti").getForm().url = "Report/Cuti";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportRekapCuti').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.CheckAbsensi = Ext.extend(Ext.Window, {
    title: "Check Absensi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-CheckAbsensi",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        // typeAhead: true,
                        triggerAction: 'all',
//                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: cabang_value == 1 ? 'cabang_id' : '',
//                        emptyText: 'Tampilkan dari semua cabang',
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
//                    {
//                        xtype: 'checkbox',
//                        fieldLabel: "",
//                        boxLabel: 'Tampilkan Per Lokasi Absen',
//                        name: 'show_detail',
//                        ref: '../show_detail'
//                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_excel",
//                    text: "Save to Excel",
//                    ref: "../btnSave"
//                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.CheckAbsensi.superclass.initComponent.call(this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        if (!this.cabang.getValue()) {
            Ext.Msg.alert('Error', "Cabang belum dipilih!");
            return;
        }
        Ext.getCmp("form-CheckAbsensi").getForm().standardSubmit = !0;
        Ext.getCmp("form-CheckAbsensi").getForm().url = "Report/CheckAbsensi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-CheckAbsensi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.InfoWin = Ext.extend(Ext.Window, {
    title: 'Info',
    note: '',
    modez: 1,
    width: 400,
    height: 150,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-InfoWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'label',
                        text: this.note
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tutup',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.InfoWin.superclass.initComponent.call(this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    onbtnCancelclick: function () {
        if(this.btnCancel.getText() === 'Copy & Tutup') {
            var copyText = this.note;
            navigator.clipboard.writeText(copyText);
            alert("Link berhasil di Copy. Silahkan Paste di Google Chrome.");
        }
        this.close();
    }
});

jun.AMOSAttendance = Ext.extend(Ext.Window, {
    title: "AMOS Attendance",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-AMOSAttendance",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NIK',
                        ref: '../pegawai',
                        maxLength: 6,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Presensi",
                    ref: "../btnPresensi"
                }
            ]
        };
        jun.AMOSAttendance.superclass.initComponent.call(this);
        this.btnPresensi.on("click", this.onbtnPresensiclick, this);
    },
    onbtnPresensiclick: function () {
        Ext.Ajax.request({
            url: 'Fp/SaveFpFromBeta',
            method: 'POST',
            scope: this,
            params: {
                pegawai: this.pegawai.getValue()
            },
            success: function (f, a) {
                console.log(f);
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});