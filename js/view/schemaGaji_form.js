var codeMirrorMinimalForm;
jun.SchemaGajiWin = Ext.extend(Ext.Window, {
    title: 'Schema Gaji',
    modez: 1,
    width: 1000,
    height: 600,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-SchemaGaji',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bisnis Unit',
                        store: jun.rztBuCmp,
                        hiddenName: 'bu_id',
                        valueField: 'bu_id',
                        displayField: 'bu_name',
                        anchor: '40%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Schema',
                        store: jun.rztSchemaCmp,
                        hiddenName: 'schema_id',
                        valueField: 'schema_id',
                        displayField: 'nama_skema',
                        ref: '../schema',
                        anchor: '40%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztStatusCmp,
                        valueField: 'status_id',
                        hiddenName: 'status_id',
                        displayField: 'nama',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{kode}</b> : {nama}</span>',
                            "</div></tpl>"),
                        ref: '../status',
                        anchor: '30%'
                    },
                    codeMirrorMinimalForm = new Ext.ux.form.CodeMirror({
                        codeMirrorPath: codeMirrorPath,
                        mode: "php",
                        codeMirrorConfig: {
                            lineNumbers: true,
                            matchBrackets: true,
                            foldGutter: true,
                            indentUnit: 4,
                            indentWithTabs: true
                        },
                        height: 455,
                        //hidden:true,
                        name: 'formula',
                        id: 'formulaid',
                        ref: '../formula',
                        fieldLabel: "Formula",
                        value: '<?php \n$hasilFormula__ = 0;',
                        anchor: '100%'
                    })//,
                    // {
                    //     xtype: 'textarea',
                    //     fieldLabel: 'Formula',
                    //     hideLabel: false,
                    //     height: 455,
                    //     //hidden:true,
                    //     name: 'formula',
                    //     id: 'formulaid',
                    //     ref: '../formula',
                    //     maxLength: 100,
                    //     value: '$hasilFormula__ = 0;',
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SchemaGajiWin.superclass.initComponent.call(this);
        // this.on('show', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    activate: function () {
        var editor = CodeMirror.fromTextArea(document.getElementById("formulaid"), {
            lineNumbers: true,
            matchBrackets: true,
            mode: "application/x-httpd-php",
            indentUnit: 4,
            indentWithTabs: true
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'SchemaGaji/update/id/' + this.id;
        } else {
            urlz = 'SchemaGaji/create/';
        }
        Ext.Ajax.request({
            url: urlz,
            method: 'POST',
            scope: this,
            params: {
                status_id: this.status.getValue(),
                schema_id: this.schema.getValue(),
                'formula': this.formula.getValue().replace('<?php\n', '').replace('<?php', '').replace('<?', '').replace('?>', '')
            },
            success: function (f, a) {
                jun.rztSchemaGaji.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-SchemaGaji').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
        // Ext.getCmp('form-SchemaGaji').getForm().submit({
        //     url: urlz,
        //     timeout: 1000,
        //     scope: this,
        //     success: function (f, a) {
        //         jun.rztSchemaGaji.reload();
        //         var response = Ext.decode(a.response.responseText);
        //         Ext.MessageBox.show({
        //             title: 'Info',
        //             msg: response.msg,
        //             buttons: Ext.MessageBox.OK,
        //             icon: Ext.MessageBox.INFO
        //         });
        //         if (this.modez == 0) {
        //             Ext.getCmp('form-SchemaGaji').getForm().reset();
        //             this.btnDisabled(false);
        //         }
        //         if (this.closeForm) {
        //             this.close();
        //         }
        //     },
        //     failure: function (f, a) {
        //         switch (a.failureType) {
        //             // case Ext.form.Action.CLIENT_INVALID:
        //             //     Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //             //     break;
        //             case Ext.form.Action.CONNECT_FAILURE:
        //                 Ext.Msg.alert('Failure', 'Schema tidak bisa disimpan karena formula salah.');
        //                 break;
        //             case Ext.form.Action.SERVER_INVALID:
        //                 Ext.Msg.alert('Failure', a.result.msg);
        //         }
        //         this.btnDisabled(false);
        //     }
        // });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});