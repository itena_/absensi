jun.Schemastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Schemastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SchemaStoreId',
            url: 'Schema',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'schema_id'},
                {name: 'nama_skema'},
                {name: 'type_'},
                {name: 'seq'},
                {name: 'bu_id'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztSchema = new jun.Schemastore();
jun.rztSchemaCmp = new jun.Schemastore();
jun.rztSchemaLib = new jun.Schemastore();
jun.rztSchemaLib.load();
