jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 800,
    height: 500,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout: "column",
                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Shift",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "101"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "IP",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "102"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Keterangan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "103"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Template",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "106"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Jenis Periode",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "129"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Periode",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Area",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "115"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "116"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Leveling",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "117"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Golongan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "118"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Komponen Gaji",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "119"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Master Gaji",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "120"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Schema",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "127"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Schema Gaji",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "121"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status Pegawai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "123"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Jabatan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "124"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Cabang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "125"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Pegawai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "122"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Bisnis Unit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "128"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Pegawai Spesial",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "130"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status HK",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "126"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Assign Shift",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "224"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Review Absensi Fase 1",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "223"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Review Absensi Fase 2",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "225"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Review Off",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "226"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Absensi Payroll",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "227"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Lock Payroll",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "228"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Recalculate All Lembur",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "229"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Transfer Pegawai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "230"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Lock & Post",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "231"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "List Import Data",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "232"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status HK Per Periode",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "234"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: 'Report FP',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "301"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Rekap Pegawai',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Master Gaji',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "305"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Rekap Payroll',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Rekap Absensi',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "304"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Less Time & Lupa Absen',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "306"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Shift',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "307"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Absen All & Time Sheet AX',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "308"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Rekap Cuti',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "309"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report KPI',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "310"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Absen',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "311"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Absen (Jam)',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "312"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Finger Print After Validasi',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "313"
                            }

                        ]
                    },
                    {
                        xtype: "panel",
                        title: "AMOS Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "AMOS Smart Attendance Prediction",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "501"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "RESET ABSENSI",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "404"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "002"
                            }
                        ]
                    }
                    // new jun.CabangBuGrid({
                    //     modez: this.modez == 0 ? 0 : this.id
                    //     // frameHeader: !1,
                    //     // header: !1
                    // }),
                    // new jun.LevelingSRGrid({
                    //     modez: this.modez == 0 ? 0 : this.id
                    //     // frameHeader: !1,
                    //     // header: !1
                    // })
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/";
        Ext.getCmp("form-SecurityRoles").getForm().submit({
            url: a,
            timeout: 1e3,
            scope: this,
            params: {
                cabang: Ext.encode(Ext.pluck(
                    jun.rztCabangBu.data.items, "data")),
                level: Ext.encode(Ext.pluck(
                    jun.rztLevelingSR.data.items, "data"))
            },
            success: function (a, b) {
                jun.rztSecurityRoles.reload();
                var c = Ext.decode(b.response.responseText);
                this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
            },
            failure: function (a, b) {
                Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                this.btnDisabled(!1);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});