jun.SecurityRolesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SecurityRolesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SecurityRolesStoreId',
            url: 'SecurityRoles',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'security_roles_id'},
                {name: 'role'},
                {name: 'ket'},
                {name: 'sections'},
                {name: '001'},
                {name: '002'},
                {name: '101'},
                {name: '102'},
                {name: '103'},
                {name: '104'},
                {name: '105'},
                {name: '106'},
                {name: '107'},
                {name: '108'},
                {name: '109'},
                {name: '110'},
                {name: '111'},
                {name: '112'},
                {name: '113'},
                {name: '114'},
                {name: '115'},
                {name: '116'},
                {name: '117'},
                {name: '118'},
                {name: '119'},
                {name: '120'},
                {name: '121'},
                {name: '122'},
                {name: '123'},
                {name: '124'},
                {name: '125'},
                {name: '126'},
                {name: '127'}, // SchemaGrid
                {name: '128'}, // SchemaGrid
                {name: '129'}, // SchemaGrid
                {name: '130'}, // Pegawai Spesial
                {name: '201'},
                {name: '202'},
                {name: '203'},
                {name: '204'},
                {name: '205'},
                {name: '206'},
                {name: '207'},
                {name: '208'},
                {name: '209'},
                {name: '210'},
                {name: '211'},
                {name: '212'},
                {name: '213'},
                {name: '214'},
                {name: '215'},
                {name: '216'},
                {name: '217'},
                {name: '218'},
                {name: '219'},
                {name: '220'},
                {name: '221'},
                {name: '222'},
                {name: '223'},
                {name: '224'},
                {name: '225'},
                {name: '226'},
                {name: '227'},
                {name: '228'},
                {name: '229'}, //Recalxulate All 
                {name: '230'}, //Transfer pegawai
                {name: '231'}, //Lock & Post
                {name: '232'}, //Upload TimeSheet
                {name: '233'}, //Upload Attlog
                {name: '234'},

                {name: '301'}, //Report FP & Absen
                {name: '302'}, //RekapPegawai
                {name: '303'}, //RekapPayroll
                {name: '304'}, //Rekap Absensi
                {name: '305'}, //Report Master Gaji
                {name: '306'}, //Report Less Time & Lupa Absen 
                {name: '307'}, //Report Shift
                {name: '308'}, //Report Absen All

                {name: '309'}, //Report Cuti
                {name: '310'},
                {name: '311'},
                {name: '312'},
                {name: '313'},
                {name: '314'},
                {name: '315'},
                {name: '316'},
                {name: '317'},
                {name: '318'},
                {name: '319'},
                {name: '320'},
                {name: '321'},
                {name: '322'},

                {name: '401'},
                {name: '402'},
                {name: '403'},
                {name: '404'} //RESET ABSENSI

                ,{name: '501'} // AMOS
            ]
        }, cfg));
    }
});
jun.rztSecurityRoles = new jun.SecurityRolesstore();
//jun.rztSecurityRoles.load();
