jun.ShiftPinStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ShiftPinStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ShiftPinStoreId',
            url: 'ShiftPin',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'shiftpin_id'},
                {name: 'shift_id'},
                {name: 'pin_id'}
            ]
        }, cfg));
    }
});
jun.rztShiftPin = new jun.ShiftPinStore();
jun.rztShiftPinLib = new jun.ShiftPinStore();