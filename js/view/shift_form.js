jun.ShiftWin = Ext.extend(Ext.Window, {
    title: 'Shift',
    modez: 1,
    width: 450,
    height: 360,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background: none; padding: 10px',
                id: 'form-Shift',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode',
                        allowBlank: false,
                        name: 'kode_shift',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'In time',
                        allowBlank: false,
                        regex: /^\d{2}:\d{2}:\d{2}$/i,
                        emptyText: 'hh:mm:ss',
                        name: 'in_time',
                        ref: '../in_time',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'compositefield',
                        fieldLabel: 'Range (In time)',
                        defaults:{
                            xtype: 'textfield',
                            regex: /^\d{2}:\d{2}:\d{2}$/i,
                            allowBlank: false,
                            emptyText: 'hh:mm:ss'
                        },
                        items: [
                            {
                                fieldLabel: 'Early',
                                name: 'earlyin_time',
                                flex: 1
                            },
                            {
                                xtype: 'label',
                                text: ' s/d '
                            },
                            {
                                fieldLabel: 'Late',
                                name: 'latein_time',
                                flex: 1
                            }
                        ]
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Early time (< time)',
                        hideLabel: false,
                        allowBlank: false,
                        regex: /^\d{2}:\d{2}:\d{2}$/i,
                        emptyText: 'hh:mm:ss',
                        name: 'early_time',
                        maxLength: 100,
                        anchor: '100%'
                    },                 
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Late time (> time)',
                        hideLabel: false,
                        allowBlank: false,
                        regex: /^\d{2}:\d{2}:\d{2}$/i,
                        emptyText: 'hh:mm:ss',
                        name: 'late_time',
                        maxLength: 100,
                        anchor: '100%'
                    },                 
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Out time',
                        hideLabel: false,
                        allowBlank: false,
                        regex: /^\d{2}:\d{2}:\d{2}$/i,
                        emptyText: 'hh:mm:ss',
                        name: 'out_time',
                        maxLength: 100,
                        anchor: '100%'
                    },                                        
                    {
                        xtype: 'compositefield',
                        fieldLabel: 'Range (Out time)',
                        defaults:{
                            xtype: 'textfield',
                            regex: /^\d{2}:\d{2}:\d{2}$/i,
                            allowBlank: false,
                            emptyText: 'hh:mm:ss'
                        },
                        items: [
                            {
                                fieldLabel: 'Early',
                                name: 'earlyout_time',
                                flex: 1
                            },
                            {
                                xtype: 'label',
                                text: ' s/d '
                            },
                            {
                                fieldLabel: 'Late',
                                name: 'lateout_time',
                                flex: 1
                            }
                        ]
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Least time (< time)',
                        hideLabel: false,
                        allowBlank: false,
                        regex: /^\d{2}:\d{2}:\d{2}$/i,
                        emptyText: 'hh:mm:ss',
                        name: 'least_time',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Over time (> time)',
                        hideLabel: false,
                        allowBlank: false,
                        regex: /^\d{2}:\d{2}:\d{2}$/i,
                        emptyText: 'hh:mm:ss',
                        name: 'over_time',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
//                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: cabang_value == 1 ? 'cabang_id' : '',
//                        emptyText: 'Tampilkan dari semua cabang',
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tandai sebagai shift OG, OB atau Security',
                        name: 'kelompok_shift',
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        ref: '../kelompok_shift'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ShiftWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // this.in_time.on('blur', this.in_timeOnBlur, this);
        
        this.btnCancel.setText(this.modez < 2? 'Batal':'Tutup');
        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else if(this.modez == 1) { //edit
            if (jun.securityroles != SECURITY_ROLE_ADMINISTRATOR) {
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(false);
            }
        } else { //view
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({ readOnly: true });
        }
    },
    in_timeOnBlur: function(a) {
        if(/^\d{2}:\d{2}:\d{2}$/i.test(this.in_time.getValue())) {
            // console.log((new Date((new Date("February 9, 2012, " + this.in_time.getValue())).getTime() + (60 * 60 * 1000))).getHours());
            console.log(((new Date((new Date("February 9, 2012, " + this.in_time.getValue())).getTime() + (60 * 60 * 1000))).toLocaleTimeString()));
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
//        this.btnDisabled(true);
        var urlz;
        if (this.modez == 0) {
            urlz = 'Shift/create/';
        } else if (this.modez == 1) {
            urlz = 'Shift/update/id/' + this.id;
        }
        this.formz.getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztShift.reload();
                jun.rztShiftLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});

jun.ShiftCloneWin = Ext.extend(Ext.Window, {
    title: 'Shift',
    modez: 1,
    width: 350,
    height: 120,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background: none; padding: 10px',
                id: 'form-ShiftCloneWin',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [

                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangCmp,
                        ref: '../cabang',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ShiftCloneWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    saveForm: function () {
        this.formz.getForm().submit({
            url: 'Shift/Clone/',
            timeout: 1000,
            scope: this,
            params: {
                shift_id: this.shift_id
            },
            success: function (f, a) {
                jun.rztShift.reload();
                jun.rztShiftLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});