jun.ShiftGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Shift",
    id: 'docs-jun.ShiftGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Kode Shift',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_shift',
            width: 100
        },        
        {
            header: 'Cabang',
            sortable: true,
            resizable: true,
            // dataIndex: 'cabang_id',
            width: 100,
            // renderer: jun.renderCabang
            dataIndex: 'kode_cabang',
            filter: {xtype: "textfield", filterName: "kode_cabang"}
        },
        {
            header: 'Kelompok Shift',
            sortable: true,
            resizable: true,
            dataIndex: 'kelompok_shift',
            width: 100,
            renderer: function (v, m, r) {
                if(r.get('kelompok_shift') == 1){
                    return 'OG,OB atau Security';
                } else return 'Karyawan';
            }
        },
        {
            header: 'Status Aktif',
            sortable: true,
            resizable: true,
            dataIndex: 'active',
            width: 100,
            renderer: function (v, m, r) {
                if(r.get('active') == 1){
                    return 'AKTIF';
                } else return 'NON AKTIF';
            }
        }
        
    ],
    userPermission: function(actCreate, actEdit, actDelete){
        this.actCreate = actCreate;
        this.actEdit = actEdit;
        this.actDelete = actDelete;
        this.actView = true;
    },
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        if (jun.rztCabangCmp.getTotalCount() === 0) {
            jun.rztCabangCmp.load();
        }
        this.store = jun.rztShift;
        //switch(UROLE){
        //    default:
                this.userPermission(1,1,1);
        //}
        
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    ref: '../botbar',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add',
                    hidden: this.actCreate?false:true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat',
                    iconCls: this.actEdit?'silk13-pencil':'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Clone Shift',
                    ref: '../btnCloneShift'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Toggle Kelompok Shift',
                    ref: '../btnKelompokShift'
                },
                '->',
                {
                    xtype: 'button',
                    text: 'Toggle Aktivasi Shift',
                    ref: '../btnAktivasiShift'
                }
            ]
        };
        
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        
        jun.ShiftGrid.superclass.initComponent.call(this);
        this.btnAdd.on('click', this.loadForm, this);
        this.btnEdit.on('click', this.loadEditForm, this);
        this.btnCloneShift.on('click', this.loadbtnCloneShift, this);

        this.btnKelompokShift.on('Click', this.btnKelompokShiftClick, this);
        this.btnAktivasiShift.on('Click', this.btnAktivasiShiftClick, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        
        this.fieldsearch = "";
        this.valuesearch = "";
    },
    btnKelompokShiftClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Karyawan");
            return;
        }
        Ext.Ajax.request({
            url: 'Shift/KelompokShift/',
            method: 'POST',
            scope: this,
            params: {
                shift_id: selectedz.json.shift_id
            },
            success: function (f, a) {
                jun.rztShift.reload();
                jun.rztShiftCmp.reload(); 
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnAktivasiShiftClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Karyawan");
            return;
        }
        Ext.Ajax.request({
            url: 'Shift/AktivasiShift/',
            method: 'POST',
            scope: this,
            params: {
                shift_id: selectedz.json.shift_id
            },
            success: function (f, a) {
                jun.rztShift.reload();
                jun.rztShiftCmp.reload();                
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadbtnCloneShift: function () {
        var selectedz = this.sm.getSelected();
        var form = new jun.ShiftCloneWin({
            shift_id: selectedz.json.shift_id,
            title: "Clone Shift",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadForm: function () {
        var form = new jun.ShiftWin({
            modez: 0,
            title: "Buat Data Shift",
            iconCls: 'silk13-add'
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data Shift");
            return;
        }
        var idz = selectedz.json.shift_id;
        var form = new jun.ShiftWin({
            modez: this.actEdit?1:2,
            id: idz,
            title: (this.actEdit?"Ubah":"Lihat")+" Data Shift : "+selectedz.json.kode_shift,
            iconCls: this.actEdit?'silk13-pencil':'silk13-eye'
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
//    setFilterData: function(n){
//        if (parseInt(this.store.baseParams.tipe_shift_id) == n){
//            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
//            this.botbar.doRefresh();
//        } else {
//            this.store.baseParams = {fieldsearch: this.fieldsearch, valuesearch: this.valuesearch};
//            this.botbar.moveFirst();
//        }
//    },
//    find: function (c, e) {
//        if (e.getKey() == 8 || e.getKey() == 46 || /[a-z\d]/i.test(String.fromCharCode(e.getKey()))) {
//            this.fieldsearch = this.btnFind.fieldsearch;
//            this.valuesearch = c.getValue();
//            this.store.baseParams = { fieldsearch: this.fieldsearch, valuesearch: this.valuesearch };
//            this.botbar.moveFirst();
//        }
//    },    
});
