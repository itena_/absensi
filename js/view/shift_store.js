jun.ShiftStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ShiftStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ShiftStoreId',
            url: 'Shift',
            root: 'results',
            totalProperty: 'total',
            // autoLoad: true,
            fields: [
                {name: 'shift_id'},
                {name: 'in_time'},
                {name: 'out_time'},
                {name: 'kode_shift'},
                {name: 'jabatan_id'},
                {name: 'earlyin_time'},
                {name: 'latein_time'},
                {name: 'earlyout_time'},
                {name: 'lateout_time'},                
                {name: 'early_time'},
                {name: 'late_time'},
                {name: 'least_time'},
                {name: 'over_time'},                                
                {name: 'sunday'},
                {name: 'monday'},
                {name: 'tuesday'},
                {name: 'wednesday'},
                {name: 'thursday'},
                {name: 'friday'},
                {name: 'saturday'},
                {name: 'cabang_id'},
                {name: 'kelompok_shift'},
                {name: 'kode_cabang'},
                {name: 'active'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztShift = new jun.ShiftStore();
jun.rztShiftCmp = new jun.ShiftStore();
jun.rztShiftLib = new jun.ShiftStore({url : 'DayShift/DS'});