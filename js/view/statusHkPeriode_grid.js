jun.StatusHkPeriodeGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "StatusHkPeriode",
    id: 'docs-jun.StatusHkPeriodeGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Periode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_periode',
            width: 100
        },
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100
        },
        {
            header: 'Nama Lengkap',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100
        },
        {
            header: '<b><p class="redInline">Status HK</p></b>',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Diperbaharui oleh',
            sortable: true,
            resizable: true,
            dataIndex: 'id_user',
            width: 100
        },
        {
            header: 'Diperbaharui pada',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate',
            width: 100
        }

    ],
    initComponent: function () {
        if (jun.rztStatusHkCmp.getTotalCount() === 0)
            jun.rztStatusHkCmp.load();

        this.store = jun.rztStatusHkPeriode;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: 'Periode:'
                },
                {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeLib,
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                        <br>\n\<b>Start: </b>{periode_start}\n\\n\
                        <br>\n\<b>End: </b>{periode_end}\n\</span>',
                        "</div></tpl>"),
                    ref: '../periode',
                    width: 200,
                    id: 'periodeOnStatusHkPeriode'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Edit Status HK',
                    ref: '../btnEdit'
                }
            ]
        };

        jun.StatusHkPeriodeGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.periode.on('select', this.periodeOnSelect, this);
        this.on('celldblclick', this.loadEditForm);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    periodeOnSelect: function (c, r, i) {
        Ext.Ajax.request({
            url: 'StatusHkPeriode/SaveData',
            method: 'POST',
            scope: this,
            params: {
                bu_id: jun.bu_id,
                periode_id: c.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.store.reload();
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();

        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.status_hk_periode_id;
        var form = new jun.StatusHkPeriodeWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});
