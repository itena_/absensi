jun.StatusHkPeriodestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StatusHkPeriodestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StatusHkPeriodeStoreId',
            url: 'StatusHkPeriode',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'status_hk_periode_id'},
                {name: 'status_hk_id'},
                {name: 'periode_id'},
                {name: 'pegawai_id'},
                {name: 'user_id'},
                {name: 'cabang_id'},
                {name: 'nama'},
                {name: 'kode_periode'},
                {name: 'nik'},
                {name: 'nama_lengkap'},
                {name: 'id_user'},
                {name: 'tdate'}
            ]
        }, cfg));
        this.on('beforeload', this.refreshData, this);
    },
    refreshData: function (a) {
        this.baseParams = {periode_id: Ext.getCmp('periodeOnStatusHkPeriode').getValue(), bu_id: jun.bu_id};
    }
});
jun.rztStatusHkPeriode = new jun.StatusHkPeriodestore();
jun.rztStatusHkPeriodeCmp = new jun.StatusHkPeriodestore();
