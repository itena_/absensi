jun.StatusHkGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "StatusHk",
    id: 'docs-jun.StatusHkGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Bisnis Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'bu_name',
            width: 100
        },
        {
            header: 'Nama Status HK',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Utama',
            sortable: true,
            resizable: true,
            dataIndex: 'default',
            width: 100,
            renderer: function (v, m, r) {
                if(r.get('default') == 1){
                    return 'YA';
                } else return '-';
            }
        },
        {
            header: 'Active',
            sortable: true,
            resizable: true,
            dataIndex: 'active',
            width: 100,
            renderer: function (v, m, r) {
                if(r.get('active') == 1){
                    return 'AKTIF';
                } else return 'NON AKTIF';
            }
        },
        {
            header: 'Diperbaharui oleh',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100
        },
        {
            header: 'Diperbaharui pada',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate',
            width: 100
        }

    ],
    initComponent: function () {
        if (jun.rztBuCmp.getTotalCount() === 0)
            jun.rztBuCmp.load();

        this.store = jun.rztStatusHk;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Daftarkan sebagai Status HK Utama',
                    ref: '../btnUtama'
                },
                '->',
                {
                    xtype: 'button',
                    text: 'Toggle Aktivasi Status HK',
                    ref: '../btnAktivasiStatusHk'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();

        jun.StatusHkGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnUtama.on('Click', this.btnUtamaonClick, this);
        this.btnAktivasiStatusHk.on('Click', this.btnAktivasiStatusHkClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    btnUtamaonClick: function () {
        var status_hk_id = this.record.data.status_hk_id;
        Ext.Ajax.request({
            url: 'StatusHk/AssignStatusHkUtama/',
            method: 'POST',
            scope: this,
            params: {
                status_hk_id: status_hk_id
            },
            success: function (f, a) {
                jun.rztStatusHk.reload();
                jun.rztStatusHkCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },

    loadForm: function () {
        var form = new jun.StatusHkWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.status_hk_id;
        var form = new jun.StatusHkWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    btnAktivasiStatusHkClick: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Karyawan");
            return;
        }
        Ext.Ajax.request({
            url: 'StatusHk/AktivasiStatusHk/',
            method: 'POST',
            scope: this,
            params: {
                status_hk_id: selectedz.json.status_hk_id
            },
            success: function (f, a) {
                jun.rztStatusHk.reload();
                jun.rztStatusHkCmp.reload();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
