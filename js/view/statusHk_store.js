jun.StatusHkstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StatusHkstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StatusHkStoreId',
            url: 'StatusHk',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'status_hk_id'},
                {name: 'nama'},
                {name: 'active'},
                {name: 'user_id'},
                {name: 'nik'},
                {name: 'bu_id'},
                {name: 'bu_name'},
                {name: 'default'},
                {name: 'tdate'}

            ]
        }, cfg));
        this.on('beforeload', this.refreshData, this);
    },
    refreshData: function (a) {
        this.baseParams = {bu_id: jun.bu_id};
    }
});
jun.rztStatusHk = new jun.StatusHkstore();
jun.rztStatusHkCmp = new jun.StatusHkstore();
//jun.rztStatusHk.load();
