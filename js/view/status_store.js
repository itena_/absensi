jun.Statusstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Statusstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StatusStoreId',
            url: 'Status',
            root: 'results',
            totalProperty: 'total',
            autoLoad: true,
            fields: [
                {name: 'status_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'bu_id'}
            ]
        }, cfg));
    }
});
jun.rztStatus = new jun.Statusstore();
jun.rztStatusCmp = new jun.Statusstore();
// jun.rztStatusCmp.load();
