jun.SysPrefsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "SysPrefs",
    id: 'docs-jun.SysPrefsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Name',
            sortable: true,
            resizable: true,
            dataIndex: 'name_',
            width: 100
        },
        {
            header: 'Value',
            sortable: true,
            resizable: true,
            dataIndex: 'value_',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 200
        }

    ],
    initComponent: function () {
        this.store = jun.rztSysPrefs;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};

        jun.SysPrefsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },

    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },

    loadForm: function () {
        var form = new jun.SysPrefsWin({modez: 0});
        form.show();
    },

    loadEditForm: function () {

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sys_prefs_id;
        var form = new jun.SysPrefsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});
