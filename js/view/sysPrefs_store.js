jun.SysPrefsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SysPrefsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SysPrefsStoreId',
            url: 'SysPrefs',
            root: 'results',
            autoLoad: true,
            totalProperty: 'total',
            fields: [
                {name: 'sys_prefs_id'},
                {name: 'name_'},
                {name: 'value_'},
                {name: 'note'}
            ]
        }, cfg));
    }
});
jun.rztSysPrefs = new jun.SysPrefsstore();
