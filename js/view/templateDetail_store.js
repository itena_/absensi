jun.TemplateDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TemplateDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TemplateDetailStoreId',
            url: 'TemplateDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'template_detail_id'},
                {name: 'template_id'},
                {name: 'shift_id'},
                {name: 'sunday'},
                {name: 'monday'},
                {name: 'tuesday'},
                {name: 'wednesday'},
                {name: 'thursday'},
                {name: 'friday'},
                {name: 'saturday'}

            ]
        }, cfg));
    }
});
jun.rztTemplateDetail = new jun.TemplateDetailstore();
jun.rztTemplateDetailLib = new jun.TemplateDetailstore({url : 'ShiftPin'});
//jun.rztTemplateDetail.load();
