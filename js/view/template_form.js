jun.TemplateWin = Ext.extend(Ext.Window, {
    title: 'Template',
    modez: 1,
    width: 730,
    height: 350,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Template',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Nama Template:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'name',
                        id: 'nameid',
                        ref: '../name',
                        x: 105,
                        y: 2,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        }
                    },
                    new jun.TemplateDetailGrid({
                        x: 5,
                        y: 30,
                        height: 285,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    })

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TemplateWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on('close', this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
        this.btnDisabled(true);
        var urlz;

        if (this.griddetils.store.data.length == 0) {
            Ext.Msg.alert('Error', "Template belum dipilih!");
            this.btnDisabled(false);
            return;
        }
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Template/update/id/' + this.id;
        } else {
            urlz = 'Template/create/';
        }

        Ext.getCmp('form-Template').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                        this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztTemplate.reload();
                jun.rztTemplateDetailLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Template').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    }

});