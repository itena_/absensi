jun.TemplateGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Template",
    id: 'docs-jun.TemplateGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
//        {
//            header: 'template_id',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'template_id',
//            width: 100
//        },
        {
            header: 'Template',
            sortable: true,
            resizable: true,
            dataIndex: 'name',
            width: 100
        }

    ],
    
    initComponent: function () {
        if (jun.rztShiftLib.getTotalCount() === 0)
            jun.rztShiftLib.load();
        this.store = jun.rztTemplate;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
//                    hidden: this.actCreate ? false : true
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    iconCls: 'silk13-pencil',
//                    text: this.actEdit ? 'Ubah' : 'Lihat',
//                    iconCls: this.actEdit ? 'silk13-pencil' : 'silk13-eye',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete'
//                    hidden: this.actDelete ? false : true
                }
            ]
        };
//        this.store.baseParams = {mode: "grid"};
        this.store.reload();
//        this.store.baseParams = {};
        jun.TemplateGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.on('rowdblclick', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;

        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TemplateWin({modez: 0});
        form.griddetils.store.removeAll();
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.template_id;
        var form = new jun.TemplateWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.griddetils.store.baseParams = {
            template_id: idz
        };
        form.griddetils.store.load();
//        jun.rztTemplateDetail.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'Template/delete/id/' + record.json.template_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTemplate.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
