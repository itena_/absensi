jun.Templatestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Templatestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TemplateStoreId',
            url: 'Template',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'template_id'},
                {name: 'name'}

            ]
        }, cfg));
    }
});
jun.rztTemplate = new jun.Templatestore();
jun.rztTemplateLib = new jun.Templatestore();
//jun.rztTemplate.load();
