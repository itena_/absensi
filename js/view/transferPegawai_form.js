jun.TransferPegawaiWin = Ext.extend(Ext.Window, {
    title: 'Perbantuan Pegawai',
    modez: 1,
    width: 400,
    height: 250+70-20,
    lintasBu: 0,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TransferPegawai',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Nomor Surat Tugas', 
                        allowBlank: true,  
                        ref: '../no_surat_tugas',
                        name: 'no_surat_tugas',
                        maxLength: 100,
                        anchor: '100%', 
                        disabled: false,
                        width: 170, 
                        height: 50
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Pegawai',
                        style: 'margin-bottom:2px',
                        mode: 'remote',
                        triggerAction: 'query',
                        autoSelect: false,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        listWidth: 450,
                        lastQuery: "",
                        lazyRender: true, 
                        forceSelection: true,
                        store: jun.rztPegawaiLib,
                        hiddenName: 'pegawai_id',
                        valueField: 'pegawai_id',
                        ref: '../pegawai_id',
                        displayField: 'nik',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{nik}</b><br>{nama_lengkap}</span>',
                            "</div></tpl>"),
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Cabang Asal',
                        readOnly: true, 
                        store: jun.rztCabangCmp,
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        ref: '../cabang_id',
                        displayField: 'kode_cabang', 
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'BU Alias',
                        ref: '../bu_nama_alias',
                        name: 'bu_nama_alias',
                        hidden: true,
                        anchor: '100%',
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NIK 8 digit',
                        ref: '../nik',
                        name: 'nik',
                        hidden: true,
                        anchor: '100%',
                    },
                    {
                        xtype: 'button',
                        text: 'Cek data karyawan',
                        hidden: true,
                        ref: '../btnCheck'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama lengkap',
                        ref: '../nama_lengkap',
                        name: 'nama_lengkap',
                        readOnly: true,
                        hidden: true,
                        anchor: '100%',
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Cabang Asal',
                        ref: '../cabang_asal',
                        name: 'cabang_asal',
                        readOnly: true,
                        hidden: true,
                        anchor: '100%',
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Menuju Cabang',
                        store: jun.rztCabangCmp,
                        hiddenName: 'cabang_transfer_id',
                        valueField: 'cabang_id',
                        ref: '../cabang_transfer_id',
                        displayField: 'kode_cabang', 
                        anchor: '100%'
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Dari tgl',
                        name: 'tglin', 
                        ref: '../tglin',
                        hiddenName: 'tglin', 
                        format: 'Y-m-d',
                        anchor: '100%' 
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Sampai tgl',
                        name: 'tglout', 
                        ref: '../tglout',
                        hiddenName: 'tglout', 
                        format: 'Y-m-d',
                        anchor: '100%' 
                    },
                    new jun.comboStatusKota({
                        id: 'comboStatusKota',
                        fieldLabel: 'Status Kota',
                        ref: '../status_kota',
                        width: 150
                    }),
                    {
                        xtype: 'hidden',
                        name: 'binding_id'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Edit Perbantuan',
                    hidden: true,
                    ref: '../btnEditTransfer'
                },
                {
                    xtype: 'button',
                    text: 'Batalkan Perbantuan',
                    hidden: true,
                    ref: '../btnCancelTransfer'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferPegawaiWin.superclass.initComponent.call(this); 
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnCheck.on('click', this.onbtnCheckclick, this);
        this.btnEditTransfer.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancelTransfer.on('click', this.onbtnCancelTransferclick, this);
        this.pegawai_id.on('select', this.pinOnSelect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
        if (this.modez == 'cancel'){
            this.btnCancelTransfer.setVisible(true);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        }
        if (this.modez == 'edit'){
            this.btnEditTransfer.setVisible(true);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        }
    },
    onbtnCheckclick: function () {
        if(this.nik.getValue().length !== 8) {
            Ext.MessageBox.alert("Warning", "NIK harus 8 digit!");
            return;
        }
        var bu_nama_alias = this.bu_nama_alias.getValue();
        var nik = this.nik.getValue();

        Ext.Ajax.request({
            url: 'TransferPegawai/GetPegawaiLintasBu',
            method: 'POST',
            scope: this,
            params: {
                bu_nama_alias: bu_nama_alias,
                nik: nik
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if(response.msg == '') {
                    this.cabang_id.setValue(response.cabang_id);
                    this.cabang_asal.setValue(response.cabang_asal);
                    this.pegawai_id.setValue(response.pegawai_id);
                    this.nama_lengkap.setValue(response.nama_lengkap);
                } else {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    pinOnSelect: function (combo, record, index) {
        var as = combo.store.findExact('pegawai_id', combo.getValue());
        var b = combo.store.getAt(as);
        this.cabang_id.setValue(b.get('cabang_id'));
    },
    saveForm: function () {
        if(this.cabang_id.getValue() == this.cabang_transfer_id.getValue()){
            Ext.MessageBox.alert("Warning", "Cabang asal dan cabang tujuan tidak boleh sama!");
            return;
        }
        Ext.getCmp('form-TransferPegawai').getForm().submit({
            url: 'TransferPegawai/create/', 
            scope: this,
            params: {
                mode: this.modez,
                lintasBu: this.lintasBu
            },
            success: function (f, a) {
                jun.rztTransferPegawai.reload();
                jun.rztPegawaiLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                }); 
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false); 
            } 
        });
    },    
    onbtnCancelTransferclick: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membatalkan transfer ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var binding_id = this.binding_id;
        Ext.getCmp('form-TransferPegawai').getForm().submit({
            url: 'TransferPegawai/cancel' ,
            scope: this,
            params: {
                binding_id: binding_id
            },
            success: function (f, a) {
                jun.rztTransferPegawai.reload();
                jun.rztPegawaiLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                }); 
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false); 
            } 
        }); 
    },    
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});