jun.TransferPegawaiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Perbantuan Pegawai",
    id: 'docs-jun.TransferPegawaiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100,
            // renderer: jun.renderPIN,
            filter: {xtype: "textfield", filterName: "nik"}
        },
        {
            header: 'Nama Pegawai',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100,
            // renderer: jun.renderPegawaiAll,
            filter: {xtype: "textfield", filterName: "nama_lengkap"}
        },
//        {
//            header: 'Periode',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'periode_id',
//            width: 100,
//            renderer: jun.renderPeriode
//        },
        {
            header: 'Cabang Asal',
            sortable: true,
            resizable: true,
            dataIndex: 'cabang_asal',
            width: 100,
            // renderer: jun.renderCabang,
            filter: {xtype: "textfield", filterName: "cabang_asal"}
        },
        {
            header: 'Cabang Transfer',
            sortable: true,
            resizable: true,
            dataIndex: 'cabang_transfer',
            width: 100,
            // renderer: jun.renderCabang,
            filter: {xtype: "textfield", filterName: "cabang_transfer"}
        },
        {
            header: 'Tanggal Mulai',
            sortable: true,
            resizable: true,
            dataIndex: 'tglin',
            width: 100
        },
        {
            header: 'Tanggal Selesai',
            sortable: true,
            resizable: true,
            dataIndex: 'tglout',
            width: 100
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 100,
            // renderer: function (v, m, r) {
            //     if(r.get('visible') == 1){
            //         return 'AKTIF';
            //     } else {
            //         return 'DIBATALKAN';
            //     }
            // },
            filter: {xtype: "textfield", filterName: "status"}
        }
    ],
    initComponent: function () {     
        if (jun.rztPegawaiLib.getTotalCount() === 0) {
            jun.rztPegawaiLib.load();
        }
        if (jun.rztNikAllGrid.getTotalCount() === 0) {
            jun.rztNikAllGrid.load();
        }
        if (jun.rztCabangCmp.getTotalCount() === 0) {
            jun.rztCabangCmp.load();
        }
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        } 
        this.store = jun.rztTransferPegawai;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    hidden: true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Lihat',
                    ref: '../btnView'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit',
                    hidden: true
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Perbantuan Lintas BU',
                    ref: '../btnAddLintas',
                    hidden: true
                },
                '->',
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Batalkan',
                    ref: '../btnCancel',
                    hidden: true
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.TransferPegawaiGrid.superclass.initComponent.call(this);
        
        if(jun.securityroles == SECURITY_ROLE_ADMINISTRATOR || jun.securityroles == SECURITY_ROLE_HRD){
            this.btnAdd.setVisible(true);
            this.btnEdit.setVisible(true);
            this.btnCancel.setVisible(true);
            // this.btnAddLintas.setVisible(true);
        }
        
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnView.on('Click', this.loadEditForm, this); 
        this.btnEdit.on('Click', this.btnEditOnClick, this);
        this.btnCancel.on('Click', this.btnCancelOnClick, this);
        this.btnAddLintas.on('Click', this.btnAddLintasOnClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    }, 
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    loadForm: function () {
        var form = new jun.TransferPegawaiWin({modez: 0});
        form.show();
    },
    btnAddLintasOnClick: function () {
        var form = new jun.TransferPegawaiWin({modez: 0, height: 250+70+50, lintasBu: 1});
        form.show();
        form.pegawai_id.setVisible(false);
        form.cabang_id.setVisible(false);

        form.bu_nama_alias.setVisible(true);
        form.nik.setVisible(true);
        form.nama_lengkap.setVisible(true);
        form.cabang_asal.setVisible(true);
        form.btnCheck.setVisible(true);
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.transfer_pegawai_id;
        var form = new jun.TransferPegawaiWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    btnEditOnClick: function () {
        var selectedz = this.sm.getSelected();        
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if(selectedz.json.visible == 0){
            Ext.MessageBox.alert("Warning", "Transfer sudah pernah dibatalkan.");
            return;
        }
        var idz = selectedz.json.transfer_pegawai_id;
        var form = new jun.TransferPegawaiWin({modez: 'edit', id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    btnCancelOnClick: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        if(selectedz.json.visible == 0){
            Ext.MessageBox.alert("Warning", "Transfer sudah pernah dibatalkan.");
            return;
        }
        var idz = selectedz.json.transfer_pegawai_id;
        var form = new jun.TransferPegawaiWin({modez: 'cancel', id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});
