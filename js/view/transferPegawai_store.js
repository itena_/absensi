jun.TransferPegawaistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferPegawaistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferPegawaiStoreId',
            url: 'TransferPegawai',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_pegawai_id'},
                {name: 'pegawai_id'},
                {name: 'cabang_id'},
                {name: 'cabang_transfer_id'},
                {name: 'tglin'},
                {name: 'tglout'},
                {name: 'no_surat_tugas'},
                {name: 'periode_id'},
                {name: 'visible'},
                {name: 'status_kota'},
                {name: 'binding_id'},

                {name: 'pin_nama'},
                {name: 'nama_lengkap'},
                {name: 'nik'},
                {name: 'status'},
                {name: 'cabang_asal'},
                {name: 'cabang_transfer'}
            ]
        }, cfg));
    }
});
jun.rztTransferPegawai = new jun.TransferPegawaistore(); 