jun.UsersGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "User Manajement",
    id: "docs-jun.UsersGrid",
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
    columns: [
        {
            header: "Username",
            dataIndex: "user_id",
            width: 100
        },
        {
            header: "Name",
            dataIndex: "name_",
            width: 100
        },
        {
            header: "NIK Nama",
            dataIndex: "pin_nama",
            width: 100
        },
        {
            header: "Role",
            dataIndex: "role",
            width: 100
        },
        {
            header: "Last Login",
            dataIndex: "last_visit_date",
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztPegawaiLib.getTotalCount() === 0) {
            jun.rztPegawaiLib.load();
        }
        this.store = jun.rztUsers, this.bbar = {
            items: [
                {
                    xtype: "paging",
                    store: this.store,
                    displayInfo: !0,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: "toolbar",
            items: [
                {
                    iconCls: "asp-user2_add",
                    xtype: "button",
                    text: "Add User",
                    ref: "../btnAdd"
                },
                {
                    xtype: "tbseparator"
                },
                {
                    xtype: "button",
                    iconCls: "asp-access",
                    text: "Reset Password",
                    ref: "../btnReset"
                },
                {
                    xtype: "tbseparator"
                },
                {
                    xtype: "button",
                    iconCls: "asp-user2_delete",
                    text: "Edit User",
                    ref: "../btnEdit"
                }
            ]
        };
        jun.rztUsers.reload();
        jun.rztSecurityRoles.reload();
        jun.UsersGrid.superclass.initComponent.call(this);
        this.btnAdd.on("Click", this.loadForm, this);
        this.btnEdit.on("Click", this.loadEditForm, this);
        this.btnReset.on("Click", this.deleteRec, this);
        this.getSelectionModel().on("rowselect", this.getrow, this);
    },
    getrow: function (a, b, c) {
        this.record = c;
        var d = this.sm.getSelections();
    },
    loadForm: function () {
        var a = new jun.UsersWin({
            modez: 0
        });
        a.show();
    },
    loadEditForm: function () {
        var a = this.sm.getSelected();
        if (a == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih User");
            return;
        }
        var b = new jun.UbahSecurity({
            modez: 0
        });
        b.show();
        b.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin mereset password user ini?", this.deleteRecYes,
            this);
    },
    deleteRecYes: function (a) {
        if (a == "no") return;
        var b = this.sm.getSelected();
        if (b == "") {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih User");
            return;
        }
        var c = jun.StringGenerator(8, "#aA"), d = jun.EncryptPass(c);
        Ext.Ajax.request({
            url: "Users/update/id/" + b.json.id,
            method: "POST",
            params: {
                password: d
            },
            scope: this,
            success: function (a, b) {
                jun.rztUsers.reload();
                var d = Ext.decode(a.responseText);
                Ext.MessageBox.show({
                    title: "Info",
                    msg: d.msg + "<br>Password : " + c,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (a, b) {
                var c = Ext.decode(a.responseText);
                Ext.MessageBox.show({
                    title: "Warning",
                    msg: c.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING
                });
            }
        });
    }
});