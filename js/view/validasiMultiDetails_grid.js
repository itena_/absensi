jun.ValidasiMultiDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.ValidasiMultiDetailsGrid',
    stripeRows: true,
    hideHeaders: false,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_ket',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'CM',
            sortable: true,
            resizable: true,
            dataIndex: 'cuti',
            width: 30
        },
        {
            header: 'Tgl Masuk',
            sortable: true,
            resizable: true,
            dataIndex: 'in_time',
            width: 100
        },
        {
            header: 'Jam Masuk',
            sortable: true,
            resizable: true,
            dataIndex: 'in_time',
            width: 100
        },
        {
            header: 'Tgl Keluar',
            sortable: true,
            resizable: true,
            dataIndex: 'out_time',
            width: 100
        },
        {
            header: 'Jam Keluar',
            sortable: true,
            resizable: true,
            dataIndex: 'out_time',
            width: 100
        },
        {
            header: 'RB',
            sortable: true,
            resizable: true,
            dataIndex: 'lanjut',
            width: 30
        },
        {
            header: 'Kode Shift',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_shift',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztValidasiMultiDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'fieldset',
                    // title: 'Actual data',
                    labelWidth: 100,
                    items :[
                        {
                            xtype: 'combo',
                            fieldLabel: 'Keterangan',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztKeteranganLib,
                            hiddenName: 'keterangan_id',
                            valueField: 'keterangan_id',
                            ref: '../../keterangan',
                            displayField: 'nama_ket',
                            emptyText: '--Keterangan--',
                            anchor: "100%"
                        },
                        {
                            xtype: 'checkbox',
                            boxLabel: 'Cuti Mendadak (CM)',
                            value: 2,
                            inputValue: 2,
                            uncheckedValue: 0,
                            disabled: true,
                            name: 'cuti',
                            ref: '../../cuti'
                        },
                        {
                            xtype: 'textarea',
                            fieldLabel: 'Note',
                            allowBlank: true,
                            emptyText: 'Nomor surat tugas / catatan',
                            ref: '../../nolk',
                            name: 'no_surat_tugas',
                            maxLength: 100,
                            disabled: false,
                            width: 150,
                            height: 50,
                            anchor: '100%'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    // title: 'Actual data',
                    labelWidth: 100,
                    items :[
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Dari tgl',
                            name: 'jam_in',
                            ref: '../../tgl_in',
                            hiddenName: 'jam_in',
                            format: 'Y-m-d',
                            minValue: '2021-04-01',
                            maxValue: '2021-04-30',
                            anchor: '100%'
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Sampai tgl',
                            name: 'jam_out',
                            ref: '../../tgl_out',
                            hiddenName: 'jam_out',
                            format: 'Y-m-d',
                            anchor: '100%'
                        },
                        {
                            xtype: 'checkbox',
                            boxLabel: 'Review Berkelanjutan (RB)',
                            value: 1,
                            inputValue: 1,
                            uncheckedValue: 0,
                            name: 'lanjut',
                            id: "lanjutid",
                            ref: '../../lanjut'
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    // title: 'Actual data',
                    labelWidth: 100,
                    items :[
                        {
                            xtype: 'combo',
                            fieldLabel: 'Shift',
                            style: 'margin-bottom:2px',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztShiftCmp,
                            hiddenName: 'shift_id',
                            valueField: 'shift_id',
                            emptyText: '--Pilih Shift--',
                            ref: '../../shift_id',
                            displayField: 'kode_shift',
                            anchor: "100%",
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<div style="width:100%;display:inline-block;">\n\
                                    <span style="font-weight: bold">{kode_shift}</span>\n\
                                    <br>\n\{in_time} - {out_time}\n\
                                    </div>',
                                "</div></tpl>")
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Jam Masuk',
                            name: 'in_time',
                            ref: '../../in_time',
                            hiddenName: 'in_time',
                            format: 'Y-m-d',
                            anchor: '100%'
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Jam Keluar',
                            name: 'out_time',
                            ref: '../../out_time',
                            hiddenName: 'out_time',
                            format: 'Y-m-d',
                            anchor: '100%'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            width: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            width: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.ValidasiMultiDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.keterangan.on('select', this.onKeteranganSelect, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        /*if (this.btnEdit.text == 'Save (F2)') {
            var record = this.sm.getSelected();
            record.set('keterangan_id', this.keterangan.getValue());
            record.set('cuti', this.cuti.getValue());
            record.set('note', this.note.getValue());
            record.set('tgl_in', this.tgl_in.getValue());
            record.set('tgl_out', this.tgl_out.getValue());
            record.set('in_time', this.in_time.getValue());
            record.set('out_time', this.out_time.getValue());
            record.set('lanjut', this.lanjut.getValue());
            record.set('shift_id', this.shift_id.getValue());
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    keterangan_id   : this.keterangan.getValue(),
                    cuti            : this.cuti.getValue(),
                    note            : this.note.getValue(),
                    tgl_in          : this.tgl_in.getValue(),
                    tgl_out         : this.tgl_out.getValue(),
                    in_time         : this.in_time.getValue(),
                    out_time        : this.out_time.getValue(),
                    lanjut          : this.lanjut.getValue(),
                    shift_id        : this.shift_id.getValue()
                });
            this.store.add(d);
        }*/
        var c = this.store.recordType,
            d = new c({
                keterangan_id   : this.keterangan.getValue(),
                nama_ket        : this.keterangan.getRawValue(),
                cuti            : this.cuti.getValue(),
                nolk            : this.nolk.getValue(),
                tgl_in          : this.tgl_in.getValue(),
                tgl_out         : this.tgl_out.getValue(),
                in_time         : this.in_time.getValue(),
                out_time        : this.out_time.getValue(),
                lanjut          : this.lanjut.getValue(),
                shift_id        : this.shift_id.getValue()
            });
        this.store.add(d);

        this.keterangan.reset();
        this.cuti.reset();
        this.nolk.reset();
        this.tgl_in.reset();
        this.tgl_out.reset();
        this.in_time.reset();
        this.out_time.reset();
        this.lanjut.reset();
        this.shift_id.reset();

        this.keterangan.focus();
    },
    onKeteranganSelect: function () {
        var ket = this.keterangan.getValue();

        this.in_time.reset();
        this.out_time.reset();
        this.shift.reset();

        switch (ket) {
            case '1' : case '3' : case '10' :
                this.in_time.setDisabled(false);
                this.out_time.setDisabled(false);
                break;
            default :
                var shift = this.shift.store.getAt(0);
                this.shift.setValue(shift.get('shift_id'));

                SET_INTIME_BUAT_REVIEW2 ?
                    this.in_time.setValue(shift.get('late_time'))
                    : this.in_time.setValue(shift.get('in_time'));

                this.out_time.setValue(shift.get('out_time'));
                this.in_time.setDisabled(true);
                this.out_time.setDisabled(true);
        }
        var ketsub = this.keterangan.getRawValue().substring(0, 4);

        if (ketsub == 'Cuti') {
            this.cuti.setDisabled(false);
        } else {
            this.cuti.setDisabled(true);
            this.cuti.reset();
        }
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});