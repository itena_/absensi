jun.ValidasiWin = Ext.extend(Ext.Window, {
    title: 'Validasi',
    modez: 1,
    width: 750,
    height: 360-90,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                width: "100%",
                id: 'form-Validasi',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'column',
                ref: 'formz',
                defaults: {
                    frame: false,
                    border: false
                },
                items: [
                    {
                        xtype: 'container',
                        layout: 'form',
                        columnWidth: .50,
                        items: [
                            {
                                xtype: 'combo',
                                fieldLabel: 'Keterangan',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztKeteranganLib, //RUMUS deklarasi store langsung di combo
                                hiddenName: 'keterangan_id',
                                valueField: 'keterangan_id',
                                ref: '../../keterangan',
                                displayField: 'nama_ket',
                                anchor: "100%",
                                emptyText: '--Keterangan--',
                                hidden: this.modez == 0 ? false : true,
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<div style="width:100%;display:inline-block;">\n\
                                        <span style="font-weight: bold">{nama_ket}</span>\n\
                                        </div>',
                                    "</div></tpl>")
                            },
                            {
                                xtype: 'checkbox',
                                boxLabel: 'Tandai WFH',
                                hidden: this.modez == 0 ? false : true,
                                value: 2,
                                inputValue: 2,
                                uncheckedValue: 0,
                                disabled: true,
                                name: 'wfh',
                                id: "wfhid",
                                ref: '../../wfh'
                            },
                            {
                                xtype: 'checkbox',
                                boxLabel: 'Cuti Mendadak ?',
                                hidden: this.modez == 0 ? false : true,
                                value: 2,
                                inputValue: 2,
                                uncheckedValue: 0,
                                disabled: true,
                                name: 'cuti',
                                id: "cutiid",
                                ref: '../../cuti'
                            },
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Note',
                                allowBlank: true,
                                emptyText: 'Nomor surat tugas / catatan',
                                ref: '../../nolk',
                                name: 'no_surat_tugas',
                                maxLength: 100,
                                anchor: '100%',
                                disabled: false,
                                x: 400,
                                y: 62,
                                height: 75,
                                value: this.no_surat
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="greenInline">Lembur 1</p>',
                                //allowBlank: false,
                                allowBlank: true,
                                ref: '../../min_awal',
                                name: 'min_over_time_awal_real',
                                maxLength: 100,
                                anchor: '100%',
                                readOnly: true,
                                hidden: this.modez == 1 ? false : true
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="greenInline">Lembur 2</p>',
                                //allowBlank: false,
                                allowBlank: true,
                                ref: '../../min_akhir',
                                name: 'min_over_time_real',
                                maxLength: 100,
                                anchor: '100%',
                                readOnly: true,
                                hidden: this.modez == 1 ? false : true
                            },
                            {
                                xtype: 'button',
                                fieldLabel: 'Lepas Limit Lembur?',
                                text: 'TIDAK',
                                scale: 'medium',
                                name: 'limitlembur',
                                ref: '../../btnToggleLimit',
                                hidden: this.modez == 1 ? false : true
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="greenInline">Lembur yg diakui</p>',
                                style: 'font-weight:bold',
                                minValue: 0,
                                ref: '../../lembur_diakui',
                                name: 'lembur_diakui',
                                maxLength: 100,
                                anchor: '100%',
                                readOnly: false,
                                hidden: this.modez == 1 ? false : true
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="greenInline">Lembur Hari</p>',
                                style: 'font-weight:bold',
                                minValue: 0,
                                ref: '../../lembur_hari',
                                name: 'lembur_hari',
                                maxLength: 100,
                                anchor: '100%',
                                readOnly: true,
                                hidden: this.modez == 1 ? false : true
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="greenInline">Approval Lembur</p>',
                                ref: '../../approval_lembur',
                                name: 'approval_lembur',
                                readOnly: true,
                                hidden: this.modez == 0 ? true : false,
                                anchor: '100%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="redInline">Less time</p>',
                                allowBlank: true,
                                ref: '../../min_late',
                                name: 'real_less_time',
                                maxLength: 100,
                                anchor: '100%',
                                readOnly: true,
                                hidden: this.modez == 1 ? false : true
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="redInline">Lesstime yg diakui</p>',
                                style: 'font-weight:bold',
                                minValue: 0,
                                ref: '../../lesstime_diakui',
                                name: 'lesstime_diakui',
                                maxLength: 100,
                                anchor: '100%',
                                readOnly: false,
                                hidden: this.modez == 1 ? false : true
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<p class="redInline">Approval Lesstime</p>',
                                ref: '../../approval_lesstime',
                                name: 'approval_lesstime',
                                readOnly: true,
                                hidden: this.modez == 0 ? true : false,
                                anchor: '100%'
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        layout: 'form',
                        columnWidth: .50,
                        padding: '0 0 0 10px',
                        bodyStyle: 'background-color: #E4E4E4',
                        items: [
                            {
                                xtype: 'combo',
                                fieldLabel: this.modez == 0 ? 'Pilih Shift' : 'Shift',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztShiftCmp,
                                hiddenName: 'shift_id',
                                valueField: 'shift_id',
                                emptyText: '--Pilih Shift--',
                                ref: '../../shift',
                                id: 'shift_validasi',
                                displayField: 'kode_shift',
                                anchor: "100%",
                                itemSelector: "div.search-item",
                                allowBlank: false,
                                hidden: false,
                                readOnly: this.modez == 0 ? false : true,
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<div style="width:100%;display:inline-block;">\n\
                                        <span style="font-weight: bold">{kode_shift}</span>\n\
                                        <br>\n\{in_time} - {out_time}\n\
                                        </div>',
                                    "</div></tpl>")
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: this.modez == 0 ? 'Dari tgl' : 'In',
                                name: 'jam_in',
                                ref: '../../jam_in',
                                hiddenName: 'jam_in',
                                format: this.modez == 0 ? 'Y-m-d' : 'Y-m-d H:i:s',
                                anchor: '100%',
                                readOnly: true,
                                value: this.jam_in
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: this.modez == 0 ? 'Sampai tgl' : 'Out',
                                name: 'jam_out',
                                ref: '../../jam_out',
                                hiddenName: 'jam_out',
                                format: this.modez == 0 ? 'Y-m-d' : 'Y-m-d H:i:s',
                                anchor: '100%',
                                readOnly: true,
                                value: this.jam_out
                            },
                            {
                                xtype: 'checkbox',
                                boxLabel: 'Tandai Review Berkelanjutan ?',
                                hidden: this.modez == 0 ? false : true,
                                value: 1,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: 'lanjut',
                                id: "lanjutid",
                                ref: '../../lanjut'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'In time',
                                allowBlank: this.modez == 5 ? true : false,
                                regex: /^\d{2}:\d{2}:\d{2}$/i,
                                emptyText: 'hh:mm:ss',
                                ref: '../../in_time',
                                id: 'in_time_validasi',
                                name: 'in_time',
                                maxLength: 100,
                                anchor: '100%',
                                hidden: this.modez == 0 ? false : true,
                                value: this.in_time
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Out time',
                                allowBlank: this.modez == 5 ? true : false,
                                regex: /^\d{2}:\d{2}:\d{2}$/i,
                                emptyText: 'hh:mm:ss',
                                ref: '../../out_time',
                                id: 'out_time_validasi',
                                name: 'out_time',
                                maxLength: 100,
                                anchor: '100%',
                                hidden: this.modez == 0 ? false : true,
                                value: this.out_time
                            },
                            {
                                xtype: 'combo',
                                fieldLabel: 'Periode',
                                style: 'margin-bottom:2px',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztPeriodeLib,
                                valueField: 'periode_id',
                                displayField: 'kode_periode',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                                <br>\n\<b>Start: </b>{periode_start}\n\\n\
                                <br>\n\<b>End: </b>{periode_end}\n\</span>',
                                    "</div></tpl>"),
                                ref: '../../periode',
                                hidden: true
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                editable: false,
                                forceSelection: true,
                                fieldLabel: 'Cuti Tahun',
                                //store: jun.rztTahunLib,
                                store: new Ext.data.ArrayStore({
                                    fields: ['cuti_tahun'],
                                    data: [
                                        [new Date().getFullYear() - 1],
                                        [new Date().getFullYear()]
                                    ]
                                }),
                                hiddenName: 'cuti_tahun',
                                valueField: 'cuti_tahun',
                                displayField: 'cuti_tahun',
                                ref: '../../cuti_tahun',
                                anchor: '100%',
                                hidden: true
                            }
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Atur Ulang Presensi',
                    hidden: true,
                    ref: '../btnRecalculateLembur'
                },
                {
                    xtype: 'button',
                    text: 'Tandai Shift 6 Jam',
                    hidden: true,
                    ref: '../btnShift6Jam'
                },
                {
                    xtype: 'button',
                    text: 'text',
                    hidden: true,
                    ref: '../btnApprovalLembur'
                },
                {
                    xtype: 'button',
                    text: 'text',
                    hidden: true,
                    ref: '../btnApprovalLesstime'
                },
                {
                    xtype: 'button',
                    text: 'Invalidasi',
                    hidden: true,
                    ref: '../btnInvalid'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ValidasiWin.superclass.initComponent.call(this);
        this.keterangan.on('select', this.onKeteranganSelect, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnInvalid.on('click', this.onbtnInvalidclick, this);
        this.btnShift6Jam.on('click', this.onbtnShift6Jamclick, this);
        this.btnApprovalLembur.on('click', this.onbtnApprovalLemburclick, this);
        this.btnApprovalLesstime.on('click', this.onbtnApprovalLesstimeclick, this);
        this.btnRecalculateLembur.on('click', this.onbtnRecalculateLemburclick, this);
        this.btnToggleLimit.on('Click', this.btnToggleLimitClick, this);
        this.wfh.on('check', this.wfhOnCheck, this);
        this.shift.on('select', this.shiftOnSelect, this);
        this.jam_in.on('select', this.jam_inOnSelect, this);
        this.cuti_tahun.on('select', this.onbtnCutiTahunclick, this);

        if (this.modez == 0) { //create
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            this.jam_in.setReadOnly(false);
            this.jam_out.setReadOnly(false);
        } else if (this.modez == 1) { //edit invalid
            this.btnSaveClose.setVisible(false);
            this.btnApprovalLembur.setVisible(true);
            this.btnApprovalLesstime.setVisible(true);
            this.btnSave.setVisible(false);
            this.btnInvalid.setVisible(true);
            this.btnShift6Jam.setVisible(true);
            this.nolk.setVisible(true);
            this.nolk.setReadOnly(true);
        }
    },
    onbtnCutiTahunclick: function (c, r, i) {

        if (this.modez == 0)
            return;

        Ext.Ajax.request({
            url: 'Validasi/ChangeCutiTahun',
            method: 'POST',
            params: {
                validasi_id: this.id,
                cuti_tahun: r.data.cuti_tahun
            },
            success: function (f, a) {

               jun.rztValidasi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });

                // if (!response.success ) {
                //     //Ext.getCmp('form-Validasi').getForm().close();
                //     this.cuti_tahun.reset();
                // }

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },
    btnToggleLimitClick: function (t) {
        if (t.getText() == 'TIDAK') {
            t.setText('YA');
        } else {
            t.setText('TIDAK');
        }
    },
    onbtnShift6Jamclick: function () {
        var in_time = this.jam_in.getValue();
        var out_time = this.jam_out.getValue();
        Ext.Ajax.request({
            url: 'Validasi/GetShift6Jam',
            method: 'POST',
            scope: this,
            params: {
                id: this.id,
                in_time: in_time,
                out_time: out_time,
                periode_id: Ext.getCmp('periodeOnValidasiGrid').getValue(),
                param2: this.btnToggleLimit.getText()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                } else {
                    this.min_late.setValue(response.real_less_time);
                    this.min_awal.setValue(response.real_lembur_pertama);
                    this.min_akhir.setValue(response.real_lembur_akhir);
                    this.lembur_diakui.setValue(parseInt(response.real_lembur_akhir) + parseInt(response.real_lembur_pertama));
                    this.lesstime_diakui.setValue(parseInt(response.real_less_time));
                    this.lembur_hari.setValue(response.real_lembur_hari);

                    jun.rztValidasi.baseParams = {
                        pegawai_id: response.pegawai_id,
                        periode_id: this.periode_id
                    };
                    jun.rztValidasi.reload();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    jam_inOnSelect: function (t, date) {
        this.jam_out.setValue(date);
    },
    shiftOnSelect: function (combo, record, index) {
        var as = combo.store.findExact('shift_id', combo.getValue());
        var b = combo.store.getAt(as);
        if (SET_INTIME_BUAT_REVIEW2) {
            this.in_time.setValue(b.get('late_time'));
        } else {
            this.in_time.setValue(b.get('in_time'));
        }
        this.out_time.setValue(b.get('out_time'));
    },
    onKeteranganSelect: function () {
        var ket = this.keterangan.getValue();
        this.in_time.reset();
        this.out_time.reset();
        this.shift.reset();
        switch (ket) {
            case '1' :
                this.in_time.setDisabled(false);
                this.out_time.setDisabled(false);
                break;
            case '3' :
                this.in_time.setDisabled(false);
                this.out_time.setDisabled(false);
                break;
            case '10' :
                this.in_time.setDisabled(false);
                this.out_time.setDisabled(false);
                break;
            default :
                jun.rztShiftCmp.baseParams = {
                    pegawai_id: this.pegawai_id
                };
                jun.rztShiftCmp.reload({
                    callback: function(){
                        var shift = jun.rztShiftCmp.getAt(0);
                        // this.shift.setValue(shift.get('shift_id'));
                        Ext.getCmp('shift_validasi').setValue(shift.get('shift_id'));

                        // this.in_time.setDisabled(true);
                        // this.out_time.setDisabled(true);
                        Ext.getCmp('in_time_validasi').setDisabled(true);
                        Ext.getCmp('out_time_validasi').setDisabled(true);

                        if (SET_INTIME_BUAT_REVIEW2) {
                            // this.in_time.setValue(shift.get('late_time'));
                            Ext.getCmp('in_time_validasi').setValue(shift.get('late_time'));
                        }
                        else {
                            // this.in_time.setValue(shift.get('in_time'));
                            Ext.getCmp('in_time_validasi').setValue(shift.get('in_time'));
                        }

                        Ext.getCmp('out_time_validasi').setValue(shift.get('out_time'));
                    }
                });
        }
        var ketraw = this.keterangan.getRawValue();
        var ketsub = ketraw.substring(0, 4);

        if (ketraw == 'Cuti Istimewa') {
            this.wfh.setDisabled(false);
        } else {
            this.wfh.setDisabled(true);
            this.wfh.reset();
        }

        if (ketsub == 'Cuti') {
            this.cuti.setDisabled(false);
        } else {
            this.cuti.setDisabled(true);
            this.cuti.reset();
        }

        if (ketraw == 'Cuti Tahunan') {
            this.cuti_tahun.setVisible(true);
            this.cuti_tahun.allowBlank = false;

        } else {
            this.cuti_tahun.allowBlank = true;
            this.cuti_tahun.setVisible(false);
            this.cuti_tahun.reset();
        }

    },
    wfhOnCheck: function (status) {
        if(status.checked == true)
            this.nolk.setValue('WFH');
        else
            this.nolk.reset();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    onbtnInvalidclick: function () {
        Ext.Ajax.request({
            url: 'Validasi/assignValidasi',
            method: 'POST',
            scope: this,
            params: {
                validasi_id: this.id,
                PIN: this.PIN,
                periode_id: this.periode_id
            },
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
//                console.log(response);
                if (response.success == true) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnApprovalLemburclick: function () {
        var lembur = parseInt(this.min_awal.getValue()) + parseInt(this.min_akhir.getValue());
        if (this.btnApprovalLembur.getText() == 'Approve Lembur' && (parseInt(this.lembur_diakui.getValue()) > lembur || parseInt(this.lembur_diakui.getValue()) < 0)) {
            Ext.Msg.alert('Error', "Lembur yg diakui tidak valid.");
            return;
        }
        Ext.Ajax.request({
            url: 'Validasi/approveLembur',
            method: 'POST',
            scope: this,
            params: {
                lembur_diakui: this.lembur_diakui.getValue(),
                validasi_id: this.id,
                status_approval: this.approval_lembur.getValue(),
                limit: this.btnToggleLimit.getText(),
                periode_id: Ext.getCmp('periodeOnValidasiGrid').getValue()
            },
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (response.success == true) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnApprovalLesstimeclick: function () {
        var lesstime = parseInt(this.min_late.getValue());
        if (this.btnApprovalLesstime.getText() == 'Approve Lesstime' && (parseInt(this.lesstime_diakui.getValue()) > lesstime || parseInt(this.lesstime_diakui.getValue()) < 0)) {
            Ext.Msg.alert('Error', "Lesstime yg diakui tidak valid.");
            return;
        }
        Ext.Ajax.request({
            url: 'Validasi/approveLesstime',
            method: 'POST',
            scope: this,
            params: {
                lesstime_diakui: this.lesstime_diakui.getValue(),
                validasi_id: this.id,
                status_approval: this.approval_lesstime.getValue(),
                periode_id: Ext.getCmp('periodeOnValidasiGrid').getValue()
            },
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (response.success == true) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnRecalculateLemburclick: function () {
        var min_awal = this.min_awal;
        var min_akhir = this.min_akhir;
        Ext.Ajax.request({
            url: 'Validasi/recalculateLembur',
            method: 'POST',
            scope: this,
            params: {
                validasi_id: this.id,
                periode_id: Ext.getCmp('periodeOnValidasiGrid').getValue(),
                param2: this.btnToggleLimit.getText()
            },
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(f.responseText);
//                Ext.Msg.alert('Aha', response.lembur1 + ' ' + response.lembur2 + ' ' + response.lemburh);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                } else {
                    min_awal.setValue(response.lembur1);
                    min_akhir.setValue(response.lembur2);
                    this.lembur_diakui.setValue(parseInt(response.lembur1) + parseInt(response.lembur2));
                    this.min_late.setValue(response.lesstime);
                    this.lesstime_diakui.setValue(parseInt(response.lesstime));
                    this.lembur_hari.setValue(parseInt(response.lemburh));

                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    saveForm: function () {
        if (!this.keterangan.getValue()) {
            Ext.Msg.alert('Error', "Keterangan belum dipilih!");
            return;
        }
        if (!this.jam_in.getValue()) {
            Ext.Msg.alert('Error', "Tgl mulai belum dipilih!");
            return;
        }
        if (!this.jam_out.getValue()) {
            Ext.Msg.alert('Error', "Tgl selesai belum dipilih!");
            return;
        }
        if (this.keterangan.getValue() == 8) {
            // console.log(this.wfh.getValue());
            if (this.nolk.getValue().toLowerCase().includes("wfh") && this.wfh.getValue() == false) {
                Ext.Msg.alert('Error', 'Jika WFH, silahkan dicentang <p class="redInline">Tandai WFH</p>');
                return;
            }
        }
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {

            urlz = 'Validasi/update/id/' + this.id;

        } else {

            urlz = 'Validasi/create/';
        }

        Ext.getCmp('form-Validasi').getForm().submit({
            url: urlz,
            timeout: 1000,
            scope: this,
            params: {
                ket: this.keterangan.getValue(),
                periode_id: this.periode_id,
                pegawai_id: this.pegawai_id, //pin_id = pegawai_id
                PIN: this.PIN,
                in_time: this.in_time.getValue(),
                shift_id: this.shift.getValue(),
                out_time: this.out_time.getValue(),
                nosurat: this.nolk.getValue(),
                status_int: 1
            },
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Validasi').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});