jun.ValidasiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Review Presensi Fase 2",
    id: 'docs-jun.ValidasiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true,
        getRowClass: function (record, rowIndex, rp, store) {
            rp.tstyle += 'height: 120px;';
        }
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Sunday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return dayne(v, m, r, 'Sunday');
            }
        },
        {
            header: 'Monday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return dayne(v, m, r, 'Monday');
            }
        },
        {
            header: 'Tuesday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return dayne(v, m, r, 'Tuesday');
            }
        },
        {
            header: 'Wednesday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return dayne(v, m, r, 'Wednesday');
            }
        },
        {
            header: 'Thursday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return dayne(v, m, r, 'Thursday');
            }
        },
        {
            header: 'Friday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return dayne(v, m, r, 'Friday');
            }
        },
        {
            header: 'Saturday',
            sortable: true,
            resizable: true,
            width: 100,
            renderer: function (v, m, r) {
                return dayne(v, m, r, 'Saturday');
            }
        }

    ],
    initComponent: function () {
        if (jun.rztPegawaiLib.getTotalCount() === 0) jun.rztPegawaiLib.load();
        if (jun.rztPeriodeLib.getTotalCount() === 0)jun.rztPeriodeLib.load();
        if (jun.rztShiftLib.getTotalCount() === 0) jun.rztShiftLib.load();

        this.store = jun.rztValidasi;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: 'Periode:'
                },
                {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeLib,
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                            <br>\n\<b>Start: </b>{periode_start}\n\\n\
                            <br>\n\<b>End: </b>{periode_end}\n\</span>',
                            "</div></tpl>"),
                    ref: '../periode',
                    width: 200,
                    id: 'periodeOnValidasiGrid'
                },
                '-',
                {
                    xtype: 'label',
                    text: 'NIK:'
                },
                {
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    mode: 'remote',
                    triggerAction: 'query',
                    autoSelect: false,
                    minChars: 3,
                    matchFieldWidth: !1,
                    // pageSize: 20,
                    listWidth: 450,
                    lastQuery: "",
                    lazyRender: true, 
                    forceSelection: true,
                    store: jun.rztPegawaiLib,
                    valueField: 'pegawai_id',
                    displayField: 'nik',
                    itemSelector: "div.search-item", 
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<span style="width:100%;display:inline-block;"><b>{nik}</b><br>{nama_lengkap}</span>',
                            "</div></tpl>"),
                    ref: '../pin',
                    width: 200,
                    id: 'pinIdOnValidasiGrid'
                },
                '-',
                {
                    xtype: 'label',
                    text: '',
                    ref: '../labelNama' 
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Buat Review',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                '-',
                {
                    xtype: 'button',
                    text: 'Buat Multi Review',
                    iconCls: 'silk13-application_form_add',
                    ref: '../btnMultiAdd'
                    , hidden: true
                }
            ]
        };
        jun.ValidasiGrid.superclass.initComponent.call(this);
        this.pin.on('select', this.loadFP, this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnMultiAdd.on('Click', this.loadFormMulti, this);
        this.on('celldblclick', this.celldc, this);
        this.on('beforedestroy', this.beforeDestroy, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadFP: function (c, r, i) { 
        jun.rztShiftCmp.baseParams = {
            pegawai_id: this.pin.getValue()
        };       
        jun.rztShiftCmp.reload();
        this.labelNama.setText(r.get('nama_lengkap'));

        if (this.periode.getValue() == '') {
            Ext.MessageBox.alert("Warning", "Anda belum memilih periode!");
            this.pin.setValue("");
            return;
        }
        this.pin_number = c.getValue();

        this.store.baseParams = {
            pegawai_id: this.pin_number,
            periode_id: this.periode.getValue(),
            PIN: this.pin.getRawValue()
        };
        jun.rztValidasi.reload();
    },
    loadForm: function (tgl) {
        if (!this.pin.getValue()) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih NIK!");
            return;
        } 
        var form = new jun.ValidasiWin({
            modez: 0,
            periode_id: this.periode.getValue(),
            pegawai_id: this.pin_number,
            PIN: this.pin.getRawValue() 
        });

        // console.log("tgl length " + Object.values(tgl).length);
        if(Object.values(tgl).length < 11){
            form.jam_in.setValue(tgl);
            form.jam_out.setValue(tgl);
        }
        form.in_time.setDisabled(true);
        form.out_time.setDisabled(true);
        form.show();
    },
    loadFormMulti: function () {
        // if (isTodayBeforeDate(new Date(2021, 3, 20)) == true){
        //     Ext.MessageBox.alert("Warning", "Menu bisa digunakan pada 2021-04-21, silahkan kembali lagi nanti. ");
        //     return;
        // }
        if (!this.pin.getValue()) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih NIK!");
            return;
        }
        var form = new jun.ValidasiMultiWin({modez: 0});
        form.show();
        form.setTitle('Review NIK ' + this.pin.getRawValue());
    },
    celldc: function (grid, rowIndex, columnIndex, e) {
        var record = this.store.getAt(rowIndex), hari;
        switch (columnIndex) {
            case 0 : hari = 'Sunday'; break;
            case 1 : hari = 'Monday'; break;
            case 2 : hari = 'Tuesday'; break;
            case 3 : hari = 'Wednesday'; break;
            case 4 : hari = 'Thursday'; break;
            case 5 : hari = 'Friday'; break;
            case 6 : hari = 'Saturday'; break;
        }
        this.recordFP = record;
        this.dataFP = record.get(hari); 
        if (this.dataFP == '') {
            return;
        }
        if (Object.values(this.dataFP).length < 4) {
            this.loadForm(this.dataFP.tgl);
        } else {
            this.loadEditForm();
        }
    },
    loadEditForm: function () { 
        if (this.dataFP == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }
        var form = new jun.ValidasiWin({
            modez       : 1,
            height      : 420,
            title       : (this.actEdit ? "Ubah" : "Lihat") + " Data Presensi",
            iconCls     : this.actEdit ? 'silk13-pencil' : 'silk13-eye',
            id          : this.dataFP.validasi_id,
            jamin       : this.dataFP.in_time,
            jamout      : this.dataFP.out_time,
            PIN         : this.dataFP.PIN,
            periode_id  : this.periode.getValue()

        });
        form.show(this); 
        form.jam_in.setValue(this.dataFP.in_time);
        form.jam_out.setValue(this.dataFP.out_time);
        form.shift.setValue(this.dataFP.shift_id);
        form.nolk.setValue(this.dataFP.no_surat_tugas);
        form.min_late.setValue(this.dataFP.real_less_time);
        form.min_awal.setValue(this.dataFP.min_over_time_awal_real);
        form.min_akhir.setValue(this.dataFP.min_over_time_real);
        form.lembur_hari.setValue(this.dataFP.lembur_hari);

        if(this.dataFP.limit_lembur == 0){
            form.btnToggleLimit.setText('TIDAK');
        } else {
            form.btnToggleLimit.setText('YA');
        }
        
        var lembur_menit = parseInt(this.dataFP.min_over_time_real) + parseInt(this.dataFP.min_over_time_awal_real);
        var lembur = '';
        var text = '';
        if(this.dataFP.approval_lembur == 0){
            lembur      = 'Disapproved';
            text        = 'Approve Lembur';
        } else {
            lembur      = 'Approved';
            text        = 'Disapprove Lembur';
        }
        form.approval_lembur.setValue(lembur);
        form.btnApprovalLembur.setText(text);
        form.lembur_diakui.setValue(lembur_menit);
        
        if(this.dataFP.approval_lembur == 0 || this.dataFP.approval_lesstime == 1){
           form.btnRecalculateLembur.setVisible(true);
        } else 
            if(this.dataFP.approval_lembur == 1 || this.dataFP.approval_lesstime == 0){
           form.btnRecalculateLembur.setVisible(false);
        }
    
        var lesstime_menit = parseInt(this.dataFP.real_less_time);
        var lesstime    = '';
        var text        = '';
        if(this.dataFP.approval_lesstime == 1){
            lesstime    = 'Approved';
            text        = 'Disapprove Lesstime';
        } else {
            lesstime    = 'Disapproved';
            text        = 'Approve Lesstime';
        }
        form.approval_lesstime.setValue(lesstime);
        form.btnApprovalLesstime.setText(text);
        form.lesstime_diakui.setValue(lesstime_menit);

        /*
         * 1 : cuti biasa
         * 2 : cuti mendadak
         */
        if (this.dataFP.cuti == 1 || this.dataFP.cuti == 2) {
            form.cuti_tahun.setVisible(true);
            form.cuti_tahun.setValue(this.dataFP.cuti_tahun);
        }
    },
    beforeDestroy: function () {
        this.store.removeAll();
        this.pin.setValue(undefined);
    }
});

function dayne(v, m, r, dayname) {
    var stt;
    var x = r.get(dayname), y = x.tgl_hari ? '<b>' + x.tgl_hari + '</b>' : '';
    switch (x.kode_ket) {
        case '1':
            stt = ' LK';
            break;
        case '2':
            stt = ' Sakit';
            break;
        case '3':
            stt = ' Lupa Absen';
            break;
        case '4':
            stt = ' OFF';
            break;
        case '5':
            stt = ' Cuti Tahunan';
            break;
        case '6':
            stt = ' Cuti Menikah';
            break;
        case '7':
            stt = ' Cuti Bersalin';
            break;
        case '8':
            stt = ' Cuti Istimewa';
            break;
        case '9':
            stt = ' Cuti Non Aktif';
            break;        
        case '10':
            stt = ' Presentasi / Pameran';
            break;        
        default:
            stt = ' Hadir';
    }
    ;    
    if (x.PIN) {
        if(x.kode_ket == 0 || x.kode_ket == 1 || x.kode_ket == 3 || x.kode_ket == 10){
            y += '</br>In : <span style="color: blue;">' + x.in_time +
                '</span></br>Out : <span style="color: blue;">' + x.out_time;
            if(x.real_less_time != 0){
                y +=  '</span></br>Less time : <span style="color: blue;">' + x.real_less_time + '</span> Menit<span style="color: blue;">' +
                    '</span></br><b>Less time</b> : ' +
                    (x.approval_lesstime == 0 ? ' <span style="color: #ff2600;">Disapproved</span>' : ' <span style="color: #ad0697;">Approved</span>');
            }
            if(x.min_over_time_awal_real != 0){
                y += '</span></br>Lembur 1: <span style="color: blue;">' + x.min_over_time_awal_real + '</span> Menit<span style="color: blue;">';
            }
            if(x.min_over_time_real != 0){
                y += '</span></br>Lembur 2: <span style="color: blue;">' + x.min_over_time_real + '</span> Menit<span style="color: blue;">';
            }
            if(x.lembur_hari != 0){
                y += '</span></br>Lembur Hari: <span style="color: blue;">' + x.lembur_hari + '</span> Hari<span style="color: blue;">';
            }
            if(x.min_over_time_awal_real != 0 || x.min_over_time_real != 0 || x.lembur_hari != 0){
                y += '</span></br><b>Lembur</b> : ' +
                    (x.approval_lembur == 0 ? ' <span style="color: #ff2600;">Disapproved</span>' : ' <span style="color: #ad0697;">Approved</span>');
            }
        }
        y += '</span></br>Status : ' + stt;
        if(x.no_surat_tugas != null){
            y += '</span></br>Note : <span style="color: blue;">' + x.no_surat_tugas;
        }
        if(x.cuti == 2){
            y += '</span></br>Cuti mendadak : Ya <span style="color: blue;">';
        }
        if(x.cuti_tahun != null){
            y += '</span></br>Cuti Tahun : <span style="color: blue;">' + x.cuti_tahun;
        }
        if(x.short_shift == 1){
         y += '</span></br>Shift 6 Jam';
        }
        if(x.status_wfh == 1){
         y += '</span></br>Work From Home';
        }
    }
    ;
    if (x.status_int == 1 && x.kode_ket == 0) {
        m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 1) {
        m.style = "background-color: #f6ff99; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 2) {
        m.style = "background-color: #ff9999; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 3) {
        m.style = "background-color: #99eaff; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 4) {
        m.style = "background-color: #c6a7a7; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 5) {
        m.style = "background-color: #fd99ff; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 6) {
        m.style = "background-color: #fd99ff; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 7) {
        m.style = "background-color: #fd99ff; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 8) {
        m.style = "background-color: #fd99ff; border: solid 1px #A5A5A5";
    } else
    if (x.status_int == 1 && x.kode_ket == 9) {
        m.style = "background-color: #fd99ff; border: solid 1px #A5A5A5";
    }
    if (x.status_int == 1 && x.kode_ket == 10) {
        m.style = "background-color: #fcda7e; border: solid 1px #A5A5A5";
    }
    return y;
}

jun.UploadWin = Ext.extend(Ext.Window, {
    title: '',
    modez: 1,
    width: 400,
    height: 150,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Upload',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                fileUpload: true,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Periode',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib,
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                            <br>\n\<b>Start: </b>{periode_start}\n\\n\
                            <br>\n\<b>End: </b>{periode_end}\n\</span>',
                                "</div></tpl>"),
                        ref: '../periode',
                        width: 200
                    },
                    {
                        xtype: 'fileuploadfield',
                        fieldLabel: 'Dokumen (XLS)',
                        hideLabel: false,
                        name: 'dokumen',
                        buttonText: '',
                        buttonCfg: {
                            iconCls: 'silk13-page_add'
                        },
                        ref: '../dokumen',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: true,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Upload',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.UploadWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function ()
    {
        this.btnDisabled(true);
        Ext.getCmp('form-Upload').getForm().submit({
            url: 'Site/Upload',
            timeout: 1000,
            scope: this,
            params: {
                periode_id: this.periode.getValue()
            },
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});