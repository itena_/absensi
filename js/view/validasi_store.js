jun.Validasistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Validasistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ValidasiStoreId',
            url: 'Validasi',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'validasi_id'},
                {name: 'pin_id'},
                {name: 'PIN'},
                {name: 'in_time'},
                {name: 'out_time'},
                {name: 'tdate'},
                {name: 'tipe_data'},
                {name: 'user_id'},
                {name: 'status_int'},                
                {name: 'status_pegawai_id'},
                {name: 'no_surat_tugas'},
                {name: 'min_early_time'},
                {name: 'min_late_time'},
                {name: 'min_least_time'},
                {name: 'min_over_time'},
                {name: 'min_over_time_awal'},
                {name: 'shift_id'},
                {name: 'Sunday'},
                {name: 'Monday'},
                {name: 'Tuesday'},
                {name: 'Wednesday'},
                {name: 'Thursday'},
                {name: 'Friday'},
                {name: 'Saturday'},
                {name: 'Saturday'},
                {name: 'min_over_time_awal_real'},
                {name: 'min_over_time_real'},
                {name: 'lembur_hari'},
                {name: 'real_less_time'},
                {name: 'approval_lembur'},
                {name: 'approval_lesstime'},
                {name: 'cuti'},
                {name: 'cuti_tahun'},
                {name: 'status_wfh'},
                {name: 'limit_lembur'},
                {name: 'lanjut'},
                {name: 'keterangan_id'},
                {name: 'nama_ket'}
            ]
        }, cfg));
    }
});
jun.rztValidasi = new jun.Validasistore();
jun.rztValidasiMultiDetails = new jun.Validasistore();