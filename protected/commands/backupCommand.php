<?php

class backupCommand extends CConsoleCommand
{

    public function run(){
        self::BackupDb();
    }

    private function BackupDb()
    {
        ini_set('max_execution_time', 600);

        $p      = SysPrefs::model()->get_val('POS_SEPARATOR');
        $conn   = Yii::app()->db;
        $user   = $conn->username;
        $pass   = $conn->password;
        $db     = '';

        if ( preg_match( '/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result ) ) {
            list( $all, $host, $db, $port ) = $result;
        }

        $basepath = SysPrefs::model()->get_val('POS_BACKUP_PATH');
        if (!$basepath || $basepath == '0') {
            echo $basepath . '  POS_BACKUP_PATH tidak ada.';
        }

        $dir_backup  = $basepath . $p;
        $date        = date("Ymd");
        $backup_name = SysPrefs::model()->get_val('POS_APP_NAME') . "_" . $db . '.sql';
        $filename    = $date . "_" . $backup_name;
        $backup_file = $dir_backup . $filename;

        $mysqldump   = SysPrefs::model()->get_val('POS_MYSQLDUMP_PATH');
        if (!$mysqldump || $mysqldump == '0') {
            $mysqldump = 'mysqldump';
        }

        $sshIp = SysPrefs::model()->get_val('SSH_IP');
        $useSshPass = SysPrefs::model()->get_val('USE_SSHPASS');
        $sshPass = "$useSshPass ssh $sshIp";
        $command = "$sshPass '$mysqldump -u $user -p$pass 2>&1 $db > $backup_file'";

        exec($command, $output, $return_var);

        if($sshIp != '' && $useSshPass != '') {
            exec("$useSshPass scp $sshIp:$backup_file $dir_backup");
            exec("$sshPass rm $backup_file");
        }

        $size = filesize($backup_file);
        if (isset($size) && $size > 0) {
            $zip = new ZipArchive();
            if ($zip->open("$backup_file.zip", ZIPARCHIVE::CREATE)) {
                $zip->addFile($backup_file, $filename);
                $zip->close();
            }
            unlink($dir_backup . $p . $filename);
        }

        foreach(glob($dir_backup.'*.zip') as $file) {
            $file_date = substr(str_replace($dir_backup, '', $file), 0, 8);
            if($file_date <= date('Ymd', strtotime('-10 days'))) {
                unlink($file);
            }
        }

        ini_set('max_execution_time', 30);
        echo $dir_backup . $p . $filename . ' berhasil di backup';
        Yii::app()->end();
    }
}