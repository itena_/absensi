<?php

/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 8/6/2015
 * Time: 14:46
 */

Yii::import('application.components.U');

class darwinboxCommand extends CConsoleCommand {

    public function actionPushattendancelog() {
        U::runCommand('darwinbox', 'Pushattendance','', 'darwinbox.log');
    }
    public function actionPushattendance()
    {
        $success = true;
        $message = 'ok';

        try {

            $data = Fp::getFpToDarwinbox();

            if (empty($data)) {
                throw new Exception('tidak ada data yang perlu di push');
            }

            // Request access token from API Gateway
            $token = CurlHelper::requestTokenGatewayApi();

            if (isset($token->access_token)) {

                $hasil = $nik_arr = $attendance = array();

                foreach ($data as $row) {
                    $nik = $row['PIN'];
                    $nik_arr[$nik] = '';
                }

                foreach ($nik_arr as $key => $value) {

                    $detail = array();
                    foreach ($data as $row) {

                        if ($row['PIN'] == $key) {
                            $detail[] = array(
                                'id' => $row['fp_id'],
                                'timestamp' => $row['DateTime_'],
                                'machineid' => $row['kode_ip'],
                                'status' => 1,
                            );

                            $hasil[$key] = $detail;
                        }
                    }
                }
                $attendance['attendance'] = $hasil;

                $json_data = json_encode($attendance);

                $raw_data = [
                    'data' => $json_data
                ];
                // Send the request to the API Gateway
                $ret = CurlHelper::SendAttedanceToGateWayApi($raw_data, $token->access_token);

                if (isset($ret->success) && $ret->success == true) {
                    $hasil_darwin = json_decode($ret->message, true);
                    $msg = $hasil_darwin['data'];

                    if (isset($msg['status']) && $msg['status'] == 1) {
                        foreach ($msg['attendance_status'] as $key_nik => $value) {

                            if ($value['status'] == 1) {
                                foreach ($hasil as $keyy_nik => $row) {
                                    /*
                                     * if nik yang sama
                                     */
                                    if ($keyy_nik == $key_nik) {
                                        foreach ($row as $roww) {
                                            $dataFp = Fp::model()->findByPk($roww['id']);
                                            $dataFp->Verified = -1;
                                            if (!$dataFp->update()) {
                                                throw new Exception($dataFp->getErrors());
                                            }
                                        }
                                    }
                                }
                                var_dump($key_nik . ' - ' . $value['message']);
                            } else {
                                var_dump($key_nik . ' - ' . $value['message']);
                            }
                        }
                    } else {
                        throw new Exception(json_encode($ret));
                    }

                } else {
                    throw new Exception(json_encode($ret));
                }


            } else {
                $failed = json_encode($token, JSON_PRETTY_PRINT);
                throw new Exception('Tidak dapat terhubung dengan API Gateway ' . $failed);
            }


        } catch (Exception $ex) {
            $success = false;
            $message = $ex->getMessage();
        }

        $tgl = date('Y-m-d H:i:s');

        //if (!$success)
            var_dump($tgl . ' - ' . $message);
    }

}
