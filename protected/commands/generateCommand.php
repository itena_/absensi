<?php

/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 8/6/2015
 * Time: 14:46
 */

//Yii::import('application.components.SoapClientYii');
//Yii::import('application.components.GenerateLembur');
class GenerateCommand extends CConsoleCommand
{
    public function actionLembur($store) {
        try {
            $vv = ValidasiView::model()->findAllByAttributes(array('kode_cabang' => $store));
            foreach ($vv as $v){
                $duras = Fp::model()->durasi($v['pin_id'], $v['shift_id'], $v['in_time'], $v['out_time'], '','');
                $validasi = Validasi::model()->findByPk($v['validasi_id']);
                
                $validasi->min_early_time = $duras['min_early'];
                $validasi->min_late_time = $duras['min_late'];
                $validasi->min_least_time = $duras['min_least'];
                $validasi->min_over_time = $duras['min_over'];
                $validasi->min_over_time_awal = $duras['min_over_sejam_awal'];
                $validasi->save();
            }
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
        }        
    }
}
