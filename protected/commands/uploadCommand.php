<?php

/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 8/6/2015
 * Time: 14:46
 */

Yii::import('application.vendors.*');
require 'tad/TADFactory.php';
require 'tad/TAD.php';
require 'tad/TADResponse.php';
require 'tad/Providers/TADSoap.php';
require 'tad/Providers/TADZKLib.php';
require 'tad/Exceptions/ConnectionError.php';
require 'tad/Exceptions/FilterArgumentError.php';
require 'tad/Exceptions/UnrecognizedArgument.php';
require 'tad/Exceptions/UnrecognizedCommand.php';
Yii::import('application.components.U');
Yii::import('application.components.TarikAbsenAll');
Yii::import('application.components.ValidasiData');

class uploadCommand extends CConsoleCommand {

    public function run($args) {

        if (isset($args[1])) {
            $param1 = $args[1];
        }

        if (isset($args[2])) {
            $param2 = $args[2];
        }
        
        if (isset($args[3])) {
            $param3 = $args[3];
        }

        $s = new ValidasiData;
//        $a = new TarikAbsen;
        $al = new TarikAbsenAll;
        switch ($args[0]) {
            case '-1' :
                $al->AbsensiAll();
                break;
            case '-2' :
                $s->step0($param1);
                break;
            case '-3' :
                $al->AbsensiPerTgl($param1,$param2,$param3);
                break;
            case '-4' :
                $al->AbsensiAllExist();
                break;
            case '-5' :
                $al->AbsensiAllDateExist($param1,$param2);
                break;
        }
    }

}
