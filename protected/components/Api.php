<?php
class Api extends GxController
{
    public static function sinkronEDM($params = []) {
        $msg     = "";
        $success = true;
        $output  = 'Gagal';
        try {
            $output = CurlHelper::sinkronEDM(
                [
                    'id' => $params['id'],
                    'action' => $params['action']
                ]);
            if (isset($output)) {
                $success = true;
            }
        } catch (Exception $ex) {
            $success = false;
            $msg = $ex->getMessage();
        }

        return $output;
    }
    public static function mutasiPegawai($params = []) {
        $msg     = "";
        $success = true;
        $output  = 'Gagal';

        $periode = Periode::model()->findByPk($params['periode_id']);
        $start = $periode->periode_start;
        $end = $periode->periode_end;

        try {
            $output = CurlHelper::mutasiPegawai(
                [
                    'pegawai' => $params['pegawai'],
                    'cabang_id' => $params['cabang_id'],
                    'start' => $start,
                    'end' => $end
                ]);
            if (isset($output)) {
                $success = true;
            }
        } catch (Exception $ex) {
            $success = false;
            $msg = $ex->getMessage();
        }

        return $output;
    }
}
