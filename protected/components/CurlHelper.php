<?php

class CurlHelper
{
    Const APPLICATION_ID = 'B7076B5F-269E-11E8-8341-6DB33A098066';

    public static function callAPI($method, $url, $data = [], $header = [], $timeOut = false, $proxy = false, $is_raw = false){
        $curl = curl_init();
        curl_reset($curl);
        $timeOut = SysPrefs::model()->get_val('POS_TIMEOUT_API');
        $proxy = SysPrefs::model()->get_val('POS_PROXY_API');
        if($proxy === 'false') {$proxy = false;}
        if ($is_raw)
            $param = $data;
        else
            $param = count($data) ? http_build_query($data) : "";
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($param)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($param)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
                break;
            default:
                if ($param)
                    $url = $url.($param? "?".$param : "");
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        if($timeOut !== false)
            curl_setopt($curl, CURLOPT_TIMEOUT, $timeOut);

        if($proxy !== false)
            curl_setopt($curl, CURLOPT_PROXY, $proxy);

        $result = curl_exec($curl);
        $curlErrNo = curl_errno($curl);
        if ($curlErrNo)
            $result = "{ \"status\": $curlErrNo, \"message\": \"".curl_error($curl)."\" }";

        curl_close($curl);

        return json_decode($result);
    }

    public static function requestToken($store = null){
        $url = SysPrefs::model()->get_val('POS_URL_API');
        $username = SysPrefs::model()->get_val('POS_USERNAME_API');
        $password = SysPrefs::model()->get_val('POS_PASSWORD_API');
      
        $result = self::callAPI(
            'POST',
            $url . 'login',
            [
                'username' => $username,
                'password' => $password
            ]
        );
        return $result;
    }

    public static function SyncEmployeeFromAmos($nik,$store,$token){
        $url = SysPrefs::model()->get_val('POS_URL_API');
        $url2 = 'Api/SyncEmployeeFromAmos';
        $result = self::callAPI(
            'POST',
            $url . $url2,
            [
                'nik' => $nik,
                'store' => $store
            ],
            [
                "token: $token"
            ]
        );
        return $result;
    }

    public static function sinkronEDM($params){
        $url = SysPrefs::model()->get_val('POS_EDM_API');
        $url2 = $params['action'];
        $key = strpos(strtolower($params['action']), 'amoscabang') ? 'cabang_id' : 'bu_id';
        $result = self::callAPI(
            'POST',
            $url . $url2,
            [
                $key => $params['id']
            ],
            ['Authorization: ' . self::APPLICATION_ID]
        );
        return $result;
    }

    public static function mutasiPegawai($params){
        $url = SysPrefs::model()->get_val('POS_EDM_API');
        $url2 = 'GetMutasiPegawai';
        $result = self::callAPI(
            'POST',
            $url . $url2,
            [
                'pegawai' => $params['pegawai'],
                'cabang_id' => $params['cabang_id'],
                'start' => $params['start'],
                'end' => $params['end']
            ],
            ['Authorization: ' . self::APPLICATION_ID]
        );
        return $result;
    }

    public static function GetPegawaiLintasBu($params){
        $url = SysPrefs::model()->get_val('POS_EDM_API');
        $url2 = 'GetAllFromAmos';
        $result = self::callAPI(
            'POST',
            $url . $url2,
            [
                'bu_nama_alias' => $params['bu_nama_alias'],
                'nik_display' => $params['nik_display']
            ],
            ['Authorization: ' . self::APPLICATION_ID]
        );
        return $result;
    }

    public static function requestTokenGatewayApi()
    {
        $url = SysPrefs::get_val('GATEWAY_URL_API') . 'api/v1/oauth/token';
        $username = SysPrefs::get_val('GATEWAY_USERNAME_API');
        $password = SysPrefs::get_val('GATEWAY_PASSWORD_API');
        $credential = base64_encode($username . ':' . $password);

        $result = self::callAPI(
            'POST',
            $url,
            [
                'grant_type' => 'client_credentials'
            ],
            [
                'Authorization: Basic ' . $credential
            ]
        );
        return $result;
    }
    public static function SendAttedanceToGateWayApi($raw_data, $token)
    {
        $url = SysPrefs::get_val('GATEWAY_URL_API');
        $url2 = 'api/v1/darwinbox/sendattedance';
        $result = self::callAPI(
            'POST',
            $url . $url2,
            $raw_data,
            [
                "Authorization: Bearer $token"
            ],
            false,
            false,
            true
        );
        return $result;
    }
  
}