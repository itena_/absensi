<?php
class MenuTree
{
    var $security_role;
    var $menu_users = array(
        'text' => 'User Manajement',
        'id' => 'jun.UsersGrid',
        'leaf' => true
    );
    var $security = array(
        'text' => 'Security Roles',
        'id' => 'jun.SecurityRolesGrid',
        'leaf' => true
    );
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
    }
    function getChildMaster()
    {
        $child = array();
        if (in_array(128, $this->security_role)) {
            $child[] = array(
                'text' => 'Bisnis Unit',
                'id' => 'jun.BuGrid',
                'leaf' => true
            );
        }
        if (in_array(115, $this->security_role)) {
            $child[] = array(
                'text' => 'Area',
                'id' => 'jun.AreaGrid',
                'leaf' => true
            );
        }
        if (in_array(125, $this->security_role)) {
            $child[] = array(
                'text' => 'Cabang',
                'id' => 'jun.CabangGrid',
                'leaf' => true
            );
        }
        if (in_array(117, $this->security_role)) {
            $child[] = array(
                'text' => 'Leveling',
                'id' => 'jun.LevelingGrid',
                'leaf' => true
            );
        }
        if (in_array(118, $this->security_role)) {
            $child[] = array(
                'text' => 'Golongan',
                'id' => 'jun.GolonganGrid',
                'leaf' => true
            );
        }
        if (in_array(124, $this->security_role)) {
            $child[] = array(
                'text' => 'Jabatan',
                'id' => 'jun.JabatanGrid',
                'leaf' => true
            );
        }
        if (in_array(116, $this->security_role)) {
            $child[] = array(
                'text' => 'Status',
                'id' => 'jun.StatusGrid',
                'leaf' => true
            );
        }

        if (in_array(123, $this->security_role)) {
            $child[] = array(
                'text' => 'Status Pegawai',
                'id' => 'jun.StatusPegawaiGrid',
                'leaf' => true
            );
        }
        if (in_array(101, $this->security_role)) {
            $child[] = array(
                'text' => 'Shift',
                'id' => 'jun.ShiftGrid',
                'leaf' => true
            );
        }
        if (in_array(102, $this->security_role)) {
            $child[] = array(
                'text' => 'IP',
                'id' => 'jun.IpGrid',
                'leaf' => true
            );
        }
        if (in_array(103, $this->security_role)) {
            $child[] = array(
                'text' => 'Keterangan',
                'id' => 'jun.KeteranganGrid',
                'leaf' => true
            );
        }        

        if (in_array(129, $this->security_role)) {
            $child[] = array(
                'text' => 'Jenis Periode',
                'id' => 'jun.JenisPeriodeGrid',
                'leaf' => true
            );
        }
        if (in_array(105, $this->security_role)) {
            $child[] = array(
                'text' => 'Periode',
                'id' => 'jun.PeriodeGrid',
                'leaf' => true
            );
        }
        if (in_array(122, $this->security_role)) {
            $child[] = array(
                'text' => 'Pegawai',
                'id' => 'jun.PegawaiGrid',
                'leaf' => true
            );
        }
        if (in_array(122, $this->security_role)) {
            $child[] = array(
                'text' => 'Pegawai Non Aktif',
                'id' => 'jun.PegawaiNonAktifGrid',
                'leaf' => true
            );
        }
        if (in_array(130, $this->security_role)) {
            $child[] = array(
                'text' => 'Alamat AMOS',
                'id' => 'jun.AmosAddressGrid',
                'leaf' => true
            );
        }
        if (in_array(130, $this->security_role)) {
            $child[] = array(
                'text' => 'Pegawai Case Khusus',
                'id' => 'jun.PegawaiSpesialGrid',
                'leaf' => true
            );
        }
        if (in_array(126, $this->security_role)) {
            $child[] = array(
                'text' => 'Status HK',
                'id' => 'jun.StatusHkGrid',
                'leaf' => true
            );
        }

        return $child;
    }
    function getMaster($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Master',
            'expanded' => false,
            'children' => $child
        );
    }
    function getTransaction($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Transaction',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildTransaction()
    {
        $child = array();
        if (in_array(224, $this->security_role)) {
            $child[] = array(
                'text' => 'Assign Shift',
                'id' => 'jun.AssignShiftGrid',
                'leaf' => true
            );
        }
        if (in_array(223, $this->security_role)) {
            $child[] = array(
                'text' => 'Review Presensi Fase 1',
                'id' => 'jun.FpGrid',
                'leaf' => true
            );
        }
        if (in_array(225, $this->security_role)) {
            $child[] = array(
                'text' => 'Review Presensi Fase 2',
                'id' => 'jun.ValidasiGrid',
                'leaf' => true
            );
        }
        if (in_array(226, $this->security_role)) {
            $child[] = array(
                'text' => 'Review Off',
                'id' => 'jun.OffGrid',
                'leaf' => true
            );
        }
        if (in_array(227, $this->security_role)) {
            $child[] = array(
                'text' => 'Absensi Payroll',
                'id' => 'jun.PayrollAbsensiGrid',
                'leaf' => true
            );
        }
        if (in_array(228, $this->security_role)) {
            $child[] = array(
                'text' => 'Lock Payroll',
                'id' => 'jun.LockPayroll',
                'leaf' => true
            );
        }
        if (in_array(229, $this->security_role)) {
            $child[] = array(
                'text' => 'Recalculate All Lembur / LessTime',
                'id' => 'jun.RecalculatedAllLemburGrid',
                'leaf' => true
            );
        }
        if (in_array(230, $this->security_role)) {
            $child[] = array(
                'text' => 'Perbantuan Pegawai',
                'id' => 'jun.TransferPegawaiGrid',
                'leaf' => true
            );
        }
        if (in_array(234, $this->security_role)) {
            $child[] = array(
                'text' => 'Status HK Per Periode',
                'id' => 'jun.StatusHkPeriodeGrid',
                'leaf' => true
            );
        }
        if (in_array(231, $this->security_role)) {
            $child[] = array(
                'text' => 'Post & Lock Presensi',
                'id' => 'jun.LockPostGrid',
                'leaf' => true
            );
        }
        if (in_array(232, $this->security_role)) {
            $child[] = array(
                'text' => 'List Import Data',
                'id' => 'jun.ListImportData',
                'leaf' => true
            );
        }
        if (in_array(299, $this->security_role)) {
            $child[] = array(
                'text' => 'Attendance',
                'id' => 'jun.AMOSAttendance',
                'leaf' => true
            );
        }
        return $child;
    }
    function getReport($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Report',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildReport()
    {
        $child = array();
        if (in_array(311, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Presensi',
                'id' => 'jun.ReportAbsen',
                'leaf' => true
            );
        }
        if (in_array(312, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Presensi (Jam)',
                'id' => 'jun.ReportAbsenjam',
                'leaf' => true
            );
        }
        if (in_array(308, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Presensi All',
                'id' => 'jun.ReportAbsenAll',
                'leaf' => true
            );
        }
//        if (in_array(304, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Report Rekap Presensi',
//                'id' => 'jun.ReportRekapAbsensi',
//                'leaf' => true
//            );
//        }
        if (in_array(301, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Finger Print',
                'id' => 'jun.ReportFp',
                'leaf' => true
            );
        }
        if (in_array(301, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Finger Print Real',
                'id' => 'jun.ReportFpReal',
                'leaf' => true
            );
        }
        if (in_array(301, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Finger Print Mix',
                'id' => 'jun.ReportFpMix',
                'leaf' => true
            );
        }
        if (in_array(313, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Finger Print After Validasi',
                'id' => 'jun.ReportFpValidasi',
                'leaf' => true
            );
        }
        if (in_array(307, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Shift',
                'id' => 'jun.ReportShift',
                'leaf' => true
            );
        }
        if (in_array(310, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Presensi Shift',
                'id' => 'jun.ExportShift',
                'leaf' => true
            );
        }
        if (in_array(302, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Rekap Pegawai',
                'id' => 'jun.ReportRekapPegawai',
                'leaf' => true
            );
        }
        if (in_array(306, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Less Time',
                'id' => 'jun.ReportLessTime',
                'leaf' => true
            );
        }
        if (in_array(306, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Lupa Absen',
                'id' => 'jun.ReportLupaAbsen',
                'leaf' => true
            );
        }
        if (in_array(309, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Rekap Cuti',
                'id' => 'jun.ReportRekapCuti',
                'leaf' => true
            );
        }
        if (in_array(310, $this->security_role)) {
            $child[] = array(
                'text' => 'Report KPI',
                'id' => 'jun.ReportKpi',
                'leaf' => true
                );
            }
        if (in_array(308, $this->security_role)) {
            $child[] = array(
                'text' => 'Report Time Sheet AX',
                'id' => 'jun.TimeSheetAX',
                'leaf' => true
            );
        }
        return $child;
    }
    function getAmos($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'AMOS',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildAmos()
    {
        $child = array();
        if (in_array(501, $this->security_role)) {
            $child[] = array(
                'text' => 'AMOS Smart Attendance Prediction',
                'id' => 'jun.AmosSap',
                'leaf' => true
            );
        }
        return $child;
    }
    function getAdministration($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Administration',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildAdministration()
    {
        $child = array();
        if (in_array(401, $this->security_role)) {
            $child[] = array(
                'text' => 'User Management',
                'id' => 'jun.UsersGrid',
                'leaf' => true
            );
        }
        if (in_array(402, $this->security_role)) {
            $child[] = array(
                'text' => 'Security Roles',
                'id' => 'jun.SecurityRolesGrid',
                'leaf' => true
            );
        }
        if (in_array(401, $this->security_role)) {
            $child[] = array(
                'text' => 'System Preferences',
                'id' => 'jun.SysPrefsGrid',
                'leaf' => true
            );
        }
        if (in_array(403, $this->security_role)) {
            $child[] = array(
                'text' => 'Backup / Restore',
                'id' => 'jun.BackupRestoreWin',
                'leaf' => true
            );
        }
        if (in_array(404, $this->security_role)) {
            $child[] = array(
                'text' => 'RESET ABSENSI',
                'id' => 'jun.ResetAbsensiWin',
                'leaf' => true
            );
        }
        if (in_array(404, $this->security_role)) {
            $child[] = array(
                'text' => 'Buat Simulasi Fase1',
                'id' => 'jun.CreateSimulasiFase1Win',
                'leaf' => true
            );
        }
        return $child;
    }
    function getGeneral()
    {
        $username = Yii::app()->user->name;
        $child = array();
        if (in_array(001, $this->security_role)) {
            $child[] = array(
                'text' => 'Change Password',
                'id' => 'jun.PasswordWin',
                'leaf' => true
            );
        }
        if (in_array(002, $this->security_role)) {
            $child[] = array(
                'text' => "Logout ($username)",
                'id' => 'logout',
                'leaf' => true
            );
        }
        return $child;
    }
    public function get_menu()
    {
        $data = array();
        $master = self::getMaster(self::getChildMaster());
        if (!empty($master)) {
            $data[] = $master;
        }
        $trans = self::getTransaction(self::getChildTransaction());
        if (!empty($trans)) {
            $data[] = $trans;
        }
        $report = self::getReport(self::getChildReport());
        if (!empty($report)) {
            $data[] = $report;
        }
        $amos = self::getAmos(self::getChildAmos());
        if (!empty($amos)) {
            $data[] = $amos;
        }
        $adm = self::getAdministration(self::getChildAdministration());
        if (!empty($adm)) {
            $data[] = $adm;
        }

        $username = Yii::app()->user->name;
        $version  = SysPrefs::get_val('AMOS_VERSION');

        if (in_array(001, $this->security_role)) {
            $data[] = array(
                'text' => 'Change Password',
                'id' => 'jun.PasswordWin',
                'leaf' => true
            );
        }
        if (in_array(002, $this->security_role)) {
            $data[] = array(
                'text' => 'Logout ( <b><p style="color:black;display:inline;">' . $username . ' </p></b>)',
                'id' => 'logout',
                'leaf' => true
            );
        }
        if (in_array(002, $this->security_role)) {
            $data[] = array(
                'text' => $version == "" ? "" :
                    '<b><p style="color:red;padding-left: 15px;">AMOS Version ' . $version .'</p></b>'
                    . '<b><p style="color:green;padding-left: 18px;font-size: 10px"> Click for AMOS Tutorials.</p></b>',
                'id' => 'jun.LinkWin',
                'leaf' => true
            );
        }
        return CJSON::encode($data);
    }
}
