<?php

Yii::import('application.vendors.*');
require 'tad/TADFactory.php';
require 'tad/TAD.php';
require 'tad/TADResponse.php';
require 'tad/Providers/TADSoap.php';
require 'tad/Providers/TADZKLib.php';
require 'tad/Exceptions/ConnectionError.php';
require 'tad/Exceptions/FilterArgumentError.php';
require 'tad/Exceptions/UnrecognizedArgument.php';
require 'tad/Exceptions/UnrecognizedCommand.php';
Yii::import('application.components.U');

class TarikAbsen {

    public $count, $info;

    public function Absensi($tglin, $tglout, $ip_id) {
        $fp_id = 1;
        $count = 0;
        $exist = 0;
        $info1 = $info2 = '';
        $hariini = date('Y-m-d');
        $ipz = Yii::app()->db->createCommand(
                        "SELECT kode_ip FROM pbu_ip where ip_id = '$ip_id'")
                ->queryAll();
        
        foreach ($ipz as $k) {
            $ip         = $k['kode_ip'];
            $cbg        = Yii::app()->db->createCommand(
                            "SELECT cabang,cabang_id FROM pbu_ip where ip_id = '$ip_id'")
                            ->queryRow();
            $cabang     = $cbg['cabang'];
            $cabang_id  = $cbg['cabang_id'];
            $comkey     = Ip::model()->findByAttributes(array('kode_ip' => $ip))->com_key;
            $options    = [
                'ip' => "$ip", // '169.254.0.1' by default (totally useless!!!).
                'internal_id' => $fp_id, // 1 by default.
                'com_key' => $comkey, // 0 by default.
                'description' => 'TAD1', // 'N/A' by default.
                'soap_port' => 80, // 80 by default,
                'udp_port' => 4370, // 4370 by default.
                'encoding' => 'utf-8'    // iso8859-1 by default.
            ];
            $tad_factory = new TADPHP\TADFactory($options);
            $tad = $tad_factory->get_instance();

            try {
                $att_logs = $tad->get_att_log();
            } catch (Exception $ex) {
                $info1 = "Gagal! " . $ex->getMessage() . " cabang $cabang" . PHP_EOL;
                continue;
            }

            $filtered_att_logs = $att_logs->filter_by_date(
                    ['start' => "$tglin", 'end' => "$tglout"]
            );

            $result = json_decode($filtered_att_logs->get_response(['format' => 'json']));
            if (!isset($result->Row)) {
                $info2 = "Data absen cabang $cabang pada tgl $tglin - $tglout tidak ada." . PHP_EOL;
                continue;
            }

            $ROW = $result->Row;

            foreach ($ROW as $row) {
                if(strlen($row->PIN) == 5){
                    $pinold = substr($row->PIN, 1,4);
                    $pin    = "10$pinold";
                } else {
                    $pin    = substr($row->PIN, -6);
                }

                $check = Fp::model()->findByAttributes(array('PIN' => $pin, 'PIN_real' => $row->PIN, 'DateTime_' => $row->DateTime));
                if(!$check)
                {
                    $bu_id           = Cabang::model()->findByPk($cabang_id)->bu_id;
                    $cabang_pusat_ip = Cabang::model()->findByAttributes(['bu_id' => $bu_id, 'kepala_cabang_stat' => 1]);
                    $pegawai         = Pegawai::model()->findByAttributes(['nik'=> $pin,'cabang_id' => $cabang_pusat_ip->cabang_id]);
                    if(!$pegawai) {
                        $terminal_id    = $fp_id;
                        $cabang_id      = $cbg['cabang_id'];
                        $kode_cabang    = $cabang;
                    } else {
                        $terminal_id    = $cabang_id;
                        $cabang_id      = $cabang_pusat_ip->cabang_id;
                        $kode_cabang    = $cabang_pusat_ip->kode_cabang;
                    }

                    $fp = new Fp();
                    $fp->PIN            = $pin;
                    $fp->PIN_real       = $row->PIN;
                    $fp->DateTime_      = $row->DateTime;
                    $fp->Verified       = $row->Verified;
                    $fp->Status         = $row->Status;
                    $fp->WorkCode       = $row->WorkCode;
                    $fp->terminal_id    = $terminal_id;
                    $fp->tdate          = $hariini . ' ' . Yii::app()->dateFormatter->format('HH:mm:ss', time());
                    $fp->cabang         = $kode_cabang;
                    $fp->cabang_id      = $cabang_id;
                    $fp->save();

                    $count++;
                }
                else{
                    $exist++;
                }
            }
        }
        $all = array($count,$info1,$info2,$exist);
        return $all;
    }
}
