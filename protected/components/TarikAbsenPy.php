<?php

Yii::import('application.models.*');
Yii::import('application.globals');

class TarikAbsenPy {

    public function AbsensiAll() {
        /*
         * buat dinamis untuk IP address nya
         * buat dinamis untuk from dan to
         * buat mapping cabang_id dari IP address
         * buat pengecekan spy ga double input fingerprint
         */
        $tglin = $tglout = date('Y-m-d',strtotime("-1 days"));
        $ip_list         = Ip::model()->findAll();
        $result = $this->getFingerPrint($ip_list, $tglin, $tglout);

        foreach ($ip_list as $ip) {
            $ip->up = 0;
            $ip->save();
        }

        return $result;
    }
    public function Absensi($tglin, $tglout, $ip_id) {
        $criteria = new CDbCriteria();
        $criteria->addCondition("ip_id = '".$ip_id."'");

        $ip_list = Ip::model()->findAll($criteria);
        $result = $this->getFingerPrint($ip_list, $tglin, $tglout);

        /*foreach ($ip_list as $ip) {
            $ip->up = 0;
            $ip->save();
        }*/

        return $result;
    }
    private function getFingerPrint($ip_list, $tglin, $tglout) {
        if($ip_list) {
            $count = 0;
            $exist = 0;
            $msg = '';
            foreach ($ip_list as $ip) {
//                if($ip->up == 1) continue;

                $kode_ip    = $ip->kode_ip;
                $com_key    = $ip->com_key;

                $forceUDP = Ip::model()->findByPk($ip->ip_id);
                if(isset($forceUDP) && $forceUDP->force_udp == true)
                    $urlAMOSpython = SysPrefs::model()->findByAttributes(['name_' => 'PROJECT_PATH'])->value_ . "AMOS-Python-forceUDP.py";
                else
                    $urlAMOSpython = SysPrefs::model()->findByAttributes(['name_' => 'PROJECT_PATH'])->value_ . "AMOS-Python.py";

                $command    = "python3 ". $urlAMOSpython ." -a=$kode_ip -fr='$tglin 00:00:00' -to='$tglout 23:59:59' -pwd=$com_key";
                $cmd        = $command;

                if (substr(php_uname(), 0, 7) == "Windows") {
                    $response = pclose(popen('start /B cmd /C "' . $cmd . '"', "r"));
                } else {
                    $response = exec($cmd . ' &');
                }
//                $ip->up = 1;
//                $ip->save();

//                file_put_contents('/home/it/uploadlog.txt', $response);
                if (strpos($response, 'terminate') !== false) {
                    file_put_contents('/home/it/uploadgagalip.log', $ip .' ' . PHP_EOL , FILE_APPEND);
                    continue;
                }

                if($response){
                    $jsonData = json_decode($response, true);
                    foreach ($jsonData as $row) {
                        $row = (object) $row;
                        $length = strlen($row->PIN);

                        $bu_id    = Cabang::model()->findByPk($ip->cabang_id)->bu_id;

                        if($length == 5){
                            $pinold = substr($row->PIN, 1,4);
                            $pin = "10$pinold";
                        } else {
                            /*
                             * Pengecualian PT PNG
                             * 0d9a4384-80cd-4b6a-9db9-58d89b16c0a4
                                2f5f429d-0c7f-41e7-b922-812637b62b5e
                                536c83cf-767a-11e7-9b95-68f7286688e6
                             */
                            $haystack = '0d9a4384-80cd-4b6a-9db9-58d89b16c0a4,2f5f429d-0c7f-41e7-b922-812637b62b5e,536c83cf-767a-11e7-9b95-68f7286688e6';
                            $needle   = $bu_id;

                            if (strpos($haystack, $needle) !== false) {
                                $pin = substr($row->PIN, -8);
                            } else {
                                $pin = substr($row->PIN, -6);
                            }
                        }

                        /* //Hilangkan batasan 6 digit NIK
                         * else if($length > 6){
                            $pin = substr($row->PIN, 3);
                        }
                        else {
                            $pin = substr($row->PIN, -6);
                        }*/

                        if(!DbCmd::instance()->addFrom('{{pegawai}}')
                            ->addCondition("nik = $pin OR nik_lokal = $pin")->queryRow()){
                            continue;
                        }

                        if(Fp::model()->findByAttributes(array('PIN' => $pin, 'PIN_real' => $row->PIN, 'DateTime_' => $row->DateTime_))) {
                            $exist++;
                            continue;
                        }

                        $cabang_pusat_ip = Cabang::model()->findByAttributes(['bu_id' => $bu_id, 'kepala_cabang_stat' => 1]);
                        $pegawai         = Pegawai::model()->findByAttributes(['nik'=> $pin,'cabang_id' => $cabang_pusat_ip->cabang_id]);
                        if(!$pegawai) { //orang cabang
                            $cabang_id      = $ip->cabang_id;
                            $kode_cabang    = $ip->cabang;
                            $terminal_id    = 22;
                        } else { //orang pusat
                            $cabang_id      = $cabang_pusat_ip->cabang_id;
                            $kode_cabang    = $cabang_pusat_ip->kode_cabang;
                            $terminal_id    = $ip->cabang_id;
                        }

                        $fp                 = new Fp();
                        $fp->PIN            = $pin;
                        $fp->PIN_real       = $row->PIN;
                        $fp->DateTime_      = $row->DateTime_;
                        $fp->Verified       = 0;
                        $fp->Status         = 0;
                        $fp->WorkCode       = 22;
                        $fp->terminal_id    = $terminal_id;
                        $fp->tdate          = date('Y-m-d') . ' ' . Yii::app()->dateFormatter->format('HH:mm:ss', time());
                        $fp->cabang         = $kode_cabang;
                        $fp->cabang_id      = $cabang_id;
                        if (!$fp->save()){
                            throw new Exception("Gagal menyimpan data: ", $fp->getErrors());
                        }
                        $count++;
                    }
                } else {
                    $msg = "Data absen cabang $kode_ip pada tgl $tglin - $tglout tidak ada." . PHP_EOL . " Atau tidak ada akses ke mesin finger.";
                }
            }
            return [
                'count' => $count,
                'message' => $msg,
                'exist' => $exist
            ];
        } else {
            throw new Exception("IP cabang tidak terdaftar");
        }
    }
}
