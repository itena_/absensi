<?php
class U extends GxController
{
    static function report_absen($periode_id, $cabang_id, $show_detail, $urut_jabatan, $awal = null, $akhir = null)
    {
        $params = [
            'periode_id'    => $periode_id,
            'cabang_id'     => $cabang_id,
            'show_detail'   => $show_detail,
            'urut_jabatan'  => $urut_jabatan,
            'tglin'         => $awal,
            'tglout'        => $akhir
        ];

        $db     = Validasi::getValidasi($params);
        return $db->queryAll();
    }
    static function report_absen_all($periode_id)
    {
        $params = [
            'periode_id'    => $periode_id
        ];

        $db     = Validasi::getValidasi($params);
        return $db->queryAll();
    }
    static function report_absenjam($periode_id, $cabang_id, $show_detail, $urut_jabatan)
    {
        $params = [
            'periode_id'    => $periode_id,
            'cabang_id'     => $cabang_id,
            'show_detail'   => $show_detail,
            'urut_jabatan'  => $urut_jabatan,
            'dalam_jam'     => true
        ];

        $db     = Validasi::getValidasi($params);
        return $db->queryAll();
    }

    static function report_shift($cabang_id, $aktif = '')
    {
        $cmd = new DbCmd();
        $cmd->addFrom("{{shift}} s")
            ->addSelect("*")
            ->addSelect("concat_ws(' - ',s.earlyin_time,s.latein_time) as range_in")
            ->addSelect("concat_ws(' - ',s.earlyout_time,s.lateout_time) as range_out")
            ->addSelect("IF(s.active = 1,'AKTIF','TIDAK AKTIF') as status_shift")
            ->addSelect("IF(s.kelompok_shift = 0,'Karyawan','OG,OB/Security') as name_kelompok")
            ->addCondition("cabang_id = :cabang_id")
            ->addParam(":cabang_id", $cabang_id)
            ->addOrder("kelompok_shift, in_time")
            ;

        if($aktif == 'on'){
            $cmd->addCondition("active = :active")
                ->addParam(":active", 1);
        }

        return $cmd->queryAll();
    }

    static function report_fp($periode_id,$pegawai,$cabang_id) {
        $validasi_view = "SELECT
	`v`.`validasi_id` AS `validasi_id`,
	`v`.`pin_id` AS `pin_id`,
	`v`.`PIN` AS `PIN`,
	`v`.`pegawai_id` AS `pegawai_id`,
	`v`.`cabang_id` AS `cabang_validasi_id`,
	`v`.`in_time` AS `in_time`,
	`v`.`out_time` AS `out_time`,
	`v`.`min_early_time` AS `min_early_time`,
	`v`.`min_late_time` AS `min_late_time`,
	`v`.`min_least_time` AS `min_least_time`,
	`v`.`min_over_time_awal` AS `min_over_time_awal`,
	`v`.`min_over_time` AS `min_over_time`,
	(
		`v`.`min_over_time` + `v`.`min_over_time_awal`
	) AS `total_lembur`,
	`v`.`real_lembur_pertama` AS `min_over_time_awal_real`,
	`v`.`real_lembur_akhir` AS `min_over_time_real`,
	`v`.`real_lembur_hari` AS `lembur_hari`,
	`v`.`real_less_time` AS `real_less_time`,
	`v`.`tdate` AS `tdate`,
	`v`.`kode_ket` AS `kode_ket`,
	`v`.`no_surat_tugas` AS `no_surat_tugas`,
	`v`.`status_int` AS `status_int`,
	`v`.`result_id` AS `result_id`,
	`v`.`user_id` AS `user_id`,
	`v`.`status_pegawai_id` AS `status_pegawai_id`,
	`v`.`shift_id` AS `shift_id`,
	`v`.`approval_lembur` AS `approval_lembur`,
	`v`.`approval_lesstime` AS `approval_lesstime`,
	`p`.`cabang_id` AS `cabang_id`,
	`c`.`kode_cabang` AS `kode_cabang`,
	`c`.`nama_cabang` AS `nama_cabang`,
	`pp`.`cabang_id` AS `cabang_user`,
	`u`.`pegawai_id` AS `pegawai_user`,
	`v`.`cuti` AS `cuti`,
	`v`.`limit_lembur` AS `limit_lembur`,
	`v`.`short_shift` AS `short_shift`
FROM
	(
		(
			(
				(
					`pbu_validasi` `v`
					LEFT JOIN `pbu_pegawai` `p` ON (
						(
							`p`.`pegawai_id` = `v`.`pegawai_id`
						)
					)
				)
				LEFT JOIN `pbu_cabang` `c` ON (
					(
						`c`.`cabang_id` = `p`.`cabang_id`
					)
				)
			)
			LEFT JOIN `pbu_users` `u` ON ((`u`.`id` = `v`.`user_id`))
		)
		LEFT JOIN `pbu_pegawai` `pp` ON (
			(
				`pp`.`pegawai_id` = `u`.`pegawai_id`
			)
		)
	)
WHERE
	(`v`.`status_int` = 1)
ORDER BY
	`v`.`PIN`,
	`c`.`kode_cabang`";

        $cmd = new DbCmd();
        if($cabang_id) {
            if ($pegawai){
                $cmd->addCondition("v.cabang_validasi_id = '$cabang_id' AND t2.pegawai_id = '$pegawai'");
            } else {
                $cmd->addCondition("v.cabang_validasi_id = '$cabang_id'");
            }
        } else {
            $security_roles = self::getSecuriyRolesId();
            if ($security_roles != GET_SECURITY_ROLE_ADMINISTRATOR() && $security_roles != GET_SECURITY_ROLE_HRD()) {
                $cbgusr = Cabang::model()->checkCabangId();
                $cmd->addCondition("v.cabang_validasi_id = '$cbgusr'");
            } else {
                $arr = [];
                $cabangs = DbCmd::instance()->addSelect("cabang_id")->addFrom("{{cabang}}")
                    ->addCondition("bu_id = :bu_id")->addParam(":bu_id", $_POST['bu_id'])->queryAll();

                foreach ($cabangs as $c) {
                    $arr[] = $c['cabang_id'];
                }

                $cmd->addInCondition("v.cabang_validasi_id", $arr);
            }

            if ($pegawai){
                $cmd->addCondition("t2.pegawai_id = '$pegawai'");
            }
        }

        $periode        = Periode::model()->findByPk($periode_id);
        $periodeStart   = $periode->periode_start;
        $periodeEnd     = $periode->periode_end;

        $q_kode_ket = 'kode_ket = 3 OR kode_ket = 0 OR kode_ket = 1 OR kode_ket = 10';

        $cmd->addSelect("
            t2.* 
                , DATE_FORMAT(v.in_time, '%Y-%m-%d') as tgl_in
                , DATE_FORMAT(v.out_time, '%Y-%m-%d') as tgl_out
                , IF($q_kode_ket, DATE_FORMAT(v.in_time,'%H:%i:%s'), '-') masuk
                , IF($q_kode_ket, DATE_FORMAT(v.out_time,'%H:%i:%s'), '-') keluar                        
                , IF($q_kode_ket, IF(TIME_TO_SEC(timediff(v.out_time,v.in_time))/3600 > 24, SEC_TO_TIME((v.min_over_time + 480)*60),timediff(v.out_time,v.in_time)), '-') jam_kerja
                , IF($q_kode_ket, TIME_TO_SEC(timediff(v.out_time,v.in_time)), '') jam_kerja_hitung_in_second
                , nama_ket as keterangan, '-' alasan, v.approval_lembur
                , IF(v.approval_lembur = 1, v.min_over_time_awal_real, '-') lembur1
                , IF(v.approval_lembur = 1, v.min_over_time_real, '-') lembur2
                , IF(v.approval_lembur = 1, v.lembur_hari, '-') lemburh, v.cabang_user ,  v.no_surat_tugas, v.cabang_validasi_id
                , if(v.approval_lesstime = 1, IF($q_kode_ket,real_less_time,0),0) AS LT
                , IF(v.short_shift = 1, 'YA', '') short_shift
        ")->addFrom("
        (
            SELECT
            t1.*, p.pegawai_id, p.nama_lengkap, p.nik, p.cabang_id, b.bu_name, b.bu_kode
            FROM
            (
                " . Periode::getAllDate($periodeStart, $periodeEnd)->getQuery() . "
            ) t1,
            pbu_pegawai p
            LEFT JOIN pbu_cabang c on c.cabang_id = p.cabang_id
            LEFT JOIN pbu_bu b on b.bu_id = c.bu_id
        ) t2
        ")->addLeftJoin("($validasi_view) v","v.pegawai_id = t2.pegawai_id and DATE_FORMAT(v.in_time, '%Y-%m-%d') = t2.selected_date")
            ->addLeftJoin("{{pegawai}} p","p.pegawai_id = v.pegawai_id")
            ->addLeftJoin("{{keterangan}} k","k.keterangan_id = v.kode_ket")
            ->addInnerJoin("{{status_pegawai}} sp","sp.status_pegawai_id = p.status_pegawai_id")
//            ->addCondition("sp.kode = 'AKTIF' OR sp.kode = 'TRAINING'")
            ->addOrder("t2.nik, t2.selected_date")
            ;

        $query = $cmd->getQuery();
        /*$com = DbCmd::instance()->addFrom("($query) t2")
            ->addCondition()
        ;*/
        return $cmd->queryAll();
    }
    static function report_fp_old($periode_id,$pegawai,$cabang_id)
    {
        $where = "";
        if($cabang_id) {            
            $where2 = " where v.cabang_validasi_id = '$cabang_id' ";
            if ($pegawai){
                $where = "$where2 AND t2.pegawai_id = '$pegawai'";
            } else {
                $where = $where2;
            }
        } else {
            if ($pegawai){
                $where = " where t2.pegawai_id = '$pegawai'";
            } else {
                $security_roles = self::getSecuriyRolesId();
                if ($security_roles != GET_SECURITY_ROLE_ADMINISTRATOR() && $security_roles != GET_SECURITY_ROLE_HRD()) {
                    $cbgusr = Cabang::model()->checkCabangId();
                    $where  = " where v.cabang_validasi_id = '$cbgusr' ";
                }
            }    
        }
        $periode        = Periode::model()->findByPk($periode_id);
        $periodeStart   = $periode->periode_start;
        $periodeEnd     = $periode->periode_end;
        
        $q_kode_ket = 'kode_ket = 3 OR kode_ket = 0 OR kode_ket = 1 OR kode_ket = 10';
        
        $comm = Yii::app()->db->createCommand(
                "select 
                        t2.* 
                        , DATE_FORMAT(v.in_time, '%Y-%m-%d') as tgl_in
                        , DATE_FORMAT(v.out_time, '%Y-%m-%d') as tgl_out
                        , IF($q_kode_ket, DATE_FORMAT(v.in_time,'%H:%i:%s'), '-') masuk
                        , IF($q_kode_ket, DATE_FORMAT(v.out_time,'%H:%i:%s'), '-') keluar                        
                        , IF($q_kode_ket, IF(TIME_TO_SEC(timediff(v.out_time,v.in_time))/3600 > 24, SEC_TO_TIME((v.min_over_time + 480)*60),timediff(v.out_time,v.in_time)), '-') jam_kerja
                        , IF($q_kode_ket, TIME_TO_SEC(timediff(v.out_time,v.in_time)), '') jam_kerja_hitung_in_second
                        , nama_ket as keterangan, '-' alasan, v.approval_lembur
                        , IF(v.approval_lembur = 1, v.min_over_time_awal_real, '-') lembur1
                        , IF(v.approval_lembur = 1, v.min_over_time_real, '-') lembur2
                        , IF(v.approval_lembur = 1, v.lembur_hari, '-') lemburh, v.cabang_user ,  v.no_surat_tugas, v.cabang_validasi_id
                        , if(v.approval_lesstime = 1, IF($q_kode_ket,real_less_time,0),0) AS LT
                        , IF(v.short_shift = 1, 'YA', '') short_shift
                from
                (
                    SELECT
                    t1.*, p.pegawai_id, p.nama_lengkap, p.nik, p.cabang_id, b.bu_name, b.bu_kode
                    FROM
                    (
                        " . Periode::getAllDate($periodeStart, $periodeEnd)->getQuery() . "
                    ) t1,
                    pbu_pegawai p
                    LEFT JOIN pbu_cabang c on c.cabang_id = p.cabang_id
                    LEFT JOIN pbu_bu b on b.bu_id = c.bu_id
                ) t2
                LEFT JOIN pbu_validasi_view v on v.pegawai_id = t2.pegawai_id and DATE_FORMAT(v.in_time, '%Y-%m-%d') = t2.selected_date
                LEFT JOIN pbu_pegawai p on p.pegawai_id = v.pegawai_id
                LEFT JOIN pbu_keterangan k on k.keterangan_id = v.kode_ket                 
                INNER JOIN pbu_status_pegawai sp on sp.status_pegawai_id = p.status_pegawai_id                 
                $where AND sp.kode = 'AKTIF' OR sp.kode = 'TRAINING' #AND t2.cabang_id = $cabang_id
                order by t2.nik, t2.selected_date"
            );
        return $comm->queryAll();
    }
    static function report_fp_real($periode_id,$pegawai_id,$cabang_id)
    {
        $periode    = Periode::model()->findByPk($periode_id);
        $pstart     = $periode->periode_start;
        $pend       = $periode->periode_end;

        $cmd = new DbCmd();
        $cmd->addSelect("fp.PIN nik, p.nama_lengkap
                , DATE_FORMAT(fp.DateTime_,'%Y-%m-%d') tgl
                , DATE_FORMAT(fp.DateTime_,'%H:%i:%s') jam
                , c.kode_cabang")
            ->addFrom("{{fp}} fp")
            ->addLeftJoin("{{cabang}} c","c.cabang_id = fp.cabang_id")
            ->addLeftJoin("{{pegawai}} p","p.nik = fp.PIN AND c.cabang_id = fp.cabang_id")
            ->addCondition("fp.DateTime_ >= :pstart")
            ->addCondition("fp.DateTime_ <= (:pend + INTERVAL 8 HOUR)")
            ->addCondition("nama_lengkap is not null")
            ->addOrder("nik, tgl, jam")
            ->addParams([
                ":pstart" => $pstart,
                ":pend" => $pend
            ])
        ;

        if($pegawai_id != '') {
            $cmd->addCondition("p.pegawai_id = :pegawai_id")
                ->addParam(":pegawai_id", $pegawai_id)
            ;
        }
        if(isset($cabang_id)){
            $arr = [];
            $cabang  = Cabang::model()->findByPk($cabang_id);
            $cabangs = DbCmd::instance()->addSelect("cabang_id")->addFrom("{{cabang}}")
                ->addCondition("bu_id = :bu_id")->addParam(":bu_id", $cabang->bu_id)->queryAll();

            foreach ($cabangs as $c) {
                $arr[] = $c['cabang_id'];
            }

            $cmd->addInCondition("p.cabang_id", $arr)
                ->addCondition("c.cabang_id = :cabang_id")
                ->addParam(":cabang_id", $cabang_id)
            ;
        }

//        $result = $cmd->getQuery();
        $result = $cmd->queryAll();
        return $result;
    }
    static function report_fp_mix($periode_id,$pegawai_id, $bu_id)
    {
        $periode    = Periode::model()->findByPk($periode_id);
        $pstart     = $periode->periode_start;
        $pend       = $periode->periode_end;


        $cmd1 = DbCmd::instance()->addFrom("pbu_fp f")
            ->addSelect("f.PIN, p.nama_lengkap, DATE_FORMAT(DateTime_,'%Y-%m-%d') tgl
	                , DATE_FORMAT(DateTime_,'%H:%i:%s') jam, f.cabang_id, f.cabang")
            ->addLeftJoin("{{pegawai}} p","p.nik = f.PIN AND p.cabang_id = f.cabang_id")
            ->addCondition("DateTime_ BETWEEN :pstart AND :pend")
            ->addParams([
                ":pstart" => $pstart,
                ":pend" => $pend
            ])
            ;

        $arr = [];
        $cabangs = DbCmd::instance()->addSelect("cabang_id")->addFrom("{{cabang}}")
            ->addCondition("bu_id = :bu_id")->addParam(":bu_id", $bu_id)->queryAll();

        foreach ($cabangs as $c) {
            $arr[] = $c['cabang_id'];
        }

        $cmd1->addInCondition("f.cabang_id", $arr)
        ;

        if($pegawai_id != '') {
            $nik = Pegawai::model()->findByPk($pegawai_id)->nik;
            $cmd1->addCondition("nik = '$nik'")
            ;
        }

        $cmd = DbCmd::instance()->addSelect("PIN nik, nama_lengkap, tgl tgl_masuk, MIN(jam) jam_masuk, tgl tgl_keluar, if(MIN(jam) = MAX(jam), '-', MAX(jam)) jam_keluar, cabang_id, cabang")
            ->addFrom("(".$cmd1->getQuery().") p")
            ->addCondition("nama_lengkap is not null")
            ->addOrder("nik, tgl")
            ->addGroup("nik, tgl")
        ;

        return $cmd;
    }
    static function report_fp_validasi($periode_id,$pegawai,$cabang_id)
    {
        $where = "";
        if($cabang_id != null){
            $where .= " AND a.cabang_id = '$cabang_id'";
        }
        if($pegawai != null){
            $nik = Pegawai::model()->findByPk($pegawai)->nik;
            $where .= " AND a.PIN = '$nik'";
        }
        $periode = Periode::model()->findByPk($periode_id);
        $pstart = $periode->periode_start;
        $pend = $periode->periode_end;
        $comm = Yii::app()->db->createCommand(
            "
                SELECT
                    a.*, DATE_FORMAT(a.in_time, '%d-%m-%Y') AS tgl_in,
                    DATE_FORMAT(a.out_time, '%d-%m-%Y') AS tgl_out,
                    DATE_FORMAT(a.in_time, '%H:%i:%s') AS masuk,
                    DATE_FORMAT(a.out_time, '%H:%i:%s') AS keluar,
                    b.nama_lengkap,
                    b.store,
                    IF(a.status_int = 1, 'valid', 'invalid') as status
                FROM
                    pbu_validasi a
                LEFT JOIN pbu_pegawai b ON a.pegawai_id = b.pegawai_id
                WHERE
                    kode_ket = '0'
                AND a.in_time >= :pstart
                AND a.in_time <= :pend
                $where
                ORDER BY
                    b.store,
                    a.pin_id,
                    a.in_time
            "
        );
        $a = $comm->queryAll(true,[
            ':pstart' => $pstart,
            ':pend' => $pend
        ]);
        return $a;
    }

    static function report_kpi($from,$to,$periode_id,$pegawai,$cabang_id)
    {
        $where = "";
        if($cabang_id) {
            $where2 = " where v.cabang_validasi_id = '$cabang_id' ";
            if ($pegawai){
                $where = "$where2 AND t2.pegawai_id = '$pegawai'";
            } else {
                $where = $where2;
            }
        } else {
            if ($pegawai){
                $where = " where t2.pegawai_id = '$pegawai'";
            } else {
                Log::generateErrorMessage(false, "Pegawai tidak boleh kosong!");
            }
        }
        $pstart = $from;
        $pend = $to;

        $q_kode_ket = 'kode_ket = 3 OR kode_ket = 0 OR kode_ket = 1 OR kode_ket = 10';

        $comm = Yii::app()->db->createCommand(
            "select 
                        t2.* 
                        , DATE_FORMAT(v.in_time, '%Y-%m-%d') as tgl_in
                        , DATE_FORMAT(v.out_time, '%Y-%m-%d') as tgl_out
                        , IF($q_kode_ket, DATE_FORMAT(v.in_time,'%H:%i:%s'), '-') masuk
                        , IF($q_kode_ket, DATE_FORMAT(v.out_time,'%H:%i:%s'), '-') keluar                        
                        , IF($q_kode_ket, TIME_TO_SEC(timediff(v.out_time,v.in_time)), '') jam_kerja_hitung_in_second
                        
                        , sum(IF($q_kode_ket,IF( v.short_shift = 1,TIME_TO_SEC(timediff(v.out_time,v.in_time))-7200,TIME_TO_SEC(timediff(v.out_time,v.in_time))), '')) jam_kerja
                        , nama_ket as keterangan, '-' alasan, v.approval_lembur
                                                
                from
                (
                    SELECT
                    t1.*, p.pegawai_id, p.nama_lengkap, p.nik, p.cabang_id, b.bu_name, b.bu_kode
                    FROM
                    (
                    select * from 
                         (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) s
                        where selected_date >= DATE_FORMAT(:pstart, '%Y-%m-%d') and selected_date <= DATE_FORMAT(:pend, '%Y-%m-%d')                            
                    ) t1,
                    pbu_pegawai p
                    LEFT JOIN pbu_cabang c on c.cabang_id = p.cabang_id
                    LEFT JOIN pbu_bu b on b.bu_id = c.bu_id
                ) t2
                LEFT JOIN pbu_validasi_view v on v.pegawai_id = t2.pegawai_id and DATE_FORMAT(v.in_time, '%Y-%m-%d') = t2.selected_date
                LEFT JOIN pbu_pegawai p on p.pegawai_id = v.pegawai_id
                LEFT JOIN pbu_keterangan k on k.keterangan_id = v.kode_ket 
                $where
                GROUP  by t2.nik
                order by t2.nik, t2.selected_date"
        );
        $a = $comm->queryAll(true,[
            ':pstart' => $pstart,
            ':pend' => $pend
        ]);
        return $a;
    }
    static function report_cuti($tahun, $cabang_id)
    {
        $where = "";
        $param = array(':tahun' => $tahun,':kode_ket' => Keterangan::model()->findByAttributes(['nama_ket' => 'Cuti Tahunan'])->keterangan_id);
        $jumlah_cuti = GET_JATAH_CUTI_TAHUNAN();
        if ($cabang_id != null) {
            $where = "AND pg.cabang_id = :cabang_id";
            $param[':cabang_id'] = $cabang_id;
        }
        $comm = Yii::app()->db->createCommand("SELECT
            t1.pegawai_id,t1.nik,nama_lengkap,jatah_cuti,kode_cabang,
            jatah_cuti,
            SUM(cuti) as ambil_cuti,
            $jumlah_cuti - SUM(cuti) as sisa_cuti,
            ps.sisa_cuti as sisa_cuti_tahun_sebelumnya,
            GROUP_CONCAT(IF(cuti_value= 2, DATE_FORMAT(in_time, \"%d %b (2)\" ),DATE_FORMAT(in_time, \"%d %b\" )) ORDER BY in_time asc) as tgl_ambil_cuti
            from (
            SELECT
                      pg.pegawai_id,
                      pg.nik,
                      pg.nama_lengkap,
                      $jumlah_cuti AS jatah_cuti,
                      if(pv.cuti = 0, 1, pv.cuti) cuti,
                      pc.kode_cabang,
                      pv.in_time,
                      pv.cuti as cuti_value
                  FROM
                      pbu_validasi AS pv
                  INNER JOIN pbu_pegawai AS pg ON pv.pegawai_id = pg.pegawai_id
                  INNER JOIN pbu_cabang AS pc ON pg.cabang_id = pc.cabang_id
                  WHERE
                      pv.kode_ket = :kode_ket
                      $where
                      AND pv.status_int = '1'
                      AND YEAR (pv.in_time) >= :tahun
                      AND YEAR (pv.in_time) <= :tahun
            ) t1
            left join pbu_sisa_cuti_2023 ps on ps.pegawai_id = t1.pegawai_id
            GROUP BY t1.nik");
        $a = $comm->queryAll(true,$param);
        return $a;
    }

    static function ReportLessTime($periode_id, $cabang_id, $show_active = true)
    {
        $periode    = Periode::model()->findByPk($periode_id);
        $pstart     = $periode->periode_start;
        $pend       = $periode->periode_end;

        $cmd        = Validasi::getAbsensi($pstart, $pend);

//        $security_roles = self::getSecuriyRolesId();
//        if($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()){
//            $cmd->addCondition("v.cabang_id = :cabang")
//                ->addParam(":cabang",$cabang_id);
//        } else {
//            $cbgusr = Cabang::model()->checkCabangId();
//            $cmd->addCondition("v.cabang_id = :cabang")
//                ->addParam(":cabang",$cbgusr);
//        }

        $cmd->addCondition("v.cabang_id = :cabang")
            ->addParam(":cabang", $cabang_id)
            ->addCondition("v.approval_lesstime = 1")
            ->addGroup("v.PIN")
            ->addOrder("v.PIN")
            ->addSelect("sum(v.real_less_time) real_lembur_less_time")
            ->addSelect("sum(if(v.real_less_time > 0, 1, 0)) real_lembur_less_time_hari")
        ;

        if($show_active) $cmd->addHaving("real_lembur_less_time > 0");

        $a = $cmd->getQuery();
        return $cmd->queryAll();
    }
    static function ReportLupaAbsen($periode_id, $cabang_id)
    {
        $periode    = Periode::model()->findByPk($periode_id);
        $pstart     = $periode->periode_start;
        $pend       = $periode->periode_end;

        $cmd        = Validasi::getAbsensi($pstart, $pend);
        $cbgusr     = Cabang::model()->checkCabangId();

        $security_roles = self::getSecuriyRolesId();
        if($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()){
            $cmd->addCondition("v.cabang_id = :cabang")
                ->addParam(":cabang",$cabang_id);
        } else {
            $cbgusr = Cabang::model()->checkCabangId();
            $cmd->addCondition("v.cabang_id = :cabang")
                ->addParam(":cabang",$cbgusr);
        }

        $cmd->addCondition("v.kode_ket = 3")
            ->addSelect("count(*) jum")
            ->addGroup("v.PIN")
            ->addOrder("v.kode_cabang, v.PIN asc ")
        ;
        $a = $cmd->getQuery();

        return $cmd->queryAll();
    }

    static function report_check_absen($periode_id, $cabang_id, $nama_lengkap = "", $nik ="")
    {
        $dbperiode      = Periode::getPeriodeStartEnd($_POST['periode_id']);
        $periodenow     = $dbperiode->queryRow();
        $periodeStart   = $periodenow['ps'];
        $periodeEnd     = $periodenow['pe'];

        $db = new DbCmd();

        if($nama_lengkap != "" && $nik != "") {
            $db->addCondition("(t3.nama_lengkap like '%$nama_lengkap%' AND t3.nik like '%$nik%')");
        } else if($nama_lengkap != ""){
            $db->addCondition("(t3.nama_lengkap like '%$nama_lengkap%')");
        } else if($nik != ""){
            $db->addCondition("(t3.nik like '%$nik%')");
        }

        $t1 = Periode::getAllDate($periodeStart, $periodeEnd)->getQuery();

        $dbt2 = new DbCmd();
        $dbt2->addSelect("t1.*, p.pegawai_id, p.nama_lengkap, p.nik, p.cabang_id, b.bu_name, b.bu_kode, p.status_pegawai_id")
            ->addFrom("(" . $t1 . ")t1" . ", {{pegawai}} p")
            ->addLeftJoin("{{cabang}} c","c.cabang_id = p.cabang_id")
            ->addLeftJoin("{{bu}} b","b.bu_id = c.bu_id")
            ->addCondition("c.cabang_id = :cabang_id")
            ->addParam(":cabang_id", $cabang_id)
            ;
        $t2 = $dbt2->getQuery();

        $dbt3 = new DbCmd();
        $dbt3->addSelect("v.validasi_id,t2.*, COUNT(v.validasi_id) c")
            ->addFrom("(". $t2 . ") t2")
            ->addLeftJoin("{{validasi}} v","v.pegawai_id = t2.pegawai_id and DATE_FORMAT(v.in_time, '%Y-%m-%d') = t2.selected_date AND status_int = 1")
            ->addCondition("t2.cabang_id = :cabang_id")
            ->addCondition(DbCmd::instance()->addCondition("t2.status_pegawai_id is null")->addInCondition("t2.status_pegawai_id", get_status_pegawai_aktif(), "OR"),"AND")
            ->addGroup("pegawai_id, selected_date")
            ->addOrder("nik,t2.selected_date")
            ->addParam(":cabang_id", $cabang_id)
        ;
        $t3 = $dbt3->getQuery();

        $db->addSelect("*,IF(c > 0, 'data double', 'belum diisi') note")
            ->addFrom("(". $t3 . ") t3")
            ->addCondition("(c > 1 OR t3.validasi_id is null)")
            ;

        $data   = $db->getQuery();
        $result = $db->queryAll();

        return $result;
    }
    static function report_rekap_pegawai()
    {
        $comm = Yii::app()->db->createCommand("SELECT pp.pegawai_id,pp.nik,pg.kode AS kode_golongan,pp.nama_lengkap,pp.email,
        DATE_FORMAT(pp.tgl_masuk,'%d %b %Y') AS tgl_masuk,pp.nik_lokal,pp.bank_nama,pp.bank_kota,pp.rekening,
        pj.nama_jabatan,psp.kode AS kode_status,
        MAX(IF(pm.kode = 'GP', pmg.amount, NULL)) AS gp,
        MAX(IF(pm.kode = 'TJ', pmg.amount, NULL)) AS tj,
        MAX(IF(pm.kode = 'UM', pmg.amount, NULL)) AS um,
        MAX(IF(pm.kode = 'UT', pmg.amount, NULL)) AS ut
        FROM pbu_pegawai AS pp
        LEFT JOIN pbu_jabatan AS pj ON pp.jabatan_id = pj.jabatan_id
        LEFT JOIN pbu_status_pegawai AS psp ON pp.status_pegawai_id = psp.status_pegawai_id
        LEFT JOIN pbu_golongan AS pg ON pp.golongan_id = pg.golongan_id
        LEFT JOIN pbu_master_gaji AS pmg ON pmg.golongan_id = pg.golongan_id
        LEFT JOIN pbu_master AS pm ON pmg.master_id = pm.master_id
        GROUP BY pp.pegawai_id
        ORDER BY pg.kode DESC ");
        return $comm->queryAll(true);
    }

    static function report_master_gaji()
    {
        /** @var CDbCommand $comm */
        $sql1 = Yii::app()->db->createCommand('SELECT GROUP_CONCAT(CONCAT(\'MAX(IF(pm.kode = "\',pm.kode,\'", pmg.amount, NULL)) AS \',pm.kode))
          FROM pbu_master AS pm;')->queryScalar();
        $sql2 = 'SELECT pl.nama AS lnama,pg.nama AS gnama,pa.nama AS anama, ' . $sql1 . ',
        SUM(CASE pm.periode_ WHEN "daily" THEN pmg.amount*20 WHEN "weekly" THEN pmg.amount*4 ELSE pmg.amount END) AS asumsi
        FROM pbu_master_gaji AS pmg
        INNER JOIN pbu_master AS pm ON pmg.master_id = pm.master_id
        INNER JOIN pbu_leveling AS pl ON pmg.leveling_id = pl.leveling_id
        INNER JOIN pbu_golongan AS pg ON pmg.golongan_id = pg.golongan_id
        INNER JOIN pbu_area AS pa ON pmg.area_id = pa.area_id
        GROUP BY pl.leveling_id,pg.golongan_id,pa.area_id
        ORDER BY pl.kode DESC, pg.kode DESC';
        return Yii::app()->db->createCommand($sql2)->queryAll(true);
    }
    static function report_rekap_payroll_header($periode_id)
    {
        $comm = Yii::app()->db->createCommand('
        SELECT p1.nama_skema FROM pbu_payroll_details p1 
        INNER JOIN pbu_payroll p2 ON p1.payroll_id=p2.payroll_id 
        WHERE p2.periode_id = :periode_id GROUP BY p1.nama_skema');
        return $comm->queryAll(true, [':periode_id' => $periode_id]);
    }
    static function report_rekap_payroll($periode_id)
    {
        Yii::app()->db->createCommand('SET SESSION group_concat_max_len = 1000000;')->execute();
        $sql1 = Yii::app()->db->createCommand('SELECT GROUP_CONCAT(CONCAT(\'MAX(IF(ppd.nama_skema = "\',
        ppd.nama_skema,\'", ppd.amount, NULL)) AS \',CONCAT("\'",ppd.nama_skema,"\'")))
        FROM  (        SELECT p1.nama_skema FROM pbu_payroll_details p1 
        INNER JOIN pbu_payroll p2 ON p1.payroll_id=p2.payroll_id 
        WHERE p2.periode_id = :periode_id
        GROUP BY p1.nama_skema) ppd;')
            ->queryScalar([':periode_id' => $periode_id]);
        if ($sql1 != null) {
            $sql1 = ',' . $sql1;
        }
        $comm = Yii::app()->db->createCommand("SELECT pp.payroll_id,pp.periode_id,pp.total_hari_kerja,pp.total_lk,
            pp.total_sakit,pp.total_cuti_tahunan,pp.total_off,pp.jatah_off,pp.pegawai_id,
            pp.nik,pp.nama_lengkap,pp.nama_jabatan,DATE_FORMAT(pp.tgl_masuk,'%d-%b-%y') AS tgl_masuk,pp.email,pp.total_income,
            pp.total_deduction,pp.take_home_pay,pp.transfer,concat(pp.kode_level,' ',pp.kode_gol) AS kode_level,
            pp.golongan_id,pp.leveling_id,pp.area_id,pp.total_lembur_1,pp.total_lembur_next " . $sql1 . "
            FROM pbu_payroll AS pp
            INNER JOIN pbu_payroll_details AS ppd ON ppd.payroll_id = pp.payroll_id
            WHERE pp.periode_id = :periode_id
        GROUP BY pp.pegawai_id
        ORDER BY pp.kode_level DESC, pp.kode_gol DESC");
        return $comm->queryAll(true, [':periode_id' => $periode_id]);
    }
    static function absensiPayrollIndex($periode_id)
    {
        $id = Yii::app()->user->getId();
        $sri = Users::model()
            ->findByAttributes(array('id' => $id))
            ->security_roles_id;
        $comm = Yii::app()->db->createCommand("
        SELECT
            '$periode_id' AS periode_id,
            ppa.payroll_absensi_id,
            IFNULL(ppa.total_hari_kerja,0) total_hari_kerja,
            IFNULL(ppa.locked,0)locked,
            IFNULL(ppa.total_lk,0) total_lk,
            IFNULL(ppa.total_sakit,0) total_sakit,
            IFNULL(ppa.total_cuti_tahunan,0) total_cuti_tahunan,
            IFNULL(ppa.total_off,0) total_off,            
            IFNULL(ppa.total_cuti_menikah,0) total_cuti_menikah,
            IFNULL(ppa.total_cuti_bersalin,0) total_cuti_bersalin,
            IFNULL(ppa.total_cuti_istimewa,0) total_cuti_istimewa,
            IFNULL(ppa.total_cuti_non_aktif,0) total_cuti_non_aktif,
            IFNULL(ppa.total_lembur_1 ,0) total_lembur_1,
            IFNULL(ppa.total_lembur_next,0) total_lembur_next,
            IFNULL(ppa.jatah_off,0) jatah_off,
            IFNULL(ppa.less_time,0) less_time,
            pg.nik,pg.pegawai_id,pg.nama_lengkap
        FROM
            pbu_pegawai AS pg 
        LEFT JOIN pbu_payroll_absensi AS ppa ON ppa.pegawai_id = pg.pegawai_id AND ppa.periode_id = :periode_id
        LEFT JOIN pbu_sr_cbg_area_bu AS sr ON pg.cabang_id = sr.cabang_id AND sr.security_roles_id = :security_roles_id
        LEFT JOIN pbu_sr_level_bu as sl ON sl.leveling_id = pg.leveling_id AND sl.security_roles_id = :security_roles_id
        ");
        return $comm->queryAll(true, [
            ':periode_id' => $periode_id,
            ':security_roles_id' => $sri
        ]);
    }

    static function save_log($id,$tdate,$log){
        $user = Users::model()->findByPk($id);
        $logz = 'User '.$user.' melakukan '.$log['action'].' pada field '.$log['trans'].' di database '.$log['db'].'.';
        $model = new Log();
        $model->user_id = $id;
        $model->tdate = $tdate;
        $model->trans_id = $log['trans_id'];
        $model->log = $logz;
        if (!$model->save()){
            throw new Exception("Gagal menyimpan data.".CHtml::errorSummary($model));
        }
    }
    static function runCommand($command, $action, $id, $log = null)
    {
        console($command, $action, $id, $log);
    }
    static function makeListString($data) {
        $list = explode(";",$data);
        $result = '';
        foreach ($list as $l) {
            $result .= "'" . $l . "',";
        }
        $result = substr($result, 0, -1);
        return $result;
    }
    static function makeLikeQuery($data, $var) {
        $list = explode(";",$data);
        $result = '';
        foreach ($list as $l) {
            $result .= "$var LIKE '" . $l . "%' OR ";
        }
        $result = substr($result, 0, -4);
        $result = "(" . $result . ")";
        return $result;
    }
}
