<?php
//Yii::import('application.vendors.*');
//require 'tad/TADFactory.php';
//require 'tad/TAD.php';
//require 'tad/TADResponse.php';
//require 'tad/Providers/TADSoap.php';
//require 'tad/Providers/TADZKLib.php';
//require 'tad/Exceptions/ConnectionError.php';
//require 'tad/Exceptions/FilterArgumentError.php';
//require 'tad/Exceptions/UnrecognizedArgument.php';
//require 'tad/Exceptions/UnrecognizedCommand.php';
//Yii::import('application.components.U');

class ValidasiData {

    public function step0($periode_id) {
        echo CJSON::encode(array(
            'success' => false,
            'msg' => "Fungsi tidak tersedia"
        ));
    }

    private function X($periodeStart, $periodeEnd, $pinId) {

        date_default_timezone_set('Asia/Jakarta');
        $pegawai_id = $pinId;
        $tglcut1str = $periodeStart;
        $tglcut2str = $periodeEnd;
        $tglcut1dt = new DateTime($tglcut1str);
        $tglcut2dt = new DateTime($tglcut2str);
        $interval = $tglcut1dt->diff($tglcut2dt);
        $result = [];

        //CEK DATA PERTAMA SESUAI DENGAN JADWAL YANG ADA ATAU TIDAK
        Yii::import('application.models.Store');
        //app()->db->autoCommit = false;
        //$conn = app()->db->beginTransaction();
        //try {
        $res1 = Yii::app()->db->createCommand('
            SELECT 
            a.shift_id,
            c.kode_shift,
            b.kode_day,
            b.day_name,
            c.earlyin_time,
            c.latein_time,
            c.earlyout_time,
            c.lateout_time,
            c.in_time,
            c.out_time
            FROM 
            `pbu_dayshift` a
            INNER JOIN pbu_days b ON a.day_id=b.day_id
            INNER JOIN pbu_shift c ON a.shift_id=c.shift_id
            INNER JOIN pbu_shiftpin d ON d.shift_id=a.shift_id
            WHERE d.pin_id=:pin_id
            ');
        $shift = $res1->queryAll(true, array(
            ':pin_id' => $pegawai_id));

        for ($x = 0; $x <= $interval->format('%a'); $x++) {
            $tgl = new DateTime($tglcut1dt->format('Y-m-d H:i:s'));
            $tgl->add(new DateInterval('P' . $x . 'D'));
            $dw = $tgl->format('w');

            foreach ($shift as $row) {

                if ($row['kode_day'] == $dw) {
                    $shift_id = $row['shift_id'];
                    $expit = explode(':', $row['in_time']);
                    $expeit = explode(':', $row['earlyin_time']);
                    $explit = explode(':', $row['latein_time']);
                    $expot = explode(':', $row['out_time']);
                    $expeot = explode(':', $row['earlyout_time']);
                    $explot = explode(':', $row['lateout_time']);

                    $timeindt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $timeindt->setTime($expit[0], $expit[1], $expit[2]);

                    $earlytimeindt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $earlytimeindt->setTime($expeit[0], $expeit[1], $expeit[2]);

                    $latetimeindt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $latetimeindt->setTime($explit[0], $explit[1], $explit[2]);

                    $timeoutdt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $timeoutdt->setTime($expot[0], $expot[1], $expot[2]);

                    $earlytimeoutdt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $earlytimeoutdt->setTime($expeot[0], $expeot[1], $expeot[2]);

                    $latetimeoutdt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $latetimeoutdt->setTime($explot[0], $explot[1], $explot[2]);

                    if ($earlytimeindt > $timeindt) {
                        $earlytimeindt->sub(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($latetimeindt < $timeindt) {
                        $latetimeindt->add(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($timeindt > $timeoutdt) {
                        $timeoutdt->add(new DateInterval('P' . 1 . 'D'));
                        $earlytimeoutdt->add(new DateInterval('P' . 1 . 'D'));
                        $latetimeoutdt->add(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($earlytimeoutdt > $timeoutdt) {
                        $earlytimeoutdt->sub(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($latetimeoutdt < $timeoutdt) {
                        $latetimeoutdt->add(new DateInterval('P' . 1 . 'D'));
                    }

                    $preview = Yii::app()->db->createCommand('
                     SELECT PIN, jam_in,jam_out,shift_id,
                     status_int_in, status_int_out,
                        kode_ket_in, kode_ket_out,
                        tipe_data_in, tipe_data_out,
                        fp_id_in, fp_id_out, log_in, log_out
                     
                     FROM (
                        SELECT a.PIN, a.jam_in,b.jam_out,d.shift_id, 
                        a.status_int_in, b.status_int_out,
                        a.kode_ket_in, b.kode_ket_out,
                        a.log_in, b.log_out,
                        a.tipe_data_in, b.tipe_data_out,
                        a.fp_id_in, b.fp_id_out
                        
                        FROM (
                                SELECT
                                    aa.fp_id AS fp_id_in,
                                    aa.PIN,
                                    Min(aa.DateTime_) jam_in,
                                    aa.status_int AS status_int_in,
                                    aa.kode_ket AS kode_ket_in,
                                    aa.log AS log_in,
                                    aa.tipe_data AS tipe_data_in
                                FROM
                                    pbu_fp as aa
                                WHERE
                                    aa.DateTime_ > :dtei
                                    AND aa.DateTime_ < :dtli
                                    AND aa.status_int != 2
                                GROUP BY
                                    aa.PIN
                                    ORDER BY aa.DateTime_ ASC
                        ) AS a
                    INNER JOIN (
                        SELECT
                            bb.fp_id AS fp_id_out,
                            bb.PIN,
                            Min(bb.DateTime_) jam_out,
                            bb.status_int AS status_int_out,
                            bb.kode_ket AS kode_ket_out,
                            bb.log AS log_out,
                            bb.tipe_data AS tipe_data_out
                        FROM
                            pbu_fp as bb
                        WHERE
                            bb.DateTime_ > :dteo
                            AND bb.DateTime_ < :dtlo
                            AND bb.status_int != 2
                        GROUP BY
                            bb.PIN
                            ORDER BY bb.DateTime_ ASC
                    ) AS b ON a.PIN = b.PIN
                    INNER JOIN pbu_pin c ON c.PIN=a.PIN
                    INNER JOIN pbu_shiftpin d ON d.pin_id=c.pin_id
                    WHERE d.shift_id = :shift_id AND c.pin_id=:pegawai_id) as a1
                    ORDER BY a1.PIN ASC
                        ');
                    $previewx = $preview->queryAll(true, array(
                        ':dtei' => $earlytimeindt->format('Y-m-d H:i:s'),
                        ':dtli' => $latetimeindt->format('Y-m-d H:i:s'),
                        ':dteo' => $earlytimeoutdt->format('Y-m-d H:i:s'),
                        ':dtlo' => $latetimeoutdt->format('Y-m-d H:i:s'),
                        ':shift_id' => $shift_id,
                        ':pegawai_id' => $pegawai_id
                    ));

                    foreach ($previewx as $xf) {
                        $rslt = [];
                        $rslt['PIN'] = $xf['PIN'];
                        $rslt['jam_in'] = $xf['jam_in'];
                        $rslt['jam_out'] = $xf['jam_out'];
                        $rslt['status_int_in'] = $xf['status_int_in'];
                        $rslt['status_int_out'] = $xf['status_int_out'];
                        $rslt['tipe_data_in'] = $xf['tipe_data_in'];
                        $rslt['tipe_data_out'] = $xf['tipe_data_out'];
                        $rslt['kode_ket_in'] = $xf['kode_ket_in'];
                        $rslt['kode_ket_out'] = $xf['kode_ket_out'];
                        $rslt['log_in'] = $xf['log_in'];
                        $rslt['log_out'] = $xf['log_out'];
                        $rslt['fp_id_in'] = $xf['fp_id_in'];
                        $rslt['fp_id_out'] = $xf['fp_id_out'];
                        $result[] = $rslt;
                    }
                }
            }
        }
        return $result;
    }

}
