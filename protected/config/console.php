<?php
return array(
    'basePath' => dirname(__file__) . DIRECTORY_SEPARATOR . '..',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'application.vendors.*',
    ),
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host='.DBHOST.';dbname='.DBNAME.';port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'pbu_',
            'username' => DBUSER,
            'password' => DBPASS,
            'charset' => 'utf8',
        )
    )
);
