<?php
return array(
    'basePath' => dirname(__file__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'AMOS',
    'theme' => 'extjs',
    'sourceLanguage' => 'xx',
    'language' => 'en',
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        'application.vendors.*'
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'admin',
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array('ext.giix-core',
            ),
        ),
    ),
    'components' => array(
        'CGridViewPlus' => array(
            'class' => 'components.CGridViewPlus',
        ),
		'curl'=> array(
                'class'    => 'ext.curl.Curl',
                'base_url' => 'http://hrd.ena/api/',
                'headers'  => array(
                    'Authorization' => 'B7076B5F-269E-11E8-8341-6DB33A098066'
                ),
        ),
        'user' => array(
            'loginUrl' => array('site/login'),
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host='.DBHOST.';dbname='.DBNAME.';port=3306',
            'emulatePrepare' => true,
            'tablePrefix' => 'pbu_',
            'username' => DBUSER,
            'password' => DBPASS,
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'session' => array(
            'sessionName' => 'nscc_pbu',
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable' => false,
            'connectionID' => 'db',
            'sessionTableName' => 'pbu_session',
            'useTransparentSessionID' => isset($_POST['PHPSESSID']) ? true : false,
            'autoStart' => 'false',
            'cookieMode' => 'only',
            'timeout' => 10800
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                    'filter' => 'CLogFilter',
                    'maxFileSize' => 1024,
                    'maxLogFiles' => 10
                ),
            ),
        )
    )
);
