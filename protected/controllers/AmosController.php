<?php

Yii::import('application.models.Store');
class AmosController extends GxController {
    public function actionValidasiPhaseAll() {
        LockPost::model()->check_lockpost($_POST['periode_id']);

        $periode_id = $_POST['periode_id'];
        $cabang_id  = $_POST['cabang_id'];
        $fase1      = $_POST['fase1'];
        $fase2      = $_POST['fase2'];
        $count      = 0;
        $count2     = 0;

        $msg    = "Data gagal disimpan";
        $status = false;
        try {
            $dbperiode      = Periode::getPeriodeStartEnd($periode_id);
            $periodenow     = $dbperiode->queryRow();
            $periodeStart   = $periodenow['ps'];
            $periodeEnd     = $periodenow['pe'];

            if($fase1 === "true") {
                $tglmulai   = date('Y-m-d',strtotime("-2 day",strtotime($periodeStart)));
                $tglsampai  = date('Y-m-d',strtotime("+1 day",strtotime($periodeEnd)));
                $s          = new TarikAbsen;
                $s->Absensi($tglmulai, $tglsampai, $cabang_id);
            }

            $db = Pegawai::IndexPegawai($_POST['bu_id']);
            $db->addCondition("pp.cabang_id = :cabang_id")
                ->addParam(":cabang_id", $cabang_id)
            ;
            $pegawai = $db->queryAll();

            if($fase1 === "true") {
                foreach ($pegawai as $p) {
                    $pegawai_id         = $p['pegawai_id'];
                    $cabang_pegawai     = $p['cabang_id'];

                    $array = Validasi::addAdditionalValidasi($p['pegawai_id'], $periode_id);

                    $result = $this->X($periodeStart, $periodeEnd, $pegawai_id, $periode_id);
                    foreach ($result as $resultX) {
                        $fingerin       = Fp::model()->find("DateTime_ = :jam_in AND cabang_id = :cabang_id AND PIN = :pin",
                            [":jam_in" => $resultX["jam_in"], ":cabang_id" => $resultX["cabang_id"], ":pin" => $p['nik']]);
                        $fingerout      = Fp::model()->find("DateTime_ = :jam_out AND cabang_id = :cabang_id AND PIN = :pin",
                            [":jam_out" => $resultX["jam_out"], ":cabang_id" => $resultX["cabang_id"], ":pin" => $p['nik']]);

                        $cabangfinger   = $fingerin->cabang_id;
                        $day_in         = date_format(date_create($fingerin->DateTime_),"Y-m-d");
                        $day_out        = date_format(date_create($fingerout->DateTime_),"Y-m-d");

                        $check = Validasi::model()->find(
                            '(in_time like :in_time AND out_time like :out_time)
                            #OR (in_time like :in_time AND out_time like :in_time)
                            #OR (in_time like :out_time AND out_time like :out_time) 
                            AND pegawai_id = :pegawai_id 
                            AND status_int = 1',
                            ['in_time' => $day_in . '%','out_time' => $day_out . '%','pegawai_id' => $pegawai_id]
                        );

                        if($fingerin->status_int == 0 && $fingerout->status_int == 0) {
                            if($fingerin->Status == FP_NON_AKTIF || $fingerout->Status == FP_NON_AKTIF) {
                                continue;
                            }
                            if ($check) {
                                $validasi               = Validasi::model()->findByPk($check->validasi_id);
                                $validasi->status_int   = 2;
                                $validasi->save();
                            }

                            $tglin              = $fingerin->DateTime_;
                            $tglout             = $fingerout->DateTime_;
                            $resultX['fase2']   = 0;
                            $valid = $this->saveValidasiAmos($periode_id,$pegawai_id,$tglin,$tglout,$cabang_pegawai,$cabangfinger,$count,$resultX, $array);
                            if($valid == 'gagal'){
                                continue;
                            } else {
                                $count = $valid['count'];
                            }

                            $fingerin->kode_ket     = $valid['kode_ket'];
                            $fingerin->log          = 1;
                            $fingerin->status_int   = 1;
                            $fingerin->Status       = FP_NON_AKTIF;
                            if (!$fingerin->save()) {
                                return 'gagal';
                            }

                            $fingerout->kode_ket    = $valid['kode_ket'];
                            $fingerout->log         = 1;
                            $fingerout->status_int  = 1;
                            $fingerout->Status      = FP_NON_AKTIF;
                            if (!$fingerout->save()) {
                                return 'gagal';
                            }

                        } else {
                            continue;
                        }
                    }
                }
            }

            if($fase2 === "true") {
                foreach ($pegawai as $p) {
                    $pegawai_id     = $p['pegawai_id'];
                    $cabang_pegawai = $p['cabang_id'];
                    $dbfp = new DbCmd();
                    $dbfp->addFrom("{{fp}}")
                        ->addSelect("PIN, DATE_FORMAT(DateTime_,'%H:%i:%s') jam")
                        ->addSelect("DATE_FORMAT(DateTime_,'%Y-%m-%d') tgl")
                        ->addSelect("cabang_id, PIN, fp_id")
                        ->addCondition("DateTime_ >= :pstart")
                        ->addCondition("DateTime_ <= :pend + INTERVAL " . GET_INVTERVAL_ABSEN() . " HOUR")
                        ->addCondition("PIN = :pin")
                        ->addCondition("log = 0")
                        ->addParams([
                            ":pstart" => $periodeStart,
                            ":pend" => $periodeEnd,
                            ":pin" => $p['nik']
                        ])
                        ->addOrder("DateTime_")
                        ;
                    $listfp = $dbfp->queryAll();

                    foreach ($listfp as $fp) {
                        $cabangfinger   = $fp['cabang_id'];

                        $cmd1 = new DbCmd();
                        $cmd1->addFrom("{{validasi}}")
                            ->addSelect("DATE_FORMAT(in_time,'%H:%i:%s') masuk")
                            ->addSelect("DATE_FORMAT(out_time,'%H:%i:%s') keluar")
                            ->addSelect("shift_id")
                            ->addCondition("status_int = 1")
                            ->addCondition("PIN = :pin")
                            ->addCondition("in_time >= :pstart - INTERVAL 3 MONTH")
                            ->addCondition("in_time <= :pstart")
                            ->addParams([
                                ":pstart" => $periodeStart . " 00:00:00",
                                ":pin" => $p['nik']
                            ])
                        ;

                        $cmd2 = new DbCmd();
                        $cmd2->addSelect("*, (:jam) jam, masuk - (:jam) s1, keluar - (:jam) s2")
                            ->addFrom("(".$cmd1->getQuery().") t")
                            ->addParam(":jam", $fp['jam'])
                        ;

                        $cmd3 = new DbCmd();
                        $cmd3->addSelect("ABS(SUM(s1)) rangemasuk, ABS(SUM(s2)) rangekeluar")
                            ->addFrom("(".$cmd2->getQuery().") t2")
                            ->addOrder("masuk desc")
                        ;

                        $resultz = $cmd3->getQuery();
                        $result = $cmd3->queryRow();

                        if(!isset($result['rangemasuk']) || !isset($result['rangekeluar'])) {
                            continue;
                        }

                        if($result['rangemasuk'] < $result['rangekeluar']) {
                            $cmd2->clearSelect()->clearOrder()
                                ->addOrder("abs(masuk - (:jam))")
                            ;
                        } else {
                            $cmd2->clearSelect()->clearOrder()
                                ->addOrder("abs(keluar - (:jam))")
                            ;
                        }
                        $cmd2->addSelect("t.*, (:jam) jam")
                            ->addSelect("if(masuk < keluar,-1,1) x")
                            ->setLimit(1)
                            ->addLeftJoin("{{shift}} s","s.shift_id = t.shift_id")
                            ;

                        $qshift     = $cmd2->queryRow();

                        if($result['rangemasuk'] < $result['rangekeluar']) {
                            $tglin = $fp['tgl'] . " " . $fp['jam'];
                            if($qshift['x'] == -1){
                                $tglout = date('Y-m-d',strtotime($fp['tgl'])) . " " . $qshift['keluar'];
                            } else {
                                $tglout = date('Y-m-d',strtotime("+1 day",strtotime($fp['tgl']))) . " " . $qshift['keluar'];
                            }
                        } else {
                            $tglout = $fp['tgl'] . " " . $fp['jam'];
                            if($qshift['x'] == -1){
                                $tglin = date('Y-m-d',strtotime($fp['tgl'])) . " " . $qshift['masuk'];
                            } else {
                                $tglin = date('Y-m-d',strtotime("-1 day",strtotime($fp['tgl']))) . " " . $qshift['masuk'];
                            }
                        }

                        $check = Validasi::model()->find(
                            'in_time like :in_time 
                            AND out_time like :out_time 
                            AND pegawai_id = :pegawai_id 
                            AND status_int = 1',
                            ['in_time' => substr($tglin,0,10) . '%','out_time' => substr($tglout,0,10) . '%','pegawai_id' => $pegawai_id]
                        );
                        if ($check) {
                            continue;
                        }

                        $resultX['shift_id'] = $qshift['shift_id'];
                        $resultX['PIN']      = $p['nik'];
                        $resultX['fase2']    = 1;

                        $array = Validasi::addAdditionalValidasi($pegawai_id, $periode_id);
                        $valid = $this->saveValidasiAmos($periode_id,$pegawai_id,$tglin,$tglout,$cabang_pegawai,$cabangfinger,$count2,$resultX, $array);
                        if($valid == 'gagal'){
                            continue;
                        } else {
                            $count2 = $valid['count'];
                        }
                    }
                }
            }

            $total  = $count + $count2;
            $msg    = "Data berhasil disimpan. </br> 
                        <b>Fase 1 = " . $count . " </b> </br>
                        <b>Fase 2 = " . $count2 . " </b> </br>
                        <b>Total semuanya = " . $total . "</b>";
            $status = true;
        } catch (Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
    }

    private static function saveValidasiAmos($periode_id,$pegawai_id,$tglin,$tglout,$cabang_pegawai,$cabangfinger,$count,$resultX, $array) {
        $cabang_pusat = Cabang::model()->getCabangPusatFromBu($pegawai_id);

        $valid              = new Validasi;
        $transferpegawai    = TransferPegawai::model()->checkPegawai($periode_id, $pegawai_id, $tglin, $tglout);

        if(isset($transferpegawai)){
            if($transferpegawai->cabang_transfer_id != $cabangfinger){
                return 'gagal';
            } else {
                $kode_ket       = $transferpegawai->status_kota;
                $cabang_id      = $transferpegawai->cabang_transfer_id;
                $no_surat_tugas = $transferpegawai->no_surat_tugas;
            }
        } else {
            if ($cabang_pegawai != $cabangfinger) {
                if ($cabang_pegawai == $cabang_pusat) {
                    $cabang_id      = $cabang_pusat;
                    $kode_ket       = $resultX['fase2'] == 1 ? 3 : 0;
                    $no_surat_tugas = '';
                } else {
                    $cabang_id      = $cabangfinger;
                    $kode_ket       = 1;
                    $no_surat_tugas = '';
                }
            } else {
                $cabang_id      = $cabang_pegawai;
                $kode_ket       = $resultX['fase2'] == 1 ? 3 : 0;
                $no_surat_tugas = '';
            }
        }

        $shift_id = $resultX['shift_id'];
        if(isset($shift_id)){
            $count++;
        }

        $k['PIN']       = $resultX['PIN'];
        $k['jam_in']    = $tglin;
        $k['jam_out']   = $tglout;
        $duras          = Fp::model()->durasi($pegawai_id, $shift_id, $k['jam_in'], $k['jam_out'], '','');
        $validasi       = Validasi::saveValidasi($valid, $pegawai_id, $k, $shift_id, $duras, $kode_ket, $cabang_id, $no_surat_tugas);

        $valid->attributes = array_merge($validasi, $array);
        if (!$valid->save()) {
            return 'gagal';
        }
        return [
            'count'     => $count,
            'kode_ket'  => $kode_ket
        ];
    }
}
