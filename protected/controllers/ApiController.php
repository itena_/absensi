<?php

/**
 * Created by PhpStorm.
 * User: nove
 * Date: 2/27/18
 * Time: 2:04 PM
 */
class ApiController extends GxController
{
    const APPLICATION_ID = 'B7076B5F-269E-11E8-8341-6DB33A098066';
    const HEADER_KEY = 'Authorization';
    private $format = 'json';

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users' => array('*')
            )
        );
    }

    private function _checkAuth()
    {

        $headers = getallheaders();
        if (!array_key_exists(self::HEADER_KEY, $headers)) {
            $this->_sendResponse(401);
        }
        $value = $headers[self::HEADER_KEY];
        if ($value === false) {
            $this->_sendResponse(401);
        } elseif ($value !== self::APPLICATION_ID) {
            $this->_sendResponse(401);
        }
    }

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        header('Content-type: ' . $content_type);
        if ($body != '') {
            echo $body;
        } else {
            $message = '';
            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];
            $body = '
                        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                        <html>
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                            <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                        </head>
                        <body>
                            <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                            <p>' . $message . '</p>
                            <hr />
                            <address>' . $signature . '</address>
                        </body>
                        </html>
            ';
            echo $body;
        }
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status)
    {
        $codes = array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function getClientAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function actionGetToken()
    {
        if (!(isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW']))) {
            $this->_sendResponse(401);
        }
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        $password = hash_password($password);
        $model = new LoginForm;
        if ($username != "") {
            $model->username = $username;
            $model->password = $password;
            if ($model->validate() && $model->login()) {
                echo self::APPLICATION_ID;
            } else
                $this->_sendResponse(401);
        } else {
            $this->_sendResponse(401);
        }
    }

    public function actionGetAllValidasi()
    {
        self::_checkAuth();

        $periode_id = Periode::model()->findByAttributes([
            'periode_start' => $_POST['tglin'] . ' 00:00:00',
            'periode_end' => $_POST['tglout'] . ' 23:59:59'
        ])->periode_id;

        $command = Yii::app()->db->createCommand("
            SELECT
            v.*, shk.nama status_hk,
            IF(approval_lembur = 1,real_lembur_pertama,0) L1,
            IF(approval_lembur = 1,real_lembur_akhir,0) L2,
            IF(approval_lembur = 1,real_lembur_hari,0) LH,
            IF(approval_lesstime = 1 AND (kode_ket = 3 OR kode_ket = 0 OR kode_ket = 1 OR kode_ket = 10),real_less_time,0) LT
            from pbu_validasi v
            LEFT JOIN pbu_status_hk_periode shkp on shkp.pegawai_id = v.pegawai_id AND shkp.periode_id = :periode_id
            LEFT JOIN pbu_status_hk shk ON shk.status_hk_id = shkp.status_hk_id
            where v.cabang_id in (
                select cabang_id from 
                pbu_cabang where bu_id = :bu_id
            )
            AND in_time >= :tglin
            AND out_time <= (:tglout + INTERVAL :INVTERVAL_ABSEN HOUR)
            AND status_int = 1
        ");
        $models = $command->queryAll(true, [
            ':tglin' => $_POST['tglin'] . ' 00:00:00',
            ':tglout' => $_POST['tglout'] . ' 23:59:59',
            ':bu_id' => $_POST['bu_id'],
            ':periode_id' => $periode_id,
            ':INVTERVAL_ABSEN' => GET_INVTERVAL_ABSEN()
        ]);
        $total = count($models);
        $this->_sendResponse(200, $this->renderJsonArrWithTotal($models, $total));
    }

    public function actionGetFpFromDb()
    {
        $nik = $_POST['nik'];
        $dbcmd = new DbCmd();
        $dbcmd->addFrom("{{fp}}")
            ->addSelect("*")
            ->addCondition("PIN in ($nik)")
            ->addCondition("cabang = :cabang AND (DateTime_ >= :pstart and DateTime_ <= :pend)")
            ->addParams([
                ':cabang' => $_POST['cabang'],
                ':pstart' => $_POST['pstart'],
                ':pend' => $_POST['pend']
            ]);
        $models = $dbcmd->queryAll();
        $total = count($models);
        $this->_sendResponse(200, $this->renderJsonArrWithTotal($models, $total));
    }

    public function actionSaveFpFromAMOSMobile()
    {
        $nik = $_GET['nik'];
        $date = date('Y-m-d H:i:s');
        $cabang = $_GET['cabang'];
        $cabang_id = $_GET['cabang_id'];

        $models = [
            "fp_id" => '',
            "nik" => $nik,
            "tgl" => '',
            "cabang" => '',
            "cabang_id" => ''
        ];
        $count = 0;

        if (Pegawai::model()->find("nik = :nik", [':nik' => $nik])) {
            $result = Fp::saveFpWithReturn($nik, $date, $cabang, $cabang_id, 77);
            $models = [
                "fp_id" => $result['fp_id'],
                "nik" => $result['PIN'],
                "tgl" => $result['DateTime_'],
                "cabang" => $result['cabang'],
                "cabang_id" => $result['cabang_id']
            ];
            $count = 1;
        }

        $this->_sendResponse(200, $this->renderJsonArrWithTotal($models, $count));
    }

    public function actionGetPegawaiAmos()
    {
        $nik = $_GET['nik'];
        $command = DbCmd::instance()->addFrom("{{pegawai}} p")
            ->addSelect("nik, store, 'AKTIF' status_pegawai")
            ->addCondition("nik = :nik")
            ->addInCondition('status_pegawai_id', get_status_pegawai_aktif())
            ->addParam(':nik', $nik);
        $models = $command->getQuery();
        $models = $command->queryAll();
        $total = count($models);
        $this->_sendResponse($total > 0 ? 200 : 300, $this->renderJsonArrWithTotal($models, $total));
    }

    public function actionSaveValidasi()
    {
        $message = '';
        $success = true;
        $data = [];
        $create_new = true;
        $ambil_cuti_before = 0;
        $ambil_cuti_now = 0;
        $list_tipe = [
            'CUTI_TAHUNAN' => 5,
            'CUTI_MENIKAH' => 6,
            'CUTI_BERSALIN' => 7,
            'CUTI_ISTIMEWA' => 8,
            'CUTI_NON_AKTIF' => 9
        ];

        try {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            $nik = !isset($_POST['nik']) ? '' : $_POST['nik'];
            $list_tanggal = !isset($_POST['tanggal']) ? '' : $_POST['tanggal'];
            $tipe = !isset($_POST['tipe']) ? '' : $_POST['tipe'];
            $tahun_cuti = !isset($_POST['tahun_cuti']) ? '' : $_POST['tahun_cuti'];
            $kode_cabang = !isset($_POST['kode_cabang']) ? '' : $_POST['kode_cabang'];
            $no_pengajuan = !isset($_POST['no_pengajuan']) ? '' : $_POST['no_pengajuan'];
            $mendadak = !isset($_POST['mendadak']) ? 0 : $_POST['mendadak'];

            $perubahan = !isset($_POST['perubahan']) ? '' : $_POST['perubahan'];
            if ($perubahan != '')
                $perubahan = (object) json_decode($perubahan);

            if ($nik == '' || $tipe == '' || $kode_cabang == '' || $list_tanggal == '')
                throw new Exception('NIK/tanggal/tipe/kode cabang tidak boleh kosong');

            if ($list_tanggal != '')
                $list_tanggal = json_decode($list_tanggal);

            if (!isset($list_tipe[$tipe]))
                throw new Exception('Tipe '.$tipe.' tidak teridentifikasi di AMOS, yg tersedia ['.implode(',',array_keys($list_tipe)).']');
            else
                $kode_ket = $list_tipe[$tipe];

            $db_p = new DbCmd();
            $db_p->select('p.*, c.kode_cabang, j.kode as kode_jabatan, l.kode as level, g.kode as golongan, s.kode as status_pegawai');
            $db_p->from('pbu_pegawai as p');
            $db_p->addInnerJoin('pbu_cabang as c', 'c.cabang_id=p.cabang_id');
            $db_p->addInnerJoin('pbu_jabatan as j', 'j.jabatan_id=p.jabatan_id');
            $db_p->addInnerJoin('pbu_leveling as l', 'l.leveling_id=p.leveling_id');
            $db_p->addInnerJoin('pbu_golongan as g', 'g.golongan_id=p.golongan_id');
            $db_p->addInnerJoin('pbu_status_pegawai as s', 's.status_pegawai_id=p.status_pegawai_id');
            $db_p->addCondition('p.nik = :nik');
            $db_p->addParam(':nik', $nik);
            $pegawai = (object) $db_p->queryRow();
            if (!$pegawai)
                throw new Exception('NIK tidak ditemukan di AMOS');

            $cabang = Cabang::model()->findByAttributes(['kode_cabang' => $kode_cabang]);
            if (!$cabang)
                throw new Exception('Kode Cabang tidak ditemukan di AMOS');

            $shift = Shift::model()->find('cabang_id = :cabang_id and active=1', [':cabang_id' => $cabang->cabang_id]);
            if (!$shift)
                throw new Exception('Shift untuk cabang '.$kode_cabang.' belum dibuat di AMOS');

            foreach ($list_tanggal as $tanggal) {
                $periode = Periode::model()->find(':tanggal BETWEEN periode_start AND periode_end', [':tanggal' => $tanggal]);
                if (!$periode)
                    throw new Exception('Periode belum dibuat di AMOS');

                if ($pegawai->kode_cabang != $cabang->kode_cabang) {
                    $transferpegawai = TransferPegawai::model()->checkPegawai($periode->periode_id, $pegawai->pegawai_id, $tanggal, $tanggal);
                    if (!$transferpegawai)
                        throw new Exception('Anda tidak bisa membuat cuti untuk NIK ' . $nik . ', dikarenakan karyawan tersebut adalah karyawan cabang ' . $pegawai->kode_cabang);
                }
            }

            $tahun_kemarin = $tahun_cuti - 1;
            $sisa_cuti_kemarin = ($tahun_kemarin <= 2023) ? 0 : Validasi::HitungAmbilCuti($nik, $tahun_kemarin);

            //cek jika perintah merubah
            if ($perubahan != '')
            {
                $cnt_obj = count(get_object_vars($perubahan));
                if ($cnt_obj <= 0)
                    throw new Exception('Perubahan tidak boleh kosong');

                $list_tanggal_new = !isset($perubahan->tanggal) ? '' : $perubahan->tanggal;
                $perubahan_status = !isset($perubahan->status) ? 1 : $perubahan->status;

                //disable all cuti yg dari tanggal awal
                foreach ($list_tanggal as $tanggal) {
                    $cek = Validasi::model()->find('pegawai_id = :pegawai_id and date(in_time) = :tanggal and kode_ket = :kode_ket and status_int = 1',
                        [
                            ':pegawai_id' => $pegawai->pegawai_id,
                            ':tanggal' => $tanggal,
                            ':kode_ket' => $kode_ket
                        ]);

                    if (!$cek)
                        throw new Exception('Tidak bisa melakukan perubahan data di AMOS, dikarenakan NIK ' .
                            $nik . ' pada tanggal ' . $tanggal . ' bukan ' . $tipe);
                    else {
                        if ($cek->cuti_tahun == $tahun_kemarin)
                            $sisa_cuti_kemarin++;

                        $cek->status_int = 0;
                        if (!$cek->save())
                            throw new Exception(CHtml::errorSummary($cek));
                    }
                }

                //jika perubahan hanya untuk membatalkan cuti saja $perubahan_status harus bernilai 0
                if ($perubahan_status == 0)
                    $create_new = false;
                else
                    $list_tanggal = $list_tanggal_new; //jika diganti cuti yg lain

                $mendadak = !isset($perubahan->mendadak) ? $perubahan->mendadak : 0;
            }

            //cek apakah ada validasi lain di tanggal tsb
            //tidak boleh menimpa data yg sudah ada
            foreach ($list_tanggal as $tanggal)
            {
                $cek_validasi = Validasi::model()->find('pegawai_id = :pgw and date(in_time) = :tgl and status_int = 1',
                    [
                        ':pgw'  => $pegawai->pegawai_id,
                        ':tgl'  => $tanggal
                    ]);
                if ($cek_validasi)
                    throw new Exception('Tidak bisa membuat data di tanggal '.$tanggal.', dikarenakan masih ada data di AMOS');
            }

            if ($create_new) {
                foreach ($list_tanggal as $tanggal) {
                    $periode = Periode::model()->find(':tanggal BETWEEN periode_start AND periode_end', [':tanggal' => $tanggal]);

                    $model = new Validasi();
                    if ($sisa_cuti_kemarin >= 1)
                    {
                        $model->cuti_tahun = $tahun_kemarin;
                        $ambil_cuti_before++;
                        $sisa_cuti_kemarin--;
                    }
                    else {
                        $model->cuti_tahun = $tahun_cuti;
                        $ambil_cuti_now++;
                    }

                    if ($mendadak == 1)
                    {
                        if ($sisa_cuti_kemarin >= 1)
                        {
                            $model->cuti_tahun = $tahun_kemarin;
                            $ambil_cuti_before++;
                            $sisa_cuti_kemarin--;
                        }
                        else {
                            $model->cuti_tahun = $tahun_cuti;
                            $ambil_cuti_now++;
                        }
                    }

                    $model->pegawai_id = $pegawai->pegawai_id;
                    $model->pin_id = $pegawai->nik;
                    $model->PIN = $pegawai->nik;
                    $model->in_time = $tanggal . ' ' . $shift->in_time;
                    $model->out_time = $tanggal . ' ' . $shift->out_time;
                    $model->min_early_time = 0;
                    $model->min_late_time = 0;
                    $model->min_least_time = 0;
                    $model->min_over_time_awal = 0;
                    $model->min_over_time = 0;
                    $model->kode_ket = $kode_ket;
                    $model->no_surat_tugas = $no_pengajuan;
                    $model->status_int = 1;
                    $model->user_id = 'bank_dokumen';
//                $model->status_pegawai_id = null; ini apa ndak tau
                    $model->shift_id = $shift->shift_id;
                    $model->approval_lembur = 0;
                    $model->approval_lesstime = 0;
                    $model->real_lembur_pertama = 0;
                    $model->real_lembur_akhir = 0;
                    $model->real_lembur_hari = 0;
                    $model->real_less_time = 0;
                    $model->cabang_id = $cabang->cabang_id;
                    $model->cuti = ($mendadak == 1) ? 2 : 1;
                    $model->limit_lembur = 0;
                    $model->short_shift = 0;
                    $model->status_wfh = 0;
                    $model->jabatan = $pegawai->kode_jabatan;
                    $model->level = $pegawai->level;
                    $model->golongan = $pegawai->golongan;
                    $model->status_pegawai = $pegawai->status_pegawai;
                    $model->periode_id = $periode->periode_id;

                    if (!$model->save())
                        throw new Exception(CHtml::errorSummary($model));
                }

                $data = [
                    'ambil_cuti_before' => $ambil_cuti_before,
                    'ambil_cuti_now'    => $ambil_cuti_now,
                ];
            }
            $transaction->commit();
        }
        catch (Exception $ex)
        {
            $transaction->rollback();
            $success = false;
            $message = $ex->getMessage();
        }
        app()->db->autoCommit = true;

        $ret = [
            'success'   => $success,
            'message'   => $message,
            'data'      => $data
        ];
        $this->_sendResponse(200, CJSON::encode($ret));
    }

    public function actionGetPegawaiJatahCuti()
    {
        $message = '';
        $status = true;
        $data = [];
        $now  = new DateTime();
        $bulan_now = $now->format('n');
        $ambil_cuti_kemarin = 0;
        $proporsional = false;

        try {
            $nik = !isset($_POST['nik']) ? '' : $_POST['nik'];
            $tahun = !isset($_POST['tahun']) ? '' : $_POST['tahun'];
            $jatah_cuti_now = !isset($_POST['jatah_cuti_now']) ? '' : $_POST['jatah_cuti_now'];
            $jatah_cuti_before = !isset($_POST['jatah_cuti_before']) ? '' : $_POST['jatah_cuti_before'];
            $cuti_tahunan = ['5'];

            if ($nik == '' || $tahun == '' || $jatah_cuti_now == '')
                throw new Exception('Data tidak lengkap');

            if ($tahun <= 2023)
                throw new Exception('Tidak boleh ambil cuti sebelum 2024');

            //cek cuti apakah ada tahun cuti yg kosong di validasi
            $db_cek = new DbCmd();
            $db_cek->select('date(v.in_time) as tgl_cuti');
            $db_cek->from('pbu_pegawai as p');
            $db_cek->leftJoin('pbu_validasi as v', 'p.pegawai_id=v.pegawai_id');
            $db_cek->addCondition(' p.nik = :nik');
            $db_cek->addCondition('year(v.in_time) >= 2024 and v.status_int = 1');
            $db_cek->addInCondition('v.kode_ket', $cuti_tahunan);
            $db_cek->addParam(':nik', $nik);
            $db_cek->addCondition("v.cuti_tahun is null or v.cuti_tahun = ''");

            $cek = $db_cek->queryAll();

            if (!$cek) {
                $tahun_kemarin = $tahun - 1;

                $pg = new DbCmd();
                $pg->select('p.nik, p.nama_lengkap, p.tgl_masuk, j.kode as kode_jabatan, j.nama_jabatan');
                $pg->from('pbu_pegawai as p');
                $pg->addLeftJoin('pbu_jabatan as j', 'p.jabatan_id=j.jabatan_id');
                $pg->addCondition('p.nik = :nik');
                $pg->addParam(':nik', $nik);
                $pegawai = (object) $pg->queryRow();

                $tgl_join = new DateTime($pegawai->tgl_masuk);
                $bln_join = $tgl_join->format('n');
                $masa_kerja = $now->diff($tgl_join);
                $masa_kerja_tahun = $masa_kerja->format('%y');

                if ($masa_kerja_tahun < 1)
                    throw new Exception('Belum berhak mendapat cuti karena masa kerja kurang dari 1 tahun');

                if ($masa_kerja_tahun == 1)
                {
                    $proporsional = true;
                    $prop = (12 - $bln_join) + 1;
                    if ($prop < $jatah_cuti_now)
                        $jatah_cuti_now = $prop;
                }

                //awal pakai applikasi di tahun 2024, jadi 2023 sisa cuti 2023 dianggap 0
                if ($tahun_kemarin <= 2023)
                    $sisa_cuti_kemarin = 0;
                else {
                    if ($bulan_now < 7) {
                        $ambil_cuti_kemarin = Validasi::HitungAmbilCuti($nik, $tahun_kemarin);
                        $sisa_cuti_kemarin = $jatah_cuti_before - $ambil_cuti_kemarin;
                    }
                    else
                        $sisa_cuti_kemarin = 0;
                }

                $cuti_diambil = Validasi::HitungAmbilCuti($nik, $tahun);
                $sisa_cuti = ($sisa_cuti_kemarin + $jatah_cuti_now) - $cuti_diambil;

                $data = [
                    'nik'                   => $pegawai->nik,
                    'nama'                  => $pegawai->nama_lengkap,
                    'join_date'             => $pegawai->tgl_masuk,
                    'kode_jabatan'          => $pegawai->kode_jabatan,
                    'nama_jabatan'          => $pegawai->nama_jabatan,
                    'sisa_cuti_total'       => $sisa_cuti,
                    'sisa_cuti_before'      => $sisa_cuti_kemarin,
                    'proporsional'          => $proporsional,
                    'masa_kerja'            => $masa_kerja_tahun
                ];
            }
            else {
                $list = '';
                foreach ($cek as $row_cek)
                    $list .= $row_cek['tgl_cuti'].', ';

                throw new Exception('NIK : '.$nik.' terdapat cuti yang belum ditentukan, silahkan edit di applikasi AMOS ['.$list.']');
            }
        }
        catch (Exception $ex)
        {
            $status = false;
            $message = $ex->getMessage();
        }
        $ret = [
            'status'    => $status,
            'message'   => $message,
            'data'      => $data
        ];
        $this->_sendResponse(200, CJSON::encode($ret));
//
//
//        $total = count($models);
//        $this->_sendResponse($total > 0 ? 200 : 300, $this->renderJsonArrWithTotal($models, $total));
//
//        $kode_ket = Keterangan::model()->findByAttributes(['nama_ket' => 'Cuti Tahunan'])->keterangan_id;
//
//        $command = DbCmd::instance()->addFrom("{{pegawai}} p")
//            ->addSelect("nik, nama_lengkap, store, 'AKTIF' status_pegawai")
//            ->addSelect("p.tgl_masuk, j.nama_jabatan, COUNT(*) jumlah_cuti")
//            ->addLeftJoin("{{jabatan}} j", "j.jabatan_id = p.jabatan_id")
//            ->addLeftJoin("{{validasi}} v", "v.pegawai_id = p.pegawai_id")
//            ->addCondition("nik = :nik and v.status_int = 1")
//            ->addCondition("v.kode_ket = '$kode_ket'")
//            ->addCondition("
//                      YEAR (v.in_time) >= '$tahun' AND
//                      YEAR (v.in_time) <= '$tahun'")
//            ->addInCondition('p.status_pegawai_id', get_status_pegawai_aktif())
//            ->addParam(':nik', $nik);
//
//        $models = $command->getQuery();
//        $models = $command->queryAll();
//        $total = count($models);
//        $this->_sendResponse($total > 0 ? 200 : 300, $this->renderJsonArrWithTotal($models, $total));
    }

    public function actionGetAllValidasiTotal()
    {
        self::_checkAuth();

//      $_POST['periode_id'] = '15c1f525-88c0-11ee-b56d-000c29988254';
//      $_POST['cabang_id'] = 'a98535b2-e489-11e8-94f6-000c29988254'; //jog
//      $_POST['cabang_id'] = 'bba1f9b5-d3f4-11e7-9982-507b9d297990'; //sda
        $params = [
            'periode_id' => $_POST['periode_id'],
            'cabang_id' => $_POST['cabang_id'],
            'show_detail' => 'on',
            'urut_jabatan' => 0,
            'payroll' => 1
        ];

        $db = Validasi::getValidasi($params);
        $model = $db->getQuery();
        $model = $db->queryAll();

        $periode = Periode::model()->findByPk($_POST['periode_id']);
        $libur = U::makeLikeQuery($periode->libur, 'in_time');

        $i = 0;
        foreach ($model as $m) {
            /*if($m['nik'] == '30100232'){
                $auw = 1;
            }*/
            $jatahOff = Validasi::hitungJatahOffNew($libur, $m['pegawai_id'], $_POST['periode_id'], $_POST['cabang_id'], 'on');
            $model[$i]['jatah_off'] = $jatahOff;

            $gantiOff = Validasi::hitungGantiOff($jatahOff, $m['lemburh'], $m['OFF'], $m['S']);
            $model[$i]['ganti_off'] = $gantiOff;
            $i++;
        }

        $total = count($model);
        $this->_sendResponse(200, $this->renderJsonArrWithTotal($model, $total));
    }

    public function actionGetPresensiMaternity()
    {
        self::_checkAuth();

//        $datas = '[{"pegawai_id":"66412ada-6fbf-11eb-91d7-000c29988254","nik":"30100174","cabang_id":"3dd0a573-3977-11e8-ac21-000c29c7d1d9"},{"pegawai_id":"c79a6f20-eacf-11e7-9913-000c2915359a","nik":"30100165","cabang_id":"3dd0a573-3977-11e8-ac21-000c29c7d1d9"},{"pegawai_id":"9ad15051-e493-11e7-ac21-000c29c7d1d9","nik":"30100041","cabang_id":"bba1f9b5-d3f4-11e7-9982-507b9d297990"},{"pegawai_id":"71508a61-e491-11e7-ac21-000c29c7d1d9","nik":"30100011","cabang_id":"bba1f9b5-d3f4-11e7-9982-507b9d297990"},{"pegawai_id":"93750089-e629-11e7-ac21-000c29c7d1d9","nik":"30100053","cabang_id":"bba1f9b5-d3f4-11e7-9982-507b9d297991"}]';
//        $periode_id = '9688197d-e640-11ed-abc9-000c29332ac5';
        $params = [
            'periode_id' => $_POST['periode_id'],
            'datas' => $_POST['datas']
        ];

        $models = Validasi::getMaternityDay($params);

        $total = count($models);
        $this->_sendResponse(200, $this->renderJsonArrWithTotal($models, $total));
    }

    public function actionGetAllPeriode()
    {
        self::_checkAuth();

        $models = DbCmd::instance()->addFrom('{{periode}} p')->queryAll();

        $total = count($models);
        $this->_sendResponse(200, $this->renderJsonArrWithTotal($models, $total));
    }

    public function actionGetRekapPresensi()
    {
        self::_checkAuth();
        $from = isset($_POST['from']) ? $_POST['from'] : false;
        $to = isset($_POST['to']) ? $_POST['to'] : false;
        $store = (isset($_POST['store']) && $_POST['store'] != "") ? $_POST['store'] : false;

        $httpcode = 200;
        $models = array();
        $total = 0;
        $lanjut = 1;
        try {
            //code...
            if (!$from || !$to || !$store) {
                $lanjut = 0;
                $httpcode = 500;
            }

            if ($lanjut == 1) {
                # code...
                $cabang_id = Cabang::model()->findByAttributes([
                    'kode_cabang' => $store
                ])->cabang_id;

                if (!$cabang_id) {
                    $httpcode = 500;
                    $lanjut = 0;
                }
            }

            if ($lanjut == 1) {
                $params = [
                    'tglin' => date("Y-m-d 00:00:00", strtotime($from)),
                    'tglout' => date("Y-m-d 23:59:59", strtotime($to)),
                    'cabang_id' => $cabang_id,
                ];

                $db = Validasi::getValidasiApi($params);
                $data = $db->queryAll();
                $total = count($data);

                $keys = ['nik', 'nama_lengkap', 'kode_cabang', 'kode', 'kelompok_pegawai', 'HK'];
                foreach ($data as $key => $value) {
                    $models[] = array_intersect_key($value, array_flip($keys));
                }
            }
        } catch (Exception $th) {
            $httpcode = 500;
        }

        $this->_sendResponse($httpcode, $this->renderJsonArrWithTotal($models, $total));
    }

    public function actionSaveTransferPegawai()
    {
        $datas = $_POST['datas'];
        TransferPegawai::model()->updateAll(['visible' => 0], 'binding_id = :binding_id', [':binding_id' => $datas[0]['binding_id']]);

        foreach ($datas as $data) {
            $model = new TransferPegawai;

            $array = [];
            foreach ($data as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $array[$k] = $v;
            }
            $model->attributes = $array;
            if (!$model->save()) {
                throw new Exception('Gagal disimpan');
            }
        }

        $total = count($datas);
        $this->_sendResponse(200, $this->renderJsonArrWithTotal($datas, $total));
    }
}
