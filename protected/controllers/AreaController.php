<?php
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);
Yii::import('application.components.SoapServerYii');
//Yii::import('application.components.SoapClientYii');
class AreaController extends GxController
{
    public function actionCreate()
    {
        $model = new Area;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Area'][$k] = $v;
            }
            $model->attributes = $_POST['Area'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->area_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Area');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Area'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Area'];
            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->area_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->area_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Area')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionTestSoap()
    {

        $html = "";
        $output = Yii::app()->curl->post(Yii::app()->curl->base_url . 'GetAllArea',
            http_build_query(['bu_id' => $_POST['bu_id']]));
        $decode = json_decode($output, true);
        foreach ($decode['results'] as $result) {
            $model = Area::model()->findByPk($result['area_id']);
            if ($model == null) {
                $model = new Area;
            }
            $model->setAttributes($result);
            if (!$model->save()) {
//                Yii::log();
                $html .= CHtml::errorSummary($model);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $param = [];
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if (isset($_POST['bu_id']) && $_POST['bu_id'] != null) {
            $criteria->addCondition('bu_id = :bu_id');
            $param[':bu_id'] = $_POST['bu_id'];
        }
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = Area::model()->findAll($criteria);
        $total = Area::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}
