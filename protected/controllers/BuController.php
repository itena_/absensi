<?php

use App\SysPrefs;

class BuController extends GxController
{
    public function actionCreate()
    {
        $model = new Bu;
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Bu'][$k] = $v;
            }
            $model->attributes = $_POST['Bu'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->bu_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Bu');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Bu'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Bu'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->bu_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->bu_id));
            }
        }
    }
    public function actionUpdateDataKaryawan()
    {
        $html = "";
        $output = Yii::app()->curl->get(Yii::app()->curl->base_url . 'GetAllBu');
        $decode = json_decode($output, true);
        foreach ($decode['results'] as $result) {
            $bu = Bu::model()->findByPk($result['bu_id']);
            if ($bu == null) {
                $bu = new Bu;
            }
            $bu->setAttributes($result);
            if (!$bu->save()) {
                $html .= CHtml::errorSummary($bu);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
    public function actionAddUpdateBu()
    {
        $html = "BU berhasil ditambahkan.";
        $output = Yii::app()->curl->post(Yii::app()->curl->base_url . 'GetAllBu',
            http_build_query(['bu_nama_alias' => $_POST['bu_nama_alias']]));
        $decode = json_decode($output, true);
        foreach ($decode['results'] as $result) {
            $bu = Bu::model()->findByPk($result['bu_id']);
            if ($bu == null) {
                $bu = new Bu;
            }
            $bu->setAttributes($result);
            $bu->bu_kode = $result['pt_kode'] . $result['bu_kode'];
            if (!$bu->save()) {
                $html = CHtml::errorSummary($bu);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
    public function actionPick()
    {

        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['view'])) {
            $criteria->addCondition('item_type = :item_type');
            $param[':item_type'] = $_POST['view'];
        }
        if (isset($_POST['node_id'])) {
            $criteria->addCondition('parent = :parent');
            $param[':parent'] = $_POST['node_id'];
        }
        $criteria->params = $param;
        $model = BuTree::model()->findAll($criteria);
        $total = BuTree::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['bu_id']) && $_POST['bu_id'] != null) {
            $criteria->addCondition('bu_id = :bu_id');
            $param[':bu_id'] = $_POST['bu_id'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $criteria->order = 'bu_name';
        $model = Bu::model()->findAll($criteria);
        $total = Bu::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexSrBu()
    {
        /** @var Users $user */
        $user = Users::model()->findByPk(user()->getId());
        $comm = Yii::app()->db->createCommand("
        SELECT 	DISTINCT sr.bu_id,sr.bu_kode,sr.bu_name
        FROM pbu_sr_cbg_area_bu AS sr
        WHERE sr.security_roles_id = :security_roles_id");
        $arr = $comm->queryAll(true, [':security_roles_id' => $user->security_roles_id]);
        $this->renderJsonArr($arr);
    }
    public function actionUserBu()
    {
        /** @var Users $user */
        $user       = Users::model()->findByPk(user()->getId());
        $cabang_id  = Pegawai::model()->findByAttributes(['pegawai_id' => $user->pegawai_id])->cabang_id;

        $cmd = new DbCmd();
        $cmd->addSelect("b.bu_id,b.bu_kode,b.bu_name");

        $security_roles = self::getSecuriyRolesId();
        if($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()){
            $cmd->addFrom("{{bu}} b")
                ->addOrder("bu_name");
            $arr = $cmd->queryAll();
        } else {
            $cmd->addFrom("{{cabang}} c")
                ->addLeftJoin("{{bu}} b", "b.bu_id = c.bu_id")
                ->addCondition("c.cabang_id = :cabang_id")
                ->addParam(":cabang_id", $cabang_id);
            $arr = $cmd->queryRow();
        }

        $this->renderJsonArr($arr);
    }
}