<?php

class CabangController extends GxController
{
    public function actionCreate()
    {
        $model = new Cabang;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Cabang'][$k] = $v;
            }
            $model->attributes = $_POST['Cabang'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->cabang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Cabang');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Cabang'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Cabang'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->cabang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->cabang_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Cabang')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex()
    {
        $dbcmd = new DbCmd();
        $dbcmd->addSelect("*")
            ->addFrom("{{cabang}} c")
            ->addCondition("kode_cabang != 'ALL'")
        ;

        if (isset ($_POST['f']) && $_POST['f'] == 'usr') {
            $cabang = Cabang::model()->checkCabang();
            if ($cabang != 'ALL') {
                $dbcmd->addCondition("kode_cabang = '$cabang'");
            }
        }
        if(isset($_POST['bu_id'])) {
            $dbcmd->addInCondition("c.cabang_id", $this->getAllCabangInBu($_POST['bu_id']));
        }


        $count = $dbcmd->queryCount();

        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $dbcmd->addOrder('kode_cabang');

        $model = $dbcmd->getQuery();
        $model = $dbcmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }

    public function actionSinkron()
    {
        $html = "";

        $area = Api::sinkronEDM(['action'=> 'GetAllArea', 'id' => $_POST['bu_id']]);
        $decode = json_decode(json_encode($area), true);
        foreach ($decode['results'] as $result) {
            $model = Area::model()->findByPk($result['area_id']);
            if ($model == null) {
                $model = new Area;
            }
            $model->setAttributes($result);
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }

        $cabang = Api::sinkronEDM(['action'=> 'GetAllCabang', 'id' => $_POST['bu_id']]);
        $decode = json_decode(json_encode($cabang), true);
        foreach ($decode['results'] as $result) {
            $model = Cabang::model()->findByPk($result['cabang_id']);
            if ($model == null) {
                $model = new Cabang;
            }
            $model->setAttributes($result);
            $model->alamat_cabang = $result['alamat_cabang'];
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }

    public function actionGetUserCabang()
    {
        $cabang         = Cabang::model()->checkCabangId();
        $security_role  = SecurityRoles::model()->checkSecurityRoleId();
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $cabang,
            'sec' => $security_role
        ));
        Yii::app()->end();
    }

    public function actionAssignCabangPusat()
    {
        $cabang_id  = $_POST['cabang_id'];
        $cabang     = Cabang::model()->findByPk($cabang_id);
        $bu_id      = $cabang->bu_id;

        Yii::app()->db->createCommand(
            "UPDATE {{cabang}} SET kepala_cabang_stat = 0 WHERE bu_id = '" . $bu_id . "'")
        ->execute();

        $cabang->kepala_cabang_stat = 1;
        $cabang->save();

        Log::generateErrorMessage(true, 'Cabang Pusat berhasil ditambahkan.');
    }

    public function actionIndexOld()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset ($_POST['f']) && $_POST['f'] == 'usr') {
            $cabang = Cabang::model()->checkCabang();
            if ($cabang != 'ALL') {
                $criteria->addCondition("kode_cabang = '$cabang'");
            }
        }
        $criteria->order = 'kode_cabang ASC';
        $model = Cabang::model()->findAll($criteria);
        $total = Cabang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexSrCabang()
    {
        $criteria = new CDbCriteria();
        $param = [];
        if ($_POST['modez'] == 0) {
            $criteria->params = $param;
            $model = AllCbgBu::model()->findAll($criteria);
            $total = AllCbgBu::model()->count($criteria);
            $this->renderJson($model, $total);
        } else {
            $_POST['security_roles_id'] = $_POST['modez'];
            $on = " AND security_roles_id = :security_roles_id";
            $param[':security_roles_id'] = $_POST['security_roles_id'];
            $comm = Yii::app()->db->createCommand("
            SELECT 	c.kode_cabang,	c.nama_cabang,	c.bu_name,
            IF(sr.sr_cabang IS NULL, 0, 1) AS checked, sr.sr_cabang 
            FROM pbu_all_cbg_bu AS c
            LEFT JOIN pbu_sr_cabang AS sr ON sr.cabang_id = c.cabang_id
            $on ");
            $arr = $comm->queryAll(true, $param);
            $this->renderJsonArr($arr);
        }
    }
    public function actionIndexSrCabangCmp()
    {
        $criteria = new CDbCriteria();
        $param = [];
        if (isset ($_POST['bu_id'])) {
            $criteria->addCondition('bu_id = :bu_id');
            $param[':bu_id'] = $_POST['bu_id'];
        }
        $criteria->params = $param;
        $model = SrCbgAreaBu::model()->findAll($criteria);
        $total = SrCbgAreaBu::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}