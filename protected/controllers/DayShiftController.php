<?php

class DayShiftController extends GxController {

    public function actionCreate() {
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Dayshift::model()->deleteAll("shift_id = :shift_id", array(':shift_id' => $_POST['shift_id']));
                foreach ($detils as $detil) {
                    $item_details = new Dayshift;
                    $_POST['DayShift']['day_id'] = $detil['day_id'];
                    $_POST['DayShift']['shift_id'] = $detil['shift_id'];
                    $item_details->attributes = $_POST['DayShift'];

                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    }
                }
                $transaction->commit();
                $status = true;
                $msg = 'Data berhasil disimpan!';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            $idshift = $_POST['idshift'];

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Dayshift::model()->deleteAll("shift_id = :shift_id", array(':shift_id' => $idshift));
                foreach ($detils as $detil) {
                    $item_details = new Dayshift;
                    $_POST['DayShift']['shift_id'] = $idshift;
                    $_POST['DayShift']['day_id'] = $detil['day_id'];
                    $item_details->attributes = $_POST['DayShift'];

                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    }
                }
                $transaction->commit();
                $status = true;
                $msg = 'Data berhasil diassign!';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                Dayshift::model()->deleteAll("shift_id = :shift_id", array(':shift_id' => $id));
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = array();
//            if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
//                $criteria->join = "LEFT JOIN pbu_shift s ON s.shift_id = t.shift_id LEFT JOIN pbu_days d ON d.day_id = t.day_id";
//                $criteria->addCondition($_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'");
//            }
        if ((isset($_POST['shift_id']))) {
            $criteria->addCondition("shift_id = '" . $_POST['shift_id'] . "'");
            $criteria->group = '';
        } else {
            $criteria->group = "shift_id";
        }
        $model = Dayshift::model()->findAll($criteria);
        $total = Dayshift::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionDS() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = array();
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
            $criteria->join = "LEFT JOIN pbu_shift s ON s.shift_id = t.shift_id LEFT JOIN pbu_days d ON d.day_id = t.day_id";
            $criteria->addCondition($_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'");
        }
//        $criteria->group = '';
        $criteria->order = "kode_shift ASC";

        $model = NamaHari::model()->findAll($criteria);
        $total = NamaHari::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionGetDays() {
        $model = Days::model()->findAll();
        $total = Days::model()->count();
        $this->renderJson($model, $total);
    }

}
