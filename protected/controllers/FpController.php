<?php

//Yii::import('application.vendors.*');
//require 'tad/TADFactory.php';
//require 'tad/TAD.php';
//require 'tad/TADResponse.php';
//require 'tad/Providers/TADSoap.php';
//require 'tad/Providers/TADZKLib.php';
//require 'tad/Exceptions/ConnectionError.php';
//require 'tad/Exceptions/FilterArgumentError.php';
//require 'tad/Exceptions/UnrecognizedArgument.php';
//require 'tad/Exceptions/UnrecognizedCommand.php';
//Yii::import('application.components.U');
//Yii::import('application.components.TarikAbsen');
Yii::import('application.models.Store');

class FpController extends GxController
{
    public function actionIndex()
    {
        if (isset($_POST['periode_id'])) {
            $dbperiode      = Periode::getPeriodeStartEnd($_POST['periode_id']);
            $periodenow     = $dbperiode->queryRow();
            $periodeStart   = substr($periodenow['ps'],0,10);
            $periodeEnd     = substr($periodenow['pe'], 0 ,10);
            $pegawai_id     = $_POST['pegawai_id'];

            $result = $this->X($periodeStart, $periodeEnd, $pegawai_id, $_POST['periode_id']);

            $periode = Yii::app()->db->createCommand("
                select
                t.selected_date tgl,
                    dayname(t.selected_date) hari,
                    concat(day(t.selected_date), ' ', monthname(t.selected_date)) tgl_hari
                    from(
                        " . Periode::getAllDate($periodeStart, $periodeEnd)->getQuery() . "
                    ) t")->queryAll(true);

            $hsl = [];
            foreach ($periode as $value) {
                $arr   = $arrCheck = [];
                $count = $countday = $i = 0;
                $startcountday = false;
                foreach ($result as $v) {
                    $tglloop = substr($v['jam_in'], 0, 10);
                    if ($value['tgl'] == $tglloop) {
                        $arr            = $v;
                        $count++;
                        if(count($arrCheck) > 0){
                            if(substr($arr['jam_in'], 0, 10) == substr($arrCheck['jam_in'], 0, 10)) {
                                if($arrCheck['status_int_in'] == 1 && $arrCheck['status_int_out'] == 1){
                                    if($arr['status_int_in'] == 0 || $arr['status_int_out'] == 0){
                                        $arr = $arrCheck;
                                    }
                                }
                            }
                        } else {
                            $arrCheck = $arr;
                        }
                        continue;
                    }
                }
                $arr['tgl']      = $value['tgl'];
                $arr['hari']     = $value['hari'];
                $arr['tgl_hari'] = $value['tgl_hari'];
                $arr['count']    = $count;
                $hsl[]           = $arr;
                unset($arr[$count]);
            }

            /*$cmd = U::report_fp_mix($_POST['periode_id'],$pegawai_id, $_POST['bu_id']);
            $cmd->addCondition()
            ;*/

            $rows = [];
            $row = [];
            foreach ($hsl as $v) {
                if ($v['hari'] == 'Sunday') {
                    if (count($row) > 0)
                        $rows[] = $row;
                    $row = [];
                }
                $row[$v['hari']] = $v;
            }
            if (count($row) > 0)
                $rows[] = $row;

            $kirim['results'] = $rows;
            $kirim['total'] = count($rows);
            echo json_encode($kirim);
        }
    }

    public function actionGetAttLog()
    {
        $tglmulai   = "";
        $tglsampai  = "";

        $periode    = $_POST['periodeabsen'];
        $cabang     = $_POST['cabangabsen'];

        $definemulai    = $_POST['tglmulai'];
        $definesampai   = $_POST['tglsampai'];

        $now = date("Y-m-d");

        if ($periode == '0') //6 bulan terakhir
        {
            $d = strtotime("-6 months", strtotime($now));
            $tglmulai = date('Y-m-d', $d);
            $tglsampai = $now;
        } elseif ($periode == '1') //3 bulan terakhir
        {
            $d = strtotime("-3 months", strtotime($now));
            $tglmulai = date('Y-m-d', $d);
            $tglsampai = $now;
        } elseif ($periode == '2') //2 bulan terakhir
        {
            $d = strtotime("-2 months", strtotime($now));
            $tglmulai = date('Y-m-d', $d);
            $tglsampai = $now;
        } elseif ($periode == '3') //1 bulan terakhir
        {
            $d = strtotime("-1 months", strtotime($now));
            $tglmulai = date('Y-m-d', $d);
            $tglsampai = $now;
        } elseif ($periode == '4') //1 minggu terakhir
        {
            $d = strtotime("-7 days", strtotime($now));
            $tglmulai = date('Y-m-d', $d);
            $tglsampai = $now;
        } elseif ($periode == '5') //define
        {
            $tglmulai = substr($definemulai, 0, 10);
            $tglsampai = substr($definesampai, 0, 10);
        }

        if(GET_FP_PYTHON() == true) {
            $s = new TarikAbsenPy;
        } else {
            $s = new TarikAbsen;
        }

        $count = $exist = 0;
        $message = '';

        $ips = Ip::model()->findAll("cabang = :cabang",[':cabang' => Ip::model()->findByPk($cabang)->cabang]);
        if(count($ips) > 0) {
            foreach ($ips as $ip) {
                $info  = $s->Absensi($tglmulai, $tglsampai, $ip->ip_id);
                if($info) {
                    $count += $info['count'];
                    $message .= $info['message'];
                    $exist += $info['exist'];
                }
            }
        } else {
            $info  = $s->Absensi($tglmulai, $tglsampai, $cabang);
            if($info) {
                $count = $info['count'];
                $message = $info['message'];
                $exist = $info['exist'];
            }
        }

        $msg   = "Absen result : <br>SUCCESS (<b>$count</b>) <br> EXIST (<b>$exist</b>)";

        echo CJSON::encode(array(
            'success' => true,
            'msg' => $msg,
            'msg1' => $message
        ));
        Yii::app()->end();
    }

    public function actionImportAttlog()
    {
        $status = false;
        $msg    = "Data gagal disimpan.";

        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $security_roles = self::getSecuriyRolesId();
            $user_cabang_id = Cabang::model()->checkCabangId();
            if ($security_roles == GET_SECURITY_ROLE_HRD()) {
                Log::generateErrorMessage(false, "Hanya cabang yang bisa menggunakan fungsi ini!");
            }
            $periode        = Periode::getPeriodeStartEnd($_POST['periode'])->queryRow();
            $periodeStart   = date('Y-m-d', strtotime($periode['ps']));
            $periodeEnd     = date('Y-m-d', strtotime($periode['pe']));
            $date           = date('Y-m-d', strtotime($_POST['date']));

            if (($date >= $periodeStart) && ($date <= $periodeEnd)){
                if(Pegawai::model()->findByAttributes(['nik' => $_POST['nik']])) {
                    $check = Fp::model()->findByAttributes(['PIN' => $_POST['nik'], 'DateTime_' => $_POST['date']]);
                    if(!$check) {
                        Fp::saveFp($_POST['nik'], $_POST['date'], Cabang::model()->checkCabang(), $user_cabang_id, 99);
                        $msg = "Data berhasil disimpan.";
                    } else {
                        Log::generateErrorMessage(true, "NIK ".$_POST['nik']." pada ".$_POST['date']." sudah pernah di import.");
                    }
                } else {
                    Log::generateErrorMessage(true, "NIK ".$_POST['nik']." tidak terdaftar.");
                }
            } else {
                $msg = "Tanggal ".$_POST['date']." ada di luar periode ".$periode['kode_periode'].".";
            }

            $status = true;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionImportLark()
    {
        $msg    = "Data gagal disimpan.";
        $datas  = CJSON::decode($_POST['detil']);

        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $periode        = Periode::getPeriodeStartEnd($_POST['periode_id'])->queryRow();
            $periodeStart   = date('Y-m-d', strtotime($periode['ps']));
            $periodeEnd     = date('Y-m-d', strtotime($periode['pe']));

            foreach ($datas as $data) {
                /*$year  = substr($data['date'],0,4);
                $month = substr($data['date'], 4, strlen($data['date'])-6);
                $dt    = substr($data['date'],-2);*/

                $date = date(substr($data['date'],0,4) . '-' . substr($data['date'],4,2) . '-' . substr($data['date'],6,2));
                /*$x = DateTime::createFromFormat("d/m/Y", $date);
                $year = $x->format('Y');
                $month = $x->format('m');
                $dt = $x->format('d');

                $date      = $year.'-'.$month.'-'.$dt;*/
                $nik_lokal = substr($data['name'],0,6);

                $cabangHolding = Cabang::model()->findByAttributes(['bu_id' => Bu::model()->findByAttributes(['bu_nama_alias' => 'HOLDING'])->bu_id]);
                $in_time       = $data['in_time'];
                $out_time      = $data['out_time'];

                if (($date >= $periodeStart) && ($date <= $periodeEnd)){
                    if($data['group'] !== 'PT. Setyawan Eunike Gemilang') continue;

                    //cek dengan nik_lokal
                    $pegawai = Pegawai::model()->findByAttributes(['nik_lokal' => $nik_lokal, 'cabang_id' => $cabangHolding->cabang_id]);
                    if($pegawai) {
                        if($in_time === '-' && $out_time === '-') continue;

                        /*
                         * In Time
                         */
                        if($in_time !== '-'){
                            $check = Fp::model()->findByAttributes(['PIN' => $pegawai->nik, 'DateTime_' => $date . " " . $in_time . ":00"]);
                            if(!$check) Fp::saveFp($pegawai->nik, $date . " " . $in_time . ":00", $cabangHolding->kode_cabang, $cabangHolding->cabang_id, 98);
                        }

                        /*
                         * Out time
                         */
                        if($out_time !== '-'){
                            $check = Fp::model()->findByAttributes(['PIN' => $pegawai->nik, 'DateTime_' => $date . " " . $out_time . ":00"]);
                            if(!$check) Fp::saveFp($pegawai->nik, $date . " " . $out_time . ":00", $cabangHolding->kode_cabang, $cabangHolding->cabang_id, 98);
                        }
                    } else
                        continue;
                } else
                    continue;
            }

            $msg    = "Data berhasil disimpan.";
            $status = true;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionResetFase1()
    {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $cabang_id      = Cabang::model()->checkCabangId();
            $periodeStart   = Yii::app()->db->createCommand("SELECT DATE_FORMAT(periode_start,'%Y-%m-%d %H:%i:%s') FROM pbu_periode where periode_id = '" . $_POST['periode_id'] . "'")->queryScalar();
            $periodeEnd     = Yii::app()->db->createCommand("SELECT DATE_FORMAT(periode_end,'%Y-%m-%d %H:%i:%s') FROM pbu_periode where periode_id = '" . $_POST['periode_id'] . "'")->queryScalar();
            Yii::app()->db->createCommand("
                UPDATE pbu_fp set status_int = 0, kode_ket = 0, log = 0, Status = 0
                where PIN = :pin AND DateTime_ between :pstart AND :pend  
                AND cabang_id = :cabang_id
            ")->execute([
                ':pin'       => $_POST['PIN'],
                ':pstart'    => $periodeStart,
                ':pend'      => $periodeEnd,
                ':cabang_id' => $cabang_id
            ]);
            $status = true;
            $msg = 'Data berhasil di-reset.';
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionSaveValid()
    {
        /*
         * Check Lock dan Post
         */
        LockPost::model()->check_lockpost($_POST['periode_id']);
        /*
         * End Check
         */

        $periode_id                 = $_POST['periode_id'];
        $pegawai_id                 = $_POST['pin_id'];
        $data                       = CJSON::decode($_POST['data']);
        $condition_for_choose_shift = '1 == 1';

        try {
            $pegawai            = Pegawai::model()->findByPk($pegawai_id);
            $cabang_pusat       = Cabang::model()->getCabangPusatFromBu($pegawai_id);
            $cabang_pegawai     = $pegawai->cabang_id;
            $count              = 0;

            if (GET_NON_EMPLOYEE_ONLY()) {
                $condition_for_choose_shift = $pegawai->kelompok_pegawai == 1;
            }

            $array = Validasi::addAdditionalValidasi($pegawai->pegawai_id, $periode_id);

            foreach ($data as $datas) {
                foreach ($datas as $k) {
                    if (is_array($k) && isset($k['PIN']) ) {
                        if($_POST['limitdate'] != '')
                            if(substr($k['jam_in'], 0, 10) < substr($_POST['limitdate'], 0, 10))
                                continue;

                        $day_in     = date_format(date_create($k['jam_in']), "Y-m-d");
                        $day_out    = date_format(date_create($k['jam_out']), "Y-m-d");
                        $fpin       = $k['fp_id_in'];
                        $fpout      = $k['fp_id_out'];

                        $fingerin       = Fp::model()->findByPk($fpin);
                        $fingerout      = Fp::model()->findByPk($fpout);
                        $cabangfinger   = $fingerin->cabang_id;

                        if($fingerin->Status == FP_NON_AKTIF && $fingerout->Status == FP_NON_AKTIF)
                            continue;

                        if ($k['status_int_in'] == 0 || $k['status_int_out'] == 0) {
//                        if($fingerin->status_int == 1 || $fingerout->status_int == 1) {
//                      ------ check validasi apakah ada atau tidak -------
                            $check = $this->checkApakahAdaDiFase2($day_in, $day_out, $pegawai->pegawai_id);

                            if ($check) {
                                $validasi = Validasi::model()->findByPk($check->validasi_id);
                                $validasi->status_int = 2;
                                $validasi->save();
                            }
//                      ------ check END -------

                            $valid = new Validasi;
                            $transferpegawai = TransferPegawai::model()->checkPegawai($periode_id, $pegawai_id, $k['jam_in'], $k['jam_out']);

                            if (isset($transferpegawai)) {
                                if ($transferpegawai->cabang_transfer_id != $cabangfinger) {
                                    Log::generateErrorMessage(false, 'Perbantuan Pegawai tidak sesuai dengan lokasi absen cabang!');
                                } else {
                                    $kode_ket       = $transferpegawai->status_kota;
                                    $cabang_id      = $transferpegawai->cabang_transfer_id;
                                    $no_surat_tugas = $transferpegawai->no_surat_tugas;
                                }
                            } else {
                                if ($cabang_pegawai != $cabangfinger) {
                                    if ($cabang_pegawai == $cabang_pusat) {
                                        $cabang_id      = $cabang_pusat;
                                        $kode_ket       = 0;
                                        $no_surat_tugas = '';
                                    } else {
                                        $cabang_id      = $cabangfinger;
                                        $kode_ket       = 0;
                                        $no_surat_tugas = '';
                                    }
                                } else {
                                    $kode_ket       = 0;
                                    $cabang_id      = $cabang_pegawai;
                                    $no_surat_tugas = '';
                                }
                            }

                            /*
                             * START CHECK FINGER DOUBLE
                             */
                            $datacheck = "[]";
                            if (GET_CHOOSE_SHIFT_PHASE1() && $condition_for_choose_shift) {
                                $date = substr($k['jam_in'], 0, 10);
                                if ($k['count'] > 1) {
                                    $date1      = str_replace('-', '/', $date);
                                    $yesterday  = date('Y-m-d', strtotime($date1 . "-1 days"));
                                    $tomorrow   = date('Y-m-d', strtotime($date1 . "+1 days"));
                                    $msg        = 'checked';
                                    $datacheck  = $this->checkX($yesterday, $tomorrow, $pegawai_id, $periode_id, $date);
                                    $niknama    = $pegawai->nik . ' ' . $pegawai->nama_lengkap;
                                }
                            }

                            if ($datacheck !== "[]"){
                                $arrDatacheck = CJSON::decode($datacheck);
                                if(count($arrDatacheck) > 1) {
                                    echo CJSON::encode(array(
                                        'success'           => true,
                                        'msg'               => $msg,
                                        'nik'               => $niknama,
                                        'kode_ket'          => $kode_ket,
                                        'cabang_id'         => $cabang_id,
                                        'no_surat_tugas'    => $no_surat_tugas,
                                        'hariini'           => $date,
                                        'datacheck'         => $datacheck
                                    ));
                                    Yii::app()->end();
                                } else {
                                    $k['shift_id'] = $arrDatacheck[0]['shift_id'];
                                }
                            }
                            /*
                             * END CHECK FINGER DOUBLE
                             */

                            $shift_id = $k['shift_id'];
                            if (isset($shift_id)) {
                                $count++;
                            }

                            $duras      = Fp::model()->durasi($pegawai_id, $shift_id, $k['jam_in'], $k['jam_out'], '', '');
                            $validasi   = Validasi::saveValidasi($valid, $pegawai_id, $k, $shift_id, $duras, $kode_ket, $cabang_id, $no_surat_tugas);

                            $valid->attributes = array_merge($validasi, $array);
                            if (!$valid->save()) {
                                Log::generateErrorMessage(false, 'Gagal menyimpan validasi!' . CHtml::errorSummary($valid));
                            }

                            $fingerin->kode_ket     = $kode_ket;
                            $fingerin->log          = 1;
                            $fingerin->status_int   = 1;
                            $fingerin->Status       = FP_NON_AKTIF;
                            if (!$fingerin->save()) {
                                Log::generateErrorMessage(false, 'Gagal menyimpan fingerin!' . CHtml::errorSummary($fingerin));
                            }

                            $fingerout->kode_ket    = $kode_ket;
                            $fingerout->log         = 1;
                            $fingerout->status_int  = 1;
                            $fingerout->Status       = FP_NON_AKTIF;
                            if (!$fingerout->save()) {
                                Log::generateErrorMessage(false, 'Gagal menyimpan fingerout!' . CHtml::errorSummary($fingerout));
                            }
                        }

                    }
                }
            }
            $status = true;
            $msg    = "$count data berhasil disimpan!";
        } catch (Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
    }

    public function actionSaveOneDay()
    {
        $periode_id = $_POST['periode_id'];
        $pegawai_id = $_POST['pin_id'];
        $nik        = $_POST['PIN'];
        $kode_ket   = $_POST['kode_ket'];
        $cabang_id  = $_POST['cabang_id'];
        $no_surat_tugas = $_POST['no_surat_tugas'];
        $data       = CJSON::decode($_POST['detil']);
        $status     = true;
        $msg        = 'Data berhasil disimpan.';
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $inshift = [];
            foreach ($data as $datas) {
                /*
                 * Mematikan fingkernya dan mengambil shift dan jam in dan out yg dipilih
                 */
                if (isset($datas['count']) && $datas['count'] == true) {
                    $fpin = Fp::model()->findbyPk($datas['fp_id_in']);
                    if(isset($fpin)){
                        $dbfpin = DbCmd::instance()
                            ->addSelect("*")
                            ->addFrom("{{fp}}")
                            ->addCondition("PIN = :PIN AND cabang_id = :cabang_id")
                            ->addCondition("DateTime_ BETWEEN :time - INTERVAL 30 MINUTE AND :time + INTERVAL 30 MINUTE")
                            ->addParams([
                                ':PIN' => $fpin->PIN,
                                ':cabang_id' => $fpin->cabang_id,
                                ':time'    => $fpin->DateTime_
                            ])->queryAll();

                        foreach ($dbfpin as $fpsin) {
                            $fpsin = (Object) $fpsin;
                            $xfpin = Fp::model()->findbyPk($fpsin->fp_id);

                            $xfpin->status_int   = 1;
                            $xfpin->log          = 1;
                            $xfpin->Status       = FP_NON_AKTIF;
                            if (!$xfpin->save()) {
                                Log::generateErrorMessage(false, 'Gagal menyimpan fingerin!' . CHtml::errorSummary($xfpin));
                            }
                        }
                    }

                    $fpout = Fp::model()->findbyPk($datas['fp_id_out']);
                    if(isset($fpout)){
                        $dbfpout = DbCmd::instance()
                            ->addSelect("*")
                            ->addFrom("{{fp}}")
                            ->addCondition("PIN = :PIN AND cabang_id = :cabang_id")
                            ->addCondition("DateTime_ BETWEEN :time - INTERVAL 30 MINUTE AND :time + INTERVAL 30 MINUTE")
                            ->addParams([
                                ':PIN' => $fpout->PIN,
                                ':cabang_id' => $fpout->cabang_id,
                                ':time'    => $fpout->DateTime_
                            ])->queryAll();

                        foreach ($dbfpout as $fpsout) {
                            $fpsout = (Object) $fpsout;
                            $xfpout = Fp::model()->findbyPk($fpsout->fp_id);

                            $xfpout->status_int   = 1;
                            $xfpout->log          = 1;
                            $xfpout->Status       = FP_NON_AKTIF;
                            if (!$xfpout->save()) {
                                Log::generateErrorMessage(false, 'Gagal menyimpan fingerin!' . CHtml::errorSummary($xfpout));
                            }
                        }
                    }

                    $inshift['shift_id']    = $datas['shift_id'];
                    $inshift['jam_in']      = $datas['jam_in'];
                    $inshift['jam_out']     = $datas['jam_out'];
                }
            }
            $shift_id   = $inshift['shift_id'];
            $jam_in     = $inshift['jam_in'];
            $jam_out    = $inshift['jam_out'];

            $day_in     = substr($jam_in, 0, 10);
            $day_out    = substr($jam_out, 0, 10);
            $check      = $this->checkApakahAdaDiFase2($day_in, $day_out, $pegawai_id);

            if ($check) {
                $validasi = Validasi::model()->findByPk($check->validasi_id);
                $validasi->status_int = 2;
                $validasi->save();
            }

            $array = Validasi::addAdditionalValidasi($pegawai_id, $periode_id);

            $valid          = new Validasi;
            $k['PIN']       = $nik;
            $k['jam_in']    = $jam_in;
            $k['jam_out']   = $jam_out;
            $duras          = Fp::model()->durasi($pegawai_id, $shift_id, $k['jam_in'], $k['jam_out'], '', '');
            $validasi       = Validasi::saveValidasi($valid, $pegawai_id, $k, $shift_id, $duras, $kode_ket, $cabang_id, $no_surat_tugas);

            $valid->attributes = array_merge($validasi, $array);
            if (!$valid->save()) {
                Log::generateErrorMessage(false, 'Gagal menyimpan validasi!' . CHtml::errorSummary($valid));
            }

            $msg = date("Y-m-d H:i:s",strtotime('+30 minutes', strtotime($jam_out))); // + INTERVAL 30 MINUTES
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
    }

    private function checkApakahAdaDiFase2($day_in, $day_out, $pegawai_id) {
        return Validasi::model()->find(
            'in_time like :in_time 
                            AND out_time like :out_time 
                            AND pegawai_id = :pegawai_id 
                            AND status_int = 1',
            ['in_time' => $day_in . '%', 'out_time' => $day_out . '%', 'pegawai_id' => $pegawai_id ]
        );
    }

    private function checkX($yesterday, $tomorrow, $pegawai, $periode_id, $today)
    {
        $pegawai_id = $pegawai;
        $result = $this->X($yesterday, $tomorrow, $pegawai_id, $periode_id);
        $periode = Yii::app()->db->createCommand("select
	t.selected_date tgl,
        dayname(t.selected_date) hari,
        concat(day(t.selected_date), ' ', monthname(t.selected_date)) tgl_hari
        from(
            " . Periode::getAllDate($yesterday, $tomorrow)->getQuery() . "
        ) t")->queryAll(true);

        $hsl = [];
        foreach ($periode as $value) {
            $arr = [];
            $count = 0;
            foreach ($result as $v) {
                $a = substr($v['jam_in'], 0, 10);
                if ($value['tgl'] == $a) {
                    if ($value['tgl'] == $today) {
                        $fpin  = Fp::model()->findByPk($v['fp_id_in']);
                        $fpout = Fp::model()->findByPk($v['fp_id_out']);

                        $afpin  = $fpin->Status;
                        $afpout = $fpout->Status;

                        if($fpin->Status != FP_NON_AKTIF && $fpout->Status != FP_NON_AKTIF){
                            $arr = $v;
                            $arr['tgl']      = $value['tgl'];
                            $arr['hari']     = $value['hari'];
                            $arr['tgl_hari'] = $value['tgl_hari'];
                            $arr['count']    = $count;
                            $hsl[]           = $arr;
                            $count++;
                            continue;
                        }
                    }
                }
            }
        }
        return json_encode($hsl);
    }

    public function actionCreateSimulasiFase1() {
        $status = false;
        $msg    = "Data gagal disimpan.";
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $dbperiode      = Periode::getPeriodeStartEnd($_POST['periode_id']);
            $periodenow     = $dbperiode->queryRow();
            $periodeStart   = $periodenow['ps'];
            $periodeEnd     = $periodenow['pe'];

            $period = new DatePeriod(new DateTime($periodeStart), new DateInterval('P1D'), new DateTime($periodeEnd . ' +1 day'));
            foreach ($period as $date) {
                $dates[] = $date->format("Y-m-d");
            }

            $cabang         = $_POST['cabang'];
            $cabang_id      = $_POST['cabang_id'];

//            $pegawai        = Pegawai::model()->findAllByAttributes(['cabang_id' => $cabang_id, 'status_pegawai_id' => get_status_pegawai_aktif()]);
            $pegawai        = DbCmd::instance()->addSelect("*")
                ->addFrom("{{pegawai}}")
                ->addCondition("cabang_id = :cabang_id")
                ->addInCondition("status_pegawai_id", get_status_pegawai_aktif())
                ->addParam(":cabang_id", $cabang_id)
                ->queryAll()
                ;
            foreach ($pegawai as $p) {
                $p          = (object) $p;
                $kelompok   = $p->kelompok_pegawai;
                $shift      = Shift::model()->findByAttributes([
                    'kelompok_shift'    => $kelompok,
                    'cabang_id'         => $cabang_id
                ]);
                $index      = 0;
                $loop       = false;
                foreach ($dates as $date) {
                    $in_time    = $date . " " . $shift['in_time'];
                    $out_time   = $date . " " . $shift['out_time'];

                    if(strtotime($out_time) < strtotime($in_time)) {
                        $out_time = date('Y-m-d',strtotime(str_replace('-', '/', $out_time) . "+1 days")) . " " . $shift['out_time'];
                    }

                    switch ($index){
                        case 2 : case 11 :
                            $in_time = date('Y-m-d H:i:s', strtotime($in_time .' + 5 minute')); //lesstime t5 menit
                            break;
                        case 3 : case 12 :
                            $out_time = date('Y-m-d H:i:s', strtotime($out_time .' + 35 minute'));; //overtime 35 menit
                            break;
                        case 4 : case 5 : case 6 : case 7 : case 8 : case 9: case 10:
                            $index++;
                            $loop = true; break;
                        /*
                         * 4 lupa absen
                         * 5 sakit
                         * 6 off
                         * 7 wfh
                         * 8 cuti tahunan
                         * 9 cuti melahirkan
                         * 10 cuti istimewa
                         */
                        case 13 : //ada in saja ga ada out
                            $in_check = Fp::model()->findByAttributes(array('PIN' => $p->nik, 'DateTime_' => $in_time));
                            if(!$in_check) {
                                Fp::saveFp($p->nik, $in_time, $cabang, $cabang_id, 88);
                            }
                            $index++;
                            $loop = true; break;
                        case 14 : //ada out saja ga ada in
                            $out_check = Fp::model()->findByAttributes(array('PIN' => $p->nik, 'DateTime_' => $out_time));
                            if(!$out_check) {
                                Fp::saveFp($p->nik, $out_time, $cabang, $cabang_id, 88);
                            }
                            $index++;
                            $loop = true; break;
                        case 15 :
                            $in_time = date('Y-m-d H:i:s', strtotime($in_time .' - 180 minute')); //lesstime t5 menit
                            break;
                        case 16:
                            $out_time = date('Y-m-d H:i:s', strtotime($out_time .' - 115 minute'));; //overtime 35 menit
                            break;
                    }

                    if($loop){
                        $loop   = false;
                        continue;
                    }

                    $in_check = Fp::model()->findByAttributes(array('PIN' => $p->nik, 'DateTime_' => $in_time));
                    if(!$in_check) {
                        Fp::saveFp($p->nik, $in_time, $cabang, $cabang_id, 88);
                    }

                    $out_check = Fp::model()->findByAttributes(array('PIN' => $p->nik, 'DateTime_' => $out_time));
                    if(!$out_check) {
                        Fp::saveFp($p->nik, $out_time, $cabang, $cabang_id, 88);
                    }

                    $index++;
                }
            }
            $status = true;
            $msg    = "Data berhasil disimpan.";
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
    }

    public function actionSaveFpFromBeta() {
        $nik = $_POST['pegawai'];

        if(
            $nik != '100057' &&
            $nik != '100004' &&
            $nik != '100015' &&
            $nik != '100013' &&
            $nik != '100017' &&
            $nik != '100058'
        )
            Log::generateErrorMessage(true, 'NIK tidak diizinkan menggunakan fitur ini.');
        else
            Fp::saveFp($nik, date("Y-m-d H:i:s"), 'GENDHIS01', '75d75432-a4d7-11e9-8846-000c29988254', 77);

        echo CJSON::encode(array(
            'success' => true,
            'msg' => 'Presensi berhasil disimpan ' . $nik
        ));
        Yii::app()->end();
    }
}
