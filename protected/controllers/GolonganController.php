<?php
class GolonganController extends GxController
{
    public function actionCreate()
    {
        $model = new Golongan;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Golongan'][$k] = $v;
            }
            $model->attributes = $_POST['Golongan'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->golongan_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Golongan');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Golongan'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Golongan'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->golongan_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->golongan_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Golongan')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->addCondition("bu_id = '" . $_POST['bu_id'] . "'");
        $model = Golongan::model()->findAll($criteria);
        $total = Golongan::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionSinkron()
    {
        $html = "";
        $output = Yii::app()->curl->post(Yii::app()->curl->base_url . 'GetAllGolongan',
            http_build_query(['bu_id' => $_POST['bu_id']]));
        $decode = json_decode($output, true);
        foreach ($decode['results'] as $result) {
            $model = Golongan::model()->findByPk($result['golongan_id']);
            if ($model == null) {
                $model = new Golongan;
            }
            $model->setAttributes($result);
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
}