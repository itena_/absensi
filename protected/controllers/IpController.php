<?php

class IpController extends GxController {

    public function actionCreate() {
        $model = new Ip;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Ip'][$k] = $v;
            }
            $model->attributes  = $_POST['Ip'];
            $model->cabang      = $_POST['kode_cabang'];
            $model->cabang_id   = $_POST['cabang_id'];

            if(isset($_POST['force_udp']))
                $model->force_udp = true;
            else
                $model->force_udp = null;

            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->ip_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Ip');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Ip'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Ip'];

            if(isset($_POST['force_udp']))
                $model->force_udp = true;
            else
                $model->force_udp = null;

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->cabang;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->cabang));
            }
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Ip')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex() {
        $dbcmd = new DbCmd();
        $dbcmd->addSelect("ip.*, CONCAT(ip.cabang,\" - \",ip.kode_ip ) cabang_ip")
            ->addSelect("b.bu_id, b.bu_name, b.bu_nama_alias, c.kode_cabang, c.nama_cabang")
            ->addFrom("{{ip}} ip")
            ->addLeftJoin("{{cabang}} c", "c.cabang_id = ip.cabang_id")
            ->addLeftJoin("{{bu}} b", "b.bu_id = c.bu_id")
        ;

        if(isset($_POST['grup']) && $_POST['grup'] == true) {
            $dbcmd->addGroup("cabang")
            ;
        }

        $count = count($dbcmd->queryAll());
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $dbcmd->addOrder('b.bu_name, c.kode_cabang');
        $model = $dbcmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }

}
