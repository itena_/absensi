<?php

class LockPostController extends GxController {
        
    public function actionCreate () {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $periode_id = $_POST['periode_id'];
            $id = Yii::app()->user->getId();
            $user_name = Users::model()->findByPk($id)->name_;
            $pegawai_id = Users::model()->findByAttributes(array('id' => $id))->pegawai_id;  
            $day_intime = date('Y-m-d');
            $day_outtime = date('Y-m-d');
            $cabang_click = $_POST['cabang_id'];
            $transferpegawai = TransferPegawai::model()->checkPegawai($periode_id, $pegawai_id, $day_intime, $day_outtime);
            if(isset($transferpegawai)){
                if ($transferpegawai->cabang_transfer_id != $cabang_click){
                    $status = false;
                    $msg = "Tidak bisa posting/unposting cabang, karena tidak sesuai dengan cabang Anda sekarang!";
                    echo CJSON::encode(array(
                        'success' => $status,
                        'msg' => $msg));
                    Yii::app()->end();   
                } else {
                    $postlock = $this->savepostinglocking($cabang_click, $id);
                }                         
            } else {
                $cabang = Pegawai::model()->findByPk($pegawai_id)->cabang_id;
                if ($cabang != $cabang_click){
                    if ($_POST['col'] == 'locked'){
                        $postlock = $this->savepostinglocking($cabang_click, $id);
                    } else {
                        $status = false;
                        $msg = "Tidak bisa posting/unposting cabang, karena tidak sesuai dengan cabang Anda sekarang!";
                        echo CJSON::encode(array(
                            'success' => $status,
                            'msg' => $msg));
                        Yii::app()->end();  
                    }                     
                } else {
                    $pegawai = '';
                    $excludePegawai = '';
                    foreach (Pegawai::model()->findAllByAttributes(['cabang_id' => $cabang_click]) as $p) {
                        $pegawai .= "'" . $p['pegawai_id'] . "',";
                    }
                    $pegawai = substr($pegawai,0,strlen($pegawai)-1);
                    $output = Api::mutasiPegawai(['pegawai' => $pegawai, 'periode_id' => $periode_id, 'cabang_id' => $cabang_click]);
                    $decode = json_decode(json_encode($output), true);
                    foreach ($decode['results'] as $result) {
                        $excludePegawai .= "'" . $result['pegawai_id'] . "',";
                        $validasi = DbCmd::instance()->addFrom("{{validasi}} v")
                            ->addCondition('periode_id = :periode_id AND pegawai_id = :pegawai_id AND cabang_id = :cabang_id')
                            ->addCondition("in_time >= :in_time")
                            ->addParams([
                                ':periode_id' => $periode_id,
                                ':pegawai_id' => $result['pegawai_id'],
                                ':cabang_id'  => $cabang_click,
                                ':in_time'    => $result['divisi_jabatan_start']
                            ])->queryAll()
                        ;
                        foreach ($validasi as $vv) {
                            LockPost::applyLockPostValidasi($vv, $result);
                        }
                        LockPost::applyLockPostStatusPegawai($periode_id, $cabang_click, $result);
                    }
                    LockPost::applyValidasiKecualiYangExclude($excludePegawai, $pegawai, $periode_id, $cabang_click);
                }
            }
            $postlock = $this->savepostinglocking($cabang_click, $id);
            $transaction->commit();
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }          
        echo CJSON::encode(array(
            'success' => $status,
            'postlock' => $postlock,
            'user_name' => $user_name,
            'msg' => $status));
        Yii::app()->end();   
    }

    public function actionLockAllCabang() {
        $bu_id      = $_POST['bu_id'];
        $periode_id = $_POST['periode_id'];

        $cabang = Cabang::model()->findAll("bu_id = :bu_id", [
            ':bu_id'      => $bu_id
        ]);
        foreach ($cabang as $cab) {
            $lockPost = LockPost::model()->find("periode_id = :periode_id AND cabang_id = :cabang_id", [
                ':periode_id' => $periode_id,
                ':cabang_id'  => $cab->cabang_id
            ]);
            if(!$lockPost){
                $lockPost = new LockPost();
                $lockPost->periode_id       = $periode_id;
                $lockPost->cabang_id        = $cab->cabang_id;
                $lockPost->locked           = 1;
                $lockPost->posted           = 0;
                $lockPost->posted_user_id   = '';
                $lockPost->posted_tdate     = '';
                $lockPost->bu_id            = $bu_id;
            } else {
                $lockPost->locked           = 1;
            }

            if (!$lockPost->save()){
                Log::generateErrorMessage(false,'Gagal melakukan LOCK.');
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => 'Periode berhasil di LOCK.'));
        Yii::app()->end();
    }
    
    static function savepostinglocking($cabang_click, $id) {
        $findLockPost = LockPost::model()->find('periode_id = :periode_id AND cabang_id = :cabang_id',
                           [':periode_id' => $_POST['periode_id'], ':cabang_id' => $_POST['cabang_id']]);

        $model = !isset($findLockPost) ? new LockPost : $findLockPost;
        $model->periode_id  = $_POST['periode_id'];
        $model->bu_id       = $_POST['bu_id'];
        $model->cabang_id   = $cabang_click;
        $log = [];

        $jumlah_off         = Periode::model()->findByPk($model->periode_id)->jumlah_off;
        
        if(isset($_POST['col'])){
          if($_POST['col'] == 'posted') {
              $model->posted = $_POST['status'] == 1 ? 0 : 1;

              if($_POST['status'] == 0 && GET_HITUNG_HTM() == true) {
              /*
               * CEK KEKURANGAN AMBIL OFF
               */
                  $dbperiode    = Periode::getPeriodeStartEnd($model->periode_id);
                  $periode      = $dbperiode->queryRow();

                  $validasi     = Validasi::getAbsensi($periode['ps'], $periode['pe'])
                      ->addSelect("(sum(v.kode_ket = 2) + sum(v.kode_ket = 7) + sum(v.kode_ket = 9)) jumlah_tidak_masuk")
                      ->addSelect("sum(v.kode_ket = 4) ambil_off")
                      ->addSelect($jumlah_off . " jatah_off")
                      ->addLeftJoin("{{keterangan}} k", "keterangan_id = v.kode_ket")

                      ->addCondition("v.pegawai_id in (SELECT
                            pegawai_id from pbu_pegawai 
                            where cabang_id = :cabang_id)")

                      ->addInCondition("v.kode_ket", [
                          '2','7','9','4'
                          #sakit,cuti bersalin,cuti istimewa, cuti non aktif
                      ])->addGroup("nik")
                      ->addOrder("nik")
                      ->addParams([
                          ':cabang_id' => $model->cabang_id
                      ])
                  ;
//                  $aaas      = $validasi->getQuery();

                  $data = new DbCmd();
                  $data->addSelect("@i:=@i+1 AS nomor, t0.*, IF(jumlah_tidak_masuk > 0, if(ambil_off < jatah_off, 'no','ok'), 'ok') syarat")
                      ->addFrom("(". $validasi->getQuery() . ") AS t0, (SELECT @i:=0) AS foo")
                      ->addHaving("syarat = 'no'")
                  ;

//                  $aaa      = $data->getQuery();
                  $result   = $data->queryAll();

                  if(count($result) > 0){
                      $msg = '<b>POSTING gagal!</b>.<br>Dikarenakan ada pegawai yang jumlah pengamblan off nya kurang.';
                      $msg .= '<br>Jatah off yang tersedia pada periode ';
                      $msg .= '<b><p style="color:red;display:inline;">' . $periode['kode_periode']
                          . '</p></b>, yaitu sebanyak ( <b><p style="color:red;display:inline;">' . $periode['jumlah_off'] . '</p></b> ) hari off';
                      $msg .= ': <br><br>';
                      foreach ($result as $r) {
                          $msg .= '<b>'. $r['nomor'] . '. </b>';
                          $msg .= 'NIK : <b><p style="color:red;display:inline;">' . $r['nik'] . '</p></b>';
                          $msg .= ' off yang diambil ';
                          $msg .= '( <b><p style="color:red;display:inline;">' . $r['ambil_off'] . '</p></b> ) hari<br>';
                      }
                      $msg .= '<br>Ubah beberapa <b>Hari tidak masuk (HTM)</b> menjadi <b><p style="color:red;display:inline;"> OFF </p></b>.';
                      $msg .= '<br>Dahulukan pengambilan off terlabih dahulu, baru pengambilan <b>HTM</b>.';
                      $msg .= '<br><br>Yang termasuk <b>Hari tidak masuk (HTM)</b>: ';
                      $msg .= '<br><b><p style="color:red;display:inline;">Sakit, WFH, Cuti Bersalin, Cuti Non AKtif & Cuti Istimewa.</p></b>';
                      echo CJSON::encode(array(
                          'success' => false,
                          'msg' => $msg));
                      Yii::app()->end();
                  }

                  /*
                   * END CEK KEKURANGAN AMBIL OFF
                   */
              }

              $model->posted_user_id    = $id;
              $model->posted_tdate      = date('Y-m-d H:i:s');
              $postlock                 = $model->posted;
              /*
               * SAVELOG
               */
                $tdate          = $model->posted_tdate;
                $log['action']  = $model->posted == 1 ? 'POST' : 'UNPOST';
                $log['trans']   = $_POST['col'];
              /*
               * END SAVELOG
               */
          } else if($_POST['col'] == 'locked'){    
              $model->locked            = $_POST['status'] == 1 ? 0 : 1;
              $model->locked_user_id    = $id;
              $model->locked_tdate      = date('Y-m-d H:i:s');
              $postlock                 = $model->locked;
              /*
               * SAVELOG
               */
                $tdate          = $model->locked_tdate;
                $log['action']  = $model->locked == 1 ? 'LOCK' : 'UNLOCK';
                $log['trans']   = $_POST['col'];
              /*
               * END SAVELOG
               */
          }          
        }
        if (!$model->save()){
            throw new Exception("Gagal menyimpan data.".CHtml::errorSummary($model));
        }
        /*
        * SAVELOG
        */
            $log['trans_id']    = $model->lock_post_id;
            $log['db']          = 'pbu_lock_post';
            U::save_log($id, $tdate, $log);
        /*
        * END SAVELOG
        */
        return $postlock;
    }

    public function actionIndex() { 
        if (isset($_POST['cmp']) == 'detail') {  
            $dbcmd = DbCmd::instance()->addFrom('{{cabang}} c')
                ->addSelect('c.*, lp.posted, lp.locked, lu.name_ locked_user_name, pu.name_ posted_user_name')
                ->addLeftJoin('{{lock_post}} lp', 'lp.cabang_id = c.cabang_id AND periode_id = :periode_id')
                ->addLeftJoin('{{users}} lu', 'lu.id = lp.locked_user_id')
                ->addLeftJoin('{{users}} pu', 'pu.id = lp.posted_user_id')
                ->addCondition("c.kode_cabang != 'ALL' AND c.bu_id = :bu_id")
                ->addParam(':bu_id', $_POST['bu_id']) 
                ->addParam(':periode_id', $_POST['periode_id'])
                ->addOrder('kode_cabang ASC');

            $dbcmd->addInCondition("c.cabang_id", $this->getAllCabangInBu($_POST['bu_id']));
            $count = $dbcmd->queryCount();
            $model = $dbcmd->queryAll();
            $this->renderJsonArrWithTotal($model, $count);
        } 
        
        $dbcmd = DbCmd::instance()->addFrom('{{lock_post}} lp')
                ->addSelect('lp.*, p.kode_periode, group_concat(c.kode_cabang ORDER BY kode_cabang) kode_cabang, bu_name')
                ->addLeftJoin('{{periode}} p', 'p.periode_id = lp.periode_id')
                ->addLeftJoin('{{cabang}} c', 'c.cabang_id = lp.cabang_id') 
                ->addLeftJoin('{{bu}} b', 'b.bu_id = lp.bu_id') 
                ->addOrder('p.periode_start DESC')
                ->addGroup('kode_periode');       
            
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $dbcmd->addInCondition("c.cabang_id", $this->getAllCabangInBu($_POST['bu_id']));
        $count = $dbcmd->queryCount();
        $model = $dbcmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }
    
}
