<?php
class MasterGajiController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new MasterGaji;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['MasterGaji'][$k] = $v;
                }
                $model->attributes = $_POST['MasterGaji'];
                $msg = "Data berhasil disimpan.";
                if ($model->leveling_id == null && $model->golongan_id == null && $model->area_id == null) {
                    $comm = Yii::app()->db->createCommand("
                    INSERT INTO `pbu_master_gaji` (`master_gaji_id`, `master_id`, `leveling_id`, `golongan_id`, `area_id`, `amount`) 
                    (SELECT UUID(), :master_id, pl.leveling_id, pg.golongan_id, pa.area_id, :amount
                    FROM pbu_leveling AS pl, pbu_area AS pa, pbu_golongan AS pg) ON DUPLICATE KEY UPDATE amount = VALUES(amount);");
                    $count = $comm->execute([':master_id' => $model->master_id, ':amount' => $model->amount]);
                    $msg = $count . ' ' . $msg;
                } elseif ($model->leveling_id == null && $model->golongan_id == null) {
                    $comm = Yii::app()->db->createCommand("
                    INSERT INTO `pbu_master_gaji` (`master_gaji_id`, `master_id`, `leveling_id`, `golongan_id`, `area_id`, `amount`) 
                    (SELECT UUID(), :master_id, pl.leveling_id, pg.golongan_id, :area_id, :amount
                    FROM pbu_leveling AS pl, pbu_golongan AS pg) ON DUPLICATE KEY UPDATE amount = VALUES(amount);");
                    $count = $comm->execute([':master_id' => $model->master_id, ':area_id' => $model->area_id, ':amount' => $model->amount]);
                    $msg = $count . ' ' . $msg;
                } elseif ($model->leveling_id == null && $model->area_id == null) {
                    $comm = Yii::app()->db->createCommand("
                    INSERT INTO `pbu_master_gaji` (`master_gaji_id`, `master_id`, `leveling_id`, `golongan_id`, `area_id`, `amount`) 
                    (SELECT UUID(), :master_id, pl.leveling_id, :golongan_id, pa.area_id, :amount
                    FROM pbu_leveling AS pl,pbu_area AS pa) ON DUPLICATE KEY UPDATE amount = VALUES(amount);");
                    $count = $comm->execute([':master_id' => $model->master_id, ':golongan_id' => $model->golongan_id, ':amount' => $model->amount]);
                    $msg = $count . ' ' . $msg;
                } elseif ($model->golongan_id == null && $model->area_id == null) {
                    $comm = Yii::app()->db->createCommand("
                    INSERT INTO `pbu_master_gaji` (`master_gaji_id`, `master_id`, `leveling_id`, `golongan_id`, `area_id`, `amount`) 
                    (SELECT UUID(), :master_id, :leveling_id, pg.golongan_id, pa.area_id, :amount
                    FROM  pbu_golongan AS pg,pbu_area AS pa) ON DUPLICATE KEY UPDATE amount = VALUES(amount);");
                    $count = $comm->execute([':master_id' => $model->master_id, ':leveling_id' => $model->leveling_id, ':amount' => $model->amount]);
                    $msg = $count . ' ' . $msg;
                } elseif ($model->leveling_id == null) {
                    $comm = Yii::app()->db->createCommand("
                    INSERT INTO `pbu_master_gaji` (`master_gaji_id`, `master_id`, `leveling_id`, `golongan_id`, `area_id`, `amount`) 
                    (SELECT UUID(), :master_id, pl.leveling_id,:golongan_id, :area_id, :amount
                    FROM  pbu_leveling AS pl) ON DUPLICATE KEY UPDATE amount = VALUES(amount);");
                    $count = $comm->execute([
                        ':master_id' => $model->master_id,
                        ':golongan_id' => $model->golongan_id,
                        ':area_id' => $model->area_id,
                        ':amount' => $model->amount
                    ]);
                    $msg = $count . ' ' . $msg;
                } elseif ($model->golongan_id == null) {
                    $comm = Yii::app()->db->createCommand("
                    INSERT INTO `pbu_master_gaji` (`master_gaji_id`, `master_id`, `leveling_id`, `golongan_id`, `area_id`, `amount`) 
                    (SELECT UUID(), :master_id, :leveling_id,pg.golongan_id, :area_id, :amount
                    FROM  pbu_golongan AS pg) ON DUPLICATE KEY UPDATE amount = VALUES(amount);");
                    $count = $comm->execute([
                        ':master_id' => $model->master_id,
                        ':leveling_id' => $model->leveling_id,
                        ':area_id' => $model->area_id,
                        ':amount' => $model->amount
                    ]);
                    $msg = $count . ' ' . $msg;
                } elseif ($model->area_id == null) {
                    $comm = Yii::app()->db->createCommand("
                    INSERT INTO `pbu_master_gaji` (`master_gaji_id`, `master_id`, `leveling_id`, `golongan_id`, `area_id`, `amount`) 
                    (SELECT UUID(), :master_id, :leveling_id,:golongan_id, pa.area_id, :amount
                    FROM  pbu_area AS pa) ON DUPLICATE KEY UPDATE amount = VALUES(amount);");
                    $count = $comm->execute([
                        ':master_id' => $model->master_id,
                        ':golongan_id' => $model->golongan_id,
                        ':leveling_id' => $model->leveling_id,
                        ':amount' => $model->amount
                    ]);
                    $msg = $count . ' ' . $msg;
                } else {
                    if (!$model->save()) {
                        throw new Exception(CHtml::errorSummary($model));
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $e) {
                $transaction->rollback();
                $status = false;
                $msg = $e->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'MasterGaji');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['MasterGaji'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['MasterGaji'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->master_gaji_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->master_gaji_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'MasterGaji')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $params = [];
        if (isset($_POST['bu_id']) && $_POST['bu_id'] != null) {
            $criteria->addCondition('mg.bu_id = :bu_id');
            $params[':bu_id'] = $_POST['bu_id'];
        }
        if (isset($_POST['level'])) {
            $criteria->addCondition('pl.nama like :nama_level');
            $params[':nama_level'] = '%' . $_POST['level'] . '%';
        }
        if (isset($_POST['golongan'])) {
            $criteria->addCondition('pg.nama like :nama_gol');
            $params[':nama_gol'] = '%' . $_POST['golongan'] . '%';
        }
        if (isset($_POST['area'])) {
            $criteria->addCondition('pa.nama like :nama_area');
            $params[':nama_area'] = '%' . $_POST['area'] . '%';
        }
        if (isset($_POST['master'])) {
            $criteria->addCondition('pm.nama like :nama_master');
            $params[':nama_master'] = '%' . $_POST['master'] . '%';
        }
        $criteria->alias = 'mg';
        $criteria->join = 'LEFT JOIN pbu_leveling pl ON mg.leveling_id = pl.leveling_id ';
        $criteria->join .= 'LEFT JOIN pbu_golongan pg ON mg.golongan_id = pg.golongan_id ';
        $criteria->join .= 'LEFT JOIN pbu_area pa ON mg.area_id = pa.area_id ';
        $criteria->join .= 'LEFT JOIN pbu_master pm ON mg.master_id = pm.master_id ';
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->order = "pl.kode DESC,pg.kode DESC";
        $criteria->params = $params;
        $model = MasterGaji::model()->findAll($criteria);
        $total = MasterGaji::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}