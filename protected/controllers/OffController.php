<?php
class OffController extends GxController
{
    public function actionOffAll() {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $status  = true;
            $message = 'OK';
            $periode_id = $_POST['periode_id'];
            $pegawai  = CJSON::decode( $_POST['pegawaidetils'] );

            LockPost::model()->check_lockpost($periode_id);

            /*
             * tinggal ngesave ygy
             */
            foreach ($pegawai as $p) {
                if(!isset($p['count'])) continue;
                $libur = explode(';', $_POST['libur']);
                $in_time = '05:00:00';
                $out_time = '20:00:00';
                $pegawai_id = $p['pegawai_id'];
                $PIN = $p['nik'];
                $kode_ket = 4; /*Off*/

                $array = Validasi::addAdditionalValidasi($pegawai_id, $periode_id);

                $shift = Shift::model()->find("cabang_id = :cabang_id AND kelompok_shift = :ks AND active = 1",[
                   ':cabang_id' => $p['cabang_id'], ':ks' => $p['kelompok_pegawai']
                ]);

                if(isset($shift)) {
                    $shift_id = $shift->shift_id;
                } else {
                    $kel = $p['kelompok_pegawai'] == 0 ? 'Karyawan' : 'OG/OB/Security';
                    Log::generateErrorMessage(false, "Pegawai $PIN " .$p['nama_lengkap']. " tidak memiliki Shift. Buat dulu shift untuk kelompok pegawai $kel!");
                }

                for($i=0; $i<count($libur); $i++) {
                    $dari = $sampai = $libur[$i];

                    $cek = Validasi::model()->find("cabang_id = :cabang_id AND pegawai_id = :pegawai_id AND status_int != 2
                    AND (in_time >= :in AND out_time <= :out)",[
                        ':cabang_id' => $p['cabang_id'], ':pegawai_id' => $pegawai_id,
                        ':in' => "$dari $in_time", ':out' => "$sampai $out_time"
                    ]);
                    if(isset($cek)) continue;
                    Validasi::check_tanggal($periode_id,"$dari $in_time","$sampai $out_time");

                    $valid          = new Validasi;
                    $k['PIN']       = $PIN;
                    $k['jam_in']    = "$dari $in_time";
                    $k['jam_out']   = "$sampai $out_time";
                    $cabang_id      = $p['cabang_id'];
                    $duras          = Fp::model()->durasi($pegawai_id, $shift_id, $k['jam_in'] , $k['jam_out'], '', '');
                    $validasi = Validasi::saveValidasi($valid, $pegawai_id, $k, $shift_id, $duras, $kode_ket, $cabang_id, '', 0, 0);

                    $valid->attributes = array_merge($validasi, $array);
                    if (!$valid->save()) {
                        Log::generateErrorMessage(false, 'Gagal menyimpan validasi!' . CHtml::errorSummary($valid));
                    }
                }
            }

            $status = true;
            $msg = "Data berhasil disimpan.";
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $message = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $message));
        Yii::app()->end();
    }
    public function actionIndex()
    {
        $nama_lengkap = $nik = "";
        if (isset($_POST['nama_lengkap'])) {
            $nama_lengkap = $_POST['nama_lengkap'];
        }
        if (isset($_POST['nik'])) {
            $nik = $_POST['nik'];
        }

        $periode    = $_POST['periode_id'];
        $cabang     = $_POST['cabang_id'];
        $model      = U::report_check_absen($periode,$cabang,$nama_lengkap,$nik);
        $this->renderJsonArrWithTotal($model, count($model));
    }
}
