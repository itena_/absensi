<?php
class PayrollAbsensiController extends GxController
{
    public function actionCreate()
    {
//        $model = new PayrollAbsensi;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            app()->db->autoCommit = false;
            $detils = CJSON::decode($_POST['detil']);
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($detils as $r) {
                    PayrollAbsensi::saveData($r['periode_id'], $r['pegawai_id'], $r['total_hari_kerja'],
                        $r['total_lk'], $r['total_sakit'], $r['total_cuti_tahunan'], $r['total_off'],
                        $r['total_lembur_1'], $r['total_lembur_next'], $r['jatah_off'], $r['total_cuti_menikah'],
                        $r['total_cuti_bersalin'], $r['total_cuti_istimewa'], $r['total_cuti_non_aktif'], $r['less_time']);
                }
//                $model->save();
                $transaction->commit();
                $status = true;
                $msg = 'Absensi Payroll berhasil disimpan.';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
//            foreach ($_POST as $k => $v) {
//                if (is_angka($v)) $v = get_number($v);
//                $_POST['PayrollAbsensi'][$k] = $v;
//            }
//            $model->attributes = $_POST['PayrollAbsensi'];
//            $msg = "Data gagal disimpan.";
//            if ($model->save()) {
//                $status = true;
//                $msg = "Data berhasil di simpan dengan id " . $model->payroll_absensi_id;
//            } else {
//                $msg .= " " . CHtml::errorSummary($model);
//                $status = false;
//            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PayrollAbsensi');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PayrollAbsensi'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PayrollAbsensi'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->payroll_absensi_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->payroll_absensi_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PayrollAbsensi')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
//        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
//        $model = PayrollAbsensi::model()->findAll($criteria);
//        $total = PayrollAbsensi::model()->count($criteria);
        
        
        $arr = U::absensiPayrollIndex($_POST['periode_id']);
        $this->renderJsonArr($arr);
    }
    public function actionSaveLaporan() {
        $periode_id = $_POST['periode_id'];
        
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $res1 = Yii::app()->db->createCommand('
            SELECT * FROM pbu_result
            INNER JOIN pbu_pegawai ON pbu_pegawai.nik=pbu_result.pin_id
            WHERE periode_id=:periode_id
            ');
            $result = $res1->queryAll(true, array(
                ':periode_id' => $periode_id));
            $count = 0;
            foreach ($result as $row) {
                $count++;
                //$periode_id = $periode_id;
                $pegawai_id = $row['pegawai_id'];
                $total_hari_kerja = $row['total_hari_kerja'];
                $locked = $row['locked'];
                $total_lk = $row['total_lk'];
                $total_sakit = $row['total_sakit'];
                $total_cuti_tahunan = $row['total_cuti_tahunan'];
                $total_off = $row['total_off'];
                $total_cuti_menikah = $row['total_cuti_menikah'];
                $total_cuti_bersalin = $row['total_cuti_bersalin'];
                $total_cuti_istimewa = $row['total_cuti_istimewa'];
                $total_cuti_non_aktif = $row['total_cuti_non_aktif'];
                $total_lembur_1 = $row['total_min_lembur_awal'];
                $total_lembur_next = $row['total_min_lembur_akhir'];

                PayrollAbsensi::saveData($periode_id, $pegawai_id, $total_hari_kerja, $locked, $total_lk,
                                         $total_sakit, $total_cuti_tahunan, $total_off, $total_cuti_menikah,
                                         $total_cuti_bersalin, $total_cuti_istimewa, $total_cuti_non_aktif,
                                         $total_lembur_1, $total_lembur_next);
            }
            $status = true;
            $msg = "$count data berhasil disimpan!";
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
    }
//    static function check_lock($periode) {
//        $comm = Yii::app()->db->createCommand(
//                        "SELECT * FROM pbu_result WHERE periode_id = '" . $periode . "'")
//                ->queryScalar();
//        if (!$comm) {
//            $msg = '<b>GAGAL!</b></br>Laporan tidak ada data di Periode ini!!!';
//            echo CJSON::encode(array(
//                'success' => false,
//                'msg' => $msg
//            ));
//            Yii::app()->end();
//        } else {
//            return '';
//        }
//    }    

}