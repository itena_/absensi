<?php
class PayrollController extends GxController
{
    public function actionCreate()
    {
        $model = new Payroll;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Payroll'][$k] = $v;
            }
            $model->attributes = $_POST['Payroll'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->payroll_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionPayrollGenerate()
    {
        $periode_id = $_POST['periode_id'];
        $status = false;
        $msg = 'Data berhasil digenerate.';
//        app()->db->autoCommit = false;
//        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (Lock::isPeriodeLocked($periode_id)) {
                throw new Exception('Periode sudah di lock.');
            }
            /** @var Periode $periode */
            $periode = Periode::model()->findByPk($periode_id);
            if ($periode == null) {
                throw new Exception('Fatal Error. Periode tidak ditemukan.');
            }
            /** @var Pegawai[] $allPegawai */
            $allPegawai = Pegawai::model()->findAll();
//            $del = Yii::app()->db->createCommand('DELETE pbu_payroll, pbu_payroll_details
//              FROM pbu_payroll LEFT JOIN pbu_payroll_details ON pbu_payroll.payroll_id = pbu_payroll_details.payroll_id
//              WHERE pbu_payroll.periode_id = :periode_id;');
//            $del->execute([':periode_id' => $periode_id]);
//            $dirName = Yii::getPathOfAlias('application.runtime.' . Yii::app()->controller->id);
//            $tmpfname = tempnam($dirName, 'tpay');
            foreach ($allPegawai as $peg) {
                $peg->generatePayroll($periode);
            }
//                $handle = fopen($tmpfname, "w+");
//                $phpCode = '';
//                /** @var Cabang $cab */
//                $cab = Cabang::model()->findByPk($peg->cabang_id);
//                if ($cab == null) {
//                    continue;
//                }
//                /** @var Jabatan $jab */
//                $jab = Jabatan::model()->findByPk($peg->jabatan_id);
//                if ($jab == null) {
//                    continue;
//                }
//                if ($peg->leveling_id == null) {
//                    continue;
//                }
//                /** @var PayrollAbsensi $absensi */
//                $absensi = PayrollAbsensi::model()->findByAttributes([
//                    'periode_id' => $periode_id,
//                    'pegawai_id' => $peg->pegawai_id
//                ]);
//                if ($absensi == null) {
//                    continue;
//                }
//                $pay = new Payroll;
//                $pay->pegawai_id = $peg->pegawai_id;
//                $pay->kode_gol = $peg->golongan->kode;
//                $pay->nama_gol = $peg->golongan->nama;
//                $pay->kode_level = $peg->leveling->kode;
//                $pay->nama_level = $peg->leveling->nama;
//                $pay->kode_cab = $peg->cabang->kode_cabang;
//                $pay->nama_cab = $peg->cabang->nama_cabang;
//                $pay->kode_area = $peg->cabang->area->kode;
//                $pay->nama_area = $peg->cabang->area->nama;
//                $pay->nik = $peg->nik;
//                $pay->leveling_id = $peg->leveling_id;
//                $pay->golongan_id = $peg->golongan_id;
//                $pay->area_id = $cab->area_id;
//                $pay->nama_lengkap = $peg->nama_lengkap;
//                $pay->nama_jabatan = $jab->nama_jabatan;
//                $pay->tgl_masuk = $peg->tgl_masuk;
//                $pay->email = $peg->email;
//                $pay->periode_id = $periode_id;
//                $pay->total_hari_kerja = $absensi->total_hari_kerja;
//                $pay->total_lk = $absensi->total_lk;
//                $pay->total_cuti_tahunan = $absensi->total_cuti_tahunan;
//                $pay->total_off = $absensi->total_off;
//                $pay->total_sakit = $absensi->total_sakit;
//                $pay->total_lembur_1 = $absensi->total_lembur_1;
//                $pay->total_lembur_next = $absensi->total_lembur_next;
//                $pay->jatah_off = $absensi->jatah_off;
//                if (!$pay->save()) {
//                    throw new Exception(CHtml::errorSummary($pay));
//                }
//                $phpCode .= '$PHJ__=' . $periode->getCount() . ';' . PHP_EOL;
//                $phpCode .= '$PJOFF__=' . $periode->jumlah_off . ';' . PHP_EOL;
//                $phpCode .= '$HK__=' . $absensi->total_hari_kerja . ';' . PHP_EOL;
//                $phpCode .= '$LK__=' . $absensi->total_lk . ';' . PHP_EOL;
//                $phpCode .= '$CT__=' . $absensi->total_cuti_tahunan . ';' . PHP_EOL;
//                $phpCode .= '$GOFF__=' . $absensi->total_off . ';' . PHP_EOL;
//                $phpCode .= '$JOFF__=' . $absensi->jatah_off . ';' . PHP_EOL;
//                $phpCode .= '$SICK__=' . $absensi->total_sakit . ';' . PHP_EOL;
//                /** @var Master[] $master */
//                $master = Master::model()->findAll();
//                foreach ($master as $md1) {
//                    $phpCode .= '$' . $md1->kode . '=0;' . PHP_EOL;
//                }
//                $masterGaji = Yii::app()->db->createCommand("SELECT * FROM pbu_master_gaji_view WHERE pegawai_id = :pegawai_id")
//                    ->queryAll(true, [':pegawai_id' => $peg->pegawai_id]);
//                foreach ($masterGaji as $md) {
//                    $phpCode .= '$' . $md['mkode'] . '=' . $md['amount'] . ';' . PHP_EOL;
//                }
////                $phpCode .= '$value=$GP;' . PHP_EOL;
//                $length = 10;
//                fwrite($handle, "<?php" . PHP_EOL . $phpCode);
//                fclose($handle);
//                include $tmpfname;
//                $tmpscfname = tempnam($dirName, 'tscpay');
//                /** @var SchemaGajiView[] $schema */
//                $schema = SchemaGajiView::model()->findAllByAttributes([
//                    'status_id' => $peg->status_id
//                ]);
//                if ($schema == null) {
//                    continue;
//                }
//                $total_income = $total_deduction = $take_home_pay = 0;
//                foreach ($schema as $sc) {
//                    $hasilFormula__ = 0;
//                    $handlesc = fopen($tmpscfname, "w+");
//                    fwrite($handlesc, "<?php" . PHP_EOL . $sc->formula);
//                    fclose($handlesc);
//                    include $tmpscfname;
////                    var_dump($hasilFormula__);
//                    $payDetail = new PayrollDetails;
//                    $payDetail->payroll_id = $pay->payroll_id;
//                    $payDetail->nama_skema = $sc->nama_skema;
//                    $payDetail->type_ = $sc->type_;
//                    $payDetail->amount = $sc->type_ * $hasilFormula__;
//                    if (!$payDetail->save()) {
//                        throw new Exception(CHtml::errorSummary($payDetail));
//                    }
//                    if ($payDetail->amount >= 0) {
//                        $total_income += $payDetail->amount;
//                    } else {
//                        $total_deduction += $payDetail->amount;
//                    }
//                }
//                unlink($tmpscfname);
//                $take_home_pay = $total_income + $total_deduction;
//                if (!$pay->saveAttributes([
//                    'total_income' => $total_income,
//                    'total_deduction' => $total_deduction,
//                    'take_home_pay' => $take_home_pay
//                ])
//                ) {
//                    throw new Exception(CHtml::errorSummary($pay));
//                }
//            }
//            if ($absensi == NULL) {
//                fclose($handle);
//                unlink($tmpfname);
//            }
//            $model->save();
//            $transaction->commit();
            $status = true;
        } catch (Exception $ex) {
//            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }
    public function actionPayrollRecalculated()
    {
        $periode_id = $_POST['periode_id'];
        $pegawai_id = $_POST['pegawai_id'];
        $status = false;
        $msg = '';
//        app()->db->autoCommit = false;
//        $transaction = Yii::app()->db->beginTransaction();
        try {
            if (Lock::isPeriodeLocked($periode_id)) {
                throw new Exception('Periode sudah di lock.');
            }
//            Payroll::model()->findByAttributes([
//                'periode_id' => $periode_id,
//                'pegawai_id' => $pegawai_id
//            ])->delete();
            /** @var Pegawai $peg */
            $peg = Pegawai::model()->findByPk($pegawai_id);
            if ($peg == null) {
                throw new Exception('Pegawai tidak ditemukan.');
            }
            /** @var Periode $periode */
            $periode = Periode::model()->findByPk($periode_id);
            if ($periode == null) {
                throw new Exception('Fatal Error. Periode tidak ditemukan.');
            }
            $peg->generatePayroll($periode);
//            $mGaji = [];
//            $masterGaji = Yii::app()->db->createCommand("SELECT * FROM pbu_master_gaji_view")
//                ->queryAll(true);
//            foreach ($masterGaji as $row) {
//                $mGaji[$row['kode_golongan']][$row['kode_master']] = $row['amount'];
//            }
//            $dirName = Yii::getPathOfAlias('application.runtime.' . Yii::app()->controller->id);
//            $tmpfname = tempnam($dirName, 'tpay');
//            $handle = fopen($tmpfname, "w+");
//            $phpCode = '';
//            $gol_id = Golongan::model()->findByAttributes([
//                'kode' => $peg->kode_golongan
//            ]);
//            if ($gol_id == null) {
//                throw new Exception('Golongan tidak ditemukan.');
//            }
//            $absensi = PayrollAbsensi::model()->findByAttributes([
//                'periode_id' => $periode_id,
//                'pegawai_id' => $peg->pegawai_id
//            ]);
//            if ($absensi == null) {
//                throw new Exception('Absensi tidak ditemukan.');
//            }
//            $pay = new Payroll;
//            $pay->pegawai_id = $peg->pegawai_id;
//            $pay->nik = $peg->nik;
//            $pay->kode_golongan = $peg->kode_golongan;
//            $pay->nama_lengkap = $peg->nama_lengkap;
//            $pay->email = $peg->email;
//            $pay->periode_id = $periode_id;
//            $pay->total_hari_kerja = $absensi->total_hari_kerja;
//            $pay->total_lk = $absensi->total_lk;
//            $pay->total_cuti_tahunan = $absensi->total_cuti_tahunan;
//            $pay->total_off = $absensi->total_off;
//            $pay->total_sakit = $absensi->total_sakit;
//            if (!$pay->save()) {
//                throw new Exception(CHtml::errorSummary($pay));
//            }
//            $phpCode .= '$HK__=' . $absensi->total_hari_kerja . ';' . PHP_EOL;
//            $phpCode .= '$LK__=' . $absensi->total_lk . ';' . PHP_EOL;
//            $phpCode .= '$CT__=' . $absensi->total_cuti_tahunan . ';' . PHP_EOL;
//            $phpCode .= '$OFF__=' . $absensi->total_off . ';' . PHP_EOL;
//            $phpCode .= '$SICK__=' . $absensi->total_sakit . ';' . PHP_EOL;
//            /** @var Master[] $master */
//            $master = Master::model()->findAll();
//            foreach ($master as $md) {
//                $phpCode .= '$' . $md->kode . '=' . $mGaji[$peg->kode_golongan][$md->kode] . ';' . PHP_EOL;
//            }
//            $phpCode .= '$value=$GP;' . PHP_EOL;
//            $length = 10;
//            fwrite($handle, "<?php" . PHP_EOL . $phpCode);
//            fclose($handle);
//            include $tmpfname;
//            $tmpscfname = tempnam($dirName, 'tscpay');
//            $schema = SchemaGaji::model()->findAll();
//            $total_income = $total_deduction = $take_home_pay = 0;
//            foreach ($schema as $sc) {
//                $hasilFormula__ = 0;
//                $handlesc = fopen($tmpscfname, "w+");
//                fwrite($handlesc, "<?php" . PHP_EOL . $sc->formula);
//                fclose($handlesc);
//                include $tmpscfname;
////                    var_dump($hasilFormula__);
//                $payDetail = new PayrollDetails;
//                $payDetail->payroll_id = $pay->payroll_id;
//                $payDetail->nama_skema = $sc->nama_skema;
//                $payDetail->type_ = $sc->type_;
//                $payDetail->amount = $sc->type_ * $hasilFormula__;
//                if (!$payDetail->save()) {
//                    throw new Exception(CHtml::errorSummary($payDetail));
//                }
//                if ($payDetail->amount >= 0) {
//                    $total_income += $payDetail->amount;
//                } else {
//                    $total_deduction += $payDetail->amount;
//                }
//            }
//            unlink($tmpscfname);
//            $take_home_pay = $total_income + $total_deduction;
//            if (!$pay->saveAttributes([
//                'total_income' => $total_income,
//                'total_deduction' => $total_deduction,
//                'take_home_pay' => $take_home_pay
//            ])
//            ) {
//                throw new Exception(CHtml::errorSummary($pay));
//            }
//            unlink($tmpfname);
//            $transaction->commit();
            $status = true;
        } catch (Exception $ex) {
//            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }
    public function actionUpdate($id)
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan.";
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg//,
            ));
            Yii::app()->end();
        }
    }
    public function actionLock()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data payroll berhasil di lock.";
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
//                Payroll::model()->updateAll(['lock' => 1],
//                    'periode_id = :periode_id',
//                    [':periode_id' => $_POST['periode_id']]);
                if (Lock::isPeriodeLocked($_POST['periode_id'])) {
                    throw new Exception('Periode sudah di lock.');
                } else {
                    $l = new Lock;
                    $l->periode_id = $_POST['periode_id'];
                    if (!$l->save()) {
                        throw new Exception(CHtml::errorSummary($l));
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg//,
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Payroll')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $id = Yii::app()->user->getId();
        $sri = Users::model()
            ->findByAttributes(array('id' => $id))
            ->security_roles_id;
        $criteria = new CDbCriteria();
        $params = [];
        $criteria->select = 'py.*';
//        $criteria->select = "pp.payroll_id,pp.periode_id,pp.total_hari_kerja,pp.total_lk,pp.total_sakit,
//        pp.total_cuti_tahunan,pp.total_off,pp.pegawai_id,pp.nik,pp.nama_lengkap,pp.email,pp.total_income,
//        abs(pp.total_deduction) total_deduction,pp.take_home_pay,pp.transfer,pp.golongan_id,pp.leveling_id,pp.area_id,
//        pp.total_lembur_1,pp.total_lembur_next,pp.nama_jabatan,pp.tgl_masuk,pp.nama_gol,pp.kode_gol,
//        pp.kode_level,pp.nama_level,pp.kode_area,pp.nama_area,pp.kode_cab,pp.nama_cab,pp.jatah_off";
        $criteria->alias = "py";
        $criteria->join = '
            INNER JOIN pbu_pegawai as pp ON py.pegawai_id = pp.pegawai_id
            INNER JOIN pbu_sr_cbg_area_bu AS sr ON pp.cabang_id = sr.cabang_id
            INNER JOIN pbu_sr_level_bu as sl ON pp.leveling_id = pp.leveling_id';
        $criteria->condition = 'sr.security_roles_id = :security_roles_id AND sl.security_roles_id = :security_roles_id';
        $params[':security_roles_id'] = $sri;
        $criteria->order = "pp.nik";
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $params;
        $model = Payroll::model()->findAll($criteria);
        $total = Payroll::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}
