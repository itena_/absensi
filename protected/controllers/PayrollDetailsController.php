<?php
class PayrollDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new PayrollDetails;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PayrollDetails'][$k] = $v;
            }
            $model->attributes = $_POST['PayrollDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->payroll_detail_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpload()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data sukses disimpan.";
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if (Lock::isPeriodeLocked($_POST['periode_id'])) {
                    throw new Exception('Periode sudah di lock.');
                }
                /** @var Payroll $payroll */
                $payroll = Payroll::model()->findByAttributes([
                    'periode_id' => $_POST['periode_id'],
                    'nik' => $_POST['nik']
                ]);
                if ($payroll == null) {
                    throw new Exception('Payroll tidak ditemukan.');
                }
//                /** @var Schema $skema */
//                $skema = Schema::model()->findByPk($_POST['schema_id']);
                /** @var PayrollDetails $model */
                $model = PayrollDetails::model()->findByAttributes([
                    'schema_id' => $_POST['schema_id'],
                    'payroll_id' => $payroll->payroll_id
                ]);
                if ($model == null) {
                    throw new Exception('Komponen payroll tidak ditemukan.');
                }
                $model->amount = get_number($_POST['amount']);
                if (!$model->save()) {
                    throw new Exception('Gagal disimpan');
                }
                /** @var Payroll $payroll */
                $payroll = Payroll::model()->findByPk($model->payroll_id);
                $payroll->calculateTotal();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data sukses disimpan.";
            $total_income = $total_deduction = $take_home_pay = 0;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var PayrollDetails $model */
                $model = PayrollDetails::model()->findByPk($id);
                $model->amount = get_number($_POST['amount']);
                if (!$model->save()) {
                    CHtml::errorSummary('Gagal disimpan');
                }
                /** @var Payroll $payroll */
                $payroll = Payroll::model()->findByPk($model->payroll_id);
                if (Lock::isPeriodeLocked($payroll->periode_id)) {
                    throw new Exception('Periode sudah di lock.');
                }
                $payroll->calculateTotal();
                $transaction->commit();
                $total_income = $payroll->total_income;
                $total_deduction = $payroll->total_deduction;
                $take_home_pay = $payroll->take_home_pay;
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg,
                'total_income' => $total_income,
                'total_deduction' => $total_deduction,
                'take_home_pay' => $take_home_pay
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            $total_income = $total_deduction = $take_home_pay = 0;
            try {
                /** @var PayrollDetails $detil */
                $detil = PayrollDetails::model()->findByPk($id);
                /** @var Payroll $payroll */
                $payroll = Payroll::model()->findByPk($detil->payroll_id);
                $detil->delete();
                $payroll->calculateTotal();
                $total_income = $payroll->total_income;
                $total_deduction = $payroll->total_deduction;
                $take_home_pay = $payroll->take_home_pay;
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg,
                'total_income' => $total_income,
                'total_deduction' => $total_deduction,
                'take_home_pay' => $take_home_pay
            ));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $params = [];
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['payroll_id'])) {
            $criteria->addCondition('payroll_id = :payroll_id');
            $params[':payroll_id'] = $_POST['payroll_id'];
        }
        $criteria->params = $params;
        $model = PayrollDetails::model()->findAll($criteria);
        $total = PayrollDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}