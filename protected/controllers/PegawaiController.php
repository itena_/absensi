<?php
class PegawaiController extends GxController
{
    public function actionCreate()
    {
        $model = new Pegawai;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            if(GET_ALLOW_ADD() == FALSE){
                if($this->getSecuriyRolesId() !== GET_SECURITY_ROLE_ADMINISTRATOR()) {
                    Log::generateErrorMessage('<p class="redInline">Invalid request. Adding/edit Pegawai is not allowed. Please contact to your HR.</p>');
                }
            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Pegawai'][$k] = $v;
            }
            $model->attributes = $_POST['Pegawai'];
            //save store
            $store = Cabang::model()->findByAttributes(array('cabang_id' => $_POST['cabang_id']));
            $model->store = $store->kode_cabang;
            $model->cabang_id = $_POST['cabang_id'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pegawai_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $cabang = Cabang::model()->checkCabang();
        if(GET_ALLOW_ADD() == FALSE){
                echo CJSON::encode(array(
                'success' => FALSE,
                'msg' => 'Invalid request. Adding/edit Pegawai is not allowed.'));
            Yii::app()->end();
        }

        $model = $this->loadModel($id, 'Pegawai');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Pegawai'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Pegawai'];
            $store = Cabang::model()->findByAttributes(array('cabang_id' => $_POST['cabang_id']));
            $model->store = $store->kode_cabang;
            $model->cabang_id = $_POST['cabang_id'];
            $model->last_update_id = Yii::app()->user->id;
            $model->last_update = new CDbExpression('NOW()');
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pegawai_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->pegawai_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Pegawai')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionCreatePegawaiforUser(){
        $msg    = "Data gagal disimpan";
        $status = false;
        try {
            $store               = Cabang::model()->findByAttributes(array('cabang_id' => $_POST['cabang_id']));

            $model                      = new Pegawai;
            $model->nik                 = $_POST['nama'];
            $model->nama_lengkap        = $_POST['nama'];
            $model->cabang_id           = $_POST['cabang_id'];
            $model->tgl_masuk           = date('Y-m-d');
            $model->store               = $store->kode_cabang;
            $model->status_pegawai_id   = get_status_pegawai_resign(); //RESIGN
            if (!$model->save()){
                throw new Exception($msg . CHtml::errorSummary($model));
            }

            $msg    = "Data berhasil disimpan";
            $status = true;
        } catch (Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success'       => $status,
            'msg'           => $msg,
            'pegawai_id'    => $model->pegawai_id,
            'nik'           => $model->nik,
            'nama_lengkap'  => $model->nama_lengkap
        ));
    }

    public function actionIndex() {
        if(isset($_POST['all_pegawai']))
            $db = Pegawai::IndexPegawai();
        else
            $db = Pegawai::IndexPegawai($_POST['bu_id']);

        $cabang = Cabang::model()->checkCabang();

        if (isset($_POST['nik'])) {
            $db->addCondition('nik like :nik')
                ->addParam(":nik",'%' . $_POST['nik'] . '%');
        }
        if (isset($_POST['nama_lengkap'])) {
            $db->addCondition('nama_lengkap like :nama_lengkap')
                ->addParam(":nama_lengkap",'%' . $_POST['nama_lengkap'] . '%');
        }
        if (isset($_POST['kode_cabang'])) {
            $db->addCondition('kode_cabang like :kode_cabang')
                ->addParam(":kode_cabang",'%' . $_POST['kode_cabang'] . '%');
        }
        if (isset($_POST['cabang_id'])) {
            $db->addCondition('pp.cabang_id = :cabang_id')
                ->addParam(":cabang_id", $_POST['cabang_id']);
        }
        if (isset($_POST['query'])) {
            if($this->getSecuriyRolesId() == GET_SECURITY_ROLE_ADMINISTRATOR()) {
                $db->clearCondition()->clearParam()->addInCondition("status_pegawai_id", get_status_pegawai_aktif());
            }

            $db->addCondition("(nik like :nik OR nama_lengkap like :nama_lengkap OR pp.pegawai_id like :pegawai_id)");
            $db->addParams([
                ':nik' => "%" . $_POST['query'] . "%",
                ':nama_lengkap' => "%" . $_POST['query'] . "%",
                ':pegawai_id' => "%" . $_POST['query'] . "%"
            ]);
        }
        if (isset($_POST['status_pegawai']) && $_POST['status_pegawai'] == 'nonAktif') {
            if (isset($_POST['kode'])) {
                $db->addCondition('sp.kode like :kode')
                    ->addParam(':kode', '%'.$_POST['kode'].'%');
            }

            $db->addLeftJoin("{{status_pegawai}} sp", "sp.status_pegawai_id = pp.status_pegawai_id")
                ->addSelect("sp.kode")
                ->addInCondition("sp.status_pegawai_id", get_status_pegawai_non_aktif())
            ;
        } else {
            if (isset($_POST['query'])) {

            } else
                $db->addInCondition("status_pegawai_id", get_status_pegawai_aktif())
            ;
        }

        $count = $db->queryCount();

        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            if($this->getSecuriyRolesId() !== GET_SECURITY_ROLE_ADMINISTRATOR() && $this->getSecuriyRolesId() !== GET_SECURITY_ROLE_HRD()) {
                $db->addCondition("store = '$cabang'");
                $count = $db->queryCount();
            }

            $db->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $x = $db->getQuery();
        $model = $db->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }

    public function actionCheckUser(){
        $pegawaipost_id = $_POST['pegawai_id'];
        $id = Yii::app()->user->getId();
        $pegawai_id = Users::model()->findByAttributes(array('id' => $id))->pegawai_id;
        if($pegawaipost_id == $pegawai_id){
            echo CJSON::encode(array(
                'success' => true,
                'msg' => true));
            Yii::app()->end();
        } else {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => false));
            Yii::app()->end();
        }
    }
    public function actionKelompokPegawai()
    {
        $model = Pegawai::model()->findByPk($_POST['pegawai_id']);
        if($model->kelompok_pegawai == 0){
           $model->kelompok_pegawai = 1;
        } else {
            $model->kelompok_pegawai = 0;
        }
        $html = '';
        if (!$model->save()) {
            $html .= CHtml::errorSummary($model);
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }

    // sync pegawai to nwis get non aktif employee

    public function actionSinkron()
    {
        $html = "";

        $security_roles = self::getSecuriyRolesId();
        if ($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()) {
            $output = Api::sinkronEDM(['action'=> 'GetAllFromAmos', 'id' => $_POST['bu_id']]);
        } else {
            $cabangid   = Cabang::model()->checkCabangId();
            $output = Api::sinkronEDM(['action'=> 'GetAllFromAmosCabang', 'id' => $cabangid]);
        }

        $decode = json_decode(json_encode($output), true);

        $oldJabatan = Jabatan::model()->findAllByAttributes(['bu_id' => $_POST['bu_id']]);
        if(count($decode['results']['jabatan']) != count($oldJabatan)) {
            Jabatan::model()->deleteAllByAttributes(['bu_id' => $_POST['bu_id']]);
            Jabatan::model()->deleteAllByAttributes(['bu_id' => '']);
        }
        foreach ($decode['results']['jabatan'] as $result) {
            $model = Jabatan::model()->findByPk($result['jabatan_id']);
            if ($model == null) {
                $model = new Jabatan();
            }
            $model->setAttributes($result);
            $model->kode       = $result['kode_jabatan'];
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }

        $oldLevel = Leveling::model()->findAllByAttributes(['bu_id' => $_POST['bu_id']]);
        if(count($decode['results']['level']) != count($oldLevel)) {
            Leveling::model()->deleteAllByAttributes(['bu_id' => $_POST['bu_id']]);
            Leveling::model()->deleteAllByAttributes(['bu_id' => '']);
        }
        foreach ($decode['results']['level'] as $result) {
            $model = Leveling::model()->findByPk($result['lvljabatan_id']);
            if ($model == null) {
                $model = new Leveling();
            }
            $model->leveling_id = $result['lvljabatan_id'];
            $model->kode        = $result['urutan_romawi'];
            $model->nama        = $result['nama_level'];
            $model->bu_id       = $result['bu_id'];
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }

        $oldGolongan = Golongan::model()->findAllByAttributes(['bu_id' => $_POST['bu_id']]);
        if(count($decode['results']['golongan']) != count($oldGolongan)) {
            Golongan::model()->deleteAllByAttributes(['bu_id' => $_POST['bu_id']]);
            Golongan::model()->deleteAllByAttributes(['bu_id' => '']);
        }
        foreach ($decode['results']['golongan'] as $result) {
            $model = Golongan::model()->findByPk($result['lvl2jabatan_id']);
            if ($model == null) {
                $model = new Golongan();
            }
            $model->golongan_id = $result['lvl2jabatan_id'];
            $model->kode        = $result['urutan_alps'];
            $model->nama        = $result['nama'];
            $model->bu_id       = $result['bu_id'];
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }

        /*
         * UPDATE semua status_pegawai_id jadi RESIGN
         */
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $sp_resign = '6e772fd4-773c-11e7-9b95-68f7286688e6';
            if ($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()) {
                $stringCabang = $this->makeStringCabangFromBuId($_POST['bu_id']);
                Pegawai::model()->updateAll(['status_pegawai_id' => $sp_resign], 'cabang_id in (:cabang_id)', [':cabang_id' => $stringCabang]);
            } else {
                $cabangid   = Cabang::model()->checkCabangId();
                Pegawai::model()->updateAll(['status_pegawai_id' => $sp_resign], 'cabang_id in (:cabang_id)', [':cabang_id' => $cabangid]);
            }
            foreach ($decode['results']['pegawai'] as $result) {
                $model = Pegawai::model()->findByPk($result['pegawai_id']);
                if ($model == null) {
                    $model = new Pegawai;
                }
                $model->setAttributes($result);

                if(Cabang::isCabangHolding($model->cabang_id))
                    $model->nik = $model->nik_lokal;

                if ($result['pt_name'] == 'PESONA NATASHA GEMILANG') {
                    $model->nik = substr($result['nik_display'], 2);
                }

                $model->store       = Cabang::model()->findByPk($result['cabang_id'])->kode_cabang;
                $model->leveling_id = $result['lvljabatan_id'];
                $model->golongan_id = $result['lvl2jabatan_id'];

                $model->tgl_cabang                 = $result['cabang_start'] == null || $result['cabang_start'] == '' ? $result['tgl_masuk'] : $result['cabang_start'] ;
                $model->tgl_level_golongan_jabatan = $result['divisi_jabatan_start'] == null || $result['divisi_jabatan_start'] == '' ? $result['tgl_masuk'] : $result['divisi_jabatan_start'] ;
                $model->tgl_status_pegawai         = $result['status_pegawai_start'] == null || $result['status_pegawai_start'] == '' ? $result['tgl_masuk'] : $result['status_pegawai_start'] ;
                if (!$model->save()) {
                    $html .= $model->nama_lengkap . ' | ' . CHtml::errorSummary($model) ;
                }
            }

            $status = true;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }

        // sinkron non aktif employe ke NWIS
        if(SysPrefs::get_val('POS_URL_API') != 'false') {
            $sinkNWis = self::sinkronEmployeeNwis($_POST['bu_id']);
            if ($sinkNWis['success'] === false) {
                $html .= $sinkNWis['msg'];
            }
        }

        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
    private function sinkronEmployeeNwis($bu_id = "")
    {
        $msg     = "";
        $success = true;
        $db      = Pegawai::IndexPegawai($bu_id);
        $cabang  = Cabang::model()->checkCabang();

        $db->addLeftJoin("{{status_pegawai}} sp", "sp.status_pegawai_id = pp.status_pegawai_id")
        ->addSelect("sp.kode")
        ->addInCondition("sp.status_pegawai_id", get_status_pegawai_non_aktif());

        $db->addCondition("store != 'KP'");
        if($this->getSecuriyRolesId() !== GET_SECURITY_ROLE_ADMINISTRATOR() && $this->getSecuriyRolesId() !== GET_SECURITY_ROLE_HRD()) {
            $db->addCondition("store = '$cabang'");
            $count = $db->queryCount();
        }

        $output = $db->queryAll(true);

        try {
            if (count($output)> 0) {
                # code...
                foreach ($output as $result) {
             
                    $token = CurlHelper::requestToken();
              
                    if (isset($token->token)) {
                        $output = CurlHelper::SyncEmployeeFromAmos($result['nik'],$result['kode_cabang'],$token->token);
                        if (isset($output)) {
                            $success = true;
                        } else {
                         continue;
                        }
                    } else {
                        continue;
                    }
                    
                }
            }

        } catch (Exception $ex) {
            $success = false;
            $msg = $ex->getMessage();
        }
        
        return ['success' => $success,'msg' => $msg];
    }
}
