<?php

class PegawaiSpesialController extends GxController
{

    public function actionCreate()
    {
        $model = new PegawaiSpesial;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PegawaiSpesial'][$k] = $v;
            }
            $model->attributes = $_POST['PegawaiSpesial'];
            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pegawai_spesial_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();

        }

    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PegawaiSpesial');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PegawaiSpesial'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PegawaiSpesial'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pegawai_spesial_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->pegawai_spesial_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PegawaiSpesial')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex()
    {
        $dbcmd = DbCmd::instance()->addFrom('{{pegawai_spesial}} s')
            ->addSelect("s.*, p.nik, p.nama_lengkap, concat(p.nik,' ',p.nama_lengkap) pin_nama")
            ->addLeftJoin('{{pegawai}} p', 'p.pegawai_id = s.pegawai_id')
            ->addOrder('p.nik asc');

        if (isset($_POST['nik'])){
            $dbcmd->addCondition('nik like :nik');
            $dbcmd->addParam(':nik', '%'.$_POST['nik'].'%');
        }
        if (isset($_POST['nama_lengkap'])){
            $dbcmd->addCondition('nama_lengkap like :nama_lengkap');
            $dbcmd->addParam(':nama_lengkap', '%'.$_POST['nama_lengkap'].'%');
        }

        $count = $dbcmd->queryCount();
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $model = $dbcmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);

    }

    public function actionTarikData() {
        $status = true;
        $msg = 'Data berhasil ditarik.';
        $arr2 = [];
        $periode = Periode::model()->findByPk($_POST['periode_id']);
        if(isset($_POST['pegawai_id']) && $_POST['pegawai_id'] != '') {
            $peg = PegawaiSpesial::model()->findByAttributes([
                'pegawai_id' => $_POST['pegawai_id']
            ]);
            $p = Pegawai::model()->findByPk($_POST['pegawai_id']);
            $nik = $p->nik;
            $nik = $peg->nik_lain;
            $arr2[0]['nik_lain'] = $peg->nik_lain;
            $arr2[0]['nik'] = $p->nik;
            $arr2[0]['cabang_lain'] = $peg->cabang;
            $arr2[0]['cabang'] = $p->store;
            $arr2[0]['cabang_id'] = $p->cabang_id;
            $arr2[0]['bu_kode'] = $peg->bu_kode;
        } else {
            $pegawai = PegawaiSpesial::model()->findAll();
            $i = 0;
            foreach ($pegawai as $peg){
                $p = Pegawai::model()->findByPk($peg->pegawai_id);
                $nik = $peg->nik_lain;
                $arr2[$i]['nik_lain'] = $peg->nik_lain;
                $arr2[$i]['nik'] = $p->nik;
                $arr2[$i]['cabang_lain'] = $peg->cabang;
                $arr2[$i]['cabang'] = $p->store;
                $arr2[$i]['cabang_id'] = $p->cabang_id;
                $arr2[$i]['bu_kode'] = $peg->bu_kode;
                $i++;
            }
        }

        foreach ($arr2 as $array) {
            $splitbu = explode (";", $array['bu_kode']);
            foreach ($splitbu as $sb) {
                $amosAddress = AmosAddress::model()->findByAttributes(['bu_kode' => $sb]);
                if($amosAddress == null)
                    Log::generateErrorMessage(false,$sb . ' tidak tedaftar di Alamat AMOS');

                $output = Yii::app()->curl->post($amosAddress->amos_address . '/api/GetFpFromDb',
                    http_build_query([
                        'nik'       => $array['nik_lain'] ? $array['nik_lain'] : $array['nik'],
                        'cabang'    => $array['cabang_lain'] ? $array['cabang_lain'] : $amosAddress->kode_cabang,
                        'pstart'    => $periode->periode_start,
                        'pend'      => $periode->periode_end
                    ])
                );

                $decode = json_decode($output, true);
                foreach ($decode['results'] as $abs) {
                    if(Fp::model()->findByPk($abs['fp_id'])) {
                        $model = Fp::model()->findByPk($abs['fp_id']);
                    } else {
                        $model = new Fp;
                    }
                    foreach ($abs as $k => $v) {
                        if (is_angka($v)) $v = get_number($v);
                        $abs['Fp'][$k] = $v;
                    }
                    $model->attributes = $abs['Fp'];
                    $model->PIN = $array['nik'];
                    $model->status_int = 0;
                    $model->kode_ket = 0;
                    $model->tipe_data = 0;
                    $model->status_int = 0;
                    $model->cabang = $array['cabang'];
                    $model->cabang_id = $array['cabang_id'];
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Fp')) . CHtml::errorSummary($model));
                    }
                }
            }
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

}