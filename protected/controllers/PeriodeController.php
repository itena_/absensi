<?php
class PeriodeController extends GxController
{
    public function actionCreate()
    {
        $model = new Periode;
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $dataPeriode = [];
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
//                if($k == 'periode_start' || $k == 'periode_end'){
////                    $date = date_create_from_format('d-m-Y H:i:s', $v);
//                    $v = date_format($v, "Y-m-d H:i:s");
//                }
                $dataPeriode[$k] = $v;
            }
            $model->attributes = $dataPeriode;
            $msg = "";
            if ($model->save()) {
                $status = true;
                $msg = "Data Periode berhasil disimpan";
            } else {
                $msg = "Data Periode gagal disimpan. " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Periode');
        if (isset($_POST) && !empty($_POST)) {
            $dataPeriode = [];
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                if ($k == 'periode_start' || $k == 'periode_end') {
                    $date = date_create_from_format('Y-m-d H:i:s', $v);
                    $v = date_format($date, "Y-m-d H:i:s");
                }
                $dataPeriode[$k] = $v;
            }
            $model->attributes = $dataPeriode;
            $msg = "";
            if ($model->save()) {
                $status = true;
                $msg = "Perubahan data berhasil disimpan";
            } else {
                $msg = "Perubahan data gagal disimpan " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->periode_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Periode')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->alias = 'p';
        $criteria->join = '
            INNER JOIN pbu_jenis_periode AS pjp ON p.jenis_periode_id = pjp.jenis_periode_id
        ';
        $param = array();
        if (isset($_POST['bu_id']) && $_POST['bu_id'] != null) {
            $criteria->addCondition('pjp.bu_id = :bu_id');
            $param[':bu_id'] = $_POST['bu_id'];
        }
        if (isset($_POST['periode_id']) && $_POST['periode_id'] != null) {
            $criteria->addCondition('p.periode_id = :periode_id');
            $param[':periode_id'] = $_POST['periode_id'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = array();
        if (isset($_POST['fieldsearch']) && isset($_POST['valuesearch']) && $_POST['valuesearch'] != "") {
            $criteria->addCondition($_POST['fieldsearch'] . " LIKE '%" . $_POST['valuesearch'] . "%'");
        }
        $criteria->order = 'periode_start desc';
        $criteria->params = $param;
        $model = Periode::model()->findAll($criteria);
        $total = Periode::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    /*
    public function actionGetTahunCuti()
    {
        $now = new DateTime();
        $year_now =$now->format('Y');
        $year_prev = $year_now - 1;
        $model = [
            ['cuti_tahun' => $year_prev],
            ['cuti_tahun' => $year_now]
        ];
        $this->renderJsonArr($model);
    }
    */
}