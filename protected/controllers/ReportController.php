<?php
Yii::import('application.components.U');
Yii::import("application.components.tbs_class", true);
Yii::import("application.components.tbs_plugin_excel", true);
Yii::import('application.components.phpExcelReportHelper', true);
class ReportController extends GxController
{
    private $TBS;
    private $logo;
    private $format;
    public $is_excel;
    public function init()
    {
        parent::init();
        if (!isset($_POST) || empty($_POST)) {
            $this->redirect(url('/'));
        }
        $this->logo = bu() . "/images/bianti2.png";
        $this->TBS = new clsTinyButStrong;
        $this->layout = "report";
        $this->format = $_POST['format'];
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
            $this->is_excel = true;
        }
        error_reporting(E_ERROR);
    }

    public function actionAbsen()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $awal           = isset($_POST['awal']) ? $_POST['awal'] : null;
            $akhir          = isset($_POST['akhir']) ? $_POST['akhir'] : null;
            $periode_id     = $_POST['periode_id'];
            $cabang         = $_POST['cabang_id'];
            $show_detail    = $_POST['show_detail'];
            $urut_jabatan   = $_POST['urut_jabatan'];
            $jatah_off      = $_POST['jatah_off'];
            $periode        = Periode::model()->findByPk($periode_id);
            $kode_periode   = $periode->kode_periode;
            $mutasi         = U::report_absen($periode_id,$cabang,$show_detail,$urut_jabatan, $awal, $akhir);
            $kode_cabang    = Cabang::model()->findByPk($cabang)->kode_cabang;

            $libur = U::makeLikeQuery($periode->libur, 'in_time');
            $i = 0;
            foreach ($mutasi as $m) {
                if($m['nik'] == '10100052') {
                    $auw = 1;
                }

                $jatahOff = Validasi::hitungJatahOffNew($libur, $m['pegawai_id'], $periode_id, $cabang, $show_detail);
                $mutasi[$i]['jatah_off'] = $jatahOff;

                $gantiOff = Validasi::hitungGantiOff($jatahOff, $m['lemburh'], $m['OFF'], $m['S']);
                $mutasi[$i]['ganti_off'] = $gantiOff;
                $i++;
            }

            $dataProvider   = new CArrayDataProvider($mutasi, array(
                'id' => 'Absensi',
                'pagination' => false
            ));
            $file = GET_USE_STATUS_HK() == true ? 'AbsensiHk' : 'Absensi';
            /*if($jatah_off) {
                $auw->save();
            }*/

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Presensi-$kode_periode-$kode_cabang.xls");
                echo $this->render($file, array(
                    'dp'            => $dataProvider,
                    'periode_id'    => $periode,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang,
                    'jatah_off'     => $jatah_off,
                    'title'         => 'Presensi',
                    'pageTitle'     => 'Laporan Presensi'
                ), true);
            } else {
                $this->render($file, array(
                    'dp'            => $dataProvider,
                    'periode_id'    => $periode,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang,
                    'jatah_off'     => $jatah_off,
                    'title'         => 'Presensi',
                    'pageTitle'     => 'Laporan Presensi'
                ));
            }
        }
    }
    public function actionAbsenjam()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode        = $_POST['periode_id'];
            $cabang         = $_POST['cabang_id'];
            $show_detail    = $_POST['show_detail'];
            $urut_jabatan   = $_POST['urut_jabatan'];
            $kode_periode   = Yii::app()->db->createCommand(
                "SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'")
                ->queryScalar();
            $mutasi         = U::report_absenjam($periode,$cabang,$show_detail,$urut_jabatan);
            $kode_cabang    = Cabang::model()->findByPk($cabang)->kode_cabang;
            $dataProvider   = new CArrayDataProvider($mutasi, array(
                'id' => 'Absenjam',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Absen_jam-$kode_periode-$kode_cabang.xls");
                echo $this->render('Absensijam', array(
                    'dp'            => $dataProvider,
                    'periode_id'    => $periode,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang,
                    'title'         => 'Presensi Jam',
                    'pageTitle'     => 'Laporan Presensi Jam'
                ), true);
            } else {
                $this->render('Absensijam', array(
                    'dp'            => $dataProvider,
                    'periode_id'    => $periode,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang,
                    'title'         => 'Presensi Jam',
                    'pageTitle'     => 'Laporan Presensi Jam'
                ));
            }
        }
    }
    public function actionAbsenAll()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode        = $_POST['periode_id'];
            $kode_periode   = Yii::app()->db->createCommand(
                "SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'")
                ->queryScalar();
            $mutasi         = U::report_absen_all($periode);
            $dataProvider   = new CArrayDataProvider($mutasi, array(
                'id' => 'AbsensiAll',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=PresensiAll-$kode_periode.xls");
                echo $this->render('AbsensiHk', array(
                    'dp'            => $dataProvider,
                    'periode_id'    => $periode,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => 'ALL',
                    'title'         => 'Presensi All',
                    'pageTitle'     => 'Laporan Presensi All'
                ), true);
            } else {
                $this->render('AbsensiHk', array(
                    'dp'            => $dataProvider,
                    'periode_id'    => $periode,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => 'ALL',
                    'title'         => 'Presensi All',
                    'pageTitle'     => 'Laporan Presensi All'
                ));
            }
        }
    }
    public function actionRekapAbsensi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periodeStart   = Yii::app()->db->createCommand("SELECT DATE_FORMAT(periode_start,'%Y-%m-%d') FROM pbu_periode where periode_id = '" . $_POST['periode_id'] . "'")->queryScalar();
            $periodeEnd     = Yii::app()->db->createCommand("SELECT DATE_FORMAT(periode_end,'%Y-%m-%d') FROM pbu_periode where periode_id = '" . $_POST['periode_id'] . "'")->queryScalar();
            $kode_periode   = Periode::model()->findByPk($_POST['periode_id'])->kode_periode;
            $kode_cabang    = Cabang::model()->findByPk($_POST['cabang_id'])->kode_cabang;
            $id_cabang      = $_POST['cabang_id'];
            $pegawai        = Yii::app()->db->createCommand(
                "select nik, nama_lengkap from pbu_pegawai where store = :kode_cabang order by nik"
            )->queryAll(true,[':kode_cabang' => $kode_cabang]);
            $query          = Periode::getAllDate($periodeStart, $periodeEnd, ",DATE_FORMAT(adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i),'%d') tgl")
                                ->getQuery();
            $tgl            = Yii::app()->db->createCommand($query)->queryAll(true);
            $periode        = Yii::app()->db->createCommand("
            select t2.*, 
            CASE
                WHEN kode_ket = 0 THEN 1
                WHEN kode_ket = 1 THEN 'LK'
                WHEN kode_ket = 2 THEN 'S'
                WHEN kode_ket = 3 THEN 1
                WHEN kode_ket = 4 THEN 0
                WHEN kode_ket = 10 THEN 1
                WHEN kode_ket is null THEN '-'
                ELSE 'C'
            END masuk
            from
            (
                    SELECT
                    t1.*, p.nama_lengkap, p.nik
                    FROM
                    (
                        $query
                    ) t1,
                    pbu_pegawai p
            ) t2
            LEFT JOIN pbu_validasi_view v on v.PIN = t2.nik 
            WHERE DATE_FORMAT(v.in_time, '%Y-%m-%d') = t2.selected_date AND v.cabang_id = :id_cabang 
            order by t2.nik, t2.selected_date
            ")->queryAll(true,[':id_cabang' => $id_cabang]);
            $data           = array();

            $cmasuk = $csakit = $clk = $coff = $ccuti = 1;

            foreach($pegawai as $p){
                $row                 = [];
                $row['nik']          = $p['nik'];
                $row['nama_lengkap'] = $p['nama_lengkap'];
                foreach($tgl as $t){
                    foreach($periode as $d){
                        if($d['nik'] == $p['nik'] && $d['tgl'] == $t['tgl']){
                            $row[$t['tgl']] = $d['masuk'];
                            switch ($d['masuk']){
                                case '0' : $row['coff'] = $coff++; break;
                                case '1' : $row['cmasuk'] = $cmasuk++; break;
                                case 'LK' : $row['clk'] = $clk++; break;
                                case 'S' : $row['csakit'] = $csakit++; break;
                                case 'C' : $row['ccuti'] = $ccuti++; break;
                            }
                            if ($coff <= 1){$row['coff'] = '-';}
                            if ($cmasuk <= 1){$row['cmasuk'] = '-';}
                            if ($clk <= 1){$row['clk'] = '-';}
                            if ($csakit <= 1){$row['csakit'] = '-';}
                            if ($ccuti <= 1){$row['ccuti'] = '-';}
                            break;
                        }
                    }
                }
                $cmasuk = $csakit = $clk = $coff = $ccuti = 1;
                $data[] = $row;
            }

            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'RekapAbsensi',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapPresensi-$kode_periode-$kode_cabang.xls");
                echo $this->render('RekapAbsensi', array(
                    'dp'            => $dataProvider,
                    'tgl'           => $tgl,
                    'from'          => sql2date($periodeStart, 'dd MMM yyyy'),
                    'to'            => sql2date($periodeEnd, 'dd MMM yyyy'),
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang
                ), true);
            } else {
                $this->render('RekapAbsensi', array(
                    'dp'            => $dataProvider,
                    'tgl'           => $tgl,
                    'from'          => sql2date($periodeStart, 'dd MMM yyyy'),
                    'to'            => sql2date($periodeEnd, 'dd MMM yyyy'),
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang
                ));
            }
        }
    }
    public function actionCheckAbsensi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode        = $_POST['periode_id'];
            $cabang         = $_POST['cabang_id'];
            $kode_periode   = Yii::app()->db->createCommand(
                "SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'")
                ->queryScalar();
            $mutasi         = U::report_check_absen($periode,$cabang);
            $kode_cabang    = Cabang::model()->findByPk($cabang)->kode_cabang;
            $dataProvider   = new CArrayDataProvider($mutasi, array(
                'id' => 'CheckAbsensi',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=CheckAbsensi-$kode_periode-$kode_cabang.xls");
                echo $this->render('CheckAbsensi', array(
                    'dp' => $dataProvider,
                    'periode_id' => $periode,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang
                ), true);
            } else {
                $this->render('CheckAbsensi', array(
                    'dp' => $dataProvider,
                    'periode_id' => $periode,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang
                ));
            }
        }
    }

    public function actionFp()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode = $_POST['periode_id'];
            $pegawai = $_POST['pegawai_id'];
            $cabang = $_POST['cabang_id'];
            $kode_periode = Yii::app()->db->createCommand(
                "SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'")
                ->queryScalar();
            $nama = Yii::app()->db->createCommand(
                "SELECT nama_lengkap FROM pbu_pegawai WHERE pegawai_id = '" . $pegawai . "'")
                ->queryScalar();
            if(!$nama){
                $nama = 'ALL';
            }
            IF(!$cabang){
                $kode_cabang = 'ALL';
            } else {
                $kode_cabang = Cabang::model()->findByPk($cabang)->kode_cabang;                
            }
            
            $mutasi = U::report_fp($periode,$pegawai,$cabang);
            
            $total_lt = array_sum(array_column($mutasi, 'LT'));
            $total_lbr1 = array_sum(array_column($mutasi, 'lembur1'));
            $total_lbr2 = array_sum(array_column($mutasi, 'lembur2'));
            $total_lbrh = array_sum(array_column($mutasi, 'lemburh'));
            $total_jam_kerja_second = array_sum(array_column($mutasi, 'jam_kerja_hitung_in_second'));
            $total_jam_kerja_time = Fp::model()->time_from_seconds($total_jam_kerja_second);            
            //$array_total_hari_kerja = array_sum(array_column($mutasi, 'jam_kerja'));
            
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'Fp',
                'pagination' => false
            ));   
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Fp-$kode_periode-$kode_cabang.xls");
                echo $this->render('Fp', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang,
                    'total_jam_kerja_time' => $total_jam_kerja_time,                  
                    'total_lt' => $total_lt,                
                    'total_lbr1' => $total_lbr1,                
                    'total_lbr2' => $total_lbr2,                
                    'total_lbr3' => $total_lbrh                
                ), true);
            } else {
                $this->render('Fp', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang,
                    'total_jam_kerja_time' => $total_jam_kerja_time,                  
                    'total_lt' => $total_lt,
                    'total_lbr1' => $total_lbr1,                
                    'total_lbr2' => $total_lbr2,                
                    'total_lbr3' => $total_lbrh                   
                ));
            }
        }
    }
    public function actionFpReal()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode        = $_POST['periode_id'];
            $cabang_id      = $_POST['cabang_id'];
            $kode_periode   = Periode::model()->findByPk($periode)->kode_periode;

            $pegawai_id     = '';
            $nama           = null;
            if(isset($_POST['pegawai_id'])) {
                $pegawai_id = $_POST['pegawai_id'];
                $nama       = Pegawai::model()->findByPk($pegawai_id)->nama_lengkap;
            }

            if(!$nama){
                $nama = 'ALL';
            }
            IF(!$cabang_id){
                $kode_cabang = 'ALL';
            } else {
                $kode_cabang = Cabang::model()->findByPk($cabang_id)->kode_cabang;
            }
            
            $mutasi = U::report_fp_real($periode,$pegawai_id,$cabang_id);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'FpReal',
                'pagination' => false
            ));   
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=FpReal-$kode_periode-$kode_cabang.xls");
                echo $this->render('FpReal', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang              
                ), true);
            } else {
                $this->render('FpReal', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang  
                ));
            }
        }
    }
    public function actionFpMix()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode        = $_POST['periode_id'];
            $cabang_id      = $_POST['cabang_id'];
            $kode_periode   = Periode::model()->findByPk($periode)->kode_periode;

            $pegawai_id     = '';
            $nama           = null;
            if(isset($_POST['pegawai_id'])) {
                $pegawai_id = $_POST['pegawai_id'];
                $nama       = Pegawai::model()->findByPk($pegawai_id)->nama_lengkap;
            }

            if(!$nama){
                $nama = 'ALL';
            }
            IF(!$cabang_id){
                $kode_cabang = 'ALL';
            } else {
                $kode_cabang = Cabang::model()->findByPk($cabang_id)->kode_cabang;
            }

            $cmd = U::report_fp_mix($periode,$pegawai_id, $_POST['bu_id']);
            $mutasi = $cmd->getQuery();
            $mutasi = $cmd->queryAll();

            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'FpMix',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=FpMix-$kode_periode-$kode_cabang.xls");
                echo $this->render('FpMix', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang
                ), true);
            } else {
                $this->render('FpMix', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang
                ));
            }
        }
    }
    public function actionFpValidasi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode = $_POST['periode_id'];
            $pegawai = $_POST['pegawai_id'];
            $cabang = $_POST['cabang_id'];
            $kode_periode = Yii::app()->db->createCommand(
                "SELECT kode_periode FROM pbu_periode WHERE periode_id = :periode")
                ->queryScalar([':periode' => $periode]);
            $nama = Yii::app()->db->createCommand(
                "SELECT nama_lengkap FROM pbu_pegawai WHERE pegawai_id = :pegawai")
                ->queryScalar([':pegawai' => $pegawai]);
            if(!$nama){
                $nama = 'ALL';
            }
            IF(!$cabang){
                $kode_cabang = 'ALL';
            } else {
                $kode_cabang = Cabang::model()->findByPk($cabang)->kode_cabang;
            }
            $mutasi = U::report_fp_validasi($periode,$pegawai,$cabang);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'FpValidasi',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=FpValidasi-$kode_periode-$kode_cabang.xls");
                echo $this->render('FpValidasi', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang
                ), true);
            } else {
                $this->render('FpValidasi', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang
                ));
            }
        }
    }

    public function actionShift()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $cabang = $_POST['cabang_id'];
            $aktif  = $_POST['aktif'];
            $mutasi = U::report_shift($cabang, $aktif);
            $kode_cabang = Cabang::model()->findByPk($cabang)->kode_cabang;
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'Shift',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Shift-$kode_cabang.xls");
                echo $this->render('Shift', array(
                    'dp' => $dataProvider,
                    'kode_cabang' => $kode_cabang
                ), true);
            } else {
                $this->render('Shift', array(
                    'dp' => $dataProvider,
                    'kode_cabang' => $kode_cabang
                ));
            }
        }
    }
	public function actionExportShift()
	{
		$objReader = PHPExcel_IOFactory::createReader( 'Excel5' );
		$excel     = $objReader->load( Yii::getPathOfAlias( 'application.views.reports' ) . DIRECTORY_SEPARATOR . 'TIMESHEET.xls' );

		if (Yii::app()->request->isAjaxRequest) {
			return;
		}
		if (isset($_POST) && !empty($_POST)) {
			$cabang = $_POST['cabang_id'];

            $dbperiode      = Periode::getPeriodeStartEnd($_POST['periode_id']);
            $periodenow     = $dbperiode->queryRow();
            $periodeStart   = $periodenow['ps'];
            $periodeEnd     = $periodenow['pe'];
            $kode_periode   = $periodenow['kode_periode'];
            $kode_cabang    = Cabang::model()->findByPk($_POST['cabang_id'])->kode_cabang;

			$data = Yii::app()->db->createCommand("
			SELECT v.PIN as nik , p.nama_lengkap, DATE_FORMAT(v.in_time, '%d-%m-%Y') as tanggal, 
				if(v.kode_ket=0, s.kode_shift, k.nama_ket) as kode_shift, 
				if(v.kode_ket=0, s.in_time, '-') as shift_in,
				if(v.kode_ket=0, s.out_time, '-') as shift_out,
				if(v.kode_ket=0, time(v.in_time), '-') as masuk,
				if(v.kode_ket=0, time(v.out_time), '-') as  keluar
			from pbu_validasi as v
			inner join pbu_shift as s on v.shift_id=s.shift_id
			inner join pbu_cabang as c on v.cabang_id=c.cabang_id
			inner join pbu_pegawai as p on p.pegawai_id=v.pegawai_id
			left join pbu_keterangan as k on k.keterangan_id=v.kode_ket
			where v.cabang_id=:cabang and v.in_time>=:awal and v.out_time<=:akhir
				and v.status_int=1	
			order by v.pin, date(v.in_time)
			")->queryAll(true, [
				':awal' => $periodeStart,
				':akhir' => $periodeEnd,
				':cabang' => $cabang]);


			$dataProvider = new CArrayDataProvider($data, array(
				'id' => 'PresensiShift',
				'pagination' => false
			));
			$this->render('PresensiShift', array(
				'dp' => $dataProvider,
				'kode_periode' => $kode_periode,
				'kode_cabang' => $kode_cabang
			));
		}
	}

    public function actionPrintRekapPegawai()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $mutasi = U::report_rekap_pegawai();
            if ($this->format == 'excel') {
                $columnData = array(
                    array(
                        'A' => '#',
                        'B' => 'tgl_masuk',
                        'C' => 'nik',
                        'D' => 'nama_lengkap',
                        'E' => 'kode_golongan',
                        'F' => 'nama_jabatan',
                        'G' => 'email',
                        'H' => 'npwp',
                        'I' => 'kode_status',
                        'J' => 'bank_nama',
                        'K' => 'bank_kota',
                        'L' => 'rekening',
                        'M' => 'gp',
                        'N' => 'tj',
                        'O' => 'um',
                        'P' => 'ut'
                    )
                );
                $rowc = count($mutasi) + 5;
                $field = array(
                    'A1' => 'Rekap Pegawai ' . Yii::app()->params['nama_perusahaan'],
                    "M$rowc" => $total_income = array_sum(array_column($mutasi, 'gp')),
                    "N$rowc" => $total_income = array_sum(array_column($mutasi, 'tj'))
                );
                $report = new PhpExcelReportHelper();
                $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') .
                    DIRECTORY_SEPARATOR . 'rekap_pegawai.xlsx')
                    ->setActiveSheetIndex(0)
                    ->mergeBlock($mutasi, $columnData, 5, 1)
                    ->mergeField($field)
                    //->protectSheet()
                    ->downloadExcel('Rekap Pegawai ' . Yii::app()->params['nama_perusahaan']);
            } else {
                $dataProvider = new CArrayDataProvider($mutasi, array(
                    'id' => 'RekapPegawai',
                    'pagination' => false
                ));
                $this->render('RekapPegawai', array(
                    'dp' => $dataProvider
                ));
            }
        }
    }
    public function actionKpi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from         = $_POST['tglfrom'];
            $to           = $_POST['tglto'];
            $periode = $_POST['periode_id'];
            $pegawai = $_POST['pegawai_id'];
            $cabang = $_POST['cabang_id'];
            $kode_periode = Yii::app()->db->createCommand(
                "SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'")
                ->queryScalar();
            $nama = Yii::app()->db->createCommand(
                "SELECT nama_lengkap FROM pbu_pegawai WHERE pegawai_id = '" . $pegawai . "'")
                ->queryScalar();
            if(!$nama){
                $nama = 'ALL';
            }
            IF(!$cabang){
                $kode_cabang = 'ALL';
            } else {
                $kode_cabang = Cabang::model()->findByPk($cabang)->kode_cabang;
            }
            $i=0;
            $mutasi = U::report_kpi($from,$to,$periode,$pegawai,$cabang);

            $total_lt = array_sum(array_column($mutasi, 'LT'));
            $total_lbr1 = array_sum(array_column($mutasi, 'lembur1'));
            $total_lbr2 = array_sum(array_column($mutasi, 'lembur2'));
            $total_lbrh = array_sum(array_column($mutasi, 'lemburh'));
            $total_jam_kerja_second = array_sum(array_column($mutasi, 'jam_kerja_hitung_in_second'));
            $total_jam_kerja_time = Fp::model()->time_from_seconds($total_jam_kerja_second);
            //$array_total_hari_kerja = array_sum(array_column($mutasi, 'jam_kerja'));

            for($i=0 ;$i<count($mutasi);$i++) {
                $total_jam_group = $mutasi[$i]['jam_kerja'];
                $mutasi[$i]['group'] = Fp::model()->time_from_seconds($total_jam_group);
            }
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'Kpi',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=KPI-$kode_periode-$kode_cabang.xls");
                echo $this->render('Kpi', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang,
                    'total_jam_kerja_time' => $total_jam_kerja_time,
                    'total_lt' => $total_lt,
                    'total_lbr1' => $total_lbr1,
                    'total_lbr2' => $total_lbr2,
                    'total_lbr3' => $total_lbrh,
                    'from'=> sql2date( $from, 'dd MMM yyyy' ),
                    'to'=> sql2date( $to, 'dd MMM yyyy' ),
                ), true);
            } else {
                $this->render('Kpi', array(
                    'dp' => $dataProvider,
                    'nama' => $nama,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang,
                    'total_jam_kerja_time' => $total_jam_kerja_time,
                    'total_lt' => $total_lt,
                    'total_lbr1' => $total_lbr1,
                    'total_lbr2' => $total_lbr2,
                    'from'=> sql2date( $from, 'dd MMM yyyy' ),
                    'to'=> sql2date( $to, 'dd MMM yyyy' ),
                    'total_lbr3' => $total_lbrh
                ));
            }
        }
    }
    public function actionReportLessTime()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $namafungsi   = "ReportLessTime";
            $periode_id   = $_POST['periode_id'];
            $cabang_id    = $_POST['cabang_id'];
            $show_active  = $_POST['show_active'];

            $kode_periode   = Periode::model()->findByPk($periode_id)->kode_periode;
            $kode_cabang    = Cabang::model()->findByPk($cabang_id)->kode_cabang;

            $mutasi         = U::$namafungsi($periode_id, $cabang_id, $show_active);
            $dataProvider   = new CArrayDataProvider($mutasi, array(
                'id' => $namafungsi,
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=$namafungsi-$kode_periode-$kode_cabang.xls");
                echo $this->render($namafungsi, array(
                    'dp'            => $dataProvider,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang,
                    'namafungsi'    => $namafungsi
                ), true);
            } else {
                $this->render($namafungsi, array(
                    'dp'            => $dataProvider,
                    'kode_periode'  => $kode_periode,
                    'kode_cabang'   => $kode_cabang,
                    'namafungsi'    => $namafungsi
                ));
            }
        }
    }
    public function actionReportLupaAbsen()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $namafungsi = "ReportLupaAbsen";
            $periode = $_POST['periode_id'];
            $cabang = $_POST['cabang_id'];
            $kode_periode = Yii::app()->db->createCommand(
                "SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'")
                ->queryScalar();
            $mutasi = U::$namafungsi($periode,$cabang);
            $kode_cabang = Cabang::model()->findByPk($cabang)->kode_cabang;
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => $namafungsi,
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=$namafungsi-$kode_periode-$kode_cabang.xls");
                echo $this->render($namafungsi, array(
                    'dp' => $dataProvider,
                    'periode_id' => $periode,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang,
                    'namafungsi' => $namafungsi
                ), true);
            } else {
                $this->render($namafungsi, array(
                    'dp' => $dataProvider,
                    'periode_id' => $periode,
                    'kode_periode' => $kode_periode,
                    'kode_cabang' => $kode_cabang,
                    'namafungsi' => $namafungsi
                ));
            }
        }
    }
    public function actionCuti()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $tahun = $_POST['tahun_cuti'];
            $cabang = $_POST['cabang_id'];
            $mutasi = U::report_cuti($tahun,$cabang);
            $kode_cabang = Cabang::model()->findByPk($cabang)->kode_cabang;
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'Absensi',
                'pagination' => false
            ));

            $tahunsebelumnya = date("Y",strtotime("-1 year"));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Absensi-$kode_cabang-$tahun.xls");
                echo $this->render('Cuti', array(
                    'dp' => $dataProvider,
                    'tahun' => $tahun,
                    'kode_cabang' => $kode_cabang,
                    'tahunsebelumnya' => $tahunsebelumnya
                ), true);
            } else {
                $this->render('Cuti', array(
                    'dp' => $dataProvider,
                    'tahun' => $tahun,
                    'kode_cabang' => $kode_cabang,
                    'tahunsebelumnya' => $tahunsebelumnya
                ));
            }
        }
    }

    public function actionTimeSheetAX()
	{
		if (Yii::app()->request->isAjaxRequest) {
			return;
		}
		if (isset($_POST) && !empty($_POST)) {
			$cabang = $_POST['cabang_id'];

            $dbperiode      = Periode::getPeriodeStartEnd($_POST['periode_id']);
            $periodenow     = $dbperiode->queryRow();
            $periodeStart   = $periodenow['ps'];
            $periodeEnd     = $periodenow['pe'];
            $kode_periode   = $periodenow['kode_periode'];
            $row_cabang 	= Cabang::model()->findByPk($_POST['cabang_id']);
            $kode_cabang    = $row_cabang->kode_cabang;

			$objReader = PHPExcel_IOFactory::createReader( 'Excel5' );
			$excel     = $objReader->load( Yii::getPathOfAlias( 'application.views.reports' ) . DIRECTORY_SEPARATOR . 'TimesheetAX.xls' );

            $addtime_8 = "08:00:00";
            $addtime_6 = "08:00:00";
            if ($row_cabang->kepala_cabang_stat == 1)
            {
                $addtime_8 = "08:00:00";
                $addtime_6 = "06:00:00";
            }
            $data = Yii::app()->db->createCommand("
			select v.PIN AS NIK, DATE(v.in_time) as tanggal, TIME(v.in_time) as masuk, p.nama_lengkap, 
	TIME(if(v.short_shift=1, ADDTIME(v.in_time, :addtime_6), ADDTIME(v.in_time, :addtime_8))) as pulang,
	v.approval_lembur, v.approval_lesstime, (v.real_lembur_akhir+v.real_lembur_pertama) as lembur, (round(v.real_less_time/2,0)) as lesstime,		
v.kode_ket, v.status_wfh,
(CASE v.kode_ket
	WHEN '0' THEN ''
	WHEN '4' THEN 'OFF'
	WHEN '1' THEN 'BT'
	WHEN '2' THEN 'SICK'
	WHEN '5' THEN 'LEAVE'
	WHEN '6' THEN 'LEAVE'
	WHEN '7' THEN 'LEAVE'
	WHEN '8' THEN 'LEAVE'
	WHEN '9' THEN 'LEAVE'
END) as status
from pbu_validasi as v 
inner join pbu_pegawai as p ON p.pegawai_id=v.pegawai_id
where v.cabang_id=:cabang and v.in_time >= :awal and v.in_time <= :akhir and v.kode_ket not in (9) and v.status_int = 1
group by date(v.in_time), v.pegawai_id
order by v.PIN,date(v.in_time)
")->queryAll(true, [
                ':addtime_6' => $addtime_6,
                ':addtime_8' => $addtime_8,
                ':awal' => $periodeStart,
                ':akhir' => $periodeEnd,
                ':cabang' => $cabang]);

			$dataAX = array();
			foreach ($data as $row)
			{
				$tmp = [];
				$tmp['tanggal'] = ($this->format == 'excel') ? date('d/m/Y', strtotime($row['tanggal'])) : $row['tanggal'];
				$tmp['nama_lengkap'] = $row['nama_lengkap'];
				$tmp['ket'] = $row['status'];
				$tmp['NIK'] = $row['NIK'];
				if ($row['status'] == '' || $row['status'] == 'BT')
				{
					$tmp['masuk'] = $row['masuk'];
					$tmp['pulang'] = $row['pulang'];
					if ($row['approval_lembur'] == 1)
						$tmp['pulang'] = date('H:i:s', strtotime("+".$row['lembur']." minutes", strtotime($tmp['pulang'])));
					if ($row['approval_lesstime'] == 1)
						$tmp['pulang'] = date('H:i:s', strtotime("-".($row['lesstime'])." minutes", strtotime($tmp['pulang'])));
				}
				else
				{
					$tmp['masuk'] = '';
					$tmp['pulang'] = '';
				}
				$tmp['status_wfh'] = ($row['status_wfh'] == 1) ? 'WFH' : '';

				$dataAX[] =  $tmp;
			}

			if ($this->format == 'excel') {
				$i= 2;
				foreach ($dataAX as $row) {
					$excel->setActiveSheetIndex(0)
						->setCellValue('A'.$i, $row['NIK'])
						->setCellValue('B'.$i, $row['tanggal'])
						->setCellValue('C'.$i, $row['masuk'])
						->setCellValue('D'.$i, $row['pulang'])
						->setCellValue('E'.$i, $row['ket'])
						->setCellValue('G'.$i, $row['nama_lengkap'])
						->setCellValue('H'.$i, $row['status_wfh']);
					$i++;
				}

				header( 'Content-Type: application/vnd.ms-excel' );
				header( "Content-Disposition: attachment;filename=TIMESHEET_AX-$kode_cabang.xls");
				header( 'Cache-Control: max-age=1' );
				$objWriter = PHPExcel_IOFactory::createWriter( $excel, 'Excel5' );
				$objWriter->save( 'php://output' );
				Yii::app()->end();
			} else {
				$dataProvider = new CArrayDataProvider($dataAX, array(
					'id' => 'TimesheetAX',
					'pagination' => false
				));

				$this->render('TimeSheetAX', array(
					'dp' => $dataProvider,
					'kode_periode' => $kode_periode,
					'kode_cabang' => $kode_cabang
				));
			}
		}
	}

    public function actionPrintMasterGaji()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $mutasi = U::report_master_gaji();
            if ($this->format == 'excel') {
                $field = array(
                    'A2' => Yii::app()->params['nama_perusahaan']
                );
                $field_header = array(
                    'A' => '#',
                    'B' => 'lnama',
                    'C' => 'gnama',
                    'D' => 'anama'
                );
                $kol = 'E';
                /** @var Master[] $m */
                $m = Master::model()->findAll();
                $field_copy4 = [];
                $field_copy5 = [];
                foreach ($m as $r) {
                    $field_header[$kol] = $r->kode;
                    $field_copy4[] = $kol . "4";
                    $field_copy5[] = $kol . "5";
                    $field[$kol . "4"] = $r->kode;
                    $kol++;
                }
                $field[$kol . '4'] = 'Asumsi';
                $field_header[$kol] = 'asumsi';
                $field_copy4[] = $kol . "4";
                $field_copy5[] = $kol . "5";
                $columnData = array(
                    $field_header
                );
                $report = new PhpExcelReportHelper();
                $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') .
                    DIRECTORY_SEPARATOR . 'master_gaji.xlsx')
                    ->setActiveSheetIndex(0)
                    ->copyFormatField('E4', $field_copy4)
                    ->copyFormatField('E5', $field_copy5)
                    ->mergeBlock($mutasi, $columnData, 5, 1)
                    ->mergeField($field)
                    //->protectSheet()
                    ->downloadExcel('Master Gaji ' . Yii::app()->params['nama_perusahaan']);
            } else {
                $dataProvider = new CArrayDataProvider($mutasi, array(
                    'id' => 'RekapPegawai',
                    'pagination' => false
                ));
                $this->render('RekapPegawai', array(
                    'dp' => $dataProvider
                ));
            }
        }
    }
    public function actionPrintRekapPayroll()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $periode_id = $_POST['periode_id'];
            $header_col = U::report_rekap_payroll_header($periode_id);
            $mutasi = U::report_rekap_payroll($periode_id);
            /** @var Periode $periode */
            $periode = Periode::model()->findByPk($periode_id);
            if ($this->format == 'excel') {
                $field = array(
                    'A1' => 'Rekap Payroll ' . Yii::app()->params['nama_perusahaan'],
                    'A2' => 'Periode ' . $periode->kode_periode
                );
                $field_header = array(
                    'A' => '#',
                    'B' => 'nama_lengkap',
                    'C' => 'kode_level',
                    'D' => 'nama_jabatan',
                    'E' => 'tgl_masuk',
                    'F' => 'total_hari_kerja',
                    'G' => 'total_sakit',
                    'H' => 'total_cuti_tahunan',
                    'I' => 'jatah_off',
                    'J' => 'total_off',
                    'K' => 'total_lembur_1',
                    'L' => 'total_lembur_next'
                );
                $rHeader = 5;
                $rData = 6;
                $kol = 'M';
                $field_copy4 = [];
                $field_copy5 = [];
                foreach ($header_col as $r) {
                    $field_header[$kol] = $r['nama_skema'];
                    $field_copy4[] = $kol . $rHeader;
                    $field_copy5[] = $kol . $rData;
                    $field[$kol . $rHeader] = $r['nama_skema'];
                    $kol++;
                }
                $field[$kol . $rHeader] = 'Take Home Pay';
                $field_header[$kol] = 'take_home_pay';
                $field_copy4[] = $kol . $rHeader;
                $field_copy5[] = $kol . $rData;
                $columnData = array(
                    $field_header
                );
                $report = new PhpExcelReportHelper();
                $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') .
                    DIRECTORY_SEPARATOR . 'rekap_payroll.xlsx')
                    ->setActiveSheetIndex(0)
                    ->copyFormatField('L' . $rHeader, $field_copy4)
                    ->copyFormatField('L' . $rData, $field_copy5)
                    ->mergeBlock($mutasi, $columnData, $rData, 1)
                    ->mergeField($field)
                    //->protectSheet()
                    ->downloadExcel('Rekap Payroll ' . Yii::app()->params['nama_perusahaan']);
            } else {
                $dataProvider = new CArrayDataProvider($mutasi, array(
                    'id' => 'RekapPegawai',
                    'pagination' => false
                ));
                $this->render('RekapPegawai', array(
                    'dp' => $dataProvider
                ));
            }
        }
    }
    public function actionCetakSlip()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $payroll_id = $_POST['payroll_id'];
            /** @var Payroll $payroll */
            $payroll = Payroll::model()->findByPk($payroll_id);
            if ($payroll != null) {
                $select = "SELECT * FROM pbu_payroll_details AS am WHERE am.type_ = :type_ 
                  AND am.payroll_id = :payroll_id ORDER BY seq";
                $inc = Yii::app()->db->createCommand($select)->queryAll(true, [
                    ':type_' => 1,
                    ':payroll_id' => $payroll->payroll_id
                ]);
                $out = Yii::app()->db->createCommand($select)->queryAll(true, [
                    ':type_' => -1,
                    ':payroll_id' => $payroll->payroll_id
                ]);
                $details = [];
                $j_inc = count($inc);
                $j_out = count($out);
                $cnt = 0;
                if ($j_inc >= $j_out) {
                    for ($cnt; $cnt < $j_inc; $cnt++) {
                        $details[] = [
                            'in_n' => $inc[$cnt]['nama_skema'],
                            'in_a' => ': Rp ' . number_format($inc[$cnt]['amount']),
                            'ot_n' => $cnt < $j_out ? $out[$cnt]['nama_skema'] : '',
                            'ot_a' => $cnt < $j_out ? ': Rp ' . $out[$cnt]['amount'] : '',
                        ];
                    }
                } else {
                    for ($cnt; $cnt < $j_out; $cnt++) {
                        $details[] = [
                            'in_n' => $cnt < $j_inc ? $inc[$cnt]['nama_skema'] : '',
                            'in_a' => $cnt < $j_inc ? ': Rp ' . number_format($inc[$cnt]['amount']) : '',
                            'ot_n' => $out[$cnt]['nama_skema'],
                            'ot_a' => ': Rp ' . $out[$cnt]['amount'],
                        ];
                    }
                }
                for ($cnt; $cnt < 11; $cnt++) {
                    $details[] = [
                        'in_n' => '',
                        'in_a' => '',
                        'ot_n' => '',
                        'ot_a' => '',
                    ];
                }
//                $payroll->createSlipGajiPDF();
//                $mPDF1 = Yii::app()->ePdf->mpdf();
                # You can easily override default constructor's params

//                $this->render('CetakSlip', [
//                    'periode' => $payroll->periode->kode_periode,
//                    'tgl' => get_date_today('dd-MM-yyyy'),
//                    'cabang' => $payroll->kode_cab,
//                    'jabatan' => $payroll->nama_jabatan,
//                    'gol' => $payroll->kode_gol,
//                    'lvl' => $payroll->kode_level,
//                    'hk' => $payroll->total_hari_kerja,
//                    'nik' => $payroll->nik,
//                    'nama' => $payroll->nama_lengkap,
//                    'email' => $payroll->email,
//                    'status' => $payroll->nama_status,
//                    'npwp' => $payroll->npwp,
//                    'ktp' => '-',
//                    'inc' => number_format($payroll->total_income, 2),
//                    'pot' => number_format($payroll->total_deduction, 2),
//                    'thp' => number_format($payroll->take_home_pay, 2),
//                    'mandiri' => '-',
//                    'tabungan' => '-',
//                    'cicilan' => '-',
//                    'note' => '-',
//                    'details' => $details
//                ]);
//                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5',0,'',10,10,11,11,6,6,'P');
                $mPDF1 = Yii::app()->ePdf->mpdf('utf-8', array(279.4,228.6));
                $mPDF1->WriteHTML($this->render('CetakSlip', [
                    'periode' => $payroll->periode->kode_periode,
                    'tgl' => get_date_today('dd-MM-yyyy'),
                    'cabang' => $payroll->kode_cab,
                    'jabatan' => $payroll->nama_jabatan,
                    'gol' => $payroll->kode_gol,
                    'lvl' => $payroll->kode_level,
                    'hk' => $payroll->total_hari_kerja,
                    'nik' => $payroll->nik,
                    'nama' => $payroll->nama_lengkap,
                    'email' => $payroll->email,
                    'status' => $payroll->nama_status,
                    'npwp' => $payroll->npwp,
                    'ktp' => '-',
                    'inc' => number_format($payroll->total_income, 2),
                    'pot' => number_format($payroll->total_deduction, 2),
                    'thp' => number_format($payroll->take_home_pay, 2),
                    'mandiri' => '-',
                    'tabungan' => '-',
                    'cicilan' => '-',
                    'note' => '-',
                    'details' => $details
                ], true));
                $mPDF1->Output();
            }
        }
    }
}
