<?php
class SecurityRolesController extends GxController
{
    public function actionCreate()
    {
        $model = new SecurityRoles;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $section = array();
            foreach ($_POST as $k => $v) {
                if (is_angka($k)) {
                    $section[] = $k;
                    continue;
                }
                $_POST['SecurityRoles'][$k] = $v;
            }
            $_POST['SecurityRoles']['sections'] = implode(",",$section);
            $model->attributes = $_POST['SecurityRoles'];
            $msg = t('save.fail','app');
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id','app',array('{id}'=>$model->security_roles_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SecurityRoles');
        if (isset($_POST) && !empty($_POST)) {
            $section = array();
            foreach ($_POST as $k => $v) {
                if (is_angka($k)) {
                    $section[] = $k;
                    continue;
                }
                $_POST['SecurityRoles'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $_POST['SecurityRoles']['sections'] = implode(",",$section);
            $model->attributes = $_POST['SecurityRoles'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id','app',array('{id}'=>$model->security_roles_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->security_roles_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SecurityRoles')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = SecurityRoles::model()->findAll($criteria);
        $total = SecurityRoles::model()->count($criteria);
        foreach ($model AS $dodol) {
            $row = $dodol->getAttributes();
            $sections = explode(',',$row['sections']);
            foreach($sections as $line)
            {
                $row[$line] = 1;
            }
            $argh[] = $row;
        };
        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($argh) . '}';
        Yii::app()->end($jsonresult);
    }
}