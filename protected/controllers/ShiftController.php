<?php
class ShiftController extends GxController
{
    public function actionCreate()
    {
        $model = new Shift;
        if (!Yii::app()->request->isAjaxRequest) return;
        
        if (isset($_POST) && !empty($_POST)) {
            $dataShift = [];
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $dataShift[$k] = $v;
            }
            
            $model->attributes = $dataShift;
            
            $msg = "";
            if ($model->save()) {
                $status = true;
                $msg = "Data Shift berhasil disimpan.";
            } else {
                $msg = "Data Shift gagal disimpan. " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Shift');
        
        if (isset($_POST) && !empty($_POST)) {
            $dataShift = [];
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $dataShift[$k] = $v;
            }
            
            $model->attributes = $dataShift;
            
            $msg = "";
            if ($model->save()) {
                $status = true;
                $msg = "Perubahan data berhasil disimpan";
            } else {
                $msg = "Perubahan data gagal disimpan " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->shift_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Shift')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionClone(){
        $shift      = Shift::model()->findByPk($_POST['shift_id']);
        $dataShift  = [];
        foreach ($shift as $k => $v) {
            if (is_angka($v)) $v = get_number($v);
            $dataShift[$k] = $v;
        }
        $model              = new Shift();
        $model->attributes  = $dataShift;
        $model->shift_id    = null;
        $model->cabang_id   = $_POST['cabang_id'];
        if ($model->save()) {
            $status = true;
            $msg = "Data Shift berhasil di-Clone.";
        } else {
            $msg = "Data Shift gagal di-Clone. " . CHtml::errorSummary($model);
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionIndex()
    {
        $dbcmd = DbCmd::instance()->addFrom('{{shift}} s')
            ->addSelect("s.*, if(s.cabang_id is not null, c.kode_cabang, 'tidak ada cabang') kode_cabang")
            ->addLeftJoin('{{cabang}} c', 'c.cabang_id = s.cabang_id')
            ->addOrder('s.cabang_id,in_time asc')
        ;

        $cabang_id      = Cabang::model()->checkCabangId();
        $security_roles = self::getSecuriyRolesId();

        If (isset($_POST['pegawai_id'])){
            $pegawai          = Pegawai::model()->findByPk($_POST['pegawai_id']);
            $kelompok_pegawai = $pegawai->kelompok_pegawai;

            $cabang_pegawai_id = $pegawai->cabang_id;
            $bu_id             = Cabang::model()->findByPk($cabang_pegawai_id)->bu_id;
            $cabang_pusat_id   = Cabang::model()->findByAttributes(['bu_id' => $bu_id, 'kepala_cabang_stat' => 1]);

            if($cabang_pegawai_id == $cabang_pusat_id)
                $dbcmd->addParam(':cabang_id', $cabang_pusat_id);
            else
                $dbcmd->addParam(':cabang_id', $cabang_id);

            $dbcmd->addCondition("s.cabang_id = :cabang_id")
                ->addCondition("kelompok_shift = :kelompok_pegawai")
                ->addParam(':kelompok_pegawai', $kelompok_pegawai)
            ;
        } else {
            if ($security_roles != GET_SECURITY_ROLE_ADMINISTRATOR() && $security_roles != GET_SECURITY_ROLE_HRD()) {
                $dbcmd->addCondition("s.cabang_id = :cabang_id")
                    ->addParam(':cabang_id', $cabang_id)
                ;
                if(isset($_POST['mode']) && $_POST['mode'] !== 'grid') {
                    $dbcmd->addCondition("active = 1");
                }
            } else
                $dbcmd->addInCondition("c.cabang_id", $this->getAllCabangInBu($_POST['bu_id']));
        }
        if (isset($_POST['kode_cabang'])){
            $dbcmd->addCondition('kode_cabang like :kode_cabang');
            $dbcmd->addParam(':kode_cabang', '%'.$_POST['kode_cabang'].'%');
        }

        $count = $dbcmd->queryCount();
        $model = $dbcmd->queryAll();
        $this->renderJsonArr($model);
    }
    public function actionAktivasiShift()
    {
        $model = Shift::model()->findByPk($_POST['shift_id']);
        if($model->active == 0){
           $model->active = 1; 
        } else {
            $model->active = 0;
        }
        $html = '';
        if (!$model->save()) { 
            $html .= CHtml::errorSummary($model);
        }        
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
    public function actionKelompokShift()
    {
        $model = Shift::model()->findByPk($_POST['shift_id']);
        if($model->kelompok_shift == 0){
           $model->kelompok_shift = 1; 
        } else {
            $model->kelompok_shift = 0;
        }
        $html = '';
        if (!$model->save()) { 
            $html .= CHtml::errorSummary($model);
        }        
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
}