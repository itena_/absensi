<?php

class ShiftPinController extends GxController {

    public function actionCreate() {
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $a = Shiftpin::model()->updateAll(
                     //perubahan
                     ['visible' => 0,
                      'user_id' => Users::model()->findByPk(Yii::app()->user->getId())->id,
                      'tdate' => new CDbExpression('NOW()')   
                     ],
                    //where    
                    'pegawai_id = :pegawai_id',
                    //param    
                    [':pegawai_id' => $_POST['pegawai_id']]);
                foreach ($detils as $detil) {
                    $item_details = new Shiftpin;
                    $_POST['ShiftPin']['pin_id'] = $_POST['pin_id'];
                    $_POST['ShiftPin']['shift_id'] = $detil['shift_id'];
                    $_POST['ShiftPin']['pegawai_id'] = $_POST['pegawai_id'];
                    $item_details->attributes = $_POST['ShiftPin'];

                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    }
                }
//                $auw->save();
                $transaction->commit();
                $status = true;
                $msg = 'Data berhasil diassign!';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdateOld($id) {
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            $idpin = $_POST['pin_id'];
            
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Shiftpin::model()->deleteAll("pin_id = :pin_id", array(':pin_id' => $idpin));
                foreach ($detils as $detil) {
                    $item_details = new Shiftpin;
                    $_POST['ShiftPin']['pin_id'] = $idpin;
                    $_POST['ShiftPin']['shift_id'] = $detil['shift_id'];
                    $item_details->attributes = $_POST['ShiftPin'];

                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    }
                }
                $transaction->commit();
                $status = true;
                $msg = 'Data berhasil diassign!';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                Shiftpin::model()->deleteAll("pin_id = :pin_id", array(':pin_id' => $id));
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->params = array();
        
//        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
//                (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->limit = $limit;
        $criteria->offset = $start;
        if (isset($_POST['template_id'])){
            $criteria->addCondition("template_id = '" . $_POST['template_id'] . "'");
            $criteria->group = 'shift_id';
            
            $model = TemplateHari::model()->findAll($criteria);
            $total = TemplateHari::model()->count($criteria);
            $this->renderJson($model, $total);
        }else{
            if ((isset($_POST['pegawai_id']))) {
                $criteria->addCondition("pegawai_id = '" . $_POST['pegawai_id'] . "'");
                $criteria->group = '';
            } else {$criteria->group = "pegawai_id";}
            
            $cabang = Cabang::model()->checkCabang();
            if ($cabang != 'ALL'){
                $criteria->addCondition("store = '$cabang'");
            }
            
            $model = VAssignshift::model()->findAll($criteria);
            $total = VAssignshift::model()->count($criteria);
            $this->renderJson($model, $total);
        }
    }

    public function actionGetPin() { 
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $param = array();
        $dbcmd = DbCmd::instance()
                ->addFrom('{{pin}}'); 
        
        if (isset($_POST['query'])) {
            $param = array(
                ':PIN' => "%" . $_POST['query'] . "%", 
                ':nama_lengkap' => "%" . $_POST['query'] . "%", 
            );
            $dbcmd->addCondition("(PIN LIKE :PIN OR nama_lengkap LIKE :nama_lengkap)");
        } 
        if (isset($_POST['pegawai_id'])) {
            $dbcmd->addCondition("pegawai_id = :pegawai_id");
            $param = [':pegawai_id' => $_POST['pegawai_id']];
        }
        $count = $dbcmd->queryCount($param);
        $model = $dbcmd->queryAll(true, $param);
        $this->renderJsonArrWithTotal($model, $count);
    }

}
