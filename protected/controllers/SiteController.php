<?php
Yii::import('application.components.U');
Yii::import('application.components.SoapClientYii');
class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array('class' => 'CViewAction',),
        );
    }
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        $this->layout = 'main';
        $this->render('index');

        /*$user = Users::model()->findByPk( user()->getId() );
        if($user->user_id == 'ttratt')
            $this->redirect( url( 'site/Attendance' ) );
        else {
            $this->layout = 'main';
            $this->render('index');
        }*/
    }
    public function actionAttendance() {
        $this->layout = 'attendance';
        $this->render( 'index' );
    }
    public function actionSaveAttendance() {
        echo 'Ok';
    }

    public function actionGetDateTime()
    {
        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'success' => true,
                'datetime' => date('Y-m-d H:i:s')
            ));
            Yii::app()->end();
        }
    }
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ( $error = Yii::app()->errorHandler->error ) {
            if ( Yii::app()->request->isAjaxRequest ) {
                throw new Exception( $error['message'] );
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $error
                ));
            }
        }
    }
    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" . "Content-type: text/plain; charset=UTF-8";
                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    public function actionLogin()
    {

        if (!Yii::app()->request->isAjaxRequest) {
            $this->layout = 'login';
            $this->render('login');
        } else {
            $model = new LoginForm;
            $loginUsername = isset($_POST["loginUsername"]) ? $_POST["loginUsername"] : "";
            $loginPassword = isset($_POST["loginPassword"]) ? $_POST["loginPassword"] : "";
            if ($loginUsername != "") {
                //$model->attributes = $_POST['LoginForm'];
                $model->username = $loginUsername;
                $model->password = $loginPassword;
                // validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()){
                    echo "{success: true}";
                    $ip = $_POST['ip_local'];
                    /*
                    * SAVELOG
                    */
                        $id = Yii::app()->user->getId();
                        $log = [];
                        $log['action'] = 'LOGIN dengan IP' . $ip;
                        $log['trans'] = '-';
                        $log['db'] = 'pbu_users';
                        $log['trans_id'] = $id;
                        U::save_log($id, NULL, $log);
                    /*
                    * END SAVELOG
                    */
                }
                else
                    echo "{success: false, errors: { reason: 'Login failed. Try again.' }}";
            } else {
                echo "{success: false, errors: { reason: 'Login failed. Try again' }}";
            }
        }
    }
    public function actionBackupAll()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $conn = Yii::app()->db;
        $user = $conn->username;
        $pass = $conn->password;
        if (preg_match('/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result)) {
            list($all, $host, $db, $port) = $result;
        }
        $dir_backup = dirname(Yii::app()->request->scriptFile) . "\\backup\\";
        $files = scandir($dir_backup);
        foreach ($files as $file) {
            if (is_file("$dir_backup\\$file")) {
                unlink("$dir_backup\\$file");
            }
        }
        $backup_file = $dir_backup . $db . date("Y-m-d-H-i-s") . '.pos';
        $mysqldump = MYSQLDUMP;
        $command = "$mysqldump --opt -h $host -u $user --password=$pass -P $port " .
            "$db > $backup_file";
        system($command);
    }
    public function actionRestoreAll()
    {
        if (!Yii::app()->request->isPostRequest) {
            $this->redirect(bu());
        }
        $conn = Yii::app()->db;
        $user = $conn->username;
        $pass = $conn->password;
        if (preg_match('/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result)) {
            list($all, $host, $db, $port) = $result;
        }
        $dir_backup = dirname(Yii::app()->request->scriptFile) . "\\backup\\";
        if (isset($_FILES["filename"])) { // it is recommended to check file type and size here
            if ($_FILES["filename"]["error"] > 0) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $_FILES["file"]["error"]
                ));
            } else {
                $backup_file = $dir_backup . $_FILES["filename"]["name"];
                move_uploaded_file($_FILES["filename"]["tmp_name"], $backup_file);
                $mysql = MYSQL;
                $gzip = GZIP;
                $command = "$gzip -d $backup_file";
                system($command);
                $backup_file = substr($backup_file, 0, -3);
                if (!file_exists($backup_file)) {
                    echo CJSON::encode(array(
                        'success' => false,
                        'msg' => "Failed restore file " . $_FILES["file"]["name"]
                    ));
                } else {
                    Yii::app()->db->createCommand("DROP DATABASE $db")
                        ->execute();
                    Yii::app()->db->createCommand("CREATE DATABASE IF NOT EXISTS $db")
                        ->execute();
                    $command = "$mysql -h $host -u $user --password=$pass -P $port " .
                        "$db < $backup_file";
                    system($command);
                    Yii::app()->db->createCommand("USE $db")
                        ->execute();
                    echo CJSON::encode(array(
                        'success' => true,
                        'msg' => "Succefully restore file " . $_FILES["filename"]["name"]
                    ));
                }
                Yii::app()->end();
            }
        }
    }
    public function actionGenerate()
    {
        $templatePath = './css/silk_v013/icons';
        $files = scandir($templatePath);
        $txt = "";
        foreach ($files as $file) {
            if (is_file($templatePath . '/' . $file)) {
                $basename = explode(".", $file);
                $name = $basename[0];
                $txt .= ".silk13-$name { background-image: url(icons/$file) !important; background-repeat: no-repeat; }\n";
            }
        }
        $myFile = "silk013.css";
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $txt);
        fclose($fh);
    }
    public function actionTree()
    {
        $user = Users::model()->findByPk(user()->getId());
        $menu = new MenuTree($user->security_roles_id);
        $data = $menu->get_menu();
        Yii::app()->end($data);
    }
    public function actionPhpInfo() {
        phpinfo();
    }
    private function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
    private function save_log($username,$ip) {

    }

    public function actionResetAbsensi() {
        Yii::app()->db->createCommand("
            #TRUNCATE TABLE pbu_result;
            TRUNCATE TABLE pbu_validasi;
            TRUNCATE TABLE pbu_transfer_pegawai;
            UPDATE pbu_fp set status_int = 0, kode_ket = 0, log = 0, tipe_data = 0, Status = 0;
        ")->execute();
        echo CJSON::encode(array(
            'success' => true,
            'msg' => "SUKSES ME-RESET DATA ABSENSI."
        ));
    }

    public function actionTest() {
        $nik = '100049';
        $tahun = '2023';
        $kode_ket = Keterangan::model()->findByAttributes(['nama_ket' => 'Cuti Tahunan'])->keterangan_id;

        $command = DbCmd::instance()->addFrom("{{pegawai}} p")
            ->addSelect("nik, store, 'AKTIF' status_pegawai")
            ->addSelect("j.nama_jabatan, COUNT(*) jumlah_cuti")
            ->addLeftJoin("{{jabatan}} j", "j.jabatan_id = p.jabatan_id")
            ->addLeftJoin("{{validasi}} v", "v.pegawai_id = p.pegawai_id")
            ->addCondition("nik = :nik and v.status_int = 1")
            ->addCondition("v.kode_ket = '$kode_ket'")
            ->addCondition("
                      YEAR (v.in_time) >= '$tahun' AND 
                      YEAR (v.in_time) <= '$tahun'")
            ->addInCondition('p.status_pegawai_id', get_status_pegawai_aktif())
            ->addParam(':nik', $nik);

        $models = $command->getQuery();
        $models = $command->queryAll();
    }
}
