<?php

class StatusHkController extends GxController
{

    public function actionCreate()
    {
        $model = new StatusHk;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StatusHk'][$k] = $v;
            }
            $model->attributes = $_POST['StatusHk'];
            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->status_hk_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();

        }

    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'StatusHk');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StatusHk'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['StatusHk'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->status_hk_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->status_hk_id));
            }
        }
    }

    public function actionAssignStatusHkUtama()
    {
        $status_hk_id  = $_POST['status_hk_id'];
        $status_hk     = StatusHk::model()->findByPk($status_hk_id);
        $bu_id         = $status_hk->bu_id;

        Yii::app()->db->createCommand(
            "UPDATE {{status_hk}} SET `default` = 0 WHERE bu_id = '" . $bu_id . "'")
            ->execute();

        $status_hk->default = 1;
        $status_hk->save();

        Log::generateErrorMessage(true, 'Status HK Utama berhasil ditambahkan.');
    }

    public function actionAktivasiStatusHk()
    {
        $model = StatusHk::model()->findByPk($_POST['status_hk_id']);
        if($model->active == 0){
            $model->active = 1;
        } else {
            $model->active = 0;
        }
        $html = '';
        if (!$model->save()) {
            $html .= CHtml::errorSummary($model);
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }

    public function actionIndex()
    {
        $dbcmd = new DbCmd();
        $dbcmd->addSelect("s.*, u.user_id nik, b.bu_name")
            ->addFrom("{{status_hk}} s")
            ->addLeftJoin("{{users}} u", "u.id = s.user_id")
            ->addLeftJoin("{{bu}} b", "b.bu_id = s.bu_id")
        ;

        $dbcmd->addCondition("s.bu_id = :bu_id")->addParam(":bu_id", $_POST['bu_id']);

        //cmp select onlu aktif

        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        $dbcmd->addOrder('nama')
            ->addCondition("s.active = 1")
        ;

        $model = $dbcmd->getQuery();
        $count = $dbcmd->queryCount();
        $model = $dbcmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);

    }

}