<?php

class StatusHkPeriodeController extends GxController
{

    public function actionUpdate($id)
    {
        /*
         * Check Lock dan Post
         */
        LockPost::model()->check_lockpost($_POST['periode_id']);
        /*
         * End Check
         */

        $model = $this->loadModel($id, 'StatusHkPeriode');

        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StatusHkPeriode'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['StatusHkPeriode'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->status_hk_periode_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->status_hk_periode_id));
            }
        }
    }

    public function actionSaveData() {
        $status = false;
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if(!$_POST['periode_id'])
                Log::generateErrorMessage(false, "Periode tidak boleh kosong.");

            $bu_id      = $_POST['bu_id'];
            $periode_id = $_POST['periode_id'];

            $pegawais = DbCmd::instance()->addSelect("*")->addFrom("{{pegawai}}")
                ->addInCondition("status_pegawai_id", get_status_pegawai_aktif())
            ;

            $security_role  = SecurityRoles::model()->checkSecurityRoleId();
            if($security_role == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_role == GET_SECURITY_ROLE_HRD()) {
                $arr        = [];
                $cabangs    = Cabang::model()->findAllByAttributes(["bu_id" => $bu_id]);
                foreach ($cabangs as $c) {
                    $arr[] = $c->cabang_id;
                }
                $pegawai = $pegawais->addInCondition("cabang_id", $arr)->queryAll();
            } else {
                $cabang_id = Cabang::model()->checkCabangId();
                $pegawai = $pegawais->addCondition("cabang_id = :cabang_id")->addParam(":cabang_id", $cabang_id)->queryAll();
            }

            $statusHk = StatusHk::model()->find("bu_id = :bu_id AND `default` = 1", [
                ':bu_id' => $bu_id
            ]);

            foreach ($pegawai as $p){
                $model  = StatusHkPeriode::model()->find("pegawai_id = :pegawai_id AND periode_id = :periode_id
                AND cabang_id = :cabang_id",[
                    ':pegawai_id' => $p['pegawai_id'],
                    ':periode_id' => $periode_id,
                    ':cabang_id' => $p['cabang_id']
                ]);

                if(!$model) {
                    $model = new StatusHkPeriode();
                    $model->periode_id = $periode_id;
                    $model->pegawai_id = $p['pegawai_id'];
                    $model->cabang_id = $p['cabang_id'];
                    $model->status_hk_id = $statusHk['status_hk_id'];
                    if(!$model->save()){
                        $status = false;
                        $msg = t('save.fail','app');
                    }
                }
            }

            $status = true;
            $msg    = "Data berhasil di-generate.";
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionIndex()
    {
        if(!$_POST['periode_id'])
            throw new Exception("Periode tidak boleh kosong.");

        $dbcmd = new DbCmd();
        $dbcmd->addSelect("sp.*, p.kode_periode, pg.nik, pg.nama_lengkap, s.`nama`, u.user_id id_user")
            ->addFrom("{{status_hk_periode}} sp")
            ->addLeftJoin("{{status_hk}} s", "s.status_hk_id = sp.status_hk_id")
            ->addLeftJoin("{{periode}} p", "p.periode_id = sp.periode_id")
            ->addLeftJoin("{{pegawai}} pg", "pg.pegawai_id = sp.pegawai_id")
            ->addLeftJoin("{{users}} u", "u.id = sp.user_id")
            ->addCondition("sp.periode_id = :periode_id")->addParam(":periode_id", $_POST['periode_id'])
        ;

        $security_role  = SecurityRoles::model()->checkSecurityRoleId();
        if($security_role == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_role == GET_SECURITY_ROLE_HRD()) {
            $arr        = [];
            $cabangs    = Cabang::model()->findAllByAttributes(["bu_id" => $_POST['bu_id']]);
            foreach ($cabangs as $c) {
                $arr[] = $c->cabang_id;
            }
            $dbcmd->addInCondition("sp.cabang_id", $arr)
            ;
        } else {
            $cabang_id = Cabang::model()->checkCabangId();
            $dbcmd->addCondition("sp.cabang_id = :cabang_id")
                ->addParam(":cabang_id", $cabang_id)
            ;
        }

        $dbcmd->addOrder('pg.nik')
        ;
        $model = $dbcmd->getQuery();
        $model = $dbcmd->queryAll();
        $count = count($model);
        $this->renderJsonArrWithTotal($model, $count);

    }

}