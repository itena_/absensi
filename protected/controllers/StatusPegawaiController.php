<?php
class StatusPegawaiController extends GxController
{
    public function actionCreate()
    {
        $model = new StatusPegawai;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StatusPegawai'][$k] = $v;
            }
            $model->attributes = $_POST['StatusPegawai'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->status_pegawai_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'StatusPegawai');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StatusPegawai'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['StatusPegawai'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->status_pegawai_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->status_pegawai_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'StatusPegawai')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = StatusPegawai::model()->findAll($criteria);
        $total = StatusPegawai::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionSinkron()
    {
        $html = "";
        $output = Yii::app()->curl->get(Yii::app()->curl->base_url . 'GetAllStatusPegawai');
        $decode = json_decode($output, true);
        foreach ($decode['results'] as $result) {
            $model = StatusPegawai::model()->findByPk($result['status_pegawai_id']);
            if ($model == null) {
                $model = new StatusPegawai;
            }
            $model->status_pegawai_id = $result['status_pegawai_id'];
            $model->kode              = $result['status_pegawai_kode'];
            $model->nama_status       = $result['status_pegawai_nama'];
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
}