<?php

class TemplateController extends GxController {

    public function actionCreate() {

        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {
            $msg = 'Data berhasil disimpan.';
            $is_new = $_POST['mode'] == 0;
            $sal_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Template : $this->loadModel($sal_id, "Template");
                if ($is_new) {
                    foreach ($_POST as $k => $v) {
                        if (is_angka($v))
                            $v = get_number($v);
                        $_POST['Template'][$k] = $v;
                    }
                    $model->attributes = $_POST['Template'];
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
                    }
                } else {
                    TemplateDetail::model()->deleteAll("template_id = :template_id", array(':template_id' => $sal_id));
                }
                foreach ($detils as $detil) {
                    $item_details = new TemplateDetail;
                    $_POST['TemplateDetail']['shift_id'] = $detil['shift_id'];
                    $_POST['TemplateDetail']['template_id'] = $model->template_id;
                    $item_details->attributes = $_POST['TemplateDetail'];

                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {
            $msg = 'Data berhasil disimpan.';
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($id, "Template");
                TemplateDetail::model()->deleteAll("template_id = :template_id", array(':template_id' => $id));
                foreach ($_POST as $k => $v) {
                    if (is_angka($v))
                        $v = get_number($v);
                    $_POST['Template'][$k] = $v;
                }
                $model->attributes = $_POST['Template'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $item_details = new TemplateDetail;
                    $_POST['TemplateDetail']['shift_id'] = $detil['shift_id'];
                    $_POST['TemplateDetail']['template_id'] = $model->template_id;
                    $item_details->attributes = $_POST['TemplateDetail'];

                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Template')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $param = array();
        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['template_id'])) {
            $criteria->addCondition('template_id = :template_id');
            $param[':template_id'] = $_POST['template_id'];
        }
        $criteria->order = 'name';
        $criteria->params = $param;
        $model = Template::model()->findAll($criteria);
        $total = Template::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
