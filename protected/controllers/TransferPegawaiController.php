<?php
class TransferPegawaiController extends GxController
{     
    public function actionCreate()
    {   /*
         * Check Lock dan Post
         */
//        LockPost::model()->check_lockpost($_POST['periode_id']);        
        /*
         * End Check
         */
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            if(isset($_POST['binding_id'])) {
                $binding_id = $_POST['binding_id'];
                $binding = TransferPegawai::model()->findAll("binding_id = :binding_id", [':binding_id' => $binding_id]);
                foreach ($binding as $m){
                    $model = TransferPegawai::model()->findByPk($m->transfer_pegawai_id);
                    $model->tdate = NULL;
                    $model->user_id = NULL;
                    $model->visible = 0;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cancel Transfer Pegawai')) . CHtml::errorSummary($model));
                    }
                }
            }
            $periode_in = '';
            $periode_out = '';
            $periode = [];
            $tglin = date('Y-m-d', strtotime($_POST['tglin']));
            $tglout = date('Y-m-d', strtotime($_POST['tglout'])); 
            
            if($_POST['tglout'] < $_POST['tglin']){
                Log::generateErrorMessage(false, 'Error, tanggal akhir lebih kecil dari tanggal mulai.');
            }
            
            $checkexist_in = TransferPegawai::model()->checkExist($_POST['tglin'], $_POST['pegawai_id']);
            $checkexist_out = TransferPegawai::model()->checkExist($_POST['tglout'], $_POST['pegawai_id']);
            if ($checkexist_in == true || $checkexist_out == true){
                Log::generateErrorMessage(false, "Error, Pegawai sudah ada tugas keluar pada rentang tanggal $tglin hingga $tglout");
            }
            
            $binding_id = Yii::app()->db->createCommand("SELECT UUID();")->queryScalar();
            
            //START LOOPING
            $in_time = strtotime($tglin);
            $out_time = strtotime($tglout);
            $where = '';
            while ($in_time <= $out_time){
                $ps = date('Y-m-d',$in_time);
                $in_time = strtotime("+1 month", $in_time);
                $or = '';
                if ($in_time < $out_time) {
                    $or = " OR ";
                }
                $where .= "('$ps' >= periode_start AND '$ps' <= periode_end) $or";
            }
            $order = " order by periode_start desc";
            $periode_id = Periode::model()->findAll($where . $order);
            foreach ($periode_id as $p){   
                LockPost::model()->check_lockpost($p->periode_id);  
                $periode_start =  date('Y-m-d', strtotime($p->periode_start));
                $periode_end = date('Y-m-d', strtotime($p->periode_end));
                if (($tglin >= $periode_start) && ($tglin <= $periode_end)){
                    $periode_in = $p->periode_id;
                    if (($tglout >= $periode_start) && ($tglout <= $periode_end)){
                        $periode_out = $p->periode_id;
                    }
                    $periode[] = $p->periode_id;
                }
                else if (($tglout >= $periode_start) && ($tglout <= $periode_end)){
                    $periode_out = $p->periode_id;
                    if (($tglin >= $periode_start) && ($tglin <= $periode_end)){
                        $periode_in = $p->periode_id;
                    }
                    $periode[] = $p->periode_id;
                }
                else if ($periode_in != '' && $periode_out != ''){
                    break;
                } 
                else{
                    $periode[] = $p->periode_id;
                    continue;  
                }
            }
            
            if ($periode_in == '' || $periode_out == ''){
                Log::generateErrorMessage(false, "Error, tanggal berada di luar periode yang sudah ada!");
            }

            $no_surat_tugas = $_POST['no_surat_tugas'] . ' $(PB)$';

            if($periode_in != $periode_out){
                foreach ($periode as $pr){
                    if($pr == $periode_in){
                        $in_real = $_POST['tglin'];
                    } else $in_real = date('Y-m-d',strtotime(Periode::model()->findByPk($pr)->periode_start));
                    
                    if($pr == $periode_out){
                        $out_real = $_POST['tglout'];
                    } else $out_real = date('Y-m-d',strtotime(Periode::model()->findByPk($pr)->periode_end));

                    $model = $this->saveTransferPegawai($no_surat_tugas, $binding_id);
                    $model->tglin = $in_real;
                    $model->tglout = $out_real;
                    $model->periode_id = $pr;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Transfer Pegawai')) . CHtml::errorSummary($model));
                    }
                }
            } else {
                $model = $this->saveTransferPegawai($no_surat_tugas, $binding_id);
                $model->tglin = $_POST['tglin'];
                $model->tglout = $_POST['tglout'];
                $model->periode_id = $periode_in;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Transfer Pegawai')) . CHtml::errorSummary($model));
                }
            }
            if(isset($_POST['lintasBu']) && $_POST['lintasBu'] == 1) {
                $amosAddress = AmosAddress::model()->findByAttributes(['bu_kode' => $_POST['bu_nama_alias']]);
                if(!$amosAddress) Log::generateErrorMessage(false, 'AMOS Address <b>'. $_POST['bu_nama_alias'] .'</b> tidak terdaftar');

                $datas = TransferPegawai::model()->findAllbyAttributes(['binding_id' => $binding_id]);
                $i = 0;
                $array[$i] = [];
                foreach ($datas as $data) {
                    foreach ($data as $k => $v) {
                        $array[$i][$k] = $v;
                    }
                    $i++;
                }
                $output = Yii::app()->curl->post($amosAddress->amos_address . '/api/SaveTransferPegawai',
                    http_build_query(['datas' => $array])
                );
//                $auw->save();
            }
//            $auw->save();
            $transaction->commit();
            $status = true;
            $msg = 'Data berhasil disimpan.';
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    private function saveTransferPegawai($no_surat_tugas, $binding_id) {
        $model = new TransferPegawai();
        $model->pegawai_id = $_POST['pegawai_id'];
        $model->cabang_id = $_POST['cabang_id'];
        $model->cabang_transfer_id = $_POST['cabang_transfer_id'];
        $model->no_surat_tugas = $no_surat_tugas;
        $model->status_kota = $_POST['status_kota'];
        $model->binding_id = $binding_id;
        return $model;
    }

    public function actionCancel(){
        $binding_id = $_POST['binding_id'];
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {            
            $binding = TransferPegawai::model()->findAll("binding_id = :binding_id", [':binding_id' => $binding_id]);
            foreach ($binding as $m){
                $model = TransferPegawai::model()->findByPk($m->transfer_pegawai_id);
                $model->tdate = NULL;
                $model->user_id = NULL;
                $model->visible = 0;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cancel Transfer Pegawai')) . CHtml::errorSummary($model));
                }
            }
//          $auw->save();
            $transaction->commit();
            $status = true;
            $msg = 'Data berhasil dibatalkan.';
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }     
        echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
    }

    public function actionIndex(){
        $dbcmd = DbCmd::instance()->addFrom('{{transfer_pegawai}} t')
            ->addSelect("p.nik, p.nama_lengkap,
                            c.kode_cabang AS cabang_asal, c1.kode_cabang cabang_transfer,
                            transfer_pegawai_id, t.pegawai_id,
                            t.cabang_id, cabang_transfer_id,
                            MIN(tglin) tglin, MAX(tglout) tglout,
            #REPLACE(no_surat_tugas,' $(PB)$', '') no_surat_tugas, 
                            no_surat_tugas, visible, periode_id, t.tdate,
                            user_id, binding_id, status_kota,
                            if(visible = 1, 'AKTIF', 'DIBATALKAN') status")
            ->addLeftJoin('{{pegawai}} p', 'p.pegawai_id = t.pegawai_id')
            ->addLeftJoin('{{cabang}} c', 'c.cabang_id = t.cabang_id')
            ->addLeftJoin('{{cabang}} c1', 'c1.cabang_id = t.cabang_transfer_id')
//            ->addParam(':bu_id', $_POST['bu_id'])
//            ->addParam(':periode_id', $_POST['periode_id'])
            ->addGroup('binding_id')
            ->addOrder('t.tdate DESC');
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $dbcmd->setLimit(array_key_exists('limit', $_POST) ? $_POST['limit'] : 20, array_key_exists('start', $_POST) ? $_POST['start'] : 0);
        }
        if (isset($_POST['nik'])){
            $dbcmd->addCondition('nik like :nik')
                ->addParam(':nik', '%'.$_POST['nik'].'%');
        }
        if (isset($_POST['nama_lengkap'])){
            $dbcmd->addCondition('nama_lengkap like :nama_lengkap')
                ->addParam(':nama_lengkap', '%'.$_POST['nama_lengkap'].'%');
        }
        $security_roles = self::getSecuriyRolesId();
        if ($security_roles != GET_SECURITY_ROLE_ADMINISTRATOR() && $security_roles != GET_SECURITY_ROLE_HRD()) {
            $cbgusr = Cabang::model()->checkCabangId();
            $dbcmd->addCondition('(c1.cabang_id = :cabang_id OR c.cabang_id = :cabang_id)')
                ->addParam(':cabang_id', $cbgusr);
        }

        $count = $dbcmd->queryCount();

        if (isset($_POST['cabang_asal'])){
            $dbcmd->addHaving('cabang_asal like :cabang_asal');
            $dbcmd->addParam(':cabang_asal', '%'.$_POST['cabang_asal'].'%');
        }
        if (isset($_POST['cabang_transfer'])){
            $dbcmd->addHaving('cabang_transfer like :cabang_transfer');
            $dbcmd->addParam(':cabang_transfer', '%'.$_POST['cabang_transfer'].'%');
        }
        if (isset($_POST['status'])){
            $dbcmd->addHaving('status like :status');
            $dbcmd->addParam(':status', '%'.$_POST['status'].'%');
        }
        $model = $dbcmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }

    public function actionGetPegawaiLintasBu(){
        $msg     = "";
        $success = true;
        $output  = 'Gagal';

        $bu_nama_alias = $_POST['bu_nama_alias'];
        $nik = $_POST['nik'];

        try {
            $output = CurlHelper::GetPegawaiLintasBu(
                [
                    'bu_nama_alias' => $bu_nama_alias,
                    'nik_display' => $nik
                ]);
            if (isset($output)) {
                $success = true;
            }
        } catch (Exception $ex) {
            $success = false;
            $msg = $ex->getMessage();
        }

        if(!isset($output->results->pegawai[0])) {
            Log::generateErrorMessage(false, "Karyawan $bu_nama_alias $nik tidak ditemukan di EDM.");
        }

        $result = $output->results->pegawai[0];
        if(isset($result->area_id)) {
            $model = Area::model()->findByPk($result->area_id);
            if(!$model)
                $model = new Area;

            $array = [];
            foreach ($result as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $array[$k] = $v;
            }
            $model->attributes = $array;
            $model->kode = $array['kode_area'];
            $model->nama = $array['nama_area'];
            if (!$model->save()) {
                throw new Exception('Gagal disimpan');
            }
        }
        if(isset($result->cabang_id)) {
            $model = Cabang::model()->findByPk($result->cabang_id);
            if(!$model)
                $model = new Cabang;

            $array = [];
            foreach ($result as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $array[$k] = $v;
            }
            $model->attributes = $array;
            $model->kepala_cabang_stat = 0;

            $new_cabang = $bu_nama_alias . ' ' . $result->kode_cabang;
            $model->kode_cabang = $new_cabang;
            if (!$model->save()) {
                throw new Exception('Gagal disimpan');
            }
        }
        if(isset($result->lvljabatan_id)) {
            $model = Leveling::model()->findByPk($result->lvljabatan_id);
            if(!$model)
                $model = new Leveling;

            $array = [];
            foreach ($result as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $array[$k] = $v;
            }
            $model->attributes = $array;
            $model->leveling_id = $result->lvljabatan_id;
            $model->kode = $result->urutan_romawi;
            $model->nama = $result->urutan_romawi;
            if (!$model->save()) {
                throw new Exception('Gagal disimpan');
            }
        }
        if(isset($result->lvl2jabatan_id)) {
            $model = Golongan::model()->findByPk($result->lvl2jabatan_id);
            if(!$model)
                $model = new Golongan;

            $array = [];
            foreach ($result as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $array[$k] = $v;
            }
            $model->attributes = $array;
            $model->golongan_id = $result->lvl2jabatan_id;
            $model->kode = $result->urutan_alps;
            $model->nama = $result->urutan_alps;
            if (!$model->save()) {
                throw new Exception('Gagal disimpan');
            }
        }
        if(isset($result->jabatan_id)) {
            $model = Jabatan::model()->findByPk($result->jabatan_id);
            if(!$model)
                $model = new Jabatan;

            $array = [];
            foreach ($result as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $array[$k] = $v;
            }
            $model->attributes = $array;
            $model->kode = $result->kode_jabatan;
            if (!$model->save()) {
                throw new Exception('Gagal disimpan');
            }
        }
        if(isset($result->pegawai_id)) {
            $model = Pegawai::model()->findByPk($result->pegawai_id);
            if(!$model)
                $model = new Pegawai;

            $array = [];
            foreach ($result as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $array[$k] = $v;
            }
            $model->attributes = $array;
            $model->store = $new_cabang;
            $model->leveling_id = $result->lvljabatan_id;
            $model->golongan_id = $result->lvl2jabatan_id;
            $model->tgl_cabang = $result->cabang_start;
            $model->tgl_level_golongan_jabatan = $result->divisi_jabatan_start;
            $model->tgl_status_pegawai = $result->status_pegawai_start;
            if (!$model->save()) {
                throw new Exception('Gagal disimpan');
            }
        }

        echo CJSON::encode(array(
            'success' => $success,
            'msg' => $msg,
            'cabang_asal' => $new_cabang,
            'nama_lengkap' => $model->nama_lengkap,
            'pegawai_id' => $model->pegawai_id,
            'cabang_id' => $model->cabang_id
        ));
        Yii::app()->end();

    }
}