<?php
class UsersController extends GxController
{
    public function actionCreate()
    {
        $model = new Users;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Users'][$k] = $v;
            }
            $model->attributes = $_POST['Users'];
            $msg = t('save.fail', 'app');
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id','app',array('{id}'=>$model->id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdatePass()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $passold = $_POST['passwordold'];
            $passnew = $_POST['password'];
            $model = $this->loadModel(Yii::app()->user->getId(), 'Users');
            $msg = t('pass.wrong.old','app');
            $status = false;
            if (bCrypt::verify($passold, $model->password)) {
//                $crypt = new bCrypt();
//                $pass = $crypt->hash($passnew);
                $model->password = $passnew;
                if ($model->save()) {
                    $status = true;
                    $msg = t('pass.success','app');
                } else {
                    $status = false;
                    $msg = t('pass.fail','app');
                }
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdateRole()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['id'];
            $role = $_POST['security_roles_id'];
            $name = $_POST['name_'];
            $model = $this->loadModel($id, 'Users');
            $msg = t('save.success','app');
            $status = true;
            $model->security_roles_id = $role;
            $model->name_ = $name;
            if (!$model->save()) {
                $status = false;
                $msg = t('save.fail','app');
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Users');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Users'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $model->attributes = $_POST['Users'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id','app',array('{id}'=>$model->id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Users')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex(){
        $cmd = DbCmd::instance()
            ->addFrom('{{users}} u')
            ->addSelect("u.*, s.role, CONCAT(p.nik,' ',p.nama_lengkap) pin_nama")
            ->addInnerJoin("{{security_roles}} s","s.security_roles_id = u.security_roles_id")
            ->addLeftJoin("{{pegawai}} p", "p.pegawai_id = u.pegawai_id")
            ->addCondition("active = 1")
            ->addOrder('u.user_id');

        $count = $cmd->queryCount();
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(array_key_exists('limit', $_POST)? $_POST['limit'] : 20, array_key_exists('start', $_POST)? $_POST['start'] : 0);
        }

        $model = $cmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }
}