<?php

class ValidasiController extends GxController
{

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            /*
             * Check Lock dan Post
             */
            LockPost::model()->check_lockpost($_POST['periode_id']);
            /*
             * End Check
             */
            if (!$_POST['PIN']) Yii::app()->end();
            $status = false;
            $msg = "Data gagal disimpan.";
            $cabang_id = '';
            $mode = '';
            if (isset($_POST['mode']) && $_POST['mode'] == 'import') $mode = $_POST['mode'];

            $periode_id = $_POST['periode_id'];
            $dari = $_POST['jam_in'];
            $sampai = $_POST['jam_out'];
            $pin_id = $_POST['PIN'];
            $PIN = $_POST['PIN'];
            $no_surat = $_POST['nosurat'];
            $in_time = $_POST['in_time'];
            $out_time = $_POST['out_time'];
            $kode_ket = $_POST['ket'];
            $cuti_mendadak = isset($_POST['cuti']) ? 2 : 0;
            $wfh = isset($_POST['wfh']) ? 1 : 0;
            $cuti_tahun = isset($_POST['cuti_tahun']) ? $_POST['cuti_tahun'] : null;

            if (!isset($_POST['mode']) || (isset($_POST['mode']) && $_POST['mode'] != 'import')) {
                Validasi::check_tanggal($periode_id, "$dari $in_time", "$sampai $out_time");
            }

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($mode == 'import') {
                    $cabang_id = Cabang::model()->find("kode_cabang = :kode_cabang AND bu_id = :bu_id", [
                        'kode_cabang' => $_POST['kode_cabang'],
                        'bu_id' => $_POST['bu_id']
                    ]);
                    if ($cabang_id == null)
                        Log::generateErrorMessage(false, "Cabang " . $_POST['kode_cabang'] . " tidak ditemukan.");
                    $cabang_id = $cabang_id->cabang_id;

                    $pegawai = Pegawai::model()->find("cabang_id = :cabang_id AND nik = :nik", [
                        ':cabang_id' => $cabang_id,
                        ':nik' => $PIN
                    ]);
                    if ($pegawai == null)
                        Log::generateErrorMessage(false, "Pegawai " . $PIN . " di cabang " . $_POST['kode_cabang'] . " tidak ditemukan.");
                    $pegawai_id = $pegawai->pegawai_id;
                    $kelompok_pegawai = $pegawai->kelompok_pegawai;

                    if (($in_time == '' || $out_time == '') && $kode_ket != 3) {
                        $shift_id = Shift::model()->find("cabang_id = :cabang_id AND kelompok_shift = :kelompok_pegawai AND active = 1 ORDER BY in_time ", [
                            ':cabang_id' => $cabang_id,
                            ':kelompok_pegawai' => $kelompok_pegawai
                        ]);
                    } else {
                        $shift_id = Shift::model()->find(":in_time >= earlyin_time AND :in_time <= latein_time AND cabang_id = :cabang_id AND kelompok_shift = :kelompok_pegawai AND active = 1 ORDER BY in_time ", [
                            ':in_time' => $in_time,
                            ':cabang_id' => $cabang_id,
                            ':kelompok_pegawai' => $kelompok_pegawai
                        ]);
                    }

                    if ($shift_id == null) {
                        if ($_POST['mode'] == 'import') return;

                        Log::generateErrorMessage(false, "Shift dengan jam masuk $in_time tidak ditemukan.");
                    }
                    $shift_id = $shift_id->shift_id;

                } else {
                    $pegawai_id = $_POST['pegawai_id'];
                    $shift_id = $_POST['shift_id'];
                }

                $cuti = 0;
                if ($kode_ket == Keterangan::model()->findByAttributes(['nama_ket' => 'Cuti Tahunan'])->keterangan_id) {
                    $cuti_mendadak == 2 ? $cuti = 2 : $cuti = 1;
                }

                $array = Validasi::addAdditionalValidasi($pegawai_id, $periode_id);

                if (isset($_POST['lanjut'])) {
                    $begin = new DateTime($dari);
                    $end = new DateTime(date('Y-m-d', strtotime('+1 day', strtotime($sampai))));
                    $interval = DateInterval::createFromDateString('1 day');
                    $period = new DatePeriod($begin, $interval, $end);

                    foreach ($period as $dt) {
                        $dari = $sampai = $dt->format("Y-m-d");
                        $this->validasi($dari, $in_time, $sampai, $out_time, $periode_id, $pegawai_id, $cabang_id, $mode, $shift_id, $PIN, $pin_id, $kode_ket, $no_surat, $cuti, $wfh, $array, $cuti_tahun);
                    }
                } else {
                    $dataValidasi = $this->validasi($dari, $in_time, $sampai, $out_time, $periode_id, $pegawai_id, $cabang_id, $mode, $shift_id, $PIN, $pin_id, $kode_ket, $no_surat, $cuti, $wfh, $array, $cuti_tahun);
                    if ($mode == 'import') {
                        $validasi = $dataValidasi;
                        //cari hari sabtu minggu libur kode_ket 1 dan 3, terapkan 6 jam
                        if ($validasi) {
                            if ($validasi->kode_ket == 1 || $validasi->kode_ket == 3) {
                                $cekLibur = Periode::model()->find("periode_id = :periode_id AND libur like :libur", [':periode_id' => $periode_id, ':libur' => '%' . $dari . '%']);
                                if (date('D', strtotime($dari)) === 'Sat' || $cekLibur) {
                                    $intm = new DateTime("$dari $in_time");
                                    $outm = new DateTime("$sampai $out_time");
                                    if($intm > $outm && $mode == 'import'){
                                        $nextDay = date('Y-m-d H:i:s', strtotime('+1 day', strtotime("$sampai $out_time")));
                                    } else
                                        $nextDay = "$sampai $out_time";

                                    $duras = Fp::model()->durasi($PIN, $validasi->shift_id, "$dari $in_time", $nextDay, '6jam', '');

                                    $validasi = $this->applyShortShift($validasi, $duras);
                                    if (!$validasi->save()) {
                                        throw new Exception('GAGAL MENYIMPAN KARENA ' . CHtml::errorSummary($validasi));
                                    }
                                }
                            }
                        }
                    }
                }
                $status = true;
                $msg = "Data berhasil disimpan.";
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    private function validasi($dari, $in_time, $sampai, $out_time, $periode_id, $pegawai_id, $cabang_id, $mode, $shift_id, $PIN, $pin_id, $kode_ket, $no_surat_tugas, $cuti, $wfh, $array , $cuti_tahun = null)
    {
        $cabang_pusat = Cabang::model()->getCabangPusatFromBu($pegawai_id);
        $darix = $dari;
        $sampaix = $sampai;

        $db = new DbCmd();
        $db->addFrom("{{validasi}}")
            ->addSelect("in_time")
            ->addCondition("pegawai_id = :p AND status_int = 1")
            ->addCondition("in_time like :dari")
            ->addParams([
                ":p" => $pegawai_id,
                ":dari" => "$dari%",
            ]);
        $checkpasangan = $db->queryRow();

        if ($checkpasangan) {
            if (isset($_POST['mode']) && $_POST['mode'] == 'import') return;
            $msg = "Data pada tanggal tersebut sudah ada isinya.";
            Log::generateErrorMessage(false, $msg);
        } else {
            $db->clearFrom()->clearParam();
            $db->addSelect("kode_cabang")
                ->addFrom("{{validasi_view}}")
                ->addParams([
                    ":p" => $pegawai_id,
                    ":dari" => "$darix%",
                ]);
            $checkhari = $db->queryRow();
            $cabangasal = $checkhari['kode_cabang'];
            if ($checkhari) {
                $msg = "Duplikat absen atau absen sudah ada pada tanggal $dari di cabang $cabangasal.";
                Log::generateErrorMessage(false, $msg);
            }

            $day_intime = "$dari $in_time";
            $day_outtime = "$sampai $out_time";

            $transferpegawai = TransferPegawai::model()->checkPegawai($periode_id, $pegawai_id, $day_intime, $day_outtime);
            if (isset($transferpegawai)) {
                $cbgusr = Cabang::model()->checkCabangId();
                if ($cbgusr != $transferpegawai->cabang_transfer_id) {
                    $msg = "Tidak bisa review, karena absensi tidak sesuai cabang!";
                    Log::generateErrorMessage(false, $msg);
                }
            }

            if ($mode == 'import') {
                $cabang = $cabang_id;
            } else {
                if (isset($transferpegawai)) {
                    $cabang = $transferpegawai->cabang_transfer_id;
                    $no_surat_tugas = $no_surat_tugas . ' ' . $transferpegawai->no_surat_tugas;
                    $kode_ket = $transferpegawai->status_kota == 1 && $kode_ket == 3 ? 1 : $kode_ket;
                } else {
                    if (Pegawai::model()->findByPk($pegawai_id)->cabang_id != Cabang::model()->checkCabangId()) {
                        $security_roles = self::getSecuriyRolesId();
                        if (Pegawai::model()->findByPk($pegawai_id)->cabang_id == $cabang_pusat && ($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() && $security_roles == GET_SECURITY_ROLE_HRD())) {
                            $cabang = $cabang_pusat;
                        } else {
                            $output = Yii::app()->curl->post(Yii::app()->curl->base_url . 'GetAllHistory',
                                http_build_query(['pegawai_id' => $pegawai_id, 'tglout' => $dari]));
                            if ($output == Cabang::model()->checkCabangId()) {
                                $cabang = $output;
                            } else {
                                $msg = "Tidak bisa review, karena absensi tidak sesuai cabang & perbantuan pegawai belum dibuat!";
                                Log::generateErrorMessage(false, $msg);
                            }
                        }
                    } else {
                        $cabang = Pegawai::model()->findByPk($pegawai_id)->cabang_id;
                    }
                }
            }

            $valid = new Validasi;
            $k['PIN'] = $PIN;
            $k['jam_in'] = "$dari $in_time";
            $k['jam_out'] = "$sampai $out_time";

            $intm = new DateTime($k['jam_in']);
            $outm = new DateTime($k['jam_out']);
            if($intm > $outm && $mode == 'import'){
                $nextDay = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($k['jam_out'])));
                $k['jam_out'] = $nextDay;
            }

            $cabang_id = $cabang;
            $duras = Fp::model()->durasi($pegawai_id, $shift_id, $k['jam_in'], $k['jam_out'], '', '', $mode);
            $validasi = Validasi::saveValidasi($valid, $pegawai_id, $k, $shift_id, $duras, $kode_ket, $cabang_id, $no_surat_tugas, $cuti, $wfh, $cuti_tahun);

            $valid->attributes = array_merge($validasi, $array);

            if ($mode == 'import') {
                $valid->approval_lembur = $_POST['approval_lembur'];
            }
            if (!$valid->save()) {
                Log::generateErrorMessage(false, 'Gagal menyimpan validasi!' . CHtml::errorSummary($valid));
            }

            return $valid;
        }
    }

    public function actionChangeCutiTahun(){
        /*
         * Check Lock dan Post
         */

        //LockPost::model()->check_lockpost($_POST['periode_id']);

        /*
         * End Check
         */
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $validasi_id = $_POST['validasi_id'];
            $cuti_tahun = $_POST['cuti_tahun'];

            /*
            $now = new DateTime();
            $year_now =$now->format('Y');
            if ($cuti_tahun > $year_now || $year_now - $cuti_tahun > 1) {
               throw new Exception('FAILED!! Tahun cuti tidak sesuai aturan, pilih ulang kembali' );
            }
            */

            $model = Validasi::model()->findByPk($validasi_id);
            $model->cuti_tahun = $cuti_tahun;
            if (!$model->save()) {
                throw new Exception('GAGAL MENYIMPAN KARENA ' . CHtml::errorSummary($model));
            }
            $status = true;
            $msg = "Perubahan Tahun cuti berhasil di simpan";
            $transaction->commit();

        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionGetShift6Jam()
    {
        /*
         * Check Lock dan Post
         */
        LockPost::model()->check_lockpost($_POST['periode_id']);
        /*
         * End Check
         */
        $validasi_id = $_POST['id'];
        $param2 = '';
        if (isset($_POST['param2'])) {
            $param2 = $_POST['param2'];
        }
        $in_time = date('Y-m-d H:i:s', strtotime($_POST['in_time']));
        $out_time = date('Y-m-d H:i:s', strtotime($_POST['out_time']));
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $validasi = Validasi::model()->findByPk($validasi_id);
            $duras = Fp::model()->durasi($validasi->pegawai_id, $validasi->shift_id, $in_time, $out_time, '6jam', $param2);

            $validasi = $this->applyShortShift($validasi, $duras);
            if (!$validasi->save()) {
                throw new Exception('GAGAL MENYIMPAN KARENA ' . CHtml::errorSummary($validasi));
            }
            $status = true;
            $msg = "Data disesuaikan.";
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg,
            'real_lembur_pertama' => $duras['real_lembur_pertama'],
            'real_lembur_akhir' => $duras['real_lembur_akhir'],
            'real_less_time' => $duras['real_less_time'],
            'real_lembur_hari' => $duras['real_lembur_hari'],
            'pegawai_id' => $validasi->pegawai_id
        ));
        Yii::app()->end();
    }

    private function applyShortShift($validasi, $duras)
    {
        $validasi->real_lembur_pertama = $duras['real_lembur_pertama'];
        $validasi->real_lembur_akhir = $duras['real_lembur_akhir'];
        $validasi->real_lembur_hari = $duras['real_lembur_hari'];
        $validasi->real_less_time = $duras['real_less_time'];
        $validasi->short_shift = 1;

        return $validasi;
    }

    public function actionIndex()
    {
        $security_roles = $this->getSecuriyRolesId();
        $cabangid = Cabang::model()->checkCabangId();
        if ($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()) {
            $and = '';
        } else {
            $and = " AND v.cabang_id = '$cabangid' ";
        }
        $dbperiode = Periode::getPeriodeStartEnd($_POST['periode_id']);
        $periodenow = $dbperiode->queryRow();
        $periodeStart = substr($periodenow['ps'], 0, 10);
        $periodeEnd = substr($periodenow['pe'], 0, 10);

        $dbcmd = DbCmd::instance()->addFrom('{{validasi}} v')
            ->addSelect('v.validasi_id, v.pin_id, v.PIN, v.pegawai_id, v.cabang_id as cabang_validasi_id')
            ->addSelect('v.in_time, v.out_time, v.min_early_time, v.min_late_time, v.min_least_time, v.min_over_time_awal, v.min_over_time, (v.min_over_time + v.min_over_time_awal) as total_lembur')
            ->addSelect('v.real_lembur_pertama as min_over_time_awal_real, v.real_lembur_akhir as min_over_time_real, v.real_lembur_hari as lembur_hari, v.real_less_time')
            ->addSelect('v.tdate, v.kode_ket, v.no_surat_tugas, v.status_int, v.user_id, v.status_pegawai_id, v.shift_id, v.approval_lembur, v.approval_lesstime')
            ->addSelect('v.cuti_tahun')
            ->addSelect('p.cabang_id as cabang_id, c.kode_cabang, c.nama_cabang')
            ->addSelect('pp.cabang_id as cabang_user, u.pegawai_id as pegawai_user, v.cuti, v.limit_lembur, v.short_shift, v.status_wfh')
            ->addLeftJoin('{{pegawai}} p', 'p.pegawai_id = v.pegawai_id')
            ->addLeftJoin('{{cabang}} c', 'c.cabang_id = p.cabang_id')
            ->addLeftJoin('{{users}} u', 'u.id = v.user_id')
            ->addLeftJoin('{{pegawai}} pp', 'pp.pegawai_id = u.pegawai_id')
            ->addCondition("v.status_int = 1 AND v.pegawai_id = :pegawai_id $and")
            ->addCondition("v.in_time >= :in_time AND v.out_time <= :out_time")
            ->addParam(':pegawai_id', $_POST['pegawai_id'])
            ->addParam(':in_time', date('Y-m-d', (strtotime('-1 day', strtotime($periodeStart)))) . " 00:00:00")
            ->addParam(':out_time', date('Y-m-d', (strtotime('+1 day', strtotime($periodeEnd)))) . " 23:59:59")
            ->addOrder('kode_cabang, v.pin_id, v.in_time');
        $model = $dbcmd->getQuery();
        $model = $dbcmd->queryAll();
        $cnt = count($model);

        $periode = Yii::app()->db->createCommand("select
	t.selected_date tgl,
        dayname(t.selected_date) hari,
        concat(day(t.selected_date), ' ', monthname(t.selected_date)) tgl_hari
        from(
            " . Periode::getAllDate($periodeStart, $periodeEnd)->getQuery() . "
        ) t")->queryAll(true);

        $result = [];
        foreach ($model as $model1) {
            $result[] = $model1;
        }

        $hsl = [];
        foreach ($periode as $value) {
            $arr = [];

            foreach ($result as $v) {
                $a = substr($v['in_time'], 0, 10);
                if ($value['tgl'] == $a) {
                    $arr = $v;
                }
            }
            $arr['tgl'] = $value['tgl'];
            $arr['hari'] = $value['hari'];
            $arr['tgl_hari'] = $value['tgl_hari'];
            $hsl[] = $arr;
        }

        $rows = [];
        $row = [];
        foreach ($hsl as $v) {
            if ($v['hari'] == 'Sunday') {
                if (count($row) > 0) {
                    $rows[] = $row;
                }
                $row = [];
            }
            $row[$v['hari']] = $v;
        }

        if ($cnt < 1) {
            $rows[] = $row;
        } else {
            if (count($row) > 0) {
                $rows[] = $row;
            }
        }

        $kirim['results'] = $rows;
        $kirim['total'] = count($rows);
        echo json_encode($kirim);
    }

    public function actionAssignValidasi()
    {
        /*
         * Check Lock dan Post
         */
        LockPost::model()->check_lockpost($_POST['periode_id']);
        /*
         * End Check
         */
        $validasi_id = $_POST['validasi_id'];
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            Yii::app()->db->createCommand(
                "UPDATE pbu_validasi SET status_int = 2 WHERE validasi_id = '" . $validasi_id . "'")
                ->execute();
            $status = true;
            $msg = "Data diinvalidasi!";
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }

    public function actionApproveLembur()
    {
        /*
         * Check Lock dan Post
         */
        LockPost::model()->check_lockpost($_POST['periode_id']);
        /*
         * End Check
         */
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $approval = $_POST['status_approval'];
            $model = Validasi::model()->findByPk($_POST['validasi_id']);
            $msg = '';
            if ($approval == 'Disapproved') {
                $lemburgabung = $_POST['lembur_diakui'];
                if ($lemburgabung > GET_BATAS_OVERTIME() && $lemburgabung >= GET_BATAS_LEMBUR_HARI()) {
                    $model->min_over_time = $lemburgabung - 60;
                    $model->real_lembur_pertama = 0;
                    $model->real_lembur_akhir = 0;
                    $model->real_lembur_hari = 1;
                } else if ($lemburgabung > GET_BATAS_OVERTIME() && $lemburgabung < GET_BATAS_LEMBUR_HARI()) {
                    $model->real_lembur_pertama = 60;
                    $model->real_lembur_akhir = $_POST['limit'] == 'YA' ? $lemburgabung - 60 : GET_BATAS_OVERTIME();
                    $model->real_lembur_hari = 0;
                } else if ($lemburgabung <= GET_BATAS_OVERTIME()) {
                    if ($lemburgabung > 60) {
                        $model->real_lembur_pertama = 60;
                        $model->real_lembur_akhir = $lemburgabung - 60;
                        $model->real_lembur_hari = 0;
                    } else {
                        $model->real_lembur_pertama = $lemburgabung;
                        $model->real_lembur_akhir = 0;
                        $model->real_lembur_hari = 0;
                    }
                }
                $model->approval_lembur = 1;
                $msg = 'Data berhasil di Approve.';
            } else if ($approval == 'Approved') {
                $model->approval_lembur = 0;
                $msg = 'Data berhasil di Disapprove.';
            }
            if (!$model->save()) {
                throw new Exception('GAGAL MENYIMPAN KARENA ' . CHtml::errorSummary($model));
            }
            $status = true;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }

    public function actionApproveLesstime()
    {
        /*
         * Check Lock dan Post
         */
        LockPost::model()->check_lockpost($_POST['periode_id']);
        /*
         * End Check
         */
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $approval = $_POST['status_approval'];
            $model = Validasi::model()->findByPk($_POST['validasi_id']);
            $msg = '';
            if ($approval == 'Disapproved') {
                $lesstime = $_POST['lesstime_diakui'];
                $model->real_less_time = $lesstime;
                $model->approval_lesstime = 1;

                $msg = 'Data berhasil di Approve.';
            } else if ($approval == 'Approved') {
                $model->approval_lesstime = 0;
                $msg = 'Data berhasil di Disapprove.';
            }
            $model->save();
            $status = true;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }

    public function actionRecalculateLembur()
    {
        /*
         * Check Lock dan Post
         */
        LockPost::model()->check_lockpost($_POST['periode_id']);
        /*
         * End Check
         */
        $msg = '';
        $param2 = '';
        if (isset($_POST['param2'])) {
            $param2 = $_POST['param2'];
        }
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = Validasi::model()->findByPk($_POST['validasi_id']);
            $pegawai_id = $model->pegawai_id;
            $shift_id = $model->shift_id;
            $day_intime = $model->in_time;
            $day_outtime = $model->out_time;

            $duras = Fp::model()->durasi($pegawai_id, $shift_id, $day_intime, $day_outtime, '', $param2);

            $model->real_lembur_pertama = $duras['real_lembur_pertama'];
            $model->real_lembur_akhir = $duras['real_lembur_akhir'];
            $model->real_lembur_hari = $duras['real_lembur_hari'];
            $model->real_less_time = $duras['real_less_time'];
            $model->short_shift = 0;
            if (!$model->save()) {
                throw new Exception('GAGAL RECALCULATE KARENA ' . CHtml::errorSummary($model));
            }
            $status = true;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'lembur1' => $duras['real_lembur_pertama'],
            'lembur2' => $duras['real_lembur_akhir'],
            'lemburh' => $duras['real_lembur_hari'],
            'lesstime' => $duras['real_less_time']
        ));
        Yii::app()->end();
    }

    public function actionRecalculateLemburAll()
    {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {

            $msg = 'Berhasil di recalculate';
            $cabang_id = $_POST['cabang'];
            $model1 = Validasi::model()->findAll("periode_id = '" . $_POST['periode_id'] . "' "
                . "AND cabang_id = '$cabang_id' "
                . "AND status_int = 1 ");
            foreach ($model1 as $model) {
                $nik = Pegawai::model()->findByPk($model['pegawai_id'])->nik;
                $shift_id = $model['shift_id'];
                $day_intime = $model['in_time'];
                $day_outtime = $model['out_time'];

                if ($model->short_shift != 0)
                    $short = '6jam';
                else
                    $short = '';

                $duras = Fp::model()->durasi($nik, $shift_id, $day_intime, $day_outtime, $short, $_POST['limitLembur']);

                $model['real_lembur_pertama'] = $duras['real_lembur_pertama'];
                $model['real_lembur_akhir'] = $duras['real_lembur_akhir'];
                $model['real_lembur_hari'] = $duras['real_lembur_hari'];
                $model['real_less_time'] = $duras['real_less_time'];

                if (!$model->save()) {
                    throw new Exception('GAGAL RECALCULATE KARENA ' . CHtml::errorSummary($model));
                }
            }
            $status = true;
            $transaction->commit();
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }

}
