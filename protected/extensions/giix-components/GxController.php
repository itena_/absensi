<?php
/**
 * GxController class file.
 *
 * @author Rodrigo Coelho <giix@rodrigocoelho.com.br>
 * @link http://rodrigocoelho.com.br/giix/
 * @copyright Copyright &copy; 2010 Rodrigo Coelho
 * @license http://rodrigocoelho.com.br/giix/license/ New BSD License
 */
/**
 * GxController is the base class for the generated controllers.
 *
 * @author Rodrigo Coelho <giix@rodrigocoelho.com.br>
 * @since 1.0
 */
abstract class GxController extends CController {
    public $layout = 'plain';
    //public $layout = '//layouts/column1';
    public $menuz = array();
    public $menuzrs = array();
    /**
     * @var array Context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array The breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public function filters() {
        return array(
            'accessControl',
        );
    }
    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
                'actions' => array('login'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param mixed $id the ID of the model to be loaded
     * @param string $modelClass the model class name
     * @return GxActiveRecord the loaded model
     * @throws CHttpException
     */
    public function loadModel($id, $modelClass) {
        $model = GxActiveRecord::model($modelClass)->findByPk($id);
        if ($model === null)
                throw new CHttpException(404,
            Yii::t('app', 'The requested page does not exist.'));
        return $model;
    }
    /**
     * Performs the AJAX validation.
     * @param CModel $model the model to be validated
     * @param string $form the name of the form
     */
    protected function performAjaxValidation($model, $form) {
        if (Yii::app()->request->isAjaxRequest && $_POST['ajax'] == $form) {
            echo GxActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    /**
     * Finds the related primary keys specified in the form post.
     * Only for HAS_MANY and MANY_MANY relations.
     * @param array $form The post data.
     * @param array $relations A list of model relations.
     * @return array An array where the keys are the relation names (string) and the values arrays with the related model primary keys (int|string) or composite primary keys (array with pk name (string) => pk value (int|string)).
     * Example of returned data:
     * array(
     *   'categories' => array(1, 4),
     *   'tags' => array(array('id1' => 3, 'id2' => 7), array('id1' => 2, 'id2' => 0)) // composite pks
     * )
     * An empty array is returned in case there is no related pk data from the post.
     */
    protected function getRelatedData($form, $relations) {
        $relatedPk = array();
        foreach ($relations as $relationName => $relationData) {
            if (isset($form[$relationName]) && (($relationData[0] == GxActiveRecord::HAS_MANY)
                    || ($relationData[0] == GxActiveRecord::MANY_MANY)))
                    $relatedPk[$relationName] = $form[$relationName] === '' ? null
                            : $form[$relationName];
        }
        return $relatedPk;
    }
    public function renderJson($model, $total) {

        $argh = array();

        foreach ($model AS $dodol) {
            //foreach($model AS $SJP)
            $argh[] = $dodol->getAttributes();
        };
        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($argh) . '}';
        Yii::app()->end($jsonresult);
    }
    public function renderJsonArr($model = array()) {
        $jsonresult = '{"total":"' . count($model) . '","results":' . json_encode($model) . '}';
        Yii::app()->end($jsonresult);
    }
    public function renderJsonArrWithTotal($model = array(),$total) {
        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($model) . '}';
        Yii::app()->end($jsonresult);
    }
    public function generate_uuid(){
        $command = Yii::app()->db->createCommand("SELECT UUID();");
        return $command->queryScalar();
    }

    public function getAllCabangInBu($bu_id) {
        $cmd = new DbCmd();
        $cmd->addSelect("cabang_id")
            ->addFrom("{{cabang}}")
            ->addCondition("bu_id = :bu_id")
            ->addParam(":bu_id", $bu_id);

        return $cmd;
    }
    public static function getSecuriyRolesId(){
        $id     = Yii::app()->user->getId();
        $sr_id  = Users::model()
            ->findByAttributes(array('id' => $id))
            ->security_roles_id;

        return $sr_id;
    }
    private static function getStringCabang($cabang_id) {
        $cabangs = Cabang::model()->findAllByAttributes([
            'bu_id' => Cabang::model()->findByPk($cabang_id)->bu_id
        ]);

        $c      = "";
        $i      = 1;
        $count  = count($cabangs);
        foreach ($cabangs as $cb) {
            if($i < $count)
                $c .= "'" . $cb->cabang_id . "',";
            else
                $c .= "'" . $cb->cabang_id . "'";
            $i++;
        }
        return $c;
    }
    /*
     * Function X
     */
    public static function X($periodeStart, $periodeEnd, $pegawai_id, $periode_id) {
        Yii::import('application.models.Store');
        date_default_timezone_set('Asia/Jakarta');
        $result             = [];
        $pegawai            = Pegawai::model()->findByPk($pegawai_id);
        $cabang_id          = $pegawai->cabang_id;
        $kelompok_pegawai   = $pegawai->kelompok_pegawai;

        $check = TransferPegawai::model()->findAll("pegawai_id = :pegawai_id AND periode_id = :periode_id AND visible = 1",[
            ':periode_id' => $periode_id, ':pegawai_id' => $pegawai_id
        ]);

        $a = [];
        if(count($check) != 0){
            foreach ($check as $c){
                $a[] = $c->cabang_transfer_id;
            }
        }
//        else {
//            $a[] = $cabang_id;
//        }
        $a[] = $cabang_id;

        $cbgusr         = Cabang::model()->checkCabangId();
        $security_roles = self::getSecuriyRolesId();

        $cabang_pusat_ip = Cabang::model()->findByAttributes(['bu_id' => Cabang::model()->findByPk($cabang_id)->bu_id, 'kepala_cabang_stat' => 1])->cabang_id;

        if($cabang_pusat_ip != $cbgusr) {
            if ($security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()) {
                $c             = self::getStringCabang($cabang_id);
                $fp_per_cabang = " AND cabang_id in ($c)";
            } else {
                $fp_per_cabang = " AND cabang_id = '$cbgusr'";
            }
        } else {
            $c             = self::getStringCabang($cabang_id);
            $fp_per_cabang = " AND cabang_id in ($c)";
        }

//        $a[] = $cbgusr;

        $dbcmd = new DbCmd('{{shift}} s');
        $dbcmd->addSelect("*");
        $dbcmd->addCondition("kelompok_shift = :kelompok_pegawai AND active = 1");
        $dbcmd->addParams([':kelompok_pegawai' => $kelompok_pegawai]);
        $dbcmd->addInCondition("cabang_id", $a);
        $q      = $dbcmd->getQuery();
        $shift  = $dbcmd->queryAll();

        $tglcut1str = $periodeStart;
        $tglcut2str = $periodeEnd;
        $tglcut1dt  = new DateTime($tglcut1str);
        $tglcut2dt  = new DateTime($tglcut2str);
        $interval   = $tglcut1dt->diff($tglcut2dt);

        for ($x = 0; $x <= $interval->format('%a'); $x++) {
            $tgl = new DateTime($tglcut1dt->format('Y-m-d H:i:s'));
            $tgl->add(new DateInterval('P' . $x . 'D'));

            foreach ($shift as $row) {
                if (isset($row)){
                    $expit = explode(':', $row['in_time']);
                    $expeit = explode(':', $row['earlyin_time']);
                    $explit = explode(':', $row['latein_time']);
                    $expot = explode(':', $row['out_time']);
                    $expeot = explode(':', $row['earlyout_time']);
                    $explot = explode(':', $row['lateout_time']);

                    $timeindt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $timeindt->setTime($expit[0], $expit[1], $expit[2]);

                    $earlytimeindt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $earlytimeindt->setTime($expeit[0], $expeit[1], $expeit[2]);

                    $latetimeindt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $latetimeindt->setTime($explit[0], $explit[1], $explit[2]);

                    $timeoutdt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $timeoutdt->setTime($expot[0], $expot[1], $expot[2]);

                    $earlytimeoutdt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $earlytimeoutdt->setTime($expeot[0], $expeot[1], $expeot[2]);

                    $latetimeoutdt = new DateTime($tgl->format('Y-m-d H:i:s'));
                    $latetimeoutdt->setTime($explot[0], $explot[1], $explot[2]);

                    if ($earlytimeindt > $timeindt) {
                        $earlytimeindt->sub(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($latetimeindt < $timeindt) {
                        $latetimeindt->add(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($timeindt > $timeoutdt) {
                        $timeoutdt->add(new DateInterval('P' . 1 . 'D'));
                        $earlytimeoutdt->add(new DateInterval('P' . 1 . 'D'));
                        $latetimeoutdt->add(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($earlytimeoutdt > $timeoutdt) {
                        $earlytimeoutdt->sub(new DateInterval('P' . 1 . 'D'));
                    }
                    if ($latetimeoutdt < $timeoutdt) {
                        $latetimeoutdt->add(new DateInterval('P' . 1 . 'D'));
                    }

                    $preview = self::previewX($fp_per_cabang);

                    $dtei = $earlytimeindt->format('Y-m-d H:i:s');
                    $dtli = $latetimeindt->format('Y-m-d H:i:s');
                    $dteo = $earlytimeoutdt->format('Y-m-d H:i:s');
                    $dtlo = $latetimeoutdt->format('Y-m-d H:i:s');

                    $preview->addParams([
                        ':dtei' => $dtei,
                        ':dtli' => $dtli,
                        ':dteo' => $dteo,
                        ':dtlo' => $dtlo,
                        ':pegawai_id' => $pegawai_id
                    ]);
                    $queryX = $preview->getQuery();

                    $previewx = $preview->queryAll(true, array(
                        ':dtei' => $dtei,
                        ':dtli' => $dtli,
                        ':dteo' => $dteo,
                        ':dtlo' => $dtlo,
                        ':pegawai_id' => $pegawai_id
                    ));

                    foreach ($previewx as $xf) {
//                        $cabang_id = Cabang::model()->checkCabangId();

                        $rslt = [];
                        $rslt['PIN']            = $xf['PIN'];
                        $rslt['jam_in']         = $xf['jam_in'];
                        $rslt['jam_out']        = $xf['jam_out'];
                        $rslt['shift_id']       = $row['shift_id'];
                        $rslt['kode_shift']     = $row['kode_shift'];
                        $rslt['in_time']        = $row['in_time'];
                        $rslt['out_time']       = $row['out_time'];
                        $rslt['status_int_in']  = $xf['status_int_in'];
                        $rslt['status_int_out'] = $xf['status_int_out'];
                        $rslt['tipe_data_in']   = $xf['tipe_data_in'];
                        $rslt['tipe_data_out']  = $xf['tipe_data_out'];
                        $rslt['Status_in']      = $xf['Status_in'];
                        $rslt['Status_out']     = $xf['Status_out'];
                        $rslt['kode_ket_in']    = $xf['kode_ket_in'];
                        $rslt['kode_ket_out']   = $xf['kode_ket_out'];
                        $rslt['log_in']         = $xf['log_in'];
                        $rslt['log_out']        = $xf['log_out'];
                        $rslt['fp_id_in']       = $xf['fp_id_in'];
                        $rslt['fp_id_out']      = $xf['fp_id_out'];
                        $rslt['cabang_id']      = $xf['cabang_id'];
                        if($cabang_pusat_ip == $cbgusr || $security_roles == GET_SECURITY_ROLE_ADMINISTRATOR() || $security_roles == GET_SECURITY_ROLE_HRD()) {
                            $result[] = $rslt;
                        } else {
                            $transfer_pegawai = TransferPegawai::model()->checkPegawai($periode_id, $pegawai_id, $xf['jam_in'], $xf['jam_out']);
                            if(isset($transfer_pegawai)){
                                if($cbgusr != $transfer_pegawai->cabang_transfer_id){
                                    continue;
                                }
                                else if ($cbgusr == $transfer_pegawai->cabang_transfer_id){
                                    $check_shift_cabang = Shift::model()->findByAttributes([
                                        'shift_id' => $row['shift_id'],
                                        'cabang_id' => $transfer_pegawai->cabang_transfer_id
                                    ]);
                                    if ($check_shift_cabang != null)
                                        $result[] = $rslt;
                                } else {
                                    continue;
                                }
                            } else if ($cbgusr == $xf['cabang_id']){
                                $check_shift_cabang = Shift::model()->findByAttributes([
                                    'shift_id' => $row['shift_id'],
                                    'cabang_id' => $xf['cabang_id']
                                ]);
                                if ($check_shift_cabang != null)
                                    $result[] = $rslt;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }
    private static function previewX($fp_per_cabang) {
        $db2 = new DbCmd();
        $db2->addSelect("aa.fp_id AS fp_id_in,
                                    aa.PIN,
                                    Min(aa.DateTime_) jam_in,
                                    aa.status_int AS status_int_in,
                                    aa.kode_ket AS kode_ket_in,
                                    aa.log AS log_in,
                                    aa.tipe_data AS tipe_data_in,
                                    aa.Status AS Status_in,
                                    aa.cabang_id
                                ")
            ->addFrom("{{fp}} as aa")
            ->addCondition("aa.DateTime_ > :dtei
                                    AND aa.DateTime_ < :dtli
                                    AND aa.status_int != 2 
                                    $fp_per_cabang")
            ->addGroup("aa.PIN")
            ->addOrder("aa.DateTime_ ASC")
        ;

        $db3 = new DbCmd();
        $db3->addSelect("bb.fp_id AS fp_id_out,
                            bb.PIN,
                            Min(bb.DateTime_) jam_out,
                            bb.status_int AS status_int_out,
                            bb.kode_ket AS kode_ket_out,
                            bb.log AS log_out,
                            bb.tipe_data AS tipe_data_out,
                            bb.Status AS Status_out
                        ")
            ->addFrom("{{fp}} as bb")
            ->addCondition("bb.DateTime_ > :dteo
                            AND bb.DateTime_ < :dtlo
                            AND bb.status_int != 2 
                            $fp_per_cabang")
            ->addGroup("bb.PIN")
            ->addOrder("bb.DateTime_ ASC")
        ;

        $db1 = new DbCmd();
        $db1->addSelect("a.PIN, a.jam_in,b.jam_out, 
                        a.status_int_in, b.status_int_out,
                        a.kode_ket_in, b.kode_ket_out,
                        a.log_in, b.log_out,
                        a.tipe_data_in, b.tipe_data_out,
                        a.Status_in, b.Status_out,
                        a.fp_id_in, b.fp_id_out, a.cabang_id")
            ->addFrom("(" . $db2->getQuery() . ") as a")
            ->addInnerJoin("(" . $db3->getQuery() . ") as b", "a.PIN = b.PIN")
            ->addInnerJoin("{{pin}} c","c.PIN=a.PIN")
            ->addCondition("c.pegawai_id= :pegawai_id")
        ;

        $db = new DbCmd();
        $db->addSelect("PIN, jam_in,jam_out, 
                     status_int_in, status_int_out,
                        kode_ket_in, kode_ket_out,
                        tipe_data_in, tipe_data_out,
                        Status_in, Status_out,
                        fp_id_in, fp_id_out, log_in, log_out, cabang_id")
            ->addFrom("(" . $db1->getQuery() . ") as a1")
            ->addOrder("a1.PIN ASC")
        ;

        return $db;
    }

    public function makeStringCabangFromBuId($bu_id) {
        $c = '';
        foreach (Cabang::model()->findAll("bu_id = '$bu_id'") as $cbg) {
            $c .= "'".$cbg->cabang_id."',";
        }
        $c = substr($c, 0, -1);
        return $c;
    }

}