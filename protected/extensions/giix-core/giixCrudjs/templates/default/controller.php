<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass; ?> {

<?php
$authpath = 'ext.giix-core.giixCrud.templates.default.auth.';
Yii::app()->controller->renderPartial($authpath . $this->authtype);
?>
public function actionCreate() {
$model = new <?php echo $this->modelClass; ?>;
<?php if ($this->enable_ajax_validation): ?>
    $this->performAjaxValidation($model, '<?php echo $this->class2id($this->modelClass) ?>-form');
<?php endif; ?>
if (!Yii::app()->request->isAjaxRequest)
return;
if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['<?php echo $this->modelClass; ?>'][$k] = $v;
}
$model->attributes = $_POST['<?php echo $this->modelClass; ?>'];
$msg = "Data gagal disimpan.";
<?php if ($this->hasManyManyRelation($this->modelClass)): ?>
    $relatedData = <?php echo $this->generateGetPostRelatedData($this->modelClass,
        4); ?>;
<?php endif; ?>

<?php if ($this->hasManyManyRelation($this->modelClass)): ?>
    if ($model->saveWithRelated($relatedData)) {
<?php else: ?>
    if ($model->save()) {
<?php endif; ?>
$status = true;
$msg = "Data berhasil di simpan dengan id " . $model-><?php echo $this->tableSchema->primaryKey; ?>;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg));
Yii::app()->end();

}

}

public function actionUpdate($id) {
$model = $this->loadModel($id, '<?php echo $this->modelClass; ?>');

<?php if ($this->enable_ajax_validation): ?>
    $this->performAjaxValidation($model, '<?php echo $this->class2id($this->modelClass) ?>-form');
<?php endif; ?>

if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['<?php echo $this->modelClass; ?>'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['<?php echo $this->modelClass; ?>'];
<?php if ($this->hasManyManyRelation($this->modelClass)): ?>
    $relatedData = <?php echo $this->generateGetPostRelatedData($this->modelClass,
        4); ?>;
<?php endif; ?>

<?php if ($this->hasManyManyRelation($this->modelClass)): ?>
    if ($model->saveWithRelated($relatedData)) {
<?php else: ?>
    if ($model->save()) {
<?php endif; ?>

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model-><?php echo $this->tableSchema->primaryKey; ?>;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model-><?php echo $this->tableSchema->primaryKey; ?>));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, '<?php echo $this->modelClass; ?>')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}
$model = <?php echo $this->modelClass; ?>::model()->findAll($criteria);
$total = <?php echo $this->modelClass; ?>::model()->count($criteria);

$this->renderJson($model, $total);

}

}