<?php
//require_once('settings.php');
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
define('MYSQL', 'C:\xampp\mysql\bin\mysql');
define('MYSQLDUMP', 'C:\xampp\mysql\bin\mysqldump');
define('GZIP', 'C:\xampp\gzip');
define('SOAP_CUSTOMER','http://localhost:81/siena/query/quote');
define('PHP_EXE','php');
//define('PHP_EXE','d:\xampp\php\php.exe'); //untuk testing //budi

define('FP_NON_AKTIF',9);

global $systypes_array;
$systypes_array = array(
);
$class_types = array(
);

/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}

/**
 * @param $number
 * @return int
 */
function is_angka($number)
{
    if (strlen($number) == 0) {
        return false;
    }
    switch (gettype($number)) {
        case "NULL":
            return false;
        case "resource":
            return false;
        case "object":
            return false;
        case "array":
            return false;
        case "unknown type":
            return false;
    }
    return preg_match("/^-?([\$]?)([0-9,\s]*\.?[0-9]{0,2})$/", $number);
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
    return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null)
{
    static $baseUrl;
    if ($baseUrl === null) {
        $baseUrl = Yii::app()->getRequest()->getBaseUrl();
    }
    return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name)
{
    return Yii::app()->params[$name];
}

function Encrypt($string)
{
    return base64_encode(Yii::app()->getSecurityManager()->encrypt($string));
}

function Decrypt($string)
{
    return Yii::app()->getSecurityManager()->decrypt(base64_decode($string));
}

function Now($formatDate = 'yyyy-MM-dd')
{
    return get_date_today($formatDate) . ' ' . get_time_now();
}

function get_number($number)
{
    return str_replace(",", "", $number);
}

function sql2date($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    return Yii::app()->dateFormatter->format($format, $timestamp);
}

function get_time_now()
{
    return Yii::app()->dateFormatter->format('HH:mm:ss', time());
}

function get_date_today($format = 'yyyy-MM-dd')
{
    return Yii::app()->dateFormatter->format($format, time());
}

function logToFile($text, $file = 'error.log', $flags = FILE_APPEND) {
    file_put_contents('./protected/runtime/'.$file, $text, $flags);
}

function console($command, $action, $option = null, $log) {
//    $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . 'yiic';
//    $cmd = PHP_EXE . ' ' . $yiic . ' ' . $command;
//
//    if (substr(php_uname(), 0, 7) == "Windows") {
//        pclose(popen('start /B cmd /C "' . $cmd . '"', "r"));
//    } else {
//        exec($cmd . ' &');
//    }
    $log = Yii::app()->basePath . DS . 'runtime' . DS . $log;
    $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . 'yiic';
    if ($log == '' || $log == null) {
        $cmd = PHP_EXE . ' ' . $yiic . ' ' . $command . ' ' . $action . ' ' . $option;
    } else {
        $cmd = PHP_EXE . ' ' . $yiic . ' ' . $command . ' ' . $action . ' ' . $option . ' >> ' . $log . ' 2>&1';
    }
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen('start /B cmd /C "' . $cmd . '"', "r"));
    } else {
        exec($cmd . ' &');
    }
}

function get_status_pegawai_aktif() {
    $arr    = [];
    $data   = StatusPegawai::model()->findAll("kode = 'AKTIF' OR kode = 'TRAINING'");
    foreach ($data as $dt) {
        $arr[] = $dt->status_pegawai_id;
    }

    return $arr;
}

function get_status_pegawai_non_aktif() {
    $arr    = [];
    $data   = StatusPegawai::model()->findAll("kode != 'AKTIF' AND kode != 'TRAINING'");
    foreach ($data as $dt) {
        $arr[] = $dt->status_pegawai_id;
    }

    return $arr;
}

function get_status_pegawai_resign() {
    return StatusPegawai::model()->findByAttributes(["kode" => 'RESIGN'])->status_pegawai_id;
}

function GET_BATAS_OVERTIME() {
    return SysPrefs::model()->findByAttributes(['name_' => 'BATAS_OVERTIME'])->value_;
}

function GET_BATAS_LEMBUR_HARI() {
    return SysPrefs::model()->findByAttributes(['name_' => 'BATAS_LEMBUR_HARI'])->value_;
}

function GET_PENGALI_LESSTIME() {
    return SysPrefs::model()->findByAttributes(['name_' => 'PENGALI_LESSTIME'])->value_;
}

function GET_ALLOW_ADD() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'ALLOW_ADD'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'ALLOW_ADD value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_SET_INTIME_BUAT_REVIEW2() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'SET_INTIME_BUAT_REVIEW2'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'SET_INTIME_BUAT_REVIEW2 value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_ALL_CABANG() {
    return SysPrefs::model()->findByAttributes(['name_' => 'ALL_CABANG'])->value_;
}

function GET_JATAH_CUTI_TAHUNAN() {
    return SysPrefs::model()->findByAttributes(['name_' => 'JATAH_CUTI_TAHUNAN'])->value_;
}

function GET_TAMPILKAN_SISA_CUTI() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'TAMPILKAN_SISA_CUTI'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'TAMPILKAN_SISA_CUTI value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_INVTERVAL_ABSEN() {
    return SysPrefs::model()->findByAttributes(['name_' => 'INVTERVAL_ABSEN'])->value_;
}

function GET_CHOOSE_SHIFT_PHASE1() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'CHOOSE_SHIFT_PHASE1'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'CHOOSE_SHIFT_PHASE1 value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_NON_EMPLOYEE_ONLY() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'NON_EMPLOYEE_ONLY'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'NON_EMPLOYEE_ONLY value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_SECURITY_ROLE_ADMINISTRATOR() {
    return SecurityRoles::model()->findByAttributes(['role' => 'System Administrator'])->security_roles_id;
}

function GET_SECURITY_ROLE_HRD() {
    return SecurityRoles::model()->findByAttributes(['role' => 'HRD'])->security_roles_id;
}

function GET_FP_PYTHON() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'FP_PYTHON'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'FP_PYTHON value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_USE_STATUS_HK() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'USE_STATUS_HK'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'USE_STATUS_HK value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_SHOW_AMOS_PHASE_2() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'SHOW_AMOS_PHASE_2'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'SHOW_AMOS_PHASE_2 value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}

function GET_HITUNG_HTM() {
    $result = SysPrefs::model()->findByAttributes(['name_' => 'HITUNG_HTM'])->value_;
    if($result != 'true' && $result != 'false' )
        Log::generateErrorMessage(false,'HITUNG_HTM value in SysPrefs must either true or false!');

    return strtolower($result) == 'true' ? true : false;
}