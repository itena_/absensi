<?php

class m180804_064020_add_transfer_pegawai_and_some_indexes extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			CREATE INDEX `idx1` ON `pbu_fp`(`DateTime_`, `status_int`) USING BTREE ;
		")->execute();	
		
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_ip` ADD CONSTRAINT `pbu_ip_ibfk_1` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE CASCADE ON UPDATE CASCADE;
		")->execute();
		
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_periode` ADD CONSTRAINT `pbu_periode_ibfk_1` FOREIGN KEY (`jenis_periode_id`) REFERENCES `pbu_jenis_periode` (`jenis_periode_id`) ON DELETE CASCADE ON UPDATE CASCADE;
		")->execute();
		
		Yii::app()->db->createCommand("
			CREATE TABLE `pbu_transfer_pegawai` (
		`transfer_pegawai_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		`pegawai_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
		`cabang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
		`tglin`  date NOT NULL ,
		`tglout`  date NOT NULL ,
		`no_surat_tugas`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
		`visible`  tinyint(3) NULL DEFAULT 1 ,
		`cabang_transfer_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
		`periode_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
		`tdate`  datetime NULL DEFAULT NULL ,
		`user_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		`binding_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
		`status_kota`  tinyint(2) NOT NULL ,
		PRIMARY KEY (`transfer_pegawai_id`),
		CONSTRAINT `transfer_pegawai_fk1` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
		CONSTRAINT `transfer_pegawai_fk2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
		CONSTRAINT `transfer_pegawai_fk3` FOREIGN KEY (`cabang_transfer_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
		CONSTRAINT `transfer_pegawai_fk4` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
		INDEX `transfer_pegawai_fk1` (`pegawai_id`) USING BTREE ,
		INDEX `transfer_pegawai_fk2` (`cabang_id`) USING BTREE ,
		INDEX `transfer_pegawai_fk3` (`cabang_transfer_id`) USING BTREE ,
		INDEX `transfer_pegawai_fk4` (`periode_id`) USING BTREE 
		)
		ENGINE=InnoDB
		DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
		ROW_FORMAT=Compact
		;
		")->execute();
		 
	}

	public function down()
	{
		echo "m180804_064020_add_transfer_pegawai_and_some_indexes does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}