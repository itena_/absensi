<?php

class m180809_033509_add_cabang_id_di_shift extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_shift` ADD COLUMN `cabang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `over_time`;
			ALTER TABLE `pbu_shift` ADD CONSTRAINT `shift_fk1` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			CREATE INDEX `shift_fk1` ON `pbu_shift`(`cabang_id`) USING BTREE ;
			CREATE DEFINER=`root`@`127.0.0.1` TRIGGER `shift_befor_insert` BEFORE INSERT ON `pbu_shift`
			FOR EACH ROW BEGIN
			IF NEW.shift_id IS NULL OR LENGTH(NEW.shift_id) = 0 THEN
			  SET NEW.shift_id = UUID();
			END IF;
			END;
			
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_3` FOREIGN KEY (`cabang_transfer_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_4` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
		")->execute();	
	}

	public function down()
	{
		echo "m180809_033509_add_cabang_id_di_shift does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}