<?php

class m180818_043514_add_kelompok_pegawai_shift extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_pegawai` ADD COLUMN `kelompok_pegawai`  tinyint(1) NULL DEFAULT 0 COMMENT '0 Untuk Karyawan, 1 Untuk OB,OG atau Security' AFTER `store`;
			
			ALTER TABLE `pbu_shift` ADD COLUMN `kelompok_shift`  tinyint(1) NULL DEFAULT 0 COMMENT '0 Untuk Karyawan, 1 Untuk OB,OG atau Security' AFTER `cabang_id`;
			ALTER TABLE `pbu_shift` ADD CONSTRAINT `pbu_shift_ibfk_1` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
		")->execute();	
	}

	public function down()
	{
		echo "m180818_043514_add_kelompok_pegawai_shift does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}