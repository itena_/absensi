<?php

class m180821_084338_add extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			CREATE TABLE `pbu_lock_post` (
			`lock_post_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
			`periode_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
			`cabang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
			`locked`  tinyint(1) NULL DEFAULT 0 ,
			`locked_user_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
			`locked_tdate`  datetime NULL DEFAULT NULL ,
			`posted`  tinyint(1) NULL DEFAULT 0 ,
			`posted_user_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
			`posted_tdate`  datetime NULL DEFAULT NULL ,
			`bu_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
			PRIMARY KEY (`lock_post_id`),
			CONSTRAINT `lock_post_fk1` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
			CONSTRAINT `lock_post_fk2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
			INDEX `lock_post_fk1` (`periode_id`) USING BTREE ,
			INDEX `lock_post_fk2` (`cabang_id`) USING BTREE 
			)
			ENGINE=InnoDB
			DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
			ROW_FORMAT=Compact
		")->execute();	
	}

	public function down()
	{
		echo "m180821_084338_add does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}