<?php

class m180906_035148_change_jabatanid_to_active_in_pbushift extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_shift` ADD COLUMN `active`  tinyint(1) NULL DEFAULT 1 AFTER `kode_shift`;
			ALTER TABLE `pbu_shift` DROP COLUMN `jabatan_id`;
		")->execute();	
	}

	public function down()
	{
		echo "m180906_035148_change_jabatanid_to_active_in_pbushift does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}