<?php

class m181005_035003_add_pbu_log extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			CREATE TABLE `pbu_log` (
`user_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`tdate`  datetime NULL DEFAULT NULL ,
`log`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
`trans_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
ROW_FORMAT=Compact
;
		")->execute();	
	}

	public function down()
	{
		echo "m181005_035003_add_pbu_log does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}