<?php

class m190221_081325_modify_validasi_result extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
				ALTER TABLE `pbu_validasi` MODIFY COLUMN `result_id`  varchar(36) NULL DEFAULT NULL AFTER `status_int`;
				ALTER TABLE `pbu_validasi` ADD COLUMN `cuti`  tinyint(3) NULL DEFAULT 0 AFTER `cabang_id`;
				ALTER TABLE `pbu_result` MODIFY COLUMN `total_hari_kerja`  double NULL DEFAULT 0 AFTER `periode_id`;
ALTER TABLE `pbu_result` MODIFY COLUMN `locked`  tinyint(1) NULL DEFAULT 0 AFTER `total_hari_kerja`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_lk`  tinyint(3) NULL DEFAULT 0 AFTER `locked`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_sakit`  tinyint(3) NULL DEFAULT 0 AFTER `total_lk`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_off`  tinyint(3) NULL DEFAULT 0 AFTER `total_sakit`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_cuti_tahunan`  tinyint(3) NULL DEFAULT 0 AFTER `total_off`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_cuti_menikah`  tinyint(3) NULL DEFAULT 0 AFTER `total_cuti_tahunan`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_cuti_bersalin`  tinyint(3) NULL DEFAULT 0 AFTER `total_cuti_menikah`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_cuti_istimewa`  tinyint(3) NULL DEFAULT 0 AFTER `total_cuti_bersalin`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_cuti_non_aktif`  tinyint(3) NULL DEFAULT 0 AFTER `total_cuti_istimewa`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_min_lembur_awal`  int(6) NULL DEFAULT 0 AFTER `total_cuti_non_aktif`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_min_lembur_akhir`  int(6) NULL DEFAULT 0 AFTER `total_min_lembur_awal`;
ALTER TABLE `pbu_result` MODIFY COLUMN `total_min_late_time`  int(6) NULL DEFAULT 0 AFTER `total_min_lembur_akhir`;
ALTER TABLE `pbu_result` MODIFY COLUMN `cabang_id`  varchar(36) NOT NULL AFTER `total_real_less_time`;
ALTER TABLE `pbu_result` MODIFY COLUMN `user_id`  varchar(36) NOT NULL AFTER `cabang_id`;
		")->execute();	
	}

	public function down()
	{
		echo "m190221_081325_modify_validasi_result does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}