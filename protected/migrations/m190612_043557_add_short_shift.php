<?php

class m190612_043557_add_short_shift extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_validasi` ADD COLUMN `short_shift`  tinyint(3) NULL DEFAULT 0 AFTER `limit_lembur`;
		")->execute();
		Yii::app()->db->createCommand("
			ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `pbu_validasi_view` AS 
SELECT
	`v`.`validasi_id` AS `validasi_id`,
	`v`.`pin_id` AS `pin_id`,
	`v`.`PIN` AS `PIN`,
	`v`.`pegawai_id` AS `pegawai_id`,
	`v`.`cabang_id` AS `cabang_validasi_id`,
	`v`.`in_time` AS `in_time`,
	`v`.`out_time` AS `out_time`,
	`v`.`min_early_time` AS `min_early_time`,
	`v`.`min_late_time` AS `min_late_time`,
	`v`.`min_least_time` AS `min_least_time`,
	`v`.`min_over_time_awal` AS `min_over_time_awal`,
	`v`.`min_over_time` AS `min_over_time`,
	(
		`v`.`min_over_time` + `v`.`min_over_time_awal`
	) AS `total_lembur`,
	`v`.`real_lembur_pertama` AS `min_over_time_awal_real`,
	`v`.`real_lembur_akhir` AS `min_over_time_real`,
	`v`.`real_lembur_hari` AS `lembur_hari`,
	`v`.`real_less_time` AS `real_less_time`,
	`v`.`tdate` AS `tdate`,
	`v`.`kode_ket` AS `kode_ket`,
	`v`.`no_surat_tugas` AS `no_surat_tugas`,
	`v`.`status_int` AS `status_int`,
	`v`.`result_id` AS `result_id`,
	`v`.`user_id` AS `user_id`,
	`v`.`status_pegawai_id` AS `status_pegawai_id`,
	`v`.`shift_id` AS `shift_id`,
	`v`.`approval_lembur` AS `approval_lembur`,
	`v`.`approval_lesstime` AS `approval_lesstime`,
	`p`.`cabang_id` AS `cabang_id`,
	`c`.`kode_cabang` AS `kode_cabang`,
	`c`.`nama_cabang` AS `nama_cabang`,
	`pp`.`cabang_id` AS `cabang_user`,
	`u`.`pegawai_id` AS `pegawai_user`,
  `v`.`cuti` AS `cuti`,
	v.limit_lembur as limit_lembur,
v.short_shift as short_shift 
FROM
	(
		(
			(
				(
					`pbu_validasi` `v`
					LEFT JOIN `pbu_pegawai` `p` ON (
						(
							`p`.`pegawai_id` = `v`.`pegawai_id`
						)
					)
				)
				LEFT JOIN `pbu_cabang` `c` ON (
					(
						`c`.`cabang_id` = `p`.`cabang_id`
					)
				)
			)
			LEFT JOIN `pbu_users` `u` ON ((`u`.`id` = `v`.`user_id`))
		)
		LEFT JOIN `pbu_pegawai` `pp` ON (
			(
				`pp`.`pegawai_id` = `u`.`pegawai_id`
			)
		)
	)
WHERE
	(`v`.`status_int` = 1)
ORDER BY
	`v`.`PIN`,
	`c`.`kode_cabang` ;
		")->execute();
	}

	public function down()
	{
		echo "m190612_043557_add_short_shift does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}