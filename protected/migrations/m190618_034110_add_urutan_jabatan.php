<?php

class m190618_034110_add_urutan_jabatan extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_jabatan` ADD COLUMN `urutan`  tinyint(3) NULL DEFAULT 0 AFTER `nama_jabatan`;
		")->execute();
	}

	public function down()
	{
		echo "m190618_034110_add_urutan_jabatan does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}