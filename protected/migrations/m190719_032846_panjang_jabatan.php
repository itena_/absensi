<?php

class m190719_032846_panjang_jabatan extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		ALTER TABLE `pbu_jabatan`
MODIFY COLUMN `nama_jabatan`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `kode`;
")->execute();

	}

	public function down()
	{
		echo "m190719_032846_panjang_jabatan does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}