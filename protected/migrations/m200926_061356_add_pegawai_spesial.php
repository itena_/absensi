<?php

class m200926_061356_add_pegawai_spesial extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		CREATE TABLE `pbu_pegawai_spesial`  (
  `pegawai_spesial_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pegawai_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `finger_naava` tinyint(3) NULL DEFAULT NULL COMMENT 'pakai mesin finger naavagreen',
  `nik_lain` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'punya nik lain',
  `visible` tinyint(3) NULL DEFAULT 1,
  PRIMARY KEY (`pegawai_spesial_id`) USING BTREE,
  INDEX `ps_fk1`(`pegawai_id`) USING BTREE,
  CONSTRAINT `ps_fk1` FOREIGN KEY (`pegawai_id`) REFERENCES `am`.`pbu_pegawai` (`pegawai_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;
		")->execute();
	}

	public function down()
	{
		echo "m200926_061356_add_pegawai_spesial does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}