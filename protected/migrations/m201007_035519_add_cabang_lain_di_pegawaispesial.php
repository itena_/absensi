<?php

class m201007_035519_add_cabang_lain_di_pegawaispesial extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		ALTER TABLE `pbu_pegawai_spesial` ADD COLUMN `cabang` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `nik_lain`;
		")->execute();
	}

	public function down()
	{
		echo "m201007_035519_add_cabang_lain_di_pegawaispesial does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}