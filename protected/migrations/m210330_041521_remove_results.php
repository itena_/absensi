<?php

class m210330_041521_remove_results extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		SET FOREIGN_KEY_CHECKS=0;
		DROP VIEW `pbu_result_view`;
		DROP VIEW `pbu_result_view_2`;
		DROP TABLE `pbu_result`;
		SET FOREIGN_KEY_CHECKS=1;
		")->execute();
	}

	public function down()
	{
		echo "m210330_041521_remove_results does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}