<?php

class m210414_042809_add_amos extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `pbu_fp` DROP INDEX `fp_fk1`;

DROP TRIGGER `fp_before_insert`;

CREATE TRIGGER `fp_bfinsrt` BEFORE INSERT ON `pbu_fp` FOR EACH ROW BEGIN
IF NEW.fp_id IS NULL OR LENGTH(NEW.fp_id) = 0 THEN
  SET NEW.fp_id = UUID();
END IF;
END;

ALTER TABLE `pbu_ip` DROP FOREIGN KEY `cabang_id`;

ALTER TABLE `pbu_ip` ADD CONSTRAINT `pbu_ip_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pbu_jabatan` MODIFY COLUMN `nama_jabatan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `kode`;

ALTER TABLE `pbu_lock_post` DROP FOREIGN KEY `lock_post_fk1`;

ALTER TABLE `pbu_lock_post` DROP FOREIGN KEY `lock_post_fk2`;

ALTER TABLE `pbu_lock_post` ADD CONSTRAINT `pbu_lock_post_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pbu_lock_post` ADD CONSTRAINT `pbu_lock_post_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pbu_periode` DROP FOREIGN KEY `fk_pbu_periode`;

ALTER TABLE `pbu_periode` ADD CONSTRAINT `pbu_periode_ibfk_2` FOREIGN KEY (`jenis_periode_id`) REFERENCES `pbu_jenis_periode` (`jenis_periode_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pbu_shift` DROP FOREIGN KEY `shift_fk1`;

ALTER TABLE `pbu_shift` ADD CONSTRAINT `pbu_shift_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `transfer_pegawai_fk1`;

ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `transfer_pegawai_fk2`;

ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `transfer_pegawai_fk3`;

ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `transfer_pegawai_fk4`;

ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_5` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_6` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_7` FOREIGN KEY (`cabang_transfer_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_8` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pbu_validasi` DROP INDEX `validasi_fk1`;

ALTER TABLE `pbu_validasi` DROP FOREIGN KEY `validasi_fk1`;

ALTER TABLE `pbu_validasi` MODIFY COLUMN `in_time` datetime(0) NOT NULL AFTER `PIN`;

ALTER TABLE `pbu_validasi` MODIFY COLUMN `out_time` datetime(0) NOT NULL AFTER `in_time`;


SET FOREIGN_KEY_CHECKS=1;
		")->execute();
	}

	public function down()
	{
		echo "m210414_042809_add_amos does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}