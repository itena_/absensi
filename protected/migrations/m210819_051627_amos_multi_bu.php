<?php

class m210819_051627_amos_multi_bu extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_fp` MODIFY COLUMN `PIN`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `fp_id`;
			ALTER TABLE `pbu_fp` ADD CONSTRAINT `fp_idx1` FOREIGN KEY (`PIN`) REFERENCES `pbu_pegawai` (`nik`) ON DELETE CASCADE ON UPDATE CASCADE;
			CREATE INDEX `fp_idx1` ON `pbu_fp`(`PIN`) USING BTREE ;
			ALTER TABLE `pbu_lock_post` DROP FOREIGN KEY `pbu_lock_post_ibfk_1`;
			ALTER TABLE `pbu_lock_post` ADD CONSTRAINT `pbu_lock_post_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_lock_post` DROP FOREIGN KEY `pbu_lock_post_ibfk_2`;
			ALTER TABLE `pbu_lock_post` ADD CONSTRAINT `pbu_lock_post_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			DROP INDEX `idx_nik` ON `pbu_pegawai`;
			CREATE UNIQUE INDEX `idx_nik` ON `pbu_pegawai`(`nik`, `cabang_id`) USING BTREE ;
			ALTER TABLE `pbu_shift` DROP FOREIGN KEY `pbu_shift_ibfk_2`;
			ALTER TABLE `pbu_shift` ADD CONSTRAINT `pbu_shift_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `pbu_transfer_pegawai_ibfk_5`;
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_5` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `pbu_transfer_pegawai_ibfk_6`;
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_6` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `pbu_transfer_pegawai_ibfk_7`;
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_7` FOREIGN KEY (`cabang_transfer_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			ALTER TABLE `pbu_transfer_pegawai` DROP FOREIGN KEY `pbu_transfer_pegawai_ibfk_8`;
			ALTER TABLE `pbu_transfer_pegawai` ADD CONSTRAINT `pbu_transfer_pegawai_ibfk_8` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			DROP TABLE `pbu_result`;
		")->execute();
	}

	public function down()
	{
		echo "m210819_051627_amos_multi_bu does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}