<?php

class m210914_040516_modify_bu extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_bu` MODIFY COLUMN `jenis_usaha`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `tgl_berdiri`;
		")->execute();
	}

	public function down()
	{
		echo "m210914_040516_modify_bu does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}