<?php

class m211001_042819_modify_fp_terminal_id extends CDbMigration
{
	public function up()
	{	
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_fp` MODIFY COLUMN `terminal_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' AFTER `WorkCode`;
		")->execute();
	}

	public function down()
	{
		echo "m211001_042819_modify_fp_terminal_id does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}