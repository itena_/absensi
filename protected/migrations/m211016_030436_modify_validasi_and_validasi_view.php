<?php

class m211016_030436_modify_validasi_and_validasi_view extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_validasi` ADD COLUMN `status_wfh`  tinyint(3) NULL DEFAULT 0 AFTER `short_shift`;
		")->execute();
		Yii::app()->db->createCommand("
			ALTER 
			ALGORITHM=UNDEFINED 
			DEFINER=`root`@`127.0.0.1` 
			SQL SECURITY DEFINER 
			VIEW `pbu_validasi_view` AS 
				select `v`.`validasi_id` AS `validasi_id`,`v`.`pin_id` AS `pin_id`,`v`.`PIN` AS `PIN`,`v`.`pegawai_id` AS `pegawai_id`,`v`.`cabang_id` AS `cabang_validasi_id`,`v`.`in_time` AS `in_time`,`v`.`out_time` AS `out_time`,`v`.`min_early_time` AS `min_early_time`,`v`.`min_late_time` AS `min_late_time`,`v`.`min_least_time` AS `min_least_time`,`v`.`min_over_time_awal` AS `min_over_time_awal`,`v`.`min_over_time` AS `min_over_time`,(`v`.`min_over_time` + `v`.`min_over_time_awal`) AS `total_lembur`,`v`.`real_lembur_pertama` AS `min_over_time_awal_real`,`v`.`real_lembur_akhir` AS `min_over_time_real`,`v`.`real_lembur_hari` AS `lembur_hari`,`v`.`real_less_time` AS `real_less_time`,`v`.`tdate` AS `tdate`,`v`.`kode_ket` AS `kode_ket`,`v`.`no_surat_tugas` AS `no_surat_tugas`,`v`.`status_int` AS `status_int`,`v`.`result_id` AS `result_id`,`v`.`user_id` AS `user_id`,`v`.`status_pegawai_id` AS `status_pegawai_id`,`v`.`shift_id` AS `shift_id`,`v`.`approval_lembur` AS `approval_lembur`,`v`.`approval_lesstime` AS `approval_lesstime`,`p`.`cabang_id` AS `cabang_id`,`c`.`kode_cabang` AS `kode_cabang`,`c`.`nama_cabang` AS `nama_cabang`,`pp`.`cabang_id` AS `cabang_user`,`u`.`pegawai_id` AS `pegawai_user`,`v`.`cuti` AS `cuti`,`v`.`limit_lembur` AS `limit_lembur`,`v`.`short_shift` AS `short_shift` from ((((`pbu_validasi` `v` left join `pbu_pegawai` `p` on((`p`.`pegawai_id` = `v`.`pegawai_id`))) left join `pbu_cabang` `c` on((`c`.`cabang_id` = `p`.`cabang_id`))) left join `pbu_users` `u` on((`u`.`id` = `v`.`user_id`))) left join `pbu_pegawai` `pp` on((`pp`.`pegawai_id` = `u`.`pegawai_id`))) where (`v`.`status_int` = 1) order by `v`.`PIN`,`c`.`kode_cabang` ;
		")->execute();
		Yii::app()->db->createCommand("
			TRUNCATE TABLE pbu_sys_prefs;
		")->execute();		
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`) VALUES ('7fcfcd58-2e29-11ec-980f-588a5a1e729e', 'AMOS_VERSION', '3.2');
		")->execute();
	}

	public function down()
	{
		echo "m211016_030436_modify_validasi_and_validasi_view does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}