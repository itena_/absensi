<?php

class m211029_072452_updateversion33 extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			UPDATE pbu_sys_prefs SET value_ = '3.3' WHERE name_ = 'AMOS_VERSION';
		")->execute();
	}

	public function down()
	{
		echo "m211029_072452_updateversion33 does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}