<?php

class m211116_052118_move_settingsphp_to_sysprefs extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_sys_prefs` ADD COLUMN `note`  varchar(255) NOT NULL AFTER `value_`;
		")->execute();
		
		Yii::app()->db->createCommand("
			UPDATE pbu_sys_prefs SET value_ = '3.4' WHERE name_ = 'AMOS_VERSION';
		")->execute();
		
		Yii::app()->db->createCommand("
			UPDATE pbu_sys_prefs SET note = 'menampilkan versi amos ke berapa' WHERE name_ = 'AMOS_VERSION';
		")->execute();
		
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('060c8cd4-4433-11ec-b370-588a5a1e729e', 'ALL_CABANG', 'd2d94c22-d3f4-11e7-9982-507b9d297990', 'untuk cabang_id ADMINISTRATOR');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('310d4e32-442a-11ec-b370-588a5a1e729e', 'PENGALI_LESSTIME', '2', 'menit less time auto dikali dgn nilai value');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('4acbb8c6-444f-11ec-b370-588a5a1e729e', 'FP_PYTHON', 'true', 'true ambil fp pake PYTHON, false pake SOAP');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('5b6f56bf-4434-11ec-b370-588a5a1e729e', 'TAMPILKAN_SISA_CUTI', 'falses', 'false menampilkan sisa cuti, true menampilkan cuti yg diambil');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('88b250d6-4428-11ec-b370-588a5a1e729e', 'BATAS_OVERTIME', '180', 'menentukan batas maximal overtime');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('90716698-4435-11ec-b370-588a5a1e729e', 'ALLOW_ADD', 'false', 'false ga bisa manual add pegawai di AMOS, true bisa');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('9dc187c0-442b-11ec-b370-588a5a1e729e', 'SET_INTIME_BUAT_REVIEW2', 'false', 'kalo true ambil dari late_time, kalau false ambil dari in_time');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('cd863616-4434-11ec-b370-588a5a1e729e', 'INVTERVAL_ABSEN', '10', 'Menambahkan range out_time sebanyak value untuk tipe karyawan OB/OG/Sec');
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('f6b98003-4433-11ec-b370-588a5a1e729e', 'JATAH_CUTI_TAHUNAN', '12', 'jatah cuti setahun');
		")->execute();
	}

	public function down()
	{
		echo "m211116_052118_move_settingsphp_to_sysprefs does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}