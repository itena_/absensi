<?php

class m211116_093027_add_sysprefs_nonemployee extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			UPDATE pbu_sys_prefs SET value_ = 'false' WHERE name_ = 'TAMPILKAN_SISA_CUTI';
		")->execute();
		
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('c00fcd26-46bf-11ec-8852-588a5a1e729e', 'NON_EMPLOYEE_ONLY', 'false', 'false pengecekan double shift utk seluruh kel pegawai, true hanya non employee');
		")->execute();
	}

	public function down()
	{
		echo "m211116_093027_add_sysprefs_nonemployee does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}