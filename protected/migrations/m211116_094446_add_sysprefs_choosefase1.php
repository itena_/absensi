<?php

class m211116_094446_add_sysprefs_choosefase1 extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('cc707fda-46c1-11ec-8852-588a5a1e729e', 'CHOOSE_SHIFT_PHASE1', 'true', 'true fase 1 bisa pilih double shift, false tidak');
		")->execute();
	}

	public function down()
	{
		echo "m211116_094446_add_sysprefs_choosefase1 does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}