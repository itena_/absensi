<?php

class m211117_024341_add_index_sysprefs extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			CREATE UNIQUE INDEX `sys_prefs_idx1` ON `pbu_sys_prefs`(`name_`) USING BTREE ;
		")->execute();
		
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('be1b7d01-4749-11ec-a4f0-588a5a1e729e', 'BATAS_LEMBUR_HARI', '480', 'lebih dari value dianggap lembur hari');
		")->execute();
	}

	public function down()
	{
		echo "m211117_024341_add_index_sysprefs does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}