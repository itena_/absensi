<?php

class m211119_090317_add_statushkperiode extends CDbMigration
{
	public function up()
	{	
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_bu` MODIFY COLUMN `bu_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL FIRST ;		
		")->execute();
						
		Yii::app()->db->createCommand("
			CREATE TABLE `pbu_status_hk` (
			`status_hk_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
			`nama`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
			`active`  tinyint(3) NULL DEFAULT NULL ,
			`user_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
			`tdate`  datetime NULL DEFAULT NULL ,
			`bu_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
			`default`  tinyint(3) NULL DEFAULT 0 ,
			PRIMARY KEY (`status_hk_id`),
			CONSTRAINT `status_hk_idx1` FOREIGN KEY (`bu_id`) REFERENCES `pbu_bu` (`bu_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
			INDEX `status_hk_idx1` (`bu_id`) USING BTREE 
			)
			ENGINE=InnoDB
			DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
			ROW_FORMAT=Compact
			;
		")->execute();
	}

	public function down()
	{
		echo "m211119_090317_add_statushkperiode does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}