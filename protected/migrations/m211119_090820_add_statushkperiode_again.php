<?php

class m211119_090820_add_statushkperiode_again extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			CREATE TABLE `pbu_status_hk_periode` (
`status_hk_periode_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`status_hk_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`periode_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`pegawai_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
`user_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`tdate`  datetime NULL DEFAULT NULL ,
`cabang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
PRIMARY KEY (`status_hk_periode_id`),
CONSTRAINT `status_hkp_idx1` FOREIGN KEY (`status_hk_id`) REFERENCES `pbu_status_hk` (`status_hk_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
CONSTRAINT `status_hkp_idx2` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `status_hkp_idx3` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `status_hkp_idx4` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `status_hkp_idx1` (`status_hk_id`) USING BTREE ,
INDEX `status_hkp_idx2` (`periode_id`) USING BTREE ,
INDEX `status_hkp_idx3` (`pegawai_id`) USING BTREE ,
INDEX `status_hkp_idx4` (`cabang_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
ROW_FORMAT=Compact
;
		")->execute();
	}

	public function down()
	{
		echo "m211119_090820_add_statushkperiode_again does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}