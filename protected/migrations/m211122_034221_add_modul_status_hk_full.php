<?php

class m211122_034221_add_modul_status_hk_full extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('535b1587-4b3d-11ec-b3ae-588a5a1e729e', 'USE_STATUS_HK', 'false', 'menggunakan status hk di AMOS');
		")->execute();
	}

	public function down()
	{
		echo "m211122_034221_add_modul_status_hk_full does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}