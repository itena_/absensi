<?php

class m211122_043618_add_SHOW_AMOS_PHASE_2 extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('96b66214-4b49-11ec-b3ae-588a5a1e729e', 'SHOW_AMOS_PHASE_2', 'false', 'tampilkan fase 2 AMOS, true tampil, false ngga');
		")->execute();
	}

	public function down()
	{
		echo "m211122_043618_add_SHOW_AMOS_PHASE_2 does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}