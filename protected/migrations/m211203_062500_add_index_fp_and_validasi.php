<?php

class m211203_062500_add_index_fp_and_validasi extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_fp` DROP INDEX `idx1`;
			CREATE INDEX `fp_idx1` ON `pbu_fp`(`PIN`, `Verified`, `Status`, `WorkCode`, `status_int`, `tipe_data`, `kode_ket`, `log`, `cabang`, `PIN_real`, `cabang_id`) USING BTREE ;
		")->execute();
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_validasi` ADD CONSTRAINT `validasi_fk1` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE;
			CREATE INDEX `validasi_idx1` ON `pbu_validasi`(`pegawai_id`, `pin_id`, `PIN`, `kode_ket`, `status_int`, `user_id`, `status_pegawai_id`, `shift_id`, `cabang_id`) USING BTREE ;
		")->execute();
	}

	public function down()
	{
		echo "m211203_062500_add_index_fp_and_validasi does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}