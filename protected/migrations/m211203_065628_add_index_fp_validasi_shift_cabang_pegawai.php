<?php

class m211203_065628_add_index_fp_validasi_shift_cabang_pegawai extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_fp` DROP INDEX `fp_idx1`;
			ALTER TABLE `pbu_fp` MODIFY COLUMN `PIN`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `fp_id`;
			ALTER TABLE `pbu_fp` MODIFY COLUMN `terminal_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' AFTER `WorkCode`;
			CREATE INDEX `fp_idx01` ON `pbu_fp`(`PIN`) USING BTREE ;
			CREATE INDEX `fp_idx2` ON `pbu_fp`(`DateTime_`) USING BTREE ;
			CREATE INDEX `fp_idx3` ON `pbu_fp`(`Status`) USING BTREE ;
			CREATE INDEX `fp_idx4` ON `pbu_fp`(`status_int`) USING BTREE ;
			CREATE INDEX `fp_idx5` ON `pbu_fp`(`cabang`) USING BTREE ;
			CREATE INDEX `fp_idx6` ON `pbu_fp`(`cabang_id`) USING BTREE ;
		")->execute();
		
		Yii::app()->db->createCommand("
			DROP INDEX `idx_nik` ON `pbu_pegawai`;
			CREATE UNIQUE INDEX `idx_nik` ON `pbu_pegawai`(`nik`) USING BTREE ;
			CREATE INDEX `idx_pbu_pegawai_1` ON `pbu_pegawai`(`cabang_id`) USING BTREE ;
			CREATE INDEX `idx_pbu_pegawai_2` ON `pbu_pegawai`(`store`) USING BTREE ;
			CREATE INDEX `idx_pbu_pegawai_3` ON `pbu_pegawai`(`kelompok_pegawai`) USING BTREE ;
		")->execute();
		
		Yii::app()->db->createCommand("
			ALTER TABLE `pbu_shift` DROP FOREIGN KEY `pbu_shift_ibfk_1`;
			ALTER TABLE `pbu_shift` ADD CONSTRAINT `pbu_shift_ibfk_1` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
			CREATE INDEX `shift_fk2` ON `pbu_shift`(`kelompok_shift`) USING BTREE ;
			CREATE INDEX `shift_fk3` ON `pbu_shift`(`active`) USING BTREE ;
		")->execute();
		
		Yii::app()->db->createCommand("
		SET FOREIGN_KEY_CHECKS = 0;
			DROP INDEX `validasi_idx1` ON `pbu_validasi`;
			CREATE INDEX `validasi_idx1` ON `pbu_validasi`(`pegawai_id`) USING BTREE ;
			CREATE INDEX `validasi_idx2` ON `pbu_validasi`(`in_time`) USING BTREE ;
			CREATE INDEX `validasi_idx3` ON `pbu_validasi`(`out_time`) USING BTREE ;
			CREATE INDEX `validasi_idx4` ON `pbu_validasi`(`status_int`) USING BTREE ;
			CREATE INDEX `validasi_idx5` ON `pbu_validasi`(`shift_id`) USING BTREE ;
			CREATE INDEX `validasi_idx6` ON `pbu_validasi`(`cabang_id`) USING BTREE ;
		SET FOREIGN_KEY_CHECKS = 1;	
		")->execute();		
	}

	public function down()
	{
		echo "m211203_065628_add_index_fp_validasi_shift_cabang_pegawai does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}