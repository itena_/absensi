<?php

class m211224_085623_create_amos_address extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		CREATE TABLE `pbu_amos_address` (
		`amos_address_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		`bu_kode`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		`kode_cabang`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		`amos_address`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
		PRIMARY KEY (`amos_address_id`)
		)
		ENGINE=InnoDB
		DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
		ROW_FORMAT=Compact
		;
		")->execute();
	}

	public function down()
	{
		echo "m211224_085623_create_amos_address does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}