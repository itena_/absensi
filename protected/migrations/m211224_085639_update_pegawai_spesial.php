<?php

class m211224_085639_update_pegawai_spesial extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		ALTER TABLE `pbu_pegawai_spesial` DROP COLUMN `bu_kode`;
		")->execute();
		
		Yii::app()->db->createCommand("
		ALTER TABLE `pbu_pegawai_spesial` ADD COLUMN `bu_kode`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'kode BU, misal ANI;PNG;NGI' AFTER `pegawai_id`;		
		ALTER TABLE `pbu_pegawai_spesial` DROP COLUMN `finger_naava`;
		")->execute();
	}

	public function down()
	{
		echo "m211224_085639_update_pegawai_spesial does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}