<?php

class m211224_085705_updateversion38 extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			UPDATE pbu_sys_prefs SET value_ = '3.8' WHERE name_ = 'AMOS_VERSION';
		")->execute();
	}

	public function down()
	{
		echo "m211224_085705_updateversion38 does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}