<?php

class m220217_072205_add_pbu_link extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			CREATE TABLE `pbu_link` (
`link_id`  varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`note`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`link`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
`visible`  tinyint(3) NULL DEFAULT NULL ,
PRIMARY KEY (`link_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
ROW_FORMAT=Compact
;
		")->execute();
		
		Yii::app()->db->createCommand("
			INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('3e96b99f-8fa8-11ec-bb24-588a5a1e729e', 'Tutorial Pengerjaan AMOS secara reguler', 'https://www.youtube.com/watch?v=XQOsI0XhRls&ab_channel=RexyAPR', '1');
		")->execute();
	}

	public function down()
	{
		echo "m220217_072205_add_pbu_link does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}