<?php

class m220218_061207_remove_pbu_lock extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			DROP TABLE pbu_lock;
		")->execute();
	}

	public function down()
	{
		echo "m220218_061207_remove_pbu_lock does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}