<?php

class m220221_080053_rename_npwp_ke_niklokal extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		ALTER TABLE `pbu_pegawai`
		CHANGE COLUMN `npwp` `nik_lokal`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `tgl_masuk`;
		")->execute();

	}

	public function down()
	{
		echo "m220221_080053_rename_npwp_ke_niklokal does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}