<?php

class m220321_080752_add_hitung_htm extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
		INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('188ae545-a8ed-11ec-bb30-588a5a1e729e', 'HITUNG_HTM', 'true', 'true akan menghitung HTM');
		")->execute();
	}

	public function down()
	{
		echo "m220321_080752_add_hitung_htm does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}