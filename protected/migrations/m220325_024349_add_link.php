<?php

class m220325_024349_add_link extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand("
			INSERT IGNORE INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('051259a7-abe5-11ec-a986-588a5a1e729e', 'Status HK Per Periode', 'https://www.youtube.com/watch?v=ihQi6xkNB1c', '8');
			INSERT IGNORE INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('3e96b99f-8fa8-11ec-bb24-588a5a1e729e', 'Tutorial Pengerjaan AMOS secara reguler', 'https://www.youtube.com/watch?v=XQOsI0XhRls', '1');
			INSERT IGNORE INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('9cfee12f-abe4-11ec-a986-588a5a1e729e', 'Presensi kelompok pegawai OB/OG/Security', 'https://www.youtube.com/watch?v=MWEdW0jL8rQ', '2');
			INSERT IGNORE INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('bb3a4649-abe4-11ec-a986-588a5a1e729e', 'Update AMOS Version 3.8', 'https://www.youtube.com/watch?v=FpLRTN8TfJ8&ab_channel=RexyAPR', '0');
		")->execute();		

	}

	public function down()
	{
		echo "m220325_024349_add_link does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}