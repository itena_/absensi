<?php

class m220708_032452_add_index_sysprefs_api extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('01730f9c-fdce-11ec-912a-a81e843c3480', 'POS_TIMEOUT_API', '60', '');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('11abc83e-fdce-11ec-912a-a81e843c3480', 'POS_USERNAME_API', 'sync', '');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('15967f12-fdce-11ec-912a-a81e843c3480', 'POS_PASSWORD_API', 'sync', '');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('b58b29cd-fdcd-11ec-912a-a81e843c3480', 'POS_URL_API', 'http://pos.sin01/api/', 'url api ke nwis');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('e3551f22-fdcd-11ec-912a-a81e843c3480', 'POS_PROXY_API', 'false', '');		
        ")->execute();
	}

	public function down()
	{
		echo "m220708_032452_add_index_sysprefs_api does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}