<?php

class m220902_062122_add_url_edm extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('57120c48-203f-11ed-972b-588a5a1e729e', 'POS_EDM_API', 'http://hrd.ena/api/', 'url EDM');
        ")->execute();

	}

	public function down()
	{
		echo "m220902_062122_add_url_edm does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}