<?php

class m220928_045504_add_libur_in_periode extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            ALTER TABLE `pbu_periode` ADD COLUMN `libur`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `jenis_periode_id`;
        ")->execute();
	}

	public function down()
	{
		echo "m220928_045504_add_libur_in_periode does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}