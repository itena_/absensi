<?php

class m221018_074735_modify_validasi_payroll extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            ALTER TABLE `pbu_validasi`
            ADD COLUMN `jabatan`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `status_wfh`,
            ADD COLUMN `level`  varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `jabatan`,
            ADD COLUMN `golongan`  varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `level`;
        ")->execute();
	}

	public function down()
	{
		echo "m221018_074735_modify_validasi_payroll does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}