<?php

class m221025_064328_modify_pegawai_payroll extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            ALTER TABLE `pbu_pegawai` ADD COLUMN `tgl_cabang`  date NULL DEFAULT NULL AFTER `kelompok_pegawai`;
            ALTER TABLE `pbu_pegawai` ADD COLUMN `tgl_level_golongan_jabatan`  date NULL DEFAULT NULL AFTER `tgl_cabang`;
            ALTER TABLE `pbu_pegawai` ADD COLUMN `tgl_status_pegawai`  date NULL DEFAULT NULL AFTER `tgl_level_golongan_jabatan`;
            
            ALTER TABLE `pbu_golongan`
            ADD COLUMN `bu_id`  varchar(36) NULL AFTER `nama`;
            
            ALTER TABLE `pbu_jabatan`
            ADD COLUMN `bu_id`  varchar(36) NULL AFTER `urutan`;


        ")->execute();
	}

	public function down()
	{
		echo "m221025_064328_modify_pegawai_payroll does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}