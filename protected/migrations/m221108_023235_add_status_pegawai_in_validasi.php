<?php

class m221108_023235_add_status_pegawai_in_validasi extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            ALTER TABLE `pbu_validasi` ADD COLUMN `status_pegawai`  varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `golongan`;
            ALTER TABLE `pbu_validasi` ADD COLUMN `periode_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `status_pegawai`;
        ")->execute();
	}

	public function down()
	{
		echo "m221108_023235_add_status_pegawai_in_validasi does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}