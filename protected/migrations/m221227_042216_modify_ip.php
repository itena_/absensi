<?php

class m221227_042216_modify_ip extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
        ALTER TABLE `pbu_ip`
ADD COLUMN `up`  tinyint(3) NOT NULL DEFAULT 0 AFTER `com_key`;
        ")->execute();
	}

	public function down()
	{
		echo "m221227_042216_modify_ip does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}