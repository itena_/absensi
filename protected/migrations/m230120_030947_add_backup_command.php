<?php

class m230120_030947_add_backup_command extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('3a6156a9-8114-11e9-8daf-507b9d297991', 'POS_APP_NAME', 'AMOS', '');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('3a6156a9-8114-11e9-8daf-507b9d297990', 'POS_BACKUP_PATH', '/home/it/backup', '');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('00e624b4-8115-11e9-8daf-507b9d297990', 'POS_MYSQLDUMP_PATH', 'mysqldump', '');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('d8e143f6-8118-11e9-8daf-507b9d297990', 'POS_SEPARATOR', '/', '');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('b9cd113b-97a2-11ed-bab0-588a5a1e729e', 'SSH_IP', '', 'dikosongkan jika APP dan DB jadi 1 tempat');
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('4791399d-9832-11ed-b56d-000c29988254', 'USE_SSHPASS', '', 'dikosongkan jika APP dan DB jadi 1 tempat');
        ")->execute();
	}

	public function down()
	{
		echo "m230120_030947_add_backup_command does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}