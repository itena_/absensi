<?php

class m230321_042553_add_project_path extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            INSERT INTO `pbu_sys_prefs` (`sys_prefs_id`, `name_`, `value_`, `note`) VALUES ('01395ae0-c7a0-11ed-920e-588a5a1e729e', 'PROJECT_PATH', '/var/www/absensi/', 'path utk project');
        ")->execute();
	}

	public function down()
	{
		echo "m230321_042553_add_project_path does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}