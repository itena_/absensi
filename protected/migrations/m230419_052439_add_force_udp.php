<?php

class m230419_052439_add_force_udp extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
            ALTER TABLE `pbu_cabang`
            CHANGE COLUMN `no_telp_cabang` `force_udp`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `alamat_id`;
        ")->execute();
	}

	public function down()
	{
		echo "m230419_052439_add_force_udp does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}