<?php

class m230424_054550_move_force_udp_to_ip extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
        ALTER TABLE `pbu_ip`
        ADD COLUMN `force_udp`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `up`;
        
        ALTER TABLE `pbu_cabang`
        DROP COLUMN `force_udp`;
        ")->execute();
	}

	public function down()
	{
		echo "m230424_054550_move_force_udp_to_ip does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}