<?php

class m230921_025933_modify_pegawai_spesial extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
                ALTER TABLE `pbu_pegawai_spesial`
        MODIFY COLUMN `nik_lain`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'punya nik lain' AFTER `bu_kode`,
        MODIFY COLUMN `cabang`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER `nik_lain`;
        ")->execute();
	}

	public function down()
	{
		echo "m230921_025933_modify_pegawai_spesial does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}