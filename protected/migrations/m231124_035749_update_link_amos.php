<?php

class m231124_035749_update_link_amos extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand("
        TRUNCATE TABLE pbu_link;
        ")->execute();
        Yii::app()->db->createCommand("
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('bb3a4649-abe4-11ec-a986-588a5a1e729e', 'Update AMOS Version 3.8', 'https://www.youtube.com/watch?v=FpLRTN8TfJ8&ab_channel=RexyAPR', '0');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('3e96b99f-8fa8-11ec-bb24-588a5a1e729e', 'Tutorial Pengerjaan AMOS secara reguler', 'https://www.youtube.com/watch?v=XQOsI0XhRls', '1');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('9cfee12f-abe4-11ec-a986-588a5a1e729e', 'Presensi kelompok pegawai OB/OG/Security', 'https://www.youtube.com/watch?v=MWEdW0jL8rQ', '2');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('4ffc105d-8a7c-11ee-ad7c-588a5a1e729e', 'Macam Report', 'https://youtu.be/9qER5X1da5w?si=y_1W553w9HaIpJQu', '3');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('9c8bfaa7-8a7c-11ee-ad7c-588a5a1e729e', 'Lepas limit lembur', 'https://youtu.be/Fc1knAs5p6o?si=BkLUB52TMsNKOwzy', '4');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('aa382d35-8a7c-11ee-ad7c-588a5a1e729e', 'Review Presensi berkelanjutan', 'https://youtu.be/X7w0vJ-Exrw?si=wd8jo4HGz0_JlbYk', '5');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('caba9c8a-8a7c-11ee-ad7c-588a5a1e729e', 'Error jam masuk > pulang', 'https://youtu.be/pc-kU6l-pWk?si=Y3PnfBXJRqWThF0-', '7');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('051259a7-abe5-11ec-a986-588a5a1e729e', 'Status HK Per Periode', 'https://www.youtube.com/watch?v=ihQi6xkNB1c', '8');
        INSERT INTO `pbu_link` (`link_id`, `note`, `link`, `visible`) VALUES ('dcc6e33b-8a7c-11ee-ad7c-588a5a1e729e', 'Empty Cache & Hard Reload', 'https://youtu.be/7tg7HxSno6E?si=Sy15DS_Cg5dx9NBR', '99');
        ")->execute();
	}

	public function down()
	{
		echo "m231124_035749_update_link_amos does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}