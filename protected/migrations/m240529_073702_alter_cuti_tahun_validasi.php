<?php

class m240529_073702_alter_cuti_tahun_validasi extends CDbMigration
{
	public function up()
	{
				Yii::app()->db->createCommand("
ALTER TABLE `pbu_validasi` ADD COLUMN `cuti_tahun` year NULL DEFAULT NULL AFTER `cuti`;
		")->execute();	
	}

	public function down()
	{
		echo "m240529_073702_alter_cuti_tahun_validasi does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}