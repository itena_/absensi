<?php

Yii::import('application.models._base.BaseAmosAddress');

class AmosAddress extends BaseAmosAddress
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function beforeValidate() {
        if ($this->amos_address_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->amos_address_id = $uuid;
        }
        return parent::beforeValidate();
    }
}