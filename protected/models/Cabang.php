<?php
Yii::import('application.models._base.BaseCabang');
class Cabang extends BaseCabang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->cabang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->cabang_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function isCabangHolding($cabang_id){
        return Bu::model()->findByPk(Cabang::model()->findByPk($cabang_id)->bu_id)->bu_nama_alias === 'HOLDING' ? true : false;
    }
    
    public function checkCabang(){
        $id         = Yii::app()->user->getId();
        $pegawai_id = Users::model()->findByAttributes(array('id' => $id))->pegawai_id;
        $store      = Pegawai::model()->findByAttributes(array('pegawai_id' => $pegawai_id))->store;
        return $store;
    }
    public function checkCabangId(){
        $id         = Yii::app()->user->getId();
        $pegawai_id = Users::model()->findByAttributes(array('id' => $id))->pegawai_id;
        $cabang_id  = Pegawai::model()->findByAttributes(array('pegawai_id' => $pegawai_id))->cabang_id;
        return $cabang_id;
    }
    public function getCabangPusatFromBu($pegawai_id) {
        $bu_id              = Cabang::model()->findByAttributes(array('cabang_id' => Pegawai::model()->findByAttributes(array('pegawai_id' => $pegawai_id))->cabang_id))->bu_id;
        $bu_name            = Bu::model()->findByPk($bu_id)->bu_name;
        $cabang_pusat_id    = Cabang::model()->findByAttributes([
            'bu_id' => $bu_id, 'kepala_cabang_stat' => 1
        ]);
        if(!$cabang_pusat_id) {
            throw new Exception("Tidak mendapatkan Cabang Pusat dari BU $bu_name , silahkan laporkan kepada HRD yang bersangkutan untuk mendaftarkan Cabang Pusat.");
        }
        return $cabang_pusat_id->cabang_id;
    }
}
