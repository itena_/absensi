<?php

Yii::import('application.models._base.BaseDayshift');

class Dayshift extends BaseDayshift {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function beforeValidate()
    {
        if ($this->dayshift_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->dayshift_id = $uuid;
        }
        return parent::beforeValidate();
    }
}
