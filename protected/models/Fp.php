<?php

Yii::import('application.models._base.BaseFp');

class Fp extends BaseFp {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function beforeValidate()
    {
        if ($this->fp_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->fp_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        return parent::beforeValidate();
    }

    public function durasi($PIN, $shift_id, $in_time, $out_time, $param, $param2) {
        date_default_timezone_set('Asia/Jakarta');
        $ambil_diawal   = 0;
        $ambil_diakhir  = 0;
        $BO             = GET_BATAS_OVERTIME();

        $intm = new DateTime($in_time);
        $outm = new DateTime($out_time);
        $min60 = 60;

//        if($intm > $outm && (isset($_POST['mode']) && $_POST['mode'] != 'import')){
//        if($intm > $outm && $_POST['mode'] != 'import'){
        if($intm > $outm){
            $msg = "Tanggal atau Jam pulangnya lebih kecil dari waktu masuknya, periksa kembali. $PIN : $in_time - $out_time";
            Log::generateErrorMessage(false, $msg);
        }

        $res1 = new DbCmd();
        $res1->addFrom("{{shift}}")
            ->addSelect("*")
            ->addCondition("shift_id = :shift_id")
            ->addParam(":shift_id", $shift_id);
        $shift = $res1->queryRow();

        /*$jam_kerja = $outm->diff($intm);
        if($jam_kerja->d > 0) {
            $keluarm = $in_time;
        } else {
            if($jam_kerja->h >= 16){
                $keluarm = $in_time;
            } else {
                $keluarm = $out_time;
            }
        }*/
        $plus1day = 0;
        if(substr($in_time,0,10) != substr($out_time, 0,10)) {
            if(strtotime($shift['out_time']) < strtotime($shift['in_time'])) {
                $keluarm = $out_time;
            } else {
                $keluarm = substr($in_time,0,10) . substr($out_time,10,9);
            }
        }
        else {
            if(strtotime($shift['out_time']) < strtotime($shift['in_time'])) {
                $plus1day = 1;
            }
            $keluarm = $out_time;
        }

        $aaearlytime    = date('Y-m-d',strtotime($in_time)) . ' ' . $shift['early_time'];
        $aalatetime     = date('Y-m-d',strtotime($in_time)) . ' ' . $shift['late_time'];

        if($plus1day === 1) {
            $aaleasttime = date('Y-m-d',strtotime("+1 day", strtotime($keluarm))) . ' ' . $shift['least_time'];
            $aaovertime  = date('Y-m-d',strtotime("+1 day", strtotime($keluarm))) . ' ' . $shift['over_time'];
        } else {
            $aaleasttime = date('Y-m-d',strtotime($keluarm)) . ' ' . $shift['least_time'];
            $aaovertime  = date('Y-m-d',strtotime($keluarm)) . ' ' . $shift['over_time'];
        }

        if ($param == '6jam'){
            $tgl_dan_jam_masuk_seharusnya = date('Y-m-d',strtotime($keluarm)) . ' ' . $shift['in_time'];

            $tgl_dan_jam_masuk_seharusnya_dan_toleransi_30_min = date('Y-m-d H:i:s', strtotime("30 minutes", strtotime($tgl_dan_jam_masuk_seharusnya)));
            if(strtotime($tgl_dan_jam_masuk_seharusnya_dan_toleransi_30_min) > strtotime($in_time)) {
                $ambil_diawal  = 0;
                $ambil_diakhir = -2;
            } else {
                $ambil_diawal  = 2;
                $ambil_diakhir = 0;
            }
        }

        // cek apakah lewat hari
        /*if($aaleasttime > $aovertime) {
            $aaleasttime = date('Y-m-d',strtotime($in_time)) . ' ' . $shift['least_time'];
        }*/

        $aearlytime = date('Y-m-d H:i:s', strtotime("$ambil_diawal hours", strtotime($aaearlytime)));
        $alatetime  = date('Y-m-d H:i:s', strtotime("$ambil_diawal hours", strtotime($aalatetime)));
        $aleasttime = date('Y-m-d H:i:s', strtotime("$ambil_diakhir hours", strtotime($aaleasttime)));
        $aovertime  = date('Y-m-d H:i:s', strtotime("$ambil_diakhir hours", strtotime($aaovertime)));

        $earlytime = new DateTime($aearlytime); //explode(':', $row['least_time']);
        $latetime = new DateTime($alatetime);  //explode(':', $row['over_time']);
        $leasttime = new DateTime($aleasttime); //explode(':', $row['least_time']);
        $overtime = new DateTime($aovertime);  //explode(':', $row['over_time']);

        //-------min terlambat datang------
        if ($intm > $latetime) {
            $min_late_time = $intm->diff($latetime);
            $duras['min_late'] = self::getMinutes($min_late_time);
        } else {
            $duras['min_late'] = 0;
        }

        //-------min kecepetan pulang------
        if ($outm < $leasttime) {
            $min_least_time = $leasttime->diff($outm);
            $duras['min_least'] = self::getMinutes($min_least_time);
        } else {
            $duras['min_least'] = 0;
        }

        //-------min lembur------
        $min_early_time = $earlytime->diff($intm);
        $min_early = self::getMinutes($min_early_time);

        if ($min_early < 0){
            $duras['min_early'] = 0;
        } else {
            if($min_early + 30 == 30)
                $duras['min_early'] = 0;
            else
                $duras['min_early'] = $min_early + 30;
        }

        $min_over_time = $outm->diff($overtime);
        $min_over = self::getMinutes($min_over_time);

        if ($min_over < 0){
            $duras['min_over'] = 0;
        } else {
            if($min_over + 30 == 30)
                $duras['min_over'] = 0;
            else
                $duras['min_over'] = $min_over + 30;
        }

        //-------min lembur------
        $lemburgabung = $duras['min_early'] + $duras['min_over'];
        if ($lemburgabung > $min60) {
            $duras['min_over_sejam_awal'] = $min60;
        } else {
            $duras['min_over_sejam_awal'] = $lemburgabung;
        }

        if ($param2 == 'YA'){
            $BO = $lemburgabung;
        } else {
            if($param2 == 'TIDAK' || !$param2) {
                $BO = GET_BATAS_OVERTIME();
            }
        }

        if($lemburgabung > GET_BATAS_OVERTIME() && $lemburgabung >= GET_BATAS_LEMBUR_HARI()) {
            if($lemburgabung >= 480 && $lemburgabung < (480 * 2)) {
                $duras['real_lembur_hari'] = 1;
                $newlembur = $lemburgabung-480;
            } else if($lemburgabung > (480 * 2) && $lemburgabung < (480 * 3)) {
                $duras['real_lembur_hari'] = 2;
                $newlembur = $lemburgabung-(480 * 2);
            } else {
                $duras['real_lembur_hari'] = 3;
                $newlembur = $lemburgabung-(480 * 3);
            }

            if($newlembur - 60 > 0) {
                $duras['real_lembur_pertama'] = 60;
                $duras['real_lembur_akhir'] = $newlembur - 60;
            } else {
                $duras['real_lembur_pertama'] = $newlembur;
                $duras['real_lembur_akhir'] = 0;
            }
        } else if ($lemburgabung > GET_BATAS_OVERTIME() && $lemburgabung < GET_BATAS_LEMBUR_HARI()) {
            $duras['real_lembur_pertama'] = $duras['min_over_sejam_awal'];
            $duras['real_lembur_akhir'] = $BO - $duras['min_over_sejam_awal'];
            $duras['real_lembur_hari'] = 0;
        } else if ($lemburgabung <= GET_BATAS_OVERTIME()) {
            $duras['real_lembur_pertama'] = $duras['min_over_sejam_awal'];
            if($lemburgabung > $min60){
                    $duras['real_lembur_akhir'] = $lemburgabung - $duras['min_over_sejam_awal'];
                    $duras['real_lembur_hari'] = 0;
                } else {
                    $duras['real_lembur_akhir'] = 0;
                    $duras['real_lembur_hari'] = 0;
                }
        }
        $lessgabung = $duras['min_late'] + $duras['min_least'];
        $duras['real_less_time'] = $lessgabung * GET_PENGALI_LESSTIME();
        return $duras;
    }

    private static function getMinutes($time) {
        $minute = $time->days * 24 * 60;
        $minute += $time->h * 60;
        $minute += $time->i;

        if($time->invert == 0) {
            $minute = $minute * -1;
        }
        return $minute;
    }

    public function time_from_seconds($seconds) {
        $h = floor($seconds / 3600);
        $m = floor(($seconds % 3600) / 60);
        $s = $seconds - ($h * 3600) - ($m * 60);
        return sprintf('%02d:%02d:%02d', $h, $m, $s);
    }

    public static function saveFp($nik, $date, $cabang, $cabang_id, $workCode) {
        /*
         * No. Work Code:
         * 99 import Attlog
         * 98 import LARK
         * 88 data simulasi
         * 77 amos att beta
         */

        $fp             = new Fp();
        $fp->PIN        = $nik;
        $fp->PIN_real   = $nik;
        $fp->DateTime_  = $date;
        $fp->tdate      = date('Y-m-d') . ' ' . Yii::app()->dateFormatter->format('HH:mm:ss', time());
        $fp->cabang     = $cabang;
        $fp->cabang_id  = $cabang_id;
        $fp->Status     = 0;
        $fp->WorkCode   = $workCode;
        $fp->save();
    }

    public static function saveFpWithReturn($nik, $date, $cabang, $cabang_id, $workCode) {
        /*
         * No. Work Code:
         * 77 import AMOS Mobile
         */

        $fp             = new Fp();
        $fp->PIN        = $nik;
        $fp->PIN_real   = $nik;
        $fp->DateTime_  = $date;
        $fp->tdate      = date('Y-m-d') . ' ' . Yii::app()->dateFormatter->format('HH:mm:ss', time());
        $fp->cabang     = $cabang;
        $fp->cabang_id  = $cabang_id;
        $fp->Status     = 0;
        $fp->WorkCode   = $workCode;
        $fp->save();

        return $fp;
    }
    public static function getFpToDarwinbox(){
        $db = new DbCmd();
        $db->addSelect('fp.*, ip.kode_ip ');
        $db->addFrom('pbu_fp fp');
        $db->addLeftJoin('pbu_ip ip', 'ip.cabang_id = fp.cabang_id');
        $db->addCondition('fp.Verified <> -1');
        $db->addGroup('fp.fp_id');
        $db->setLimit(5000);

        return $db->queryAll();
    }

}
