<?php
Yii::import('application.models._base.BaseGolongan');
class Golongan extends BaseGolongan
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->golongan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->golongan_id = $uuid;
        }
        return parent::beforeValidate();
    }
}