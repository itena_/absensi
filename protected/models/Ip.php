<?php

Yii::import('application.models._base.BaseIp');

class Ip extends BaseIp
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->ip_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->ip_id = $uuid;
        }
        return parent::beforeValidate();
    }
}