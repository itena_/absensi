<?php
Yii::import('application.models._base.BaseJabatan');
class Jabatan extends BaseJabatan
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->jabatan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->jabatan_id = $uuid;
        }
        return parent::beforeValidate();
    }
}