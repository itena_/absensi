<?php
Yii::import('application.models._base.BaseLeveling');
class Leveling extends BaseLeveling
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->leveling_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->leveling_id = $uuid;
        }
        return parent::beforeValidate();
    }
}