<?php

Yii::import('application.models._base.BaseLink');

class Link extends BaseLink
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->link_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->link_id = $uuid;
        }
        return parent::beforeValidate();
    }
}