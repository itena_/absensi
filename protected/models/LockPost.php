<?php

Yii::import('application.models._base.BaseLockPost');

class LockPost extends BaseLockPost
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->lock_post_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->lock_post_id = $uuid;
        }
        if ($this->posted_tdate == null) {
            $this->posted_tdate = NULL;
            $this->posted_user_id = NULL;
        }
        if ($this->locked_tdate == null) {
            $this->locked_tdate = NULL;
            $this->locked_user_id = NULL;
        }
        return parent::beforeValidate();
    }

    public static function applyLockPostStatusPegawai($periode_id, $cabang_click, $result)
    {
        $validasi = DbCmd::instance()->addFrom("{{validasi}} v")
            ->addCondition('periode_id = :periode_id AND pegawai_id = :pegawai_id AND cabang_id = :cabang_id')
            ->addCondition("in_time >= :in_time")
            ->addParams([
                ':periode_id' => $periode_id,
                ':pegawai_id' => $result['pegawai_id'],
                ':cabang_id'  => $cabang_click,
                ':in_time'    => $result['status_pegawai_start']
            ])->queryAll()
        ;
        foreach ($validasi as $vv) {
            $model = Validasi::model()->findByPk($vv['validasi_id']);
            $model->status_pegawai = StatusPegawai::get_status_pegawai_by_id($result['status_pegawai_id']);
            if (!$model->save()) {
                Log::generateErrorMessage(false,CHtml::errorSummary($model));
            }
        }
    }
    public static function applyLockPostValidasi($vv, $result)
    {
        $model = Validasi::model()->findByPk($vv['validasi_id']);

        $level = Leveling::model()->findByPk($result['lvljabatan_id']);
        if (!$level) {
            $lp = $result['urutan_romawi'];
            if (!$lp) Log::generateErrorMessage(false, "Level " . $vv['PIN'] . " tidak boleh kosong!");

            $model->level = $lp;
        } else {
            $model->level = $level->kode;
        }

        $gol = Golongan::model()->findByPk($result['lvl2jabatan_id']);;
        if (!$gol) {
            $gp = $result['urutan_alps'];
            if (!$gp) Log::generateErrorMessage(false, "Golongan " . $vv['PIN'] . " tidak boleh kosong!");

            $model->golongan = $gp;
        } else {
            $model->golongan = $gol->kode;
        }

        if (!$model->save()) {
            Log::generateErrorMessage(false, CHtml::errorSummary($model));
        }

        $jab = Jabatan::model()->findByPk($result['jabatan_id']);
        if (!$jab) {
            $jp = $result['kode_jabatan'];
            if (!$jp) Log::generateErrorMessage(false, "Jabatan " . $vv['PIN'] . " tidak boleh kosong!");

            $model->jabatan = $jp;
        } else {
            $model->jabatan = $jab->kode;
        }

        if (!$model->save()) {
            Log::generateErrorMessage(false, CHtml::errorSummary($model));
        }
    }
    public static function applyValidasiKecualiYangExclude($excludePegawai, $pegawai, $periode_id, $cabang_click) {
        $excludePegawai = substr($excludePegawai,0,strlen($excludePegawai)-1);
        $validasi = DbCmd::instance()->addFrom("{{validasi}} v")
            ->addSelect("v.pegawai_id, nik, nama_lengkap, l.kode kode_level, g.kode kode_golongan, j.kode kode_jabatan, s.kode kode_status_pegawai")
            ->addLeftJoin("{{pegawai}} p", 'p.pegawai_id = v.pegawai_id')
            ->addLeftJoin("{{leveling}} l", 'l.leveling_id = p.leveling_id')
            ->addLeftJoin("{{golongan}} g", 'g.golongan_id = p.golongan_id')
            ->addLeftJoin("{{jabatan}} j", 'j.jabatan_id = p.jabatan_id')
            ->addLeftJoin("{{status_pegawai}} s", 's.status_pegawai_id = p.status_pegawai_id')
            ->addCondition("v.pegawai_id IN ($pegawai)")
            ->addCondition('periode_id = :periode_id AND v.cabang_id = :cabang_id')
//            ->addCondition("(`level` is null OR golongan is null OR jabatan is null OR status_pegawai is null)")
            ->addGroup("v.pegawai_id")
            ->addParams([
                ':periode_id' => $periode_id,
                ':cabang_id'  => $cabang_click
            ])
        ;
        if($excludePegawai) {
            $validasi->addCondition("v.pegawai_id NOT IN ($excludePegawai)");
        }
        $aaa = $validasi->getQuery();
        $validasi = $validasi->queryAll();
        foreach ($validasi as $vv) {
            Validasi::model()->updateAll([
                'level' => $vv['kode_level'],
                'golongan' => $vv['kode_golongan'],
                'jabatan' => $vv['kode_jabatan'],
                'status_pegawai' => $vv['kode_status_pegawai']
            ],
                'periode_id = :periode_id AND pegawai_id = :pegawai_id AND cabang_id = :cabang_id',
                [
                    ':periode_id' => $periode_id,
                    ':pegawai_id' => $vv['pegawai_id'],
                    ':cabang_id' => $cabang_click
                ]
            );
        }
    }

    public function check_lockpost($periode_id)
    {
        $cabang_id = Cabang::model()->checkCabangId();
        $lockpost = $this->model()->find("periode_id = :periode_id AND cabang_id = :cabang_id", [
            ':periode_id' => $periode_id,
            ':cabang_id' => $cabang_id
        ]);

        $nama_periode = Periode::model()->findByPk($periode_id)->kode_periode;
        $nama_cabang = Cabang::model()->findByPk($cabang_id)->kode_cabang;
        if (isset($lockpost)) {
            if ($lockpost->locked == 1) {
                $msg = "Periode <b>$nama_periode</b> pada cabang <b>$nama_cabang</b> sudah di <b>LOCK</b>!";
                Log::generateErrorMessage(false, $msg);
            }
            if ($lockpost->posted == 1) {
                $msg = "Periode <b>$nama_periode</b> pada cabang <b>$nama_cabang</b> sudah di <b>POST</b>!";
                Log::generateErrorMessage(false, $msg);
            }
        }

    }
}