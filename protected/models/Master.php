<?php
Yii::import('application.models._base.BaseMaster');
class Master extends BaseMaster
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->master_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->master_id = $uuid;
        }
        return parent::beforeValidate();
    }
}