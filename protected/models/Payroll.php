<?php
Yii::import('application.models._base.BasePayroll');
class Payroll extends BasePayroll
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->payroll_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->payroll_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function calculateTotal()
    {
        $total_income = $total_deduction = $take_home_pay = 0;
        foreach ($this->payrollDetails as $payDetail) {
            if ($payDetail->type_ == 1) {
                $total_income += floatval($payDetail->amount);
            } else {
                $total_deduction += floatval($payDetail->amount);
            }
        }
        $this->total_income = $total_income;
        $this->total_deduction = $total_deduction;
        $take_home_pay = $total_income - $total_deduction;
        $this->take_home_pay = $take_home_pay;
        if (!$this->save()) {
            throw new Exception(CHtml::errorSummary($this));
        }
    }
    public function createSlipGajiPDF()
    {
        #mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A5');

        # render (full page)
        $mPDF1->WriteHTML($this->render('CetakSlip', array(), true));

        # Outputs ready PDF
        $mPDF1->Output();

//        Yii::import('application.components.phpExcelReportHelper', true);
//        $report = new PhpExcelReportHelper();
//        $report->loadTemplate(Yii::getPathOfAlias('application.views.reports') .
//            DIRECTORY_SEPARATOR . 'slipGaji.xlsx')
//            ->setActiveSheetIndex(0)
////            ->mergeBlock($mutasi, $columnData, 5, 1)
////            ->mergeField($field)
//            //->protectSheet()
//            ->downloadPDF('Slip Gaji ' . $this->nik);
    }
    public function createSlipGaji()
    {
        Yii::import("application.components.tbs_class", true);
        Yii::import("application.components.tbs_plugin_opentbs", true);
        $tbs = new clsTinyButStrong;
        $tbs->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $tbs->LoadTemplate(
            Yii::getPathOfAlias('application.views.reports') .
            DIRECTORY_SEPARATOR . 'slip_gaji.docx', OPENTBS_ALREADY_UTF8);
        $select = "SELECT * FROM pbu_payroll_details AS am WHERE am.type_ = :type_ 
        AND am.payroll_id = :payroll_id ORDER BY seq";
        $inc = Yii::app()->db->createCommand($select)->queryAll(true, [
            ':type_' => 1,
            ':payroll_id' => $this->payroll_id
        ]);
        $out = Yii::app()->db->createCommand($select)->queryAll(true, [
            ':type_' => -1,
            ':payroll_id' => $this->payroll_id
        ]);
        $details = [];
        $j_inc = count($inc);
        $j_out = count($out);
        $cnt = 0;
        $tbs->MergeBlock('data',
            [
                [
                    'periode' => $this->periode->kode_periode,
                    'tgl' => get_date_today('dd-MM-yyyy'),
                    'cabang' => $this->kode_cab,
                    'jabatan' => $this->nama_jabatan,
                    'gol' => $this->kode_gol,
                    'lvl' => $this->kode_level,
                    'hk' => $this->total_hari_kerja,
                    'nik' => $this->nik,
                    'nama' => $this->nama_lengkap,
                    'email' => $this->email,
                    'status' => $this->nama_status,
                    'npwp' => $this->npwp,
                    'ktp' => '-',
                    'inc' => number_format($this->total_income, 2),
                    'pot' => number_format($this->total_deduction, 2),
                    'thp' => number_format($this->take_home_pay, 2),
                    'mandiri' => '-',
                    'tabungan' => '-',
                    'cicilan' => '-',
                    'note' => '-',
                ]
            ]);
        if ($j_inc >= $j_out) {
            for ($cnt; $cnt < $j_inc; $cnt++) {
                $details[] = [
                    'in_n' => $inc[$cnt]['nama_skema'],
                    'in_a' => ': Rp ' . number_format($inc[$cnt]['amount']),
                    'ot_n' => $cnt < $j_out ? $out[$cnt]['nama_skema'] : '',
                    'ot_a' => $cnt < $j_out ? ': Rp ' . $out[$cnt]['amount'] : '',
                ];
            }
        } else {
            for ($cnt; $cnt < $j_out; $cnt++) {
                $details[] = [
                    'in_n' => $cnt < $j_inc ? $inc[$cnt]['nama_skema'] : '',
                    'in_a' => $cnt < $j_inc ? ': Rp ' . number_format($inc[$cnt]['amount']) : '',
                    'ot_n' => $out[$cnt]['nama_skema'],
                    'ot_a' => ': Rp ' . $out[$cnt]['amount'],
                ];
            }
        }
        for ($cnt; $cnt < 11; $cnt++) {
            $details[] = [
                'in_n' => '',
                'in_a' => '',
                'ot_n' => '',
                'ot_a' => '',
            ];
        }
        $tbs->MergeBlock('r', $details);
        $tbs->Show(OPENTBS_DOWNLOAD, "SLIP GAJI " . $this->nik . ".docx");
    }
}