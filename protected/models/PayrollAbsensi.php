<?php
Yii::import('application.models._base.BasePayrollAbsensi');
class PayrollAbsensi extends BasePayrollAbsensi
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->payroll_absensi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->payroll_absensi_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        return parent::beforeValidate();
    }
    public static function saveData($periode_id, $pegawai_id, $total_hari_kerja, $total_lk, $total_sakit,
                                    $total_cuti_tahunan, $total_off, $total_lembur_1, $total_lembur_next,
                                    $jatah_off, $total_cuti_menikah, $total_cuti_bersalin, $total_cuti_istimewa,
                                    $total_cuti_non_aktif, $less_time)
    {
        $comm = Yii::app()->db->createCommand("INSERT INTO pbu_payroll_absensi
          (payroll_absensi_id, periode_id, total_hari_kerja,locked,tdate,total_lk,total_sakit,total_cuti_tahunan,
          total_off,pegawai_id,total_lembur_1,total_lembur_next,jatah_off,total_cuti_menikah,total_cuti_bersalin,
          total_cuti_istimewa,total_cuti_non_aktif,less_time) VALUES 
          (uuid(),:periode_id, :total_hari_kerja,0,now(),:total_lk,:total_sakit,:total_cuti_tahunan,
          :total_off,:pegawai_id,:total_lembur_1,:total_lembur_next,:jatah_off,:total_cuti_menikah,:total_cuti_bersalin,
          :total_cuti_istimewa,:total_cuti_non_aktif,:less_time) ON DUPLICATE KEY UPDATE 
          periode_id = :periode_id,total_hari_kerja = :total_hari_kerja,total_lk = :total_lk,total_sakit = :total_sakit,
          total_cuti_tahunan = :total_cuti_tahunan, total_off = :total_off, pegawai_id = :pegawai_id, 
          total_lembur_1 = :total_lembur_1,total_lembur_next = :total_lembur_next,jatah_off = :jatah_off,
          total_cuti_menikah = :total_cuti_menikah,total_cuti_bersalin = :total_cuti_bersalin,
          total_cuti_istimewa = :total_cuti_istimewa,total_cuti_non_aktif = :total_cuti_non_aktif,less_time = :less_time;");
        return $comm->execute([
            ':periode_id' => $periode_id,
            ':pegawai_id' => $pegawai_id,
            ':total_hari_kerja' => $total_hari_kerja,
            ':locked' => $locked,
            ':total_lk' => $total_lk,
            ':total_sakit' => $total_sakit,
            ':total_cuti_tahunan' => $total_cuti_tahunan,
            ':total_cuti_menikah' => $total_cuti_menikah,
            ':total_cuti_bersalin' => $total_cuti_bersalin,
            ':total_cuti_istimewa' => $total_cuti_istimewa,
            ':total_cuti_non_aktif' => $total_cuti_non_aktif,
            ':total_off' => $total_off,
            ':total_lembur_1' => $total_lembur_1,
            ':total_lembur_next' => $total_lembur_next,
            ':jatah_off' => $jatah_off,
            ':less_time' => $less_time
        ]);
    }
}