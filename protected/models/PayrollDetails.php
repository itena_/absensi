<?php
Yii::import('application.models._base.BasePayrollDetails');
class PayrollDetails extends BasePayrollDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->payroll_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->payroll_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}