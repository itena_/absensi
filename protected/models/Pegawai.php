<?php
Yii::import('application.models._base.BasePegawai');
class Pegawai extends BasePegawai
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->pegawai_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pegawai_id = $uuid;
        }
        if ($this->tuser == null) {
            $this->tuser = Yii::app()->user->id;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->last_update_id == null) {
            $this->last_update_id = Yii::app()->user->id;
        }
        if ($this->last_update == null) {
            $this->last_update = new CDbExpression('NOW()');
        }
        return parent::beforeValidate();
    }

    public static function IndexPegawai($bu_id = null)
    {
        $db = new DbCmd();
        $db->addFrom("{{pegawai}} pp")
            ->addSelect("pp.pegawai_id,pp.nik,pp.golongan_id,pp.status_id,pp.cabang_id,pp.leveling_id,pp.nama_lengkap,pp.email,pp.tgl_masuk,pp.nik_lokal,pp.bank_nama,
                pp.bank_kota,pp.rekening,pp.last_update_id,pp.status_pegawai_id, pp.kelompok_pegawai,
                CONCAT(DATE_FORMAT(pp.tdate,'%d %b %Y %h:%i:%S'),' by ',pu2.user_id,' (',pu2.name_,')') AS tdate,
                CONCAT(DATE_FORMAT(pp.last_update,'%d %b %Y %h:%i:%S'),' by ',pu1.user_id,' (',pu1.name_,')') AS last_update,
                pp.jabatan_id,pp.tuser,c.kode_cabang AS kode_cabang")
            ->addLeftJoin("{{users}} AS pu1", "pp.last_update_id = pu1.id")
            ->addLeftJoin("{{users}} AS pu2", "pp.tuser = pu2.id")
            ->addLeftJoin("{{cabang}} AS c", "c.cabang_id = pp.cabang_id")
            ->addOrder("pp.nik");
        $cmd = new DbCmd();
        $cmd->addSelect("cabang_id")
            ->addFrom("{{cabang}}")
        ;
        if($bu_id)
            $cmd->addCondition("bu_id = :bu_id")
                ->addParam(":bu_id", $bu_id);

        $db->addInCondition("c.cabang_id", $cmd);
        return $db;
    }

    public static function getPegawai()
    {
        $cmd = new DbCmd();
        $cmd->addSelect("
                p.pegawai_id, p.nama_lengkap, p.nik, 
                p.cabang_id, c.kode_cabang, b.bu_name, b.bu_kode, j.kode, j.urutan")
            ->addFrom("{{pegawai}} p")
            ->addLeftJoin("{{jabatan}} j", "j.jabatan_id = p.jabatan_id")
            ->addLeftJoin("{{cabang}} c", "c.cabang_id = p.cabang_id")
            ->addLeftJoin("{{bu}} b", "b.bu_id = c.bu_id")
            ->addLeftJoin("{{status_pegawai}} sp","sp.status_pegawai_id = p.status_pegawai_id")
            ->addOrder("p.nik");
        return $cmd;
    }

    /**
     * @param Periode $periode
     * @return bool|string
     */
    public function generatePayroll($periode)
    {
        $msg = 'Data berhasil digenerate.';
        $del = Yii::app()->db->createCommand('DELETE pbu_payroll, pbu_payroll_details
              FROM pbu_payroll LEFT JOIN pbu_payroll_details ON pbu_payroll.payroll_id = pbu_payroll_details.payroll_id
              WHERE pbu_payroll.periode_id = :periode_id AND pbu_payroll.pegawai_id = :pegawai_id;');
        $del->execute([':periode_id' => $periode->periode_id, ':pegawai_id' => $this->pegawai_id]);
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $dirName = Yii::getPathOfAlias('application.runtime.' . Yii::app()->controller->id);
            $tmpfname = tempnam($dirName, 'tpay');
            $handle = fopen($tmpfname, "w+");
            $phpCode = '';
            /** @var Cabang $cab */
            $cab = Cabang::model()->findByPk($this->cabang_id);
            if ($cab == null) {
                throw new Exception('Cabang tidak ditemukan.');
            }
            /** @var Jabatan $jab */
            $jab = Jabatan::model()->findByPk($this->jabatan_id);
            if ($jab == null) {
                throw new Exception('Jabatan tidak ditemukan.');
            }
            if ($this->leveling_id == null) {
                throw new Exception('Leveling tidak ditemukan.');
            }
            /** @var PayrollAbsensi $absensi */
            $absensi = PayrollAbsensi::model()->findByAttributes([
                'periode_id' => $periode->periode_id,
                'pegawai_id' => $this->pegawai_id
            ]);
            if ($absensi == null) {
                throw new Exception('Absensi tidak ditemukan.');
            }
            $pay = new Payroll;
            $pay->pegawai_id = $this->pegawai_id;
            $pay->kode_gol = $this->golongan->kode;
            $pay->nama_gol = $this->golongan->nama;
            $pay->kode_level = $this->leveling->kode;
            $pay->nama_level = $this->leveling->nama;
            $pay->kode_cab = $this->cabang->kode_cabang;
            $pay->nama_cab = $this->cabang->nama_cabang;
            $pay->kode_area = $this->cabang->area->kode;
            $pay->nama_area = $this->cabang->area->nama;
            $pay->nik = $this->nik;
            $pay->leveling_id = $this->leveling_id;
            $pay->golongan_id = $this->golongan_id;
            $pay->area_id = $cab->area_id;
            $pay->nama_lengkap = $this->nama_lengkap;
            $pay->nama_jabatan = $jab->nama_jabatan;
            $pay->tgl_masuk = $this->tgl_masuk;
            $pay->email = $this->email;
            $pay->npwp = $this->npwp;
            $pay->nama_status = $this->statusPegawai->nama_status;
            $pay->periode_id = $periode->periode_id;
            $pay->total_hari_kerja = $absensi->total_hari_kerja;
            $pay->total_lk = $absensi->total_lk;
            $pay->total_cuti_tahunan = $absensi->total_cuti_tahunan;
            $pay->total_cuti_menikah = $absensi->total_cuti_menikah;
            $pay->total_cuti_bersalin = $absensi->total_cuti_bersalin;
            $pay->total_cuti_istimewa = $absensi->total_cuti_istimewa;
            $pay->total_cuti_non_aktif = $absensi->total_cuti_non_aktif;
            $pay->total_off = $absensi->total_off;
            $pay->total_sakit = $absensi->total_sakit;
            $pay->total_lembur_1 = $absensi->total_lembur_1;
            $pay->total_lembur_next = $absensi->total_lembur_next;
            $pay->jatah_off = $absensi->jatah_off;
            $pay->less_time = $absensi->less_time;
            if (!$pay->save()) {
                throw new Exception(CHtml::errorSummary($pay));
            }
            $phpCode .= '$PHJ__=' . $periode->getCount() . ';' . PHP_EOL;
            $phpCode .= '$PJOFF__=' . $periode->jumlah_off . ';' . PHP_EOL;
            $phpCode .= '$HK__=' . $absensi->total_hari_kerja . ';' . PHP_EOL;
            $phpCode .= '$LK__=' . $absensi->total_lk . ';' . PHP_EOL;
            $phpCode .= '$CT__=' . $absensi->total_cuti_tahunan . ';' . PHP_EOL;
            $phpCode .= '$CM__=' . $absensi->total_cuti_menikah . ';' . PHP_EOL;
            $phpCode .= '$CS__=' . $absensi->total_cuti_bersalin . ';' . PHP_EOL;
            $phpCode .= '$CI__=' . $absensi->total_cuti_istimewa . ';' . PHP_EOL;
            $phpCode .= '$CNA__=' . $absensi->total_cuti_non_aktif . ';' . PHP_EOL;
            $phpCode .= '$GOFF__=' . $absensi->total_off . ';' . PHP_EOL;
            $phpCode .= '$JOFF__=' . $absensi->jatah_off . ';' . PHP_EOL;
            $phpCode .= '$SICK__=' . $absensi->total_sakit . ';' . PHP_EOL;
            $phpCode .= '$LA__=' . $absensi->total_lembur_1 . ';' . PHP_EOL;
            $phpCode .= '$LN__=' . $absensi->total_lembur_next . ';' . PHP_EOL;
            $phpCode .= '$LT__=' . $absensi->less_time . ';' . PHP_EOL;
            /** @var Master[] $master */
            $master = Master::model()->findAll();
            foreach ($master as $md1) {
                $phpCode .= '$' . $md1->kode . '=0;' . PHP_EOL;
            }
            $masterGaji = Yii::app()->db->createCommand("SELECT * FROM pbu_master_gaji_view WHERE pegawai_id = :pegawai_id")
                ->queryAll(true, [':pegawai_id' => $this->pegawai_id]);
            foreach ($masterGaji as $md) {
                $phpCode .= '$' . $md['mkode'] . '=' . $md['amount'] . ';' . PHP_EOL;
            }
            $length = 10;
            fwrite($handle, "<?php" . PHP_EOL . $phpCode);
            fclose($handle);
            include $tmpfname;
            $tmpscfname = tempnam($dirName, 'tscpay');
            /** @var SchemaGajiView[] $schema */
            $schema = SchemaGajiView::model()->findAllByAttributes([
                'status_id' => $this->status_id
            ]);
            if ($schema == null) {
                return false;
            }
            $total_income = $total_deduction = $take_home_pay = 0;
            foreach ($schema as $sc) {
                $hasilFormula__ = 0;
                $handlesc = fopen($tmpscfname, "w+");
                fwrite($handlesc, "<?php" . PHP_EOL . $sc->formula);
                fclose($handlesc);
                include $tmpscfname;
                $payDetail = new PayrollDetails;
                $payDetail->payroll_id = $pay->payroll_id;
                $payDetail->schema_id = $sc->schema_id;
                $payDetail->kode = $sc->kode;
                $payDetail->seq = $sc->seq;
                $payDetail->nama_skema = $sc->nama_skema;
                $payDetail->type_ = $sc->type_;
                $payDetail->amount = $hasilFormula__;
                if (!$payDetail->save()) {
                    throw new Exception(CHtml::errorSummary($payDetail));
                }
                if ($payDetail->type_ == 1) {
                    $total_income += $payDetail->amount;
                } else {
                    $total_deduction += $payDetail->amount;
                }
            }
            unlink($tmpscfname);
            $take_home_pay = $total_income - $total_deduction;
            $pay->total_income = $total_income;
            $pay->total_deduction = $total_deduction;
            $pay->take_home_pay = $take_home_pay;
            if (!$pay->save()) {
                throw new Exception(CHtml::errorSummary($pay));
            }
            if ($absensi == NULL) {
                fclose($handle);
                unlink($tmpfname);
            }
            $transaction->commit();
            return true;
        } catch (Exception $ex) {
            $transaction->rollback();
            return $ex->getMessage();
        }
    }


    // dibuat karena menambah select kolom tambahan
    public static function getPegawaiApi()
    {
        $cmd = new DbCmd();
        $cmd->addSelect("
                p.pegawai_id, p.nama_lengkap, p.nik, 
                p.cabang_id,p.kelompok_pegawai, c.kode_cabang, b.bu_name, b.bu_kode, j.kode, j.urutan")
            ->addFrom("{{pegawai}} p")
            ->addLeftJoin("{{jabatan}} j", "j.jabatan_id = p.jabatan_id")
            ->addLeftJoin("{{cabang}} c", "c.cabang_id = p.cabang_id")
            ->addLeftJoin("{{bu}} b", "b.bu_id = c.bu_id")
            ->addOrder("p.nik");
        return $cmd;
    }
}
