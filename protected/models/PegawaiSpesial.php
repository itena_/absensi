<?php

Yii::import('application.models._base.BasePegawaiSpesial');

class PegawaiSpesial extends BasePegawaiSpesial
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->pegawai_spesial_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pegawai_spesial_id = $uuid;
        }
        return parent::beforeValidate();
    }
}