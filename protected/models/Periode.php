<?php
Yii::import('application.models._base.BasePeriode');
class Periode extends BasePeriode
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->periode_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->periode_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        return parent::beforeValidate();
    }
    public function getCount()
    {
        $command = $this->dbConnection->createCommand("SELECT TIMESTAMPDIFF(DAY, periode_start,periode_end) 
        FROM pbu_periode WHERE periode_id = :periode_id;");
        return $command->queryScalar([':periode_id' => $this->periode_id]);
    }
    public static function getPeriodeStartEnd($periode_id) {
        $dbperiode = new DbCmd();
        $dbperiode->addFrom("{{periode}}")
            ->addSelect("DATE_FORMAT(periode_start,'%Y-%m-%d %H:%i:%s') ps")
            ->addSelect("DATE_FORMAT(periode_end,'%Y-%m-%d %H:%i:%s') pe")
            ->addSelect("kode_periode, jumlah_off")
            ->addCondition("periode_id = :periode_id")
            ->addParam(":periode_id", $periode_id)
        ;
        return $dbperiode;
    }
    public static function getAllDate($periodeStart, $periodeEnd, $addSelect = "") {
        $dbt1 = new DbCmd();
        $dbt1->addSelect("*")
            ->addFrom("
             (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date 
             $addSelect
             from
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) s
        ")->addCondition("selected_date >= DATE_FORMAT(:pstart, '%Y-%m-%d') and selected_date <= DATE_FORMAT(:pend, '%Y-%m-%d')")
            ->addParams([
                ":pstart"   => $periodeStart,
                ":pend"     => $periodeEnd
            ])
        ;
        return $dbt1;
    }
}
