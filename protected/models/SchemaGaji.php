<?php
Yii::import('application.models._base.BaseSchemaGaji');
class SchemaGaji extends BaseSchemaGaji
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function checkFormula($formula)
    {
        $periode_id = 'e9d36216-41ca-11e7-a778-1e5656ac5cc8';
        /** @var Periode $periode */
        $periode = Periode::model()->findByPk($periode_id);
        if ($periode == null) {
            throw new Exception('Fatal Error. Periode tidak ditemukan.');
        }
        /** @var Pegawai $allPegawai */
        $allPegawai = Pegawai::model()->find();
        $phpCode = '$PHJ__= '.$periode->getCount().';' . PHP_EOL;
        $phpCode .= '$PJOFF__=' . $periode->jumlah_off . ';' . PHP_EOL;
        $phpCode .= '$HK__= 25;' . PHP_EOL;
        $phpCode .= '$LK__= 2;' . PHP_EOL;
        $phpCode .= '$CT__= 1;' . PHP_EOL;
        $phpCode .= '$JOFF__= 4;' . PHP_EOL;
        $phpCode .= '$GOFF__= 4;' . PHP_EOL;
        $phpCode .= '$SICK__= 2;' . PHP_EOL;
//        $gol = Golongan::model()->find();
//        if ($gol == null) {
//            throw new Exception('Master Golongan harus diinput dulu.');
//        }
//        $mGaji = [];
        $masterGaji = Yii::app()->db
            ->createCommand("SELECT * FROM pbu_master_gaji_view WHERE pegawai_id = :pegawai_id")
            ->queryAll(true, [':pegawai_id' => $allPegawai]);
//        foreach ($masterGaji as $row) {
//            $mGaji[$row['master_id']][$row['leveling_id']][$row['golongan_id']] = $row['amount'];
//        }
        /** @var Master[] $master */
        $master = Master::model()->findAll();
        foreach ($master as $md1) {
            $phpCode .= '$' . $md1->kode . '=0;' . PHP_EOL;
        }
        foreach ($masterGaji as $md) {
            $phpCode .= '$' . $md['mkode'] . '=' . $md['amount'] . ';' . PHP_EOL;
        }
        $formula = $phpCode . $formula;
        $dirName = Yii::getPathOfAlias('application.runtime.' . Yii::app()->controller->id);
        $tmpfname = tempnam($dirName, 'tpay');
        $handle = fopen($tmpfname, "w+");
        fwrite($handle, "<?php\n" . $formula);
        fclose($handle);
        try {
            include $tmpfname;
        } catch (Exception $e) {
            unlink($tmpfname);
            throw new Exception('Require failed! Error: ' . $e);
        }
        unlink($tmpfname);
        if (!isset($hasilFormula__)) {
            throw new Exception('Variable $hasilFormula__ harus diset');
        }
        if (is_angka($hasilFormula__) === false) {
            throw new Exception('Hasil $hasilFormula__ harus numeric [' . $hasilFormula__ . ']');
        }
        return $phpCode;
    }
    public function beforeValidate()
    {
        if ($this->schema_gaji_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->schema_gaji_id = $uuid;
        }
        SchemaGaji::checkFormula($this->formula);
        return parent::beforeValidate();
    }
}