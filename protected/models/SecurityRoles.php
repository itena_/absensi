<?php

Yii::import('application.models._base.BaseSecurityRoles');

class SecurityRoles extends BaseSecurityRoles
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function beforeValidate()
    {
        if ($this->security_roles_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->security_roles_id = $uuid;
        }
        return parent::beforeValidate();
    }
    
    public function checkSecurityRoleId(){
        $id = Yii::app()->user->getId();
        $sec = Users::model()->findByAttributes(array('id' => $id))->security_roles_id;
        return $sec;
    }
}