<?php

Yii::import('application.models._base.BaseShift');

class Shift extends BaseShift {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function beforeValidate()
    {
        if ($this->shift_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->shift_id = $uuid;
        }
        return parent::beforeValidate();
    }
}
