<?php

Yii::import('application.models._base.BaseShiftpin');

class Shiftpin extends BaseShiftpin {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function beforeValidate()
    {
        if ($this->shiftpin_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->shiftpin_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null || $this->user_id == "") {
            $user = Users::model()->findByPk(Yii::app()->user->getId());
            $this->user_id = $user->id;
        }
        return parent::beforeValidate();
    }
}
