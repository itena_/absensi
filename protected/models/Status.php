<?php

Yii::import('application.models._base.BaseStatus');

class Status extends BaseStatus
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function beforeValidate()
    {
        if ($this->status_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->status_id = $uuid;
        }
        return parent::beforeValidate();
    }
}