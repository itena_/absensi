<?php

Yii::import('application.models._base.BaseStatusHk');

class StatusHk extends BaseStatusHk
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function beforeValidate() {
        if ($this->status_hk_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->status_hk_id = $uuid;
        }
        if ($this->active === null) {
            $this->active = 1;
        }
        $this->tdate    = new CDbExpression('NOW()');
        $this->user_id  = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}