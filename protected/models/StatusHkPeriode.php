<?php

Yii::import('application.models._base.BaseStatusHkPeriode');

class StatusHkPeriode extends BaseStatusHkPeriode
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate() {
        if ($this->status_hk_periode_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->status_hk_periode_id = $uuid;
        }
        $this->tdate    = new CDbExpression('NOW()');
        $this->user_id  = Yii::app()->user->getId();
        return parent::beforeValidate();
    }
}