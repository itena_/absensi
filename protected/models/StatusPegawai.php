<?php
Yii::import('application.models._base.BaseStatusPegawai');
class StatusPegawai extends BaseStatusPegawai
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->status_pegawai_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->status_pegawai_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function get_status_pegawai_by_id($id) {
        $sp = StatusPegawai::model()->findByPk($id)->kode;
        if($sp == 'AKTIF' || $sp == 'TRAINING')
            $status_pegawai = 'AKTIF';
        else
            $status_pegawai = 'NON AKTIF';

        return $status_pegawai;
    }
}