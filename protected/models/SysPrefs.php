<?php

Yii::import('application.models._base.BaseSysPrefs');

class SysPrefs extends BaseSysPrefs
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function beforeValidate()
    {
        if ($this->sys_prefs_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sys_prefs_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function get_val($index)
    {
        $pref = SysPrefs::model()->find('name_ = :name_',
            array(':name_' => $index));
        if($pref == null){
            return "";
        }
        return $pref->value_;
    }
}