<?php

Yii::import('application.models._base.BaseTemplate');

class Template extends BaseTemplate {

    public function beforeValidate() {
        if ($this->template_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->template_id = $uuid;
        }
        return parent::beforeValidate();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
