<?php

Yii::import('application.models._base.BaseTemplateDetail');

class TemplateDetail extends BaseTemplateDetail {

    public function beforeValidate() {
        if ($this->template_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->template_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
