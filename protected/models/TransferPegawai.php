<?php

Yii::import('application.models._base.BaseTransferPegawai');

class TransferPegawai extends BaseTransferPegawai implements \Countable
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function beforeValidate()
    {
        if ($this->transfer_pegawai_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_pegawai_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null || $this->user_id == "") {
            $user = Users::model()->findByPk(Yii::app()->user->getId());
            $this->user_id = $user->id;
        }
        return parent::beforeValidate();
    }
    
    public static function checkExistin ($tglin, $pegawai_id){
        $existin = TransferPegawai::model()->find('tglin = :tglin AND pegawai_id = :pegawai_id', [
                ':tglin' => $tglin,
                'pegawai_id' => $pegawai_id
            ]);
        return $existin;
    }
    public static function checkExist ($tgl, $pegawai_id){
        $transferpegawai = TransferPegawai::model()->findAll('pegawai_id = :pegawai_id AND visible = 1', [
            'pegawai_id' => $pegawai_id
        ]);

        $exist = FALSE;
        foreach ($transferpegawai as $tp){ 
            $tp_start = $tp->tglin;
            $tp_end = $tp->tglout;
            if (($tgl >= $tp_start) && ($tgl <= $tp_end)){  
                $exist = TRUE;
            } else {
                continue;
            }
        }         
        return $exist;
    }
    
    public function checkPegawai ($periode_id, $pegawai_id, $tglin, $tglout){
        $tp = TransferPegawai::model()->find("
                periode_id = :periode_id AND pegawai_id = :pegawai_id 
                AND (:xf_jamin BETWEEN tglin AND tglout) 
                AND (:xf_jamout BETWEEN tglin AND tglout) 
                AND visible = 1", [
                ':periode_id' => $periode_id, 'pegawai_id' => $pegawai_id,
                ':xf_jamin' => date("Y-m-d", strtotime($tglin)),
                ':xf_jamout' => date("Y-m-d", strtotime($tglout))
            ]); 
        return $tp;
    }
}