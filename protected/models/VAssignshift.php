<?php

Yii::import('application.models._base.BaseVAssignshift');

class VAssignshift extends BaseVAssignshift {

    public function primaryKey() {
        return 'shiftpin_id';
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
