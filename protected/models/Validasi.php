<?php

Yii::import('application.models._base.BaseValidasi');

class Validasi extends BaseValidasi
{

    public function beforeValidate()
    {
        if ($this->validasi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->validasi_id = $uuid;
        }
        $this->tdate = new CDbExpression('NOW()');
        if ($this->user_id == null || $this->user_id == "") {
            $user = Users::model()->findByPk(Yii::app()->user->getId());
            $this->user_id = $user->id;
        }
        return parent::beforeValidate();
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function addAdditionalValidasi($pegawai_id, $periode_id)
    {
        $pegawai = Pegawai::model()->findByPk($pegawai_id);

        $jabatan        = Jabatan::model()->findByPk($pegawai->jabatan_id);
        $level          = Leveling::model()->findByPk($pegawai->leveling_id);
        $golongan       = Golongan::model()->findByPk($pegawai->golongan_id);
        $status_pegawai = StatusPegawai::get_status_pegawai_by_id($pegawai->status_pegawai_id);

        $array = [];
        $array['jabatan']        = isset($jabatan) ? $jabatan->kode : null;
        $array['level']          = isset($level) ? $level->kode : null;
        $array['golongan']       = isset($golongan) ? $golongan->kode : null;
        $array['status_pegawai_id'] = $pegawai->status_pegawai_id;
        $array['status_pegawai'] = isset($status_pegawai) ? $status_pegawai : null;
        $array['periode_id']     = $periode_id;

        return $array;
    }

    public static function saveValidasi($valid, $pegawai_id, $k, $shift_id, $duras, $kode_ket, $cabang_id, $no_surat_tugas, $cuti = 0, $wfh = null ,$cuti_tahun = null)
    {
        $validasi = [];
        $validasi['pegawai_id'] = $pegawai_id;
        $validasi['pin_id']     = $k['PIN'];
        $validasi['PIN']        = $k['PIN'];
        $validasi['in_time']    = $k['jam_in'];
        $validasi['out_time']   = $k['jam_out'];
        $validasi['shift_id']   = $shift_id;

        $validasi['min_early_time']     = $duras['min_early'];
        $validasi['min_late_time']      = $duras['min_late'];
        $validasi['min_least_time']     = $duras['min_least'];
        $validasi['min_over_time_awal'] = $duras['min_over_sejam_awal'];
        $validasi['min_over_time']      = $duras['min_over'];

        $validasi['real_lembur_pertama']    = $duras['real_lembur_pertama'];
        $validasi['real_lembur_akhir']      = $duras['real_lembur_akhir'];
        $validasi['real_lembur_hari']       = $duras['real_lembur_hari'];
        $validasi['real_less_time']         = $duras['real_less_time'];
        //-----end hitung lembur------

        $validasi['kode_ket']           = $kode_ket;
        $validasi['status_int']         = 1;
        $validasi['cabang_id']          = $cabang_id;
        $validasi['no_surat_tugas']     = $no_surat_tugas;
        $validasi['approval_lembur']    = 0;
        $validasi['cuti']               = $cuti;
        $validasi['cuti_tahun']          = $cuti_tahun;
        $validasi['wfh']                = $wfh;

        return $validasi;
    }

    public static function check_tanggal($periode_id, $dari, $sampai)
    {
        $db = new DbCmd();
        $db->addFrom("{{periode}}")
            ->addSelect("DATE_FORMAT(periode_start,'%Y-%m-%d %H:%i:%s') pstart")
            ->addSelect("(DATE_FORMAT(periode_end,'%Y-%m-%d %H:%i:%s')+ INTERVAL :inter HOUR) pend ")
            ->addCondition("periode_id = :periode_id")
            ->addParams([
                ":periode_id" => $periode_id,
                ':inter' => GET_INVTERVAL_ABSEN()
            ]);
        $p = $db->queryRow();
        $start          = strtotime($p['pstart']);
        $end            = strtotime($p['pend']);
        $dari_absen     = strtotime($dari);
        $sampai_absen   = strtotime($sampai);

        if (($dari_absen >= $start) && ($dari_absen <= $end) && ($sampai_absen >= $start) && ($sampai_absen <= $end)) {
            return '';
        } else {
            $msg = '<b>GAGAL!</b></br>Tanggal Yang Anda Masukkan Diluar Periode!!!';
            Log::generateErrorMessage(false, $msg);
        }
    }

    public static function getAbsensi($in, $out)
    {
        $cmd = new DbCmd();
        $cmd->addFrom("{{validasi_view}} v")
            ->addSelect("j.kode")
            ->addSelect("v.kode_cabang")
            ->addSelect("v.PIN nik, p.nama_lengkap")
            ->addLeftJoin("{{pegawai}} p", "p.pegawai_id = v.pegawai_id")
            ->addLeftJoin("{{jabatan}} j", "j.jabatan_id = p.jabatan_id")
            ->addCondition("in_time >= :in_time AND out_time <= (:out_time + INTERVAL :INVTERVAL_ABSEN HOUR)")
            ->addParam(":in_time", $in)
            ->addParam(":out_time", $out)
            ->addParam(":INVTERVAL_ABSEN", GET_INVTERVAL_ABSEN());
        return $cmd;
    }

    public static function getValidasi($params)
    {
        return self::getDataValidasi($params);
    }

    public static function getValidasiApi($params)
    {
        // request from api
        return self::getDataValidasi($params, true);
    }

    private static function getDataValidasi($params, $api = false) {
        $periode_id     = isset($params['periode_id']) ? $params['periode_id'] : false;
        $bu_id          = isset($params['bu_id']) ? $params['bu_id'] : false;
        $cabang_id      = isset($params['cabang_id']) ? $params['cabang_id'] : false;
        $show_detail    = isset($params['show_detail']) ? $params['show_detail'] : false;
        $urut_jabatan   = isset($params['urut_jabatan']) ? $params['urut_jabatan'] : false;
        $dalam_jam      = isset($params['dalam_jam']) ? $params['dalam_jam'] : false;
        $tglin          = (isset($params['tglin']) && $params['tglin'] != null) ? $params['tglin'] : false;
        $tglout         = (isset($params['tglout']) && $params['tglout'] != null) ? $params['tglout'] : false;
        $payroll        = isset($params['payroll']) ? $params['payroll'] : false;
        $api            = (!$tglin) ? false : true;

        if ($periode_id) {
            $periode    = Periode::model()->findByPk($periode_id);
            $pstart     = $periode->periode_start;
            $pend       = $periode->periode_end;
        }

        $q_kode_ket = 'kode_ket = 3 OR kode_ket = 0 OR kode_ket = 1 OR kode_ket = 10';

        $db         = new DbCmd();
        $validasi   = new DbCmd();
        $dk			= new DbCmd();

        $dk->select('tz.*, SUM((DATEDIFF( IF(date(tz.tglout) > date(:akhir), date(:akhir), date(tz.tglout) ), date(tz.tglin)))+1) as lama');
        $dk->from('{{transfer_pegawai}} as tz');
        $dk->addCondition('date(tz.tglin) >= date(:awal) and tz.status_kota = 0 and tz.visible = 1');
        $dk->groupBy('tz.pegawai_id, tz.cabang_transfer_id');

        if (!$api) {
            $db->addParams([':periode_id' => $periode_id]);
            $select_param = "v.periode_id = :periode_id";
        }
        else {
            $db->addParams([':awal' => $tglin, ':akhir' => $tglout]);
            $select_param = " date(v.in_time) >= date(:awal) AND date(v.in_time) <= date(:akhir)";
        }

        if ($bu_id) {
            $bu_ids = DbCmd::instance()->addSelect("cabang_id")->addFrom("{{cabang}}")
                ->addCondition("bu_id = :bu_id")->addParam("bu_id", $bu_id)->queryAll();

            if (count($bu_ids) > 0) {
                $arr = [];
                foreach ($bu_ids as $ids) {
                    $arr[] = $ids['cabang_id'];
                }
                $validasi->addInCondition("v.cabang_id", $arr);
            }
        }

        if ($cabang_id) {
            $db->addParams([':cabang_id' => $cabang_id]);

            if ($show_detail == 'on') {
                $validasi->addCondition("v.cabang_id = :cabang_id");
            } else {
//                $validasi->addCondition("(t1.cabang_id = :cabang_id OR v.cabang_id = :cabang_id)");
                $validasi->addCondition("t1.cabang_id = :cabang_id");
            }

            if ($urut_jabatan == 'on') {
                $urutan_jabatan = !Jabatan::model()->find("urutan != 0") ? $order = "t1.kode" : $order = "t1.urutan";
            } else {
                $order = "t1.nik";
            }
            $validasi->addOrder($order);
        }

        $validasi->addSelect(
            "t1.* 
                , (SUM(kode_ket = 0) + SUM(kode_ket = 3) + SUM(kode_ket = 10)) AS HK
                , SUM(CASE WHEN $select_param THEN kode_ket = 1 ELSE 0 END ) AS LK
                , SUM(CASE WHEN $select_param THEN kode_ket = 2 ELSE 0 END ) AS S
                , SUM(CASE WHEN $select_param THEN kode_ket = 4 ELSE 0 END ) AS OFF
                , SUM(CASE WHEN $select_param THEN kode_ket = 5 ELSE 0 END ) AS CT
                , SUM(CASE WHEN $select_param THEN kode_ket = 6 ELSE 0 END ) AS CM
                , SUM(CASE WHEN $select_param THEN kode_ket = 7 ELSE 0 END ) AS CB
                , SUM(CASE WHEN $select_param THEN kode_ket = 8 AND status_wfh = 0 ELSE 0 END ) AS CI
                , SUM(CASE WHEN $select_param THEN kode_ket = 8 AND status_wfh = 1 ELSE 0 END ) AS WFH
                , SUM(CASE WHEN $select_param THEN kode_ket = 9 ELSE 0 END ) AS CNA
                , SUM(CASE WHEN $select_param THEN kode_ket = 10 ELSE 0 END ) AS PP
                , SUM(if(v.approval_lesstime = 1, IF($q_kode_ket,v.real_less_time,0),0)) AS LT
                , SUM(if(v.approval_lembur = 1,v.real_lembur_pertama,0)) AS real_lembur_pertama
                , SUM(if(v.approval_lembur = 1,v.real_lembur_akhir,0)) AS real_lembur_akhir
                , SUM(if(v.approval_lembur = 1,v.real_lembur_hari,0)) AS lemburh
                ,v.jabatan, v.`level`, v.golongan, v.status_pegawai, if(INSTR(v.no_surat_tugas,'$(PB)$')>0,1,0) pb
                ,LEFT(MIN(v.in_time),10) first_day, LEFT(MAX(v.in_time),10) last_day, v.cabang_id AS cabang_presensi_id
                "
        )->addFrom("{{validasi}} v")
            ->addCondition("v.status_int = 1
                        AND $select_param
            ")
            ->addGroup("t1.pegawai_id");

        /*$validasi->addLeftJoin("{{periode}} pr", "pr.periode_id = v.periode_id")
            ->addSelect("jumlah_off AS jatah_off,  jumlah_off - SUM(CASE WHEN $select_param THEN kode_ket = 4 ELSE 0 END ) AS sisa_off")
        ;*/

        if($api) {
            $validasi->addLeftJoin("(" . Pegawai::getPegawaiApi()->getQuery() . ") t1", "t1.pegawai_id = v.pegawai_id");
            $validasi->addLeftJoin('('.$dk->getQuery().') as tp', 'tp.pegawai_id=v.pegawai_id and tp.cabang_transfer_id=v.cabang_id');
            $validasi->addSelect('IFNULL(tp.lama, 0) AS DK');
        }
        else {
            $validasi->addInnerJoin("(" . Pegawai::getPegawai()->addCondition("p.status_pegawai_id is not null")->getQuery() . ") t1", "t1.pegawai_id = v.pegawai_id");
            /*if($payroll)
                $validasi->addInnerJoin("(" . Pegawai::getPegawai()->addCondition("p.status_pegawai_id is not null")->getQuery() . ") t1", "t1.pegawai_id = v.pegawai_id");
            else
                $validasi->addInnerJoin("(" . Pegawai::getPegawai()->addCondition("sp.kode = 'AKTIF' OR sp.kode = 'TRAINING'")->getQuery() . ") t1", "t1.pegawai_id = v.pegawai_id");*/
        }

        if ($payroll) {
            $validasi->addGroup("v.`level`, v.golongan, v.status_pegawai");
        }

        if ($dalam_jam) {
            $validasi->addSelect("            
                SUM(if(v.approval_lesstime = 1, IF($q_kode_ket,v.real_less_time,'')/60,'')) AS LT1
                , SUM(if(v.approval_lembur = 1,v.real_lembur_pertama,'')/60) AS real_lembur_pertama1
                , SUM(if(v.approval_lembur = 1,v.real_lembur_akhir,'')/60) AS real_lembur_akhir1
            ");
        }

        $db->addSelect("t2.* , (HK + LK + S + OFF + CT + CM + CB + CI + WFH + CNA) AS TOTAL")
            ->addFrom("(" . $validasi->getQuery() . ") t2");

        if (GET_USE_STATUS_HK() == true) {
            $db->addSelect("shk.nama status_hk")
                ->addLeftJoin("{{status_hk_periode}} shkp", "shkp.pegawai_id = t2.pegawai_id AND shkp.periode_id = :periode_id 
                AND shkp.cabang_id = t2.cabang_id")
                ->addLeftJoin("{{status_hk}} shk", "shk.status_hk_id = shkp.status_hk_id")
                ->addParam(":periode_id", $periode_id);
        }

        $xxx = $db->getQuery();
        return $db;
    }

    public static function getMaternityDay($params) {
        $arr = [];
        $datas = json_decode($params['datas']);

        foreach ($datas as $data) {
            $cmd = DbCmd::instance()->addFrom("{{validasi}} v")
                ->addSelect("pegawai_id, PIN, cabang_id, LEFT(MIN(in_time),10) first_day_Cuti, LEFT(MAX(in_time),10) last_day_Cuti")
                ->addCondition("pegawai_id = :pegawai_id")
                ->addCondition("cabang_id = :cabang_id")
                ->addCondition("periode_id = :periode_id")
                ->addCondition("status_int = 1 AND kode_ket = 7")
                ->addParams([
                    ':pegawai_id' => $data->pegawai_id,
                    ':cabang_id' => $data->cabang_id,
                    ':periode_id' => $params['periode_id']
                ])->queryRow()
            ;

            $arr[] = $cmd;
        }
        return $arr;
    }

    public static function hitungGantiOff($jatah_off, $lemburh, $off, $sakit) {
        /*1. Ganti off = jatah off + lembur hari - ambil off - sakit
        - kalau jatah off + lembur hari - ambil off < 0 maka bisa minus
        - kalau jatah off + lembur hari - ambil off < 0 dan ada sakit, sakit tidak mempengaruhi minus
        - kalau jatah off + lembur hari - ambil off > 0 dan ada sakit, sakit mengurangi off tapi tidak sampai minus*/

        $gantiOff = $jatah_off + $lemburh - $off;
        if($gantiOff > 0 && $sakit > 0) {
            $gantiOff = $gantiOff - $sakit;
            if($gantiOff < 0)
                $gantiOff = 0;
        }

        return $gantiOff;
    }

    public static function hitungJatahOffNew($libur, $pegawai_id, $periode_id, $cabang_id, $show_detail) {
        $cmd = DbCmd::instance()->addSelect("count(*)")
            ->addFrom("{{validasi}} v")
            ->addLeftJoin("{{pegawai}} pg", "pg.pegawai_id = v.pegawai_id")
            ->addCondition("v.pegawai_id = :pegawai_id AND periode_id = :periode_id AND status_int = 1")
            ->addCondition("kode_ket not in ('7','9')")
            ->addCondition($libur)
            ->addParams([
                ':pegawai_id' => $pegawai_id,
                ':periode_id' => $periode_id
            ])
            ;

        if ($show_detail == 'on') {
            $cmd->addCondition("v.cabang_id = :cabang_id")->addParam(':cabang_id', $cabang_id);
        } else {
            $cmd->addCondition("(pg.cabang_id = :cabang_id OR v.cabang_id = :cabang_id)")->addParam(':cabang_id', $cabang_id);
        }

        return $cmd->queryScalar();
    }

    public static function HitungAmbilCuti($nik, $tahun)
    {
        $cuti_tahunan = ['5'];

        $db = new DbCmd();
        $db->select('count(0) as jml_cuti');
        $db->from('pbu_pegawai as p');
        $db->leftJoin('pbu_validasi as v', 'p.pegawai_id=v.pegawai_id');
        $db->addCondition(' p.nik = :nik');
        //mulai pencatatan cuti di JULI 2024
        $db->addCondition('year(v.in_time) >= 2024 and v.status_int = 1');
        $db->addInCondition('v.kode_ket', $cuti_tahunan);
        $db->addParam(':nik', $nik);

        $db->addCondition(' v.cuti_tahun = :cuti_tahun');
        $db->addParam(':cuti_tahun', $tahun);
        return $db->queryScalar();
    }
}
