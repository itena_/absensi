<?php

/**
 * This is the model base class for the table "{{cabang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Cabang".
 *
 * Columns in table "{{cabang}}" available as properties of the model,
 * followed by relations of table "{{cabang}}" available as properties of the model.
 *
 * @property string $cabang_id
 * @property string $kode_cabang
 * @property string $nama_cabang
 * @property string $bu_id
 * @property string $alamat_id
 * @property string $alamat_cabang
 * @property integer $kepala_cabang_stat
 * @property string $area_id
 *
 * @property Ip[] $ips
 * @property LockPost[] $lockPosts
 * @property Shift[] $shifts
 * @property StatusHkPeriode[] $statusHkPeriodes
 * @property TransferPegawai[] $transferPegawais
 * @property TransferPegawai[] $transferPegawais1
 */
abstract class BaseCabang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{cabang}}';
	}

	public static function representingColumn() {
		return 'area_id';
	}

	public function rules() {
		return array(
			array('cabang_id, area_id', 'required'),
			array('kepala_cabang_stat', 'numerical', 'integerOnly'=>true),
			array('cabang_id, bu_id, alamat_id, area_id', 'length', 'max'=>36),
			array('kode_cabang', 'length', 'max'=>50),
			array('nama_cabang', 'length', 'max'=>100),
			array('alamat_cabang', 'safe'),
			array('kode_cabang, nama_cabang, bu_id, alamat_id, alamat_cabang, kepala_cabang_stat', 'default', 'setOnEmpty' => true, 'value' => null),
			array('cabang_id, kode_cabang, nama_cabang, bu_id, alamat_id, alamat_cabang, kepala_cabang_stat, area_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'ips' => array(self::HAS_MANY, 'Ip', 'cabang_id'),
			'lockPosts' => array(self::HAS_MANY, 'LockPost', 'cabang_id'),
			'shifts' => array(self::HAS_MANY, 'Shift', 'cabang_id'),
			'statusHkPeriodes' => array(self::HAS_MANY, 'StatusHkPeriode', 'cabang_id'),
			'transferPegawais' => array(self::HAS_MANY, 'TransferPegawai', 'cabang_id'),
			'transferPegawais1' => array(self::HAS_MANY, 'TransferPegawai', 'cabang_transfer_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'cabang_id' => Yii::t('app', 'Cabang'),
			'kode_cabang' => Yii::t('app', 'Kode Cabang'),
			'nama_cabang' => Yii::t('app', 'Nama Cabang'),
			'bu_id' => Yii::t('app', 'Bu'),
			'alamat_id' => Yii::t('app', 'Alamat'),
			'alamat_cabang' => Yii::t('app', 'Alamat Cabang'),
			'kepala_cabang_stat' => Yii::t('app', 'Kepala Cabang Stat'),
			'area_id' => Yii::t('app', 'Area'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('cabang_id', $this->cabang_id, true);
		$criteria->compare('kode_cabang', $this->kode_cabang, true);
		$criteria->compare('nama_cabang', $this->nama_cabang, true);
		$criteria->compare('bu_id', $this->bu_id, true);
		$criteria->compare('alamat_id', $this->alamat_id, true);
		$criteria->compare('alamat_cabang', $this->alamat_cabang, true);
		$criteria->compare('kepala_cabang_stat', $this->kepala_cabang_stat);
		$criteria->compare('area_id', $this->area_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}