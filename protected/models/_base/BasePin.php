<?php

/**
 * This is the model base class for the table "{{pin}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Pin".
 *
 * Columns in table "{{pin}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $pegawai_id
 * @property string $pin_id
 * @property string $PIN
 * @property string $nama_lengkap
 * @property string $store
 * @property string $cabang_id
 *
 */
abstract class BasePin extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{pin}}';
	}

	public static function representingColumn() {
		return 'pegawai_id';
	}

	public function rules() {
		return array(
			array('pegawai_id, pin_id, PIN, nama_lengkap, store, cabang_id', 'required'),
			array('pegawai_id, cabang_id', 'length', 'max'=>36),
			array('pin_id, PIN', 'length', 'max'=>50),
			array('nama_lengkap', 'length', 'max'=>100),
			array('store', 'length', 'max'=>20),
			array('pegawai_id, pin_id, PIN, nama_lengkap, store, cabang_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pegawai_id' => Yii::t('app', 'Pegawai'),
			'pin_id' => Yii::t('app', 'Pin'),
			'PIN' => Yii::t('app', 'Pin'),
			'nama_lengkap' => Yii::t('app', 'Nama Lengkap'),
			'store' => Yii::t('app', 'Store'),
			'cabang_id' => Yii::t('app', 'Cabang'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pegawai_id', $this->pegawai_id, true);
		$criteria->compare('pin_id', $this->pin_id, true);
		$criteria->compare('PIN', $this->PIN, true);
		$criteria->compare('nama_lengkap', $this->nama_lengkap, true);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('cabang_id', $this->cabang_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}