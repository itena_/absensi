ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `pbu_validasi_view` AS 
select `v`.`validasi_id` AS `validasi_id`,`v`.`pin_id` AS `pin_id`,`v`.`PIN` AS `PIN`,`v`.`in_time` AS `in_time`,`v`.`out_time` AS `out_time`,`v`.`min_early_time` AS `min_early_time`,`v`.`min_late_time` AS `min_late_time`,`v`.`min_least_time` AS `min_least_time`,`v`.`min_over_time_awal` AS `min_over_time_awal`,`v`.`min_over_time` AS `min_over_time`,`v`.`tdate` AS `tdate`,`v`.`kode_ket` AS `kode_ket`,`v`.`no_surat_tugas` AS `no_surat_tugas`,`v`.`status_int` AS `status_int`,`v`.`result_id` AS `result_id`,`v`.`user_id` AS `user_id`,`v`.`status_pegawai_id` AS `status_pegawai_id`,`v`.`shift_id` AS `shift_id`,`c`.`kode_cabang` AS `kode_cabang` from ((`pbu_validasi` `v` left join `pbu_pegawai` `p` on((`p`.`nik` = `v`.`PIN`))) left join `pbu_cabang` `c` on((`c`.`cabang_id` = `p`.`cabang_id`))) where (`v`.`status_int` = 1) order by `v`.`PIN`,`c`.`kode_cabang` ;