ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `pbu_result_view` AS 
SELECT
	`r`.`result_id` AS `result_id`,
	`r`.`pegawai_id` AS `pegawai_id`,
	`r`.`pin_id` AS `pin_id`,
	`r`.`periode_id` AS `periode_id`,
	`r`.`total_hari_kerja` AS `total_hari_kerja`,
	`r`.`locked` AS `locked`,
	`r`.`total_lk` AS `total_lk`,
	`r`.`total_sakit` AS `total_sakit`,
	`r`.`total_off` AS `total_off`,
	`r`.`total_cuti_tahunan` AS `total_cuti_tahunan`,
	`r`.`total_cuti_menikah` AS `total_cuti_menikah`,
	`r`.`total_cuti_bersalin` AS `total_cuti_bersalin`,
	`r`.`total_cuti_istimewa` AS `total_cuti_istimewa`,
	`r`.`total_cuti_non_aktif` AS `total_cuti_non_aktif`,
	`r`.`total_min_lembur_awal` AS `total_min_lembur_awal`,
	`r`.`total_min_lembur_akhir` AS `total_min_lembur_akhir`,
	`r`.`total_min_late_time` AS `total_min_late_time`,
	`r`.`cabang_id` AS `cabang_id`,
	`r`.`user_id` AS `user_id`,
	`r`.`tdate` AS `tdate`,
	`c`.`kode_cabang` AS `kode_cabang`,
	(
		SELECT
			ifnull(
				sum(`v`.`min_early_time`),
				0
			)
		FROM
			`pbu_validasi` `v`
		WHERE
			(
				(`v`.`approval_lembur` = 1)
				AND (`v`.`status_int` = 1)
				AND (`v`.`PIN` = `r`.`pin_id`)
				AND (
					`v`.`result_id` = `r`.`result_id`
				)
			)
	) AS `real_lembur_awal`,
	(
		SELECT
			ifnull(
				sum(`v`.`min_over_time_awal_real`),
				0
			)
		FROM
			`pbu_validasi_view` `v`
		WHERE
			(
				(`v`.`approval_lembur` = 1)
				AND (`v`.`status_int` = 1)
				AND (`v`.`PIN` = `r`.`pin_id`)
				AND (
					`v`.`result_id` = `r`.`result_id`
				)
			)
	) AS `real_lembur_pertama`,
	(
		SELECT
			ifnull(sum(`v`.`min_over_time_real`), 0)
		FROM
			`pbu_validasi_view` `v`
		WHERE
			(
				(`v`.`approval_lembur` = 1)
				AND (`v`.`status_int` = 1)
				AND (`v`.`PIN` = `r`.`pin_id`)
				AND (
					`v`.`result_id` = `r`.`result_id`
				)
			)
	) AS `real_lembur_akhir`,
	(
		SELECT
			ifnull(sum(`v`.`lembur_hari`), 0)
		FROM
			`pbu_validasi_view` `v`
		WHERE
			(
				(`v`.`approval_lembur` = 1)
				AND (`v`.`status_int` = 1)
				AND (`v`.`PIN` = `r`.`pin_id`)
				AND (
					`v`.`result_id` = `r`.`result_id`
				)
			)
	) AS `real_lembur_hari`
FROM
	(
		(
			`pbu_result` `r`
			LEFT JOIN `pbu_pegawai` `p` ON ((`p`.`nik` = `r`.`pin_id`))
		)
		LEFT JOIN `pbu_cabang` `c` ON (
			(
				`c`.`cabang_id` = `r`.`cabang_id`
			)
		)
	)
ORDER BY
	`r`.`pin_id`,
	`c`.`kode_cabang` ;
ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `pbu_validasi_view` AS 
SELECT
	`v`.`validasi_id` AS `validasi_id`,
	`v`.`pin_id` AS `pin_id`,
	`v`.`PIN` AS `PIN`,
	`v`.`pegawai_id` AS `pegawai_id`,
	`v`.`in_time` AS `in_time`,
	`v`.`out_time` AS `out_time`,
	`v`.`min_early_time` AS `min_early_time`,
	`v`.`min_late_time` AS `min_late_time`,
	`v`.`min_least_time` AS `min_least_time`,
	`v`.`min_over_time_awal` AS `min_over_time_awal`,
	`v`.`min_over_time` AS `min_over_time`,	
	(`v`.`min_over_time` + `v`.`min_over_time_awal`) AS total_lembur,
	if(`v`.`min_over_time` + `v`.`min_over_time_awal` > 60, if(`v`.`min_over_time` + `v`.`min_over_time_awal` >= 480 ,0,60),`v`.`min_over_time_awal`) AS `min_over_time_awal_real`,
	if(`v`.`min_over_time` + `v`.`min_over_time_awal` > 180, if(`v`.`min_over_time` + `v`.`min_over_time_awal` >= 480 ,0,180),`v`.`min_over_time`) AS `min_over_time_real`,
	if(`v`.`min_over_time` + `v`.`min_over_time_awal` >= 480, 1, 0) AS `lembur_hari`,
	`v`.`tdate` AS `tdate`,
	`v`.`kode_ket` AS `kode_ket`,
	`v`.`no_surat_tugas` AS `no_surat_tugas`,
	`v`.`status_int` AS `status_int`,
	`v`.`result_id` AS `result_id`,
	`v`.`user_id` AS `user_id`,
	`v`.`status_pegawai_id` AS `status_pegawai_id`,
	`v`.`shift_id` AS `shift_id`,
	`v`.`approval_lembur` AS `approval_lembur`,
	`p`.`cabang_id` AS `cabang_id`,
	`c`.`kode_cabang` AS `kode_cabang`,
	`c`.`nama_cabang` AS `nama_cabang`,
	`pp`.`cabang_id` AS `cabang_user`,
	`u`.`pegawai_id` AS `pegawai_user`
FROM
	(
		(
			(
				(
					`pbu_validasi` `v`
					LEFT JOIN `pbu_pegawai` `p` ON (
						(
							`p`.`pegawai_id` = `v`.`pegawai_id`
						)
					)
				)
				LEFT JOIN `pbu_cabang` `c` ON (
					(
						`c`.`cabang_id` = `p`.`cabang_id`
					)
				)
			)
			LEFT JOIN `pbu_users` `u` ON ((`u`.`id` = `v`.`user_id`))
		)
		LEFT JOIN `pbu_pegawai` `pp` ON (
			(
				`pp`.`pegawai_id` = `u`.`pegawai_id`
			)
		)
	)
WHERE
	(`v`.`status_int` = 1)
ORDER BY
	`v`.`PIN`,
	`c`.`kode_cabang` ;