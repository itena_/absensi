/*
* Add Keterangan Presentasi / Pameran
*/
INSERT INTO `am`.`pbu_keterangan` (`keterangan_id`, `nama_ket`) VALUES ('10', 'Presentasi / Pameran');

/*
* Add fields on validasi
*/
ALTER TABLE `pbu_validasi` ADD COLUMN `real_lembur_pertama`  int(4) NULL DEFAULT 0 AFTER `approval_lembur`;
ALTER TABLE `pbu_validasi` ADD COLUMN `real_lembur_akhir`  int(4) NULL DEFAULT 0 AFTER `real_lembur_pertama`;
ALTER TABLE `pbu_validasi` ADD COLUMN `real_lembur_hari`  int(4) NULL DEFAULT 0 AFTER `real_lembur_akhir`;
ALTER TABLE `pbu_validasi` ADD COLUMN `real_less_time`  int(4) NULL DEFAULT 0 AFTER `real_lembur_hari`;

/*
* Add fields on result
*/
ALTER TABLE `pbu_result` ADD COLUMN `total_real_lembur_pertama`  int(6) NULL DEFAULT 0 AFTER `total_min_late_time`;
ALTER TABLE `pbu_result` ADD COLUMN `total_real_lembur_akhir`  int(6) NULL DEFAULT 0 AFTER `total_real_lembur_pertama`;
ALTER TABLE `pbu_result` ADD COLUMN `total_real_lembur_hari`  int(6) NULL DEFAULT 0 AFTER `total_real_lembur_akhir`;
ALTER TABLE `pbu_result` ADD COLUMN `total_real_less_time`  int(6) NULL DEFAULT 0 AFTER `total_real_lembur_hari`;

/*
* Add trigger in sr_cabang
*/
CREATE TRIGGER `sr_cabang` BEFORE INSERT ON `pbu_sr_cabang`
FOR EACH ROW BEGIN
IF NEW.sr_cabang IS NULL OR LENGTH(NEW.sr_cabang) = 0 THEN
  SET NEW.sr_cabang = UUID();
END IF;
END;

/*
* Add unique index NIK in Pegawai
*/
CREATE UNIQUE INDEX `idx_nik` ON `pbu_pegawai`(`nik`) USING BTREE ;