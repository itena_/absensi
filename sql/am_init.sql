-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: am_init
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `nama_hari`
--

DROP TABLE IF EXISTS `nama_hari`;
/*!50001 DROP VIEW IF EXISTS `nama_hari`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `nama_hari` AS SELECT 
 1 AS `shift_id`,
 1 AS `kode_shift`,
 1 AS `in_time`,
 1 AS `out_time`,
 1 AS `earlyin_time`,
 1 AS `latein_time`,
 1 AS `earlyout_time`,
 1 AS `lateout_time`,
 1 AS `sunday`,
 1 AS `monday`,
 1 AS `tuesday`,
 1 AS `wednesday`,
 1 AS `thursday`,
 1 AS `friday`,
 1 AS `saturday`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pbu_all_cbg_bu`
--

DROP TABLE IF EXISTS `pbu_all_cbg_bu`;
/*!50001 DROP VIEW IF EXISTS `pbu_all_cbg_bu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_all_cbg_bu` AS SELECT 
 1 AS `cabang_id`,
 1 AS `kode_cabang`,
 1 AS `nama_cabang`,
 1 AS `bu_id`,
 1 AS `bu_name`,
 1 AS `bu_kode`,
 1 AS `checked`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pbu_all_lvl_bu`
--

DROP TABLE IF EXISTS `pbu_all_lvl_bu`;
/*!50001 DROP VIEW IF EXISTS `pbu_all_lvl_bu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_all_lvl_bu` AS SELECT 
 1 AS `leveling_id`,
 1 AS `kode`,
 1 AS `nama`,
 1 AS `bu_id`,
 1 AS `bu_name`,
 1 AS `bu_kode`,
 1 AS `checked`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pbu_area`
--

DROP TABLE IF EXISTS `pbu_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_area` (
  `area_id` varchar(36) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `bu_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_area`
--

LOCK TABLES `pbu_area` WRITE;
/*!40000 ALTER TABLE `pbu_area` DISABLE KEYS */;
INSERT INTO `pbu_area` VALUES ('2f6eff68-ec10-45ee-94c7-b0f9481c2995','06','WIL VI','536c83cf-767a-11e7-9b95-68f7286688e6'),('6ea8230b-4c75-41fa-99dc-448e0566837c','05','WIL V','536c83cf-767a-11e7-9b95-68f7286688e6'),('73c9841c-fffd-11e7-b22d-68f7286688e6','01','WIL I','536c83cf-767a-11e7-9b95-68f7286688e6'),('adb7becd-6540-4366-9660-f7d97b6d14ef','02','WIL II','536c83cf-767a-11e7-9b95-68f7286688e6'),('b78d8f1a-735f-4d1d-912e-15dcb342afe5','03','WIL III','536c83cf-767a-11e7-9b95-68f7286688e6'),('defd0b36-a1e4-4d2a-bd64-2e02b15dab59','04','WIL IV','536c83cf-767a-11e7-9b95-68f7286688e6');
/*!40000 ALTER TABLE `pbu_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_attendan`
--

DROP TABLE IF EXISTS `pbu_attendan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_attendan` (
  `attendan_id` varchar(36) NOT NULL,
  `pegawai_id` varchar(36) NOT NULL,
  `dt` datetime DEFAULT NULL,
  `result` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`attendan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_attendan`
--

LOCK TABLES `pbu_attendan` WRITE;
/*!40000 ALTER TABLE `pbu_attendan` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_attendan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_bu`
--

DROP TABLE IF EXISTS `pbu_bu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_bu` (
  `bu_id` varchar(36) NOT NULL,
  `bu_name` varchar(100) NOT NULL,
  `bu_nama_alias` varchar(100) DEFAULT NULL,
  `bu_kode` varchar(10) NOT NULL,
  `pt_id` varchar(36) NOT NULL,
  `last_kode` varchar(10) NOT NULL DEFAULT '100001',
  `tgl_berdiri` date NOT NULL,
  `jenis_usaha` varchar(25) DEFAULT NULL,
  `direktur` varchar(255) DEFAULT NULL,
  `almt` text,
  `profile` text,
  PRIMARY KEY (`bu_id`),
  UNIQUE KEY `bu_index_1` (`bu_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_bu`
--

LOCK TABLES `pbu_bu` WRITE;
/*!40000 ALTER TABLE `pbu_bu` DISABLE KEYS */;
INSERT INTO `pbu_bu` VALUES ('1','KP',NULL,'0000','','100001','0000-00-00',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pbu_bu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pbu_bu_tree`
--

DROP TABLE IF EXISTS `pbu_bu_tree`;
/*!50001 DROP VIEW IF EXISTS `pbu_bu_tree`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_bu_tree` AS SELECT 
 1 AS `node_id`,
 1 AS `kode`,
 1 AS `nama`,
 1 AS `parent`,
 1 AS `item_type`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pbu_cabang`
--

DROP TABLE IF EXISTS `pbu_cabang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_cabang` (
  `cabang_id` varchar(36) NOT NULL,
  `kode_cabang` varchar(50) DEFAULT NULL,
  `nama_cabang` varchar(100) DEFAULT NULL,
  `bu_id` varchar(36) DEFAULT NULL,
  `alamat_id` varchar(36) DEFAULT NULL,
  `no_telp_cabang` varchar(15) DEFAULT NULL,
  `alamat_cabang` longtext,
  `kepala_cabang_stat` tinyint(2) DEFAULT '1',
  `area_id` varchar(36) NOT NULL,
  PRIMARY KEY (`cabang_id`),
  KEY `idx_ena_cabang` (`bu_id`) USING BTREE,
  KEY `idx_ena_cabang_0` (`alamat_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_cabang`
--

LOCK TABLES `pbu_cabang` WRITE;
/*!40000 ALTER TABLE `pbu_cabang` DISABLE KEYS */;
INSERT INTO `pbu_cabang` VALUES ('523c01b6-180e-11e8-8e76-000c29988254','KP','KANTOR PUSAT','536c83cf-767a-11e7-9b95-68f7286688e6','ae717a82-427d-11e8-8e76-000c29988254',NULL,NULL,1,'73c9841c-fffd-11e7-b22d-68f7286688e6'),('d2d94c22-d3f4-11e7-9982-507b9d297990','ALL','ALL','1',NULL,NULL,'UNTUK IT',0,'0ce8ba78-6879-11e7-8546-00fff84e2916');
/*!40000 ALTER TABLE `pbu_cabang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_days`
--

DROP TABLE IF EXISTS `pbu_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_days` (
  `day_id` varchar(36) NOT NULL,
  `day_name` varchar(100) NOT NULL,
  `kode_day` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`day_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_days`
--

LOCK TABLES `pbu_days` WRITE;
/*!40000 ALTER TABLE `pbu_days` DISABLE KEYS */;
INSERT INTO `pbu_days` VALUES ('1_d','Sunday',0),('2_d','Monday',1),('3_d','Tuesday',2),('4_d','Wednesday',3),('5_d','Thursday',4),('6_d','Friday',5),('7_d','Saturday',6);
/*!40000 ALTER TABLE `pbu_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_days_2`
--

DROP TABLE IF EXISTS `pbu_days_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_days_2` (
  `day_id` varchar(36) NOT NULL,
  `day_name` varchar(100) NOT NULL,
  `kode_day` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`day_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_days_2`
--

LOCK TABLES `pbu_days_2` WRITE;
/*!40000 ALTER TABLE `pbu_days_2` DISABLE KEYS */;
INSERT INTO `pbu_days_2` VALUES ('1_d','Sunday',0),('2_d','Monday',1),('3_d','Tuesday',2),('4_d','Wednesday',3),('5_d','Thursday',4),('6_d','Friday',5),('7_d','Saturday',6);
/*!40000 ALTER TABLE `pbu_days_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_dayshift`
--

DROP TABLE IF EXISTS `pbu_dayshift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_dayshift` (
  `dayshift_id` varchar(36) NOT NULL,
  `shift_id` varchar(36) DEFAULT NULL,
  `day_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`dayshift_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_dayshift`
--

LOCK TABLES `pbu_dayshift` WRITE;
/*!40000 ALTER TABLE `pbu_dayshift` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_dayshift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pbu_fingerprint`
--

DROP TABLE IF EXISTS `pbu_fingerprint`;
/*!50001 DROP VIEW IF EXISTS `pbu_fingerprint`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_fingerprint` AS SELECT 
 1 AS `nama_lengkap`,
 1 AS `bu_name`,
 1 AS `bu_kode`,
 1 AS `nik`,
 1 AS `tgl`,
 1 AS `masuk`,
 1 AS `keluar`,
 1 AS `keterangan`,
 1 AS `alasan`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pbu_fp`
--

DROP TABLE IF EXISTS `pbu_fp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_fp` (
  `fp_id` varchar(36) NOT NULL,
  `PIN` varchar(36) NOT NULL,
  `DateTime_` datetime NOT NULL,
  `Verified` tinyint(4) NOT NULL DEFAULT '0',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `WorkCode` tinyint(4) NOT NULL DEFAULT '0',
  `terminal_id` int(11) NOT NULL DEFAULT '0',
  `tdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_int` tinyint(1) NOT NULL DEFAULT '0',
  `tipe_data` tinyint(1) NOT NULL DEFAULT '0',
  `kode_ket` tinyint(1) NOT NULL DEFAULT '0',
  `log` tinyint(1) NOT NULL DEFAULT '0',
  `cabang` varchar(100) NOT NULL,
  `PIN_real` varchar(36) NOT NULL,
  `cabang_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`fp_id`),
  KEY `idx1` (`DateTime_`,`status_int`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_fp`
--

LOCK TABLES `pbu_fp` WRITE;
/*!40000 ALTER TABLE `pbu_fp` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_fp` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `fp_bfinsrt` BEFORE INSERT ON `pbu_fp` FOR EACH ROW BEGIN
IF NEW.fp_id IS NULL OR LENGTH(NEW.fp_id) = 0 THEN
  SET NEW.fp_id = UUID();
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `pbu_golongan`
--

DROP TABLE IF EXISTS `pbu_golongan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_golongan` (
  `golongan_id` varchar(36) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`golongan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_golongan`
--

LOCK TABLES `pbu_golongan` WRITE;
/*!40000 ALTER TABLE `pbu_golongan` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_golongan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_ip`
--

DROP TABLE IF EXISTS `pbu_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_ip` (
  `ip_id` varchar(36) NOT NULL,
  `cabang_id` varchar(36) NOT NULL,
  `cabang` varchar(100) NOT NULL,
  `kode_ip` varchar(100) NOT NULL,
  `com_key` varchar(20) NOT NULL DEFAULT '12369',
  PRIMARY KEY (`ip_id`),
  KEY `cabang_id` (`cabang_id`) USING BTREE,
  CONSTRAINT `pbu_ip_ibfk_1` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pbu_ip_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_ip`
--

LOCK TABLES `pbu_ip` WRITE;
/*!40000 ALTER TABLE `pbu_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_ip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_jabatan`
--

DROP TABLE IF EXISTS `pbu_jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_jabatan` (
  `jabatan_id` varchar(36) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `urutan` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_jabatan`
--

LOCK TABLES `pbu_jabatan` WRITE;
/*!40000 ALTER TABLE `pbu_jabatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_jenis_periode`
--

DROP TABLE IF EXISTS `pbu_jenis_periode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_jenis_periode` (
  `jenis_periode_id` varchar(36) NOT NULL,
  `bu_id` varchar(36) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  PRIMARY KEY (`jenis_periode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_jenis_periode`
--

LOCK TABLES `pbu_jenis_periode` WRITE;
/*!40000 ALTER TABLE `pbu_jenis_periode` DISABLE KEYS */;
INSERT INTO `pbu_jenis_periode` VALUES ('35940697-439e-11e8-93e6-507b9d297990','536c83cf-767a-11e7-9b95-68f7286688e6','BULANAN');
/*!40000 ALTER TABLE `pbu_jenis_periode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_keterangan`
--

DROP TABLE IF EXISTS `pbu_keterangan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_keterangan` (
  `keterangan_id` varchar(36) NOT NULL,
  `nama_ket` varchar(50) NOT NULL,
  PRIMARY KEY (`keterangan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_keterangan`
--

LOCK TABLES `pbu_keterangan` WRITE;
/*!40000 ALTER TABLE `pbu_keterangan` DISABLE KEYS */;
INSERT INTO `pbu_keterangan` VALUES ('1','Luar Kota'),('10','Presentasi / Pameran'),('2','Sakit'),('3','Lupa Absen'),('4','Off'),('5','Cuti Tahunan'),('6','Cuti Menikah'),('7','Cuti Bersalin'),('8','Cuti Istimewa'),('9','Cuti Non Aktif');
/*!40000 ALTER TABLE `pbu_keterangan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_leveling`
--

DROP TABLE IF EXISTS `pbu_leveling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_leveling` (
  `leveling_id` varchar(36) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `bu_id` varchar(36) NOT NULL,
  PRIMARY KEY (`leveling_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_leveling`
--

LOCK TABLES `pbu_leveling` WRITE;
/*!40000 ALTER TABLE `pbu_leveling` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_leveling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_lock`
--

DROP TABLE IF EXISTS `pbu_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_lock` (
  `lock_id` varchar(36) NOT NULL,
  `periode_id` varchar(36) NOT NULL,
  `tdate` datetime NOT NULL,
  `user_id` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`lock_id`),
  KEY `idx_pbu_lock` (`periode_id`) USING BTREE,
  CONSTRAINT `pbu_lock_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_lock`
--

LOCK TABLES `pbu_lock` WRITE;
/*!40000 ALTER TABLE `pbu_lock` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_lock_post`
--

DROP TABLE IF EXISTS `pbu_lock_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_lock_post` (
  `lock_post_id` varchar(36) NOT NULL,
  `periode_id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `cabang_id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `locked` tinyint(1) DEFAULT '0',
  `locked_user_id` varchar(36) DEFAULT NULL,
  `locked_tdate` datetime DEFAULT NULL,
  `posted` tinyint(1) DEFAULT '0',
  `posted_user_id` varchar(36) DEFAULT NULL,
  `posted_tdate` datetime DEFAULT NULL,
  `bu_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`lock_post_id`),
  KEY `lock_post_fk1` (`periode_id`) USING BTREE,
  KEY `lock_post_fk2` (`cabang_id`) USING BTREE,
  CONSTRAINT `pbu_lock_post_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `pbu_lock_post_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_lock_post`
--

LOCK TABLES `pbu_lock_post` WRITE;
/*!40000 ALTER TABLE `pbu_lock_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_lock_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_log`
--

DROP TABLE IF EXISTS `pbu_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_log` (
  `user_id` varchar(36) DEFAULT NULL,
  `tdate` datetime DEFAULT NULL,
  `log` text,
  `trans_id` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_log`
--

LOCK TABLES `pbu_log` WRITE;
/*!40000 ALTER TABLE `pbu_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_master`
--

DROP TABLE IF EXISTS `pbu_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_master` (
  `master_id` varchar(36) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_master`
--

LOCK TABLES `pbu_master` WRITE;
/*!40000 ALTER TABLE `pbu_master` DISABLE KEYS */;
INSERT INTO `pbu_master` VALUES ('2ba2b526-5fc4-11e7-94ce-9f90f210751c','UT','Uang Transport'),('31d4a325-5fc4-11e7-94ce-9f90f210751c','TJ','Tunjangan'),('9c259746-529b-11e7-8b3a-8418ea51d22e','UM','Uang Makan'),('e9ec0c4c-4a99-11e7-9244-0cd292aa1540','GP','Gaji Pokok');
/*!40000 ALTER TABLE `pbu_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_master_gaji`
--

DROP TABLE IF EXISTS `pbu_master_gaji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_master_gaji` (
  `master_gaji_id` varchar(36) NOT NULL,
  `master_id` varchar(36) NOT NULL,
  `leveling_id` varchar(36) NOT NULL,
  `golongan_id` varchar(36) NOT NULL,
  `area_id` varchar(36) NOT NULL,
  `amount` decimal(30,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`master_gaji_id`),
  UNIQUE KEY `idx_pbu_master_gaji_1` (`golongan_id`,`master_id`) USING BTREE,
  KEY `idx_pbu_master_gaji` (`golongan_id`) USING BTREE,
  KEY `idx_pbu_master_gaji_0` (`master_id`) USING BTREE,
  CONSTRAINT `pbu_master_gaji_ibfk_1` FOREIGN KEY (`golongan_id`) REFERENCES `pbu_golongan` (`golongan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pbu_master_gaji_ibfk_2` FOREIGN KEY (`master_id`) REFERENCES `pbu_master` (`master_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_master_gaji`
--

LOCK TABLES `pbu_master_gaji` WRITE;
/*!40000 ALTER TABLE `pbu_master_gaji` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_master_gaji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pbu_master_gaji_view`
--

DROP TABLE IF EXISTS `pbu_master_gaji_view`;
/*!50001 DROP VIEW IF EXISTS `pbu_master_gaji_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_master_gaji_view` AS SELECT 
 1 AS `master_id`,
 1 AS `golongan_id`,
 1 AS `amount`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pbu_payroll`
--

DROP TABLE IF EXISTS `pbu_payroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_payroll` (
  `payroll_id` varchar(36) NOT NULL,
  `periode_id` varchar(36) NOT NULL,
  `total_hari_kerja` double NOT NULL DEFAULT '0',
  `total_lk` tinyint(4) NOT NULL DEFAULT '0',
  `total_sakit` tinyint(4) NOT NULL DEFAULT '0',
  `total_cuti_tahunan` tinyint(4) NOT NULL DEFAULT '0',
  `total_off` tinyint(4) NOT NULL DEFAULT '0',
  `pegawai_id` varchar(36) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `total_income` decimal(30,2) NOT NULL DEFAULT '0.00',
  `total_deduction` decimal(30,2) NOT NULL DEFAULT '0.00',
  `take_home_pay` decimal(30,2) NOT NULL DEFAULT '0.00',
  `transfer` decimal(30,2) NOT NULL DEFAULT '0.00',
  `golongan_id` varchar(36) NOT NULL,
  `leveling_id` varchar(36) NOT NULL,
  `area_id` varchar(36) NOT NULL,
  `total_lembur_1` tinyint(4) NOT NULL DEFAULT '0',
  `total_lembur_next` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payroll_id`),
  UNIQUE KEY `idx_pbu_payroll_0` (`periode_id`,`pegawai_id`) USING BTREE,
  KEY `idx_pbu_payroll` (`periode_id`) USING BTREE,
  KEY `idx_pbu_payroll_1` (`pegawai_id`) USING BTREE,
  CONSTRAINT `pbu_payroll_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pbu_payroll_ibfk_2` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_payroll`
--

LOCK TABLES `pbu_payroll` WRITE;
/*!40000 ALTER TABLE `pbu_payroll` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_payroll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_payroll_absensi`
--

DROP TABLE IF EXISTS `pbu_payroll_absensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_payroll_absensi` (
  `payroll_absensi_id` varchar(36) NOT NULL,
  `periode_id` varchar(36) NOT NULL,
  `total_hari_kerja` double NOT NULL DEFAULT '0',
  `locked` tinyint(4) NOT NULL DEFAULT '0',
  `tdate` datetime NOT NULL,
  `total_lk` tinyint(4) NOT NULL DEFAULT '0',
  `total_sakit` tinyint(4) NOT NULL DEFAULT '0',
  `total_cuti_tahunan` tinyint(4) NOT NULL DEFAULT '0',
  `total_off` tinyint(4) NOT NULL DEFAULT '0',
  `pegawai_id` varchar(36) NOT NULL,
  `total_lembur_1` tinyint(4) NOT NULL DEFAULT '0',
  `total_lembur_next` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`payroll_absensi_id`),
  UNIQUE KEY `Index 4` (`periode_id`,`pegawai_id`) USING BTREE,
  KEY `idx_pbu_payroll_absensi` (`periode_id`) USING BTREE,
  KEY `idx_pbu_payroll_absensi_0` (`pegawai_id`) USING BTREE,
  CONSTRAINT `pbu_payroll_absensi_ibfk_1` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pbu_payroll_absensi_ibfk_2` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_payroll_absensi`
--

LOCK TABLES `pbu_payroll_absensi` WRITE;
/*!40000 ALTER TABLE `pbu_payroll_absensi` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_payroll_absensi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_payroll_details`
--

DROP TABLE IF EXISTS `pbu_payroll_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_payroll_details` (
  `payroll_detail_id` varchar(36) NOT NULL,
  `payroll_id` varchar(36) NOT NULL,
  `nama_skema` varchar(100) NOT NULL,
  `type_` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = penambah\n-1 = pengurang',
  `amount` decimal(30,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`payroll_detail_id`),
  KEY `idx_pbu_payroll_details` (`payroll_id`) USING BTREE,
  CONSTRAINT `pbu_payroll_details_ibfk_1` FOREIGN KEY (`payroll_id`) REFERENCES `pbu_payroll` (`payroll_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_payroll_details`
--

LOCK TABLES `pbu_payroll_details` WRITE;
/*!40000 ALTER TABLE `pbu_payroll_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_payroll_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_pegawai`
--

DROP TABLE IF EXISTS `pbu_pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_pegawai` (
  `pegawai_id` varchar(36) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `npwp` varchar(50) DEFAULT NULL,
  `bank_nama` varchar(50) DEFAULT NULL,
  `bank_kota` varchar(50) DEFAULT NULL,
  `rekening` varchar(50) DEFAULT NULL,
  `tdate` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `last_update_id` varchar(36) DEFAULT NULL,
  `status_pegawai_id` varchar(36) DEFAULT NULL,
  `jabatan_id` varchar(36) DEFAULT NULL,
  `tuser` varchar(36) DEFAULT NULL,
  `leveling_id` varchar(36) DEFAULT NULL,
  `cabang_id` varchar(36) NOT NULL,
  `status_id` varchar(36) DEFAULT NULL,
  `golongan_id` varchar(36) DEFAULT NULL,
  `store` varchar(20) NOT NULL,
  `kelompok_pegawai` tinyint(1) DEFAULT '0' COMMENT '0 Untuk Karyawan, 1 Untuk OB,OG atau Security',
  PRIMARY KEY (`pegawai_id`),
  UNIQUE KEY `idx_nik` (`nik`) USING BTREE,
  KEY `idx_pbu_pegawai` (`status_pegawai_id`) USING BTREE,
  KEY `idx_pbu_pegawai_0` (`jabatan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_pegawai`
--

LOCK TABLES `pbu_pegawai` WRITE;
/*!40000 ALTER TABLE `pbu_pegawai` DISABLE KEYS */;
INSERT INTO `pbu_pegawai` VALUES ('c439dd59-d3f6-11e7-9982-507b9d297990','admin','SYSTEM ADMINISTRATOR',NULL,'2017-11-28',NULL,NULL,NULL,NULL,'2017-11-28 11:43:47','2017-11-28 11:43:47','c2efc8b7-6382-11e7-98a7-aac0c493942c',NULL,NULL,'c2efc8b7-6382-11e7-98a7-aac0c493942c',NULL,'d2d94c22-d3f4-11e7-9982-507b9d297990',NULL,NULL,'ALL',0),('d4e16e89-4698-11e8-ac21-000c29c7d1d9','HRDENA','HRD ENA',NULL,'2018-04-23',NULL,NULL,NULL,NULL,'2018-04-22 15:51:25','2018-04-22 15:51:25','c2efc8b7-6382-11e7-98a7-aac0c493942c',NULL,NULL,'c2efc8b7-6382-11e7-98a7-aac0c493942c',NULL,'d2d94c22-d3f4-11e7-9982-507b9d297990',NULL,NULL,'ALL',0);
/*!40000 ALTER TABLE `pbu_pegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_pegawai_spesial`
--

DROP TABLE IF EXISTS `pbu_pegawai_spesial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_pegawai_spesial` (
  `pegawai_spesial_id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `pegawai_id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `finger_naava` tinyint(3) DEFAULT NULL COMMENT 'pakai mesin finger naavagreen',
  `nik_lain` varchar(10) DEFAULT NULL COMMENT 'punya nik lain',
  `cabang` varchar(10) DEFAULT NULL,
  `visible` tinyint(3) DEFAULT '1',
  PRIMARY KEY (`pegawai_spesial_id`),
  KEY `ps_fk1` (`pegawai_id`) USING BTREE,
  CONSTRAINT `pbu_pegawai_spesial_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_pegawai_spesial`
--

LOCK TABLES `pbu_pegawai_spesial` WRITE;
/*!40000 ALTER TABLE `pbu_pegawai_spesial` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_pegawai_spesial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pbu_pegawai_view`
--

DROP TABLE IF EXISTS `pbu_pegawai_view`;
/*!50001 DROP VIEW IF EXISTS `pbu_pegawai_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_pegawai_view` AS SELECT 
 1 AS `pegawai_id`,
 1 AS `nama_lengkap`,
 1 AS `leveling_id`,
 1 AS `golongan_id`,
 1 AS `area_id`,
 1 AS `master_id`,
 1 AS `amount`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pbu_periode`
--

DROP TABLE IF EXISTS `pbu_periode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_periode` (
  `periode_id` varchar(36) NOT NULL,
  `kode_periode` varchar(45) NOT NULL,
  `periode_start` datetime NOT NULL,
  `periode_end` datetime NOT NULL,
  `tdate` datetime NOT NULL,
  `jumlah_off` tinyint(3) NOT NULL,
  `jenis_periode_id` varchar(36) NOT NULL,
  PRIMARY KEY (`periode_id`),
  UNIQUE KEY `kode_periode_UNIQUE` (`kode_periode`) USING BTREE,
  KEY `idx_pbu_periode` (`jenis_periode_id`) USING BTREE,
  CONSTRAINT `pbu_periode_ibfk_1` FOREIGN KEY (`jenis_periode_id`) REFERENCES `pbu_jenis_periode` (`jenis_periode_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pbu_periode_ibfk_2` FOREIGN KEY (`jenis_periode_id`) REFERENCES `pbu_jenis_periode` (`jenis_periode_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_periode`
--

LOCK TABLES `pbu_periode` WRITE;
/*!40000 ALTER TABLE `pbu_periode` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_periode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pbu_pin`
--

DROP TABLE IF EXISTS `pbu_pin`;
/*!50001 DROP VIEW IF EXISTS `pbu_pin`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_pin` AS SELECT 
 1 AS `pegawai_id`,
 1 AS `pin_id`,
 1 AS `PIN`,
 1 AS `nama_lengkap`,
 1 AS `store`,
 1 AS `cabang_id`,
 1 AS `pin_nama`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pbu_pin_old`
--

DROP TABLE IF EXISTS `pbu_pin_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_pin_old` (
  `pin_id` varchar(36) NOT NULL,
  `PIN` varchar(255) DEFAULT NULL,
  `jabatan_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`pin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_pin_old`
--

LOCK TABLES `pbu_pin_old` WRITE;
/*!40000 ALTER TABLE `pbu_pin_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_pin_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_result`
--

DROP TABLE IF EXISTS `pbu_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_result` (
  `result_id` varchar(36) NOT NULL,
  `pegawai_id` varchar(36) NOT NULL,
  `pin_id` varchar(36) NOT NULL,
  `periode_id` varchar(36) NOT NULL,
  `total_hari_kerja` double NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `total_lk` tinyint(3) NOT NULL DEFAULT '0',
  `total_sakit` tinyint(3) NOT NULL DEFAULT '0',
  `total_off` tinyint(3) NOT NULL DEFAULT '0',
  `total_cuti_tahunan` tinyint(3) NOT NULL DEFAULT '0',
  `total_cuti_menikah` tinyint(3) NOT NULL DEFAULT '0',
  `total_cuti_bersalin` tinyint(3) NOT NULL DEFAULT '0',
  `total_cuti_istimewa` tinyint(3) NOT NULL DEFAULT '0',
  `total_cuti_non_aktif` tinyint(3) NOT NULL DEFAULT '0',
  `total_min_lembur_awal` int(6) NOT NULL DEFAULT '0',
  `total_min_lembur_akhir` int(6) NOT NULL DEFAULT '0',
  `total_min_late_time` int(6) NOT NULL DEFAULT '0',
  `total_real_lembur_pertama` int(6) DEFAULT '0',
  `total_real_lembur_akhir` int(6) DEFAULT '0',
  `total_real_lembur_hari` int(6) DEFAULT '0',
  `total_real_less_time` int(6) DEFAULT '0',
  `cabang_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `tdate` datetime NOT NULL,
  PRIMARY KEY (`result_id`),
  KEY `result_fk1` (`pegawai_id`) USING BTREE,
  CONSTRAINT `pbu_result_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_result`
--

LOCK TABLES `pbu_result` WRITE;
/*!40000 ALTER TABLE `pbu_result` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_schema`
--

DROP TABLE IF EXISTS `pbu_schema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_schema` (
  `schema_id` varchar(36) NOT NULL,
  `nama_skema` varchar(100) NOT NULL,
  `type_` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = penambah\n-1 = pengurang',
  `seq` tinyint(4) NOT NULL,
  PRIMARY KEY (`schema_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_schema`
--

LOCK TABLES `pbu_schema` WRITE;
/*!40000 ALTER TABLE `pbu_schema` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_schema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_schema_gaji`
--

DROP TABLE IF EXISTS `pbu_schema_gaji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_schema_gaji` (
  `schema_gaji_id` varchar(36) NOT NULL,
  `formula` text NOT NULL,
  `type_` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 = penambah\n-1 = pengurang',
  `seq` tinyint(4) NOT NULL,
  `nama_skema` varchar(100) NOT NULL,
  `status_id` varchar(36) NOT NULL,
  PRIMARY KEY (`schema_gaji_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_schema_gaji`
--

LOCK TABLES `pbu_schema_gaji` WRITE;
/*!40000 ALTER TABLE `pbu_schema_gaji` DISABLE KEYS */;
INSERT INTO `pbu_schema_gaji` VALUES ('22e2ceef-5fcc-11e7-94ce-9f90f210751c','$hasilFormula__ = $UT*$HK__;',1,2,'Uang Transport','93d9cf2c-685e-11e7-8546-00fff84e2916'),('31f42d79-5fcc-11e7-94ce-9f90f210751c','$hasilFormula__ = $TJ;',1,3,'Tunjangan','93d9cf2c-685e-11e7-8546-00fff84e2916'),('dbe66b4b-5432-11e7-8ffa-9254eca67c78','$hasilFormula__ = $GP;',1,0,'Gaji Pokok','93d9cf2c-685e-11e7-8546-00fff84e2916'),('e517ea63-567f-11e7-8ff6-9253eca36e3c','$hasilFormula__= $UM*$HK__;',1,1,'Uang Makan','93d9cf2c-685e-11e7-8546-00fff84e2916');
/*!40000 ALTER TABLE `pbu_schema_gaji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_security_roles`
--

DROP TABLE IF EXISTS `pbu_security_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_security_roles` (
  `security_roles_id` varchar(36) NOT NULL,
  `role` varchar(20) NOT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `sections` mediumtext,
  PRIMARY KEY (`security_roles_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_security_roles`
--

LOCK TABLES `pbu_security_roles` WRITE;
/*!40000 ALTER TABLE `pbu_security_roles` DISABLE KEYS */;
INSERT INTO `pbu_security_roles` VALUES ('0f3d2a11-6383-11e7-98a7-aac0c493942c','System Administrator','System Administrator','101,105,125,122,130,223,225,229,230,231,232,301,302,304,306,307,309,311,501,401,402,403,001,002'),('0f3d2a11-6383-11e7-98a7-aac0c493942d','Kepala Cabang','Kepala Cabang','122,223,225,230,231,301,307,309,311,001,002'),('369c9b88-9d8b-11eb-808b-000c29332ac5','Kepala Cabang AMOS','Kepala Cabang','122,223,225,230,231,301,307,309,311,501,001,002'),('cb8f4293-4832-11e8-93e6-507b9d297990','HRD',NULL,'101,105,122,130,224,223,225,229,230,231,301,307,311,001,002');
/*!40000 ALTER TABLE `pbu_security_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_session`
--

DROP TABLE IF EXISTS `pbu_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_session` (
  `id` varchar(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_session`
--

LOCK TABLES `pbu_session` WRITE;
/*!40000 ALTER TABLE `pbu_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_shift`
--

DROP TABLE IF EXISTS `pbu_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_shift` (
  `shift_id` varchar(36) NOT NULL,
  `in_time` varchar(11) NOT NULL,
  `out_time` varchar(11) NOT NULL,
  `kode_shift` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `earlyin_time` varchar(11) NOT NULL,
  `latein_time` varchar(11) NOT NULL,
  `earlyout_time` varchar(11) NOT NULL,
  `lateout_time` varchar(11) NOT NULL,
  `early_time` varchar(11) NOT NULL,
  `late_time` varchar(11) NOT NULL,
  `least_time` varchar(11) NOT NULL,
  `over_time` varchar(11) NOT NULL,
  `cabang_id` varchar(36) DEFAULT NULL,
  `kelompok_shift` tinyint(1) DEFAULT '0' COMMENT '0 Untuk Karyawan, 1 Untuk OB,OG atau Security',
  PRIMARY KEY (`shift_id`),
  KEY `shift_fk1` (`cabang_id`) USING BTREE,
  CONSTRAINT `pbu_shift_ibfk_1` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON UPDATE CASCADE,
  CONSTRAINT `pbu_shift_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_shift`
--

LOCK TABLES `pbu_shift` WRITE;
/*!40000 ALTER TABLE `pbu_shift` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_shift` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `shift_befor_insert` BEFORE INSERT ON `pbu_shift` FOR EACH ROW BEGIN
			IF NEW.shift_id IS NULL OR LENGTH(NEW.shift_id) = 0 THEN
			  SET NEW.shift_id = UUID();
			END IF;
			END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `pbu_shiftpin`
--

DROP TABLE IF EXISTS `pbu_shiftpin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_shiftpin` (
  `shiftpin_id` varchar(36) NOT NULL,
  `shift_id` varchar(36) NOT NULL,
  `pin_id` varchar(36) NOT NULL,
  `pegawai_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(3) DEFAULT '1',
  `user_id` varchar(36) DEFAULT NULL,
  `tdate` datetime DEFAULT NULL,
  PRIMARY KEY (`shiftpin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_shiftpin`
--

LOCK TABLES `pbu_shiftpin` WRITE;
/*!40000 ALTER TABLE `pbu_shiftpin` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_shiftpin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_sr_cabang`
--

DROP TABLE IF EXISTS `pbu_sr_cabang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_sr_cabang` (
  `sr_cabang` varchar(36) NOT NULL,
  `cabang_id` mediumtext,
  `security_roles_id` varchar(36) NOT NULL,
  PRIMARY KEY (`sr_cabang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_sr_cabang`
--

LOCK TABLES `pbu_sr_cabang` WRITE;
/*!40000 ALTER TABLE `pbu_sr_cabang` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_sr_cabang` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `sr_cabang` BEFORE INSERT ON `pbu_sr_cabang` FOR EACH ROW BEGIN
IF NEW.sr_cabang IS NULL OR LENGTH(NEW.sr_cabang) = 0 THEN
  SET NEW.sr_cabang = UUID();
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary view structure for view `pbu_sr_cbg_area_bu`
--

DROP TABLE IF EXISTS `pbu_sr_cbg_area_bu`;
/*!50001 DROP VIEW IF EXISTS `pbu_sr_cbg_area_bu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_sr_cbg_area_bu` AS SELECT 
 1 AS `sr_cabang`,
 1 AS `cabang_id`,
 1 AS `security_roles_id`,
 1 AS `kode_cabang`,
 1 AS `nama_cabang`,
 1 AS `bu_id`,
 1 AS `alamat_id`,
 1 AS `no_telp_cabang`,
 1 AS `alamat_cabang`,
 1 AS `kepala_cabang_stat`,
 1 AS `area_id`,
 1 AS `bu_name`,
 1 AS `bu_kode`,
 1 AS `kode`,
 1 AS `nama`,
 1 AS `bu_nama_alias`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `pbu_sr_level_bu`
--

DROP TABLE IF EXISTS `pbu_sr_level_bu`;
/*!50001 DROP VIEW IF EXISTS `pbu_sr_level_bu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_sr_level_bu` AS SELECT 
 1 AS `sr_leveling`,
 1 AS `security_roles_id`,
 1 AS `leveling_id`,
 1 AS `kode`,
 1 AS `nama`,
 1 AS `bu_id`,
 1 AS `bu_name`,
 1 AS `bu_kode`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pbu_sr_leveling`
--

DROP TABLE IF EXISTS `pbu_sr_leveling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_sr_leveling` (
  `sr_leveling` varchar(36) NOT NULL,
  `security_roles_id` varchar(36) NOT NULL,
  `leveling_id` mediumtext,
  PRIMARY KEY (`sr_leveling`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_sr_leveling`
--

LOCK TABLES `pbu_sr_leveling` WRITE;
/*!40000 ALTER TABLE `pbu_sr_leveling` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_sr_leveling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_status`
--

DROP TABLE IF EXISTS `pbu_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_status` (
  `status_id` varchar(36) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `bu_id` varchar(36) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_status`
--

LOCK TABLES `pbu_status` WRITE;
/*!40000 ALTER TABLE `pbu_status` DISABLE KEYS */;
INSERT INTO `pbu_status` VALUES ('0a1449e2-b3b2-4e20-a092-a7fe2e7b9070','ANAK 2','ANAK 2',''),('4d3804d2-7a50-45c6-8857-230c9459d4c3','KARYAWAN','KARYAWAN',''),('5464218e-def5-42ba-8977-a0067ebd77d9','ANAK 4','ANAK 4',''),('69f982ac-8713-4757-9039-74b381507615','SUAMI / ISTRI','SUAMI / ISTRI',''),('c31045f6-dda0-4d10-9370-fdc61b6bd934','ANAK 3','ANAK 3',''),('c4b7b664-8c7a-11e7-bde6-001851d80076','ORG TUA 1 / BAPAK','ORG TUA 1 / BAPAK',''),('c4b7b693-8c7a-11e7-bde6-001851d80076','ORG TUA 2 / IBU','ORG TUA 2 / IBU',''),('ce841f78-8224-4ae1-8f38-411ff2a6d933','ANAK 1','ANAK 1',''),('e33c61b9-2397-4515-b1aa-c142f35c7055','ANAK 5','ANAK 5','');
/*!40000 ALTER TABLE `pbu_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_status_pegawai`
--

DROP TABLE IF EXISTS `pbu_status_pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_status_pegawai` (
  `status_pegawai_id` varchar(36) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama_status` varchar(50) NOT NULL,
  PRIMARY KEY (`status_pegawai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_status_pegawai`
--

LOCK TABLES `pbu_status_pegawai` WRITE;
/*!40000 ALTER TABLE `pbu_status_pegawai` DISABLE KEYS */;
INSERT INTO `pbu_status_pegawai` VALUES ('0b391ff5-7c01-11e7-9b95-68f7286688e6','GAGAL TRAINING','GAGAL TRAINING'),('4e607af4-7ce4-11e7-9b95-68f7286688e6','MUTASI UNIT USAHA','MUTASI UNIT USAHA'),('6e772be7-773c-11e7-9b95-68f7286688e6','AKTIF','AKTIF'),('6e772fd4-773c-11e7-9b95-68f7286688e6','RESIGN','RESIGN'),('6e773102-773c-11e7-9b95-68f7286688e6','MENINGGAL','MENINGGAL'),('6e7733e7-773c-11e7-9b95-68f7286688e6','PHK','PHK'),('6e7734b4-773c-11e7-9b95-68f7286688e6','NONAKTIF','NONAKTIF'),('6e773550-773c-11e7-9b95-68f7286688e6','CUTI NONAKTIF','CUTI NONAKTIF');
/*!40000 ALTER TABLE `pbu_status_pegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_sys_prefs`
--

DROP TABLE IF EXISTS `pbu_sys_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_sys_prefs` (
  `sys_prefs_id` varchar(36) NOT NULL,
  `name_` varchar(100) NOT NULL,
  `value_` varchar(255) NOT NULL,
  PRIMARY KEY (`sys_prefs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_sys_prefs`
--

LOCK TABLES `pbu_sys_prefs` WRITE;
/*!40000 ALTER TABLE `pbu_sys_prefs` DISABLE KEYS */;
INSERT INTO `pbu_sys_prefs` VALUES ('10','receipt_footer1','Barang yang sudah dibeli'),('11','receipt_footer2','Tidak bisa dikembalikan'),('12','receipt_footer3','Terima kasih atas kunjungan anda'),('13','receipt_footer4',''),('14','receipt_footer5',''),('16','coa_grup_biaya','7'),('17','coa_grup_hutang','3001'),('18','coa_grup_pendapatan','8'),('19','coa_grup_bank','1101'),('2','audit','NSC'),('20','coa_persediaan','1300'),('21','coa_hpp','6101'),('22','coa_disc','5005'),('23','coa_vat','7012'),('24','coa_grup_kas','1000'),('25','kas_cabang','1'),('26','kas_pusat','2'),('27','url_nars','http://180.250.148.165:777/backup_testing/backup'),('28','username_nars','natasha'),('29','password_nars','p1ssw0rd'),('3','receipt_header0','JOGJA 02 (JAKAL)'),('30','coa_biaya_adm_bank','7014'),('31','sync_active','0'),('32','default_row','50'),('4','receipt_header1','PT. Pesona Natasha Gemilang'),('5','receipt_header2','Jl.C.Simanjuntak Ruko YAP Square Blok.A'),('6','receipt_header3','No.1 Terban Gondokusuman Yogyakarta'),('7','receipt_header4','NPWP: 02.542.233.8-541.000'),('8','receipt_header5','Call Center: 500-422'),('9','receipt_footer0','Harga Cream/Obat sudah termasuk PPn');
/*!40000 ALTER TABLE `pbu_sys_prefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_sys_types`
--

DROP TABLE IF EXISTS `pbu_sys_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_sys_types` (
  `sys_types_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` smallint(6) NOT NULL,
  `type_no` int(11) NOT NULL,
  `next_reference` varchar(50) NOT NULL,
  PRIMARY KEY (`sys_types_id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_sys_types`
--

LOCK TABLES `pbu_sys_types` WRITE;
/*!40000 ALTER TABLE `pbu_sys_types` DISABLE KEYS */;
INSERT INTO `pbu_sys_types` VALUES (1,1,1,'TO20140000000001'),(2,2,1,'CO/311215/0000000173'),(3,3,1,'CI/080116/0000000008'),(4,4,1,'TI/111114/0000000001'),(5,5,1,'TI/261216/0000000018'),(6,6,1,'RSO20140000000001'),(7,7,1,'TND20140000000001'),(8,8,1,'AUD20140000000001'),(9,9,1,'SJ0276/12/16'),(10,10,1,'CN/260615/0000000001'),(11,11,1,'SJ/PBU/191114/0000000001'),(13,13,1,'JU/170115/0000000001'),(14,14,1,'BTRANS/110116/0000000002'),(16,16,1,'PR/2016/0000000014'),(17,17,1,'PO/2016/0000000013'),(18,18,1,'SUPPINV/010170/0000000006'),(19,19,1,'SO/301216/0000000278'),(20,20,1,'INV/010170/1'),(21,21,1,'SCNOTE/260615/0000000001'),(23,23,1,'CUSTRE/010117/0000000001'),(24,24,1,'SUPAY/140116/0000000025'),(25,25,1,'CUSPAY/080117/0000000091'),(29,29,1,'CL/280616/0000000044'),(56,56,1,'BPER/140116/00033'),(57,57,1,'PBA/060116/00001'),(100,100,1,'5'),(101,101,1,'KP4'),(102,102,1,'8'),(103,103,1,'8'),(104,104,1,'4'),(105,105,1,'3446'),(106,106,1,'4'),(107,107,1,'25'),(108,108,1,'114'),(109,109,1,'10'),(110,110,1,'102'),(111,111,1,'14'),(112,12,1,'RB/221114/0000000001'),(113,22,1,'SUPPRE/110416/0000000000'),(114,28,1,'SO0001/NHN/V/2016');
/*!40000 ALTER TABLE `pbu_sys_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_template`
--

DROP TABLE IF EXISTS `pbu_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_template` (
  `template_id` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `name_UNIQUE` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_template`
--

LOCK TABLES `pbu_template` WRITE;
/*!40000 ALTER TABLE `pbu_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_template_detail`
--

DROP TABLE IF EXISTS `pbu_template_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_template_detail` (
  `template_detail_id` varchar(36) NOT NULL,
  `template_id` varchar(36) NOT NULL,
  `shift_id` varchar(36) NOT NULL,
  PRIMARY KEY (`template_detail_id`),
  KEY `template_detail_fk1_idx` (`template_id`) USING BTREE,
  CONSTRAINT `pbu_template_detail_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `pbu_template` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_template_detail`
--

LOCK TABLES `pbu_template_detail` WRITE;
/*!40000 ALTER TABLE `pbu_template_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_template_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_transfer_pegawai`
--

DROP TABLE IF EXISTS `pbu_transfer_pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_transfer_pegawai` (
  `transfer_pegawai_id` varchar(36) NOT NULL,
  `pegawai_id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `cabang_id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `tglin` date NOT NULL,
  `tglout` date NOT NULL,
  `no_surat_tugas` varchar(100) DEFAULT NULL,
  `visible` tinyint(3) DEFAULT '1',
  `cabang_transfer_id` varchar(36) CHARACTER SET utf8mb4 NOT NULL,
  `periode_id` varchar(36) CHARACTER SET utf8mb4 DEFAULT NULL,
  `tdate` datetime DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `binding_id` varchar(36) DEFAULT NULL,
  `status_kota` tinyint(2) NOT NULL,
  PRIMARY KEY (`transfer_pegawai_id`),
  KEY `transfer_pegawai_fk1` (`pegawai_id`) USING BTREE,
  KEY `transfer_pegawai_fk2` (`cabang_id`) USING BTREE,
  KEY `transfer_pegawai_fk3` (`cabang_transfer_id`) USING BTREE,
  KEY `transfer_pegawai_fk4` (`periode_id`) USING BTREE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON UPDATE CASCADE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_2` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON UPDATE CASCADE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_3` FOREIGN KEY (`cabang_transfer_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON UPDATE CASCADE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_4` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON UPDATE CASCADE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_5` FOREIGN KEY (`pegawai_id`) REFERENCES `pbu_pegawai` (`pegawai_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_6` FOREIGN KEY (`cabang_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_7` FOREIGN KEY (`cabang_transfer_id`) REFERENCES `pbu_cabang` (`cabang_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `pbu_transfer_pegawai_ibfk_8` FOREIGN KEY (`periode_id`) REFERENCES `pbu_periode` (`periode_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_transfer_pegawai`
--

LOCK TABLES `pbu_transfer_pegawai` WRITE;
/*!40000 ALTER TABLE `pbu_transfer_pegawai` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_transfer_pegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_users`
--

DROP TABLE IF EXISTS `pbu_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_users` (
  `id` varchar(36) NOT NULL,
  `user_id` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `last_visit_date` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `security_roles_id` varchar(36) NOT NULL,
  `name_` varchar(100) DEFAULT NULL,
  `pegawai_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`) USING BTREE,
  KEY `idx_nsc_users` (`security_roles_id`) USING BTREE,
  CONSTRAINT `pbu_users_ibfk_1` FOREIGN KEY (`security_roles_id`) REFERENCES `pbu_security_roles` (`security_roles_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_users`
--

LOCK TABLES `pbu_users` WRITE;
/*!40000 ALTER TABLE `pbu_users` DISABLE KEYS */;
INSERT INTO `pbu_users` VALUES ('1fa0aef6-e7c6-11e8-9f89-000c29332ac5','100382','VXXMRPqW20pcvfCYVQTV/LdoZXqZzUk2mtlTRQ0+m+WMOrkJgT1jpLC/5d7PpcJsw62EhkL+ZEHaPa/irMtofg','2018-11-14 11:31:23',0,'cb8f4293-4832-11e8-93e6-507b9d297990','ROSA','7c4db164-4833-11e8-ac21-000c29c7d1d9'),('21fcd3b4-6589-11e9-9f89-000c29332ac5','anibac01','yXcrG6AxKXj8QrLKyrv3y/mc2euDAs0bb1smEj9zOI2tu/5ZVoNK+xphWO0qZ0CfGPuckinVAYz/mCqnjT8RuA','2021-06-25 11:48:26',1,'369c9b88-9d8b-11eb-808b-000c29332ac5','AKUN CABANG BAC01','7ce96b15-e621-11e7-ac21-000c29c7d1d9'),('2a7221a5-3659-11e9-bcbe-507b9d297990','hrdena','nwUZU5eeS4AmwIf/iTyqqp66CX5x/mffkaDd4nEBZ6HS6pXiKf2o5coIwag2BFMLv8y5QxNglxCpBmI/Xk1dwg','2021-06-23 14:39:36',1,'cb8f4293-4832-11e8-93e6-507b9d297990','HRD ENA','d4e16e89-4698-11e8-ac21-000c29c7d1d9'),('3276e1ab-bfd4-11e8-9f89-000c29332ac5','100082','P9TrVtB8soBdFr9rvdUUBgbN6OGDzR9MuoXv47sHjnKWa9eYJhE/nKkV+AuVHeNPqsbbmMesyBqn0h+GeZk7Vw','2019-04-11 12:15:55',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','PUTRI PARAMARTA AMATYA','ec0a9a97-32f6-11e8-8e76-000c29988254'),('4ba470a0-4834-11e8-ac21-000c29c7d1d9','100858','gZiDKKYWCJaW9iQ1tnnrKaCew/GTbVf0rKDtBcgIS87kN0cUAzlGuEZ/tqCr4Bn7rMmDJPM3Smv7vXG1h/FO+Q','2019-09-09 15:26:13',0,'cb8f4293-4832-11e8-93e6-507b9d297990','VANIA','7c4db164-4833-11e8-ac21-000c29c7d1d9'),('5fce5b80-8c9a-11e8-ae9d-000c29c7d1d9','100038','9aPnY5VJGYdV/BtvG+ZXGagl49n7LWXnNwyvT6oUXC4JozaOzXV+0K+b3usGoa3jfrk2oNwSARSKYNvuE2vmmQ','2019-06-21 10:30:03',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','RISA SEPTI HANDAYANI','20b2e543-e490-11e7-ac21-000c29c7d1d9'),('65f75394-3ef2-11e8-ac21-000c29c7d1d9','100074','zWxVjJAzxm7vcCc0c//EY7s/pAAp/IQShpbny+lMlASAHSsTEVfEE2joImNSJ7MI1kR8iteHG3TJeYaLFHhx/Q','2018-07-20 08:40:40',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','NURRIDHAYAH JAMALUDIN','c1dd1745-3ef2-11e8-ac21-000c29c7d1d9'),('69aaae5e-e621-11e7-ac21-000c29c7d1d9','100003','Aocf8Sj0qKK023g/b2ooQCFEggET03YLgHZ2E25XBDM1JQ/uhInao944D6ZRdgsSFewWqf8vuOA1ukQHlcyQTA','2019-09-08 11:22:58',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','YUANITA YUSRI','7ce96b15-e621-11e7-ac21-000c29c7d1d9'),('6ff962db-d3f7-11e7-9982-507b9d297990','100002','nnynz3WWgre7NpLIw85Ty7fKotxy77DGW/sTPOtmxrAZsULFu3hsXsiX1aZuDSKA1Ztqikl2rCHhhVB5bMygfw','2019-09-10 14:18:55',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','KRIS NURHAYATI','e2bedd5b-b47e-11e7-99f3-507b9d297990'),('9455ca7d-2137-11ea-8c0a-000c29332ac5','anipwt01','80pM6urKxKgxOoaBB7WDTIxw90LKyKIoddjTC8elvTKvmeadu+nPnoB/15p7FbpCWBhMFbI9CfWctq/HhZ3qag','2021-07-05 15:45:28',1,'369c9b88-9d8b-11eb-808b-000c29332ac5','AKUN CABANG PWT01','810e2360-2137-11ea-8c0a-000c29332ac5'),('9feafe8a-8bca-11e8-ae9d-000c29c7d1d9','100054','6FqDJBM1O2NY54Dnu0di/vfsYvFQXrVKee9uppfmY6ffO5Bckqb+ETgoyXypxc2i+Pe2KmaVYuMK7l66SN2SHA','2019-09-07 09:38:03',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','SRI AYU INDIRA','8a2430d8-411a-11e8-ac21-000c29c7d1d9'),('a320b72b-9ba5-11e8-ae9d-000c29c7d1d9','2101100028','6rSL1vDpzKii9G1UegB7fZkeP6p6sbwPz4QKD0HrJp/TZH0QKGoqog7Aj1+fmuY/JDNgp3OHCvWc02x9FCKr5A','2018-08-09 15:13:59',0,'cb8f4293-4832-11e8-93e6-507b9d297990','CMP. SULISTYANINGTYAS','7697d8e0-224d-11e8-8e76-000c29988254'),('b53025c7-bf7e-11eb-808b-000c29332ac5','100172','HAxjFzvL/tEZC+c1zN4a9+zJnsjaFm5ryo1hyVAbpYRS47UuxJv2sGNcJ/3w7urYNgp3dz50tnc0CzuHRwCIUQ','2021-05-28 14:20:47',1,'cb8f4293-4832-11e8-93e6-507b9d297990','EMY FAJARINI','fc6434e4-6d23-11e9-97ee-000c29988254'),('c2efc8b7-6382-11e7-98a7-aac0c493942c','admin','JtCAZGQ+5lmgvkh78RRTnEO3Orea7VR10o7kvUSjfReUtfyajvXJtNkvpU1GAiVymdzLUT4snG1SHY63zqs9Pg','2021-06-29 10:23:59',1,'0f3d2a11-6383-11e7-98a7-aac0c493942c','ADMINISTRATOR','c439dd59-d3f6-11e7-9982-507b9d297990'),('c3d49260-1f97-11e9-9f89-000c29332ac5','100104','LIzyDTCH7GtiVoFygmV9t4pZIzfw/tzkdtJq/kCUp7zvKNslblg9D8gfOfd6iB7lLb6NFRyJwlXRUYdoyTpnOA','2019-09-10 12:43:13',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','DIAH ESTIKA S. FARM., APT','0e2fa23f-e879-11e8-94f6-000c29988254'),('ca1470f4-9241-11e9-8c0a-000c29332ac5','anisda01','YyIJl1fsHYlRAVdTzJVzL9H9THIPSf4DXN4ScCKHhn22CCwtu0+Nzdg0fYzMIM2kWx6kPgBVJQvRuWq4LbjlCg','2021-07-12 15:04:49',1,'369c9b88-9d8b-11eb-808b-000c29332ac5','AKUN CABANG SDA01','8e151131-9241-11e9-8c0a-000c29332ac5'),('d78d52d7-9241-11e9-8c0a-000c29332ac5','animks01','OA83nhkmRCSdyuSuZ+6l1l3yk+4jekQCgrUPQL3HYtfy58xpjzHHbX0k/d0c2if58ckf3Ng5bqV3pQVTNfaR+g','2021-07-10 10:33:21',1,'369c9b88-9d8b-11eb-808b-000c29332ac5','AKUN CABANG MKS01','a06064e1-9241-11e9-8c0a-000c29332ac5'),('e2c54e7e-9241-11e9-8c0a-000c29332ac5','anijog01','BcN8YlYChvhkxRDA/vEAJRZR78sgiLnC3s5gVo26pFe6OYv057QiodKQ+orfEU40LXNy7zLwkAc/pBQQdjHBeQ','2021-06-28 12:13:48',1,'369c9b88-9d8b-11eb-808b-000c29332ac5','AKUN CABANG JOG01','a6fc5035-9241-11e9-8c0a-000c29332ac5'),('f4d4e436-8eed-11e8-ae9d-000c29c7d1d9','100017','QwAU8zI6dGBMDkrTHJtcIGFuczLcbEY+NCLryqbJ6ViJnNqwnSihp92yXhs32MpYBy50JfcMbdkjSF++D6Yliw','2018-07-26 16:27:30',0,'0f3d2a11-6383-11e7-98a7-aac0c493942d','DIAN PUSPITA SARI','767dc231-e628-11e7-ac21-000c29c7d1d9');
/*!40000 ALTER TABLE `pbu_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pbu_validasi`
--

DROP TABLE IF EXISTS `pbu_validasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pbu_validasi` (
  `validasi_id` varchar(36) NOT NULL,
  `pegawai_id` varchar(36) NOT NULL,
  `pin_id` varchar(36) NOT NULL,
  `PIN` varchar(36) NOT NULL,
  `in_time` datetime NOT NULL,
  `out_time` datetime NOT NULL,
  `min_early_time` int(4) DEFAULT NULL,
  `min_late_time` int(4) DEFAULT NULL,
  `min_least_time` int(4) DEFAULT NULL,
  `min_over_time_awal` int(4) DEFAULT NULL,
  `min_over_time` int(4) DEFAULT NULL,
  `tdate` datetime NOT NULL,
  `kode_ket` tinyint(1) NOT NULL,
  `no_surat_tugas` varchar(100) DEFAULT NULL,
  `status_int` tinyint(3) NOT NULL,
  `result_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `status_pegawai_id` varchar(36) DEFAULT NULL,
  `shift_id` varchar(36) DEFAULT NULL,
  `approval_lembur` tinyint(3) NOT NULL DEFAULT '0',
  `approval_lesstime` tinyint(3) NOT NULL DEFAULT '1',
  `real_lembur_pertama` int(4) DEFAULT '0',
  `real_lembur_akhir` int(4) DEFAULT '0',
  `real_lembur_hari` int(4) DEFAULT '0',
  `real_less_time` int(4) DEFAULT '0',
  `cabang_id` varchar(36) DEFAULT NULL,
  `cuti` tinyint(3) DEFAULT '0',
  `limit_lembur` tinyint(3) DEFAULT '0',
  `short_shift` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`validasi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='INI REVIEW ABSEN 2';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pbu_validasi`
--

LOCK TABLES `pbu_validasi` WRITE;
/*!40000 ALTER TABLE `pbu_validasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `pbu_validasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `pbu_validasi_view`
--

DROP TABLE IF EXISTS `pbu_validasi_view`;
/*!50001 DROP VIEW IF EXISTS `pbu_validasi_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `pbu_validasi_view` AS SELECT 
 1 AS `validasi_id`,
 1 AS `pin_id`,
 1 AS `PIN`,
 1 AS `pegawai_id`,
 1 AS `cabang_validasi_id`,
 1 AS `in_time`,
 1 AS `out_time`,
 1 AS `min_early_time`,
 1 AS `min_late_time`,
 1 AS `min_least_time`,
 1 AS `min_over_time_awal`,
 1 AS `min_over_time`,
 1 AS `total_lembur`,
 1 AS `min_over_time_awal_real`,
 1 AS `min_over_time_real`,
 1 AS `lembur_hari`,
 1 AS `real_less_time`,
 1 AS `tdate`,
 1 AS `kode_ket`,
 1 AS `no_surat_tugas`,
 1 AS `status_int`,
 1 AS `result_id`,
 1 AS `user_id`,
 1 AS `status_pegawai_id`,
 1 AS `shift_id`,
 1 AS `approval_lembur`,
 1 AS `approval_lesstime`,
 1 AS `cabang_id`,
 1 AS `kode_cabang`,
 1 AS `nama_cabang`,
 1 AS `cabang_user`,
 1 AS `pegawai_user`,
 1 AS `cuti`,
 1 AS `limit_lembur`,
 1 AS `short_shift`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base',1532154600),('m180721_062559_modify_validasi_and_validasi_view',1532154602),('m180724_084145_modify_validasi_view',1532422021),('m180804_064020_add_transfer_pegawai_and_some_indexes',1533365295),('m180809_033509_add_cabang_id_di_shift',1533785977),('m180818_043514_add_kelompok_pegawai_shift',1534567243),('m180821_084338_add',1534992474),('m180827_065425_add_real_less_time_di_validasi_view',1535360033),('m180906_035148_change_jabatanid_to_active_in_pbushift',1537773897),('m181004_080253_update_validasi_dan_validasi_view',1538640587),('m181005_035003_add_pbu_log',1538711555),('m190130_062749_add_coloumn_untuk_cuti',1548920414),('m190221_081325_modify_validasi_result',1555569302),('m190418_062426_add_limit_lembur',1555569303),('m190612_043557_add_short_shift',1560314492),('m190618_034110_add_urutan_jabatan',1560829571),('m190719_032846_panjang_jabatan',1565683963),('m200926_061356_add_pegawai_spesial',1601101125),('m201007_035519_add_cabang_lain_di_pegawaispesial',1602043046),('m210330_041521_remove_results',1618374852),('m210414_042809_add_amos',1618374863);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `template_hari`
--

DROP TABLE IF EXISTS `template_hari`;
/*!50001 DROP VIEW IF EXISTS `template_hari`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `template_hari` AS SELECT 
 1 AS `template_id`,
 1 AS `shift_id`,
 1 AS `kode_shift`,
 1 AS `in_time`,
 1 AS `out_time`,
 1 AS `sunday`,
 1 AS `monday`,
 1 AS `tuesday`,
 1 AS `wednesday`,
 1 AS `thursday`,
 1 AS `friday`,
 1 AS `saturday`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_assignshift`
--

DROP TABLE IF EXISTS `v_assignshift`;
/*!50001 DROP VIEW IF EXISTS `v_assignshift`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_assignshift` AS SELECT 
 1 AS `shiftpin_id`,
 1 AS `nama_lengkap`,
 1 AS `shift_id`,
 1 AS `pin_id`,
 1 AS `pegawai_id`,
 1 AS `visible`,
 1 AS `pin_nama`,
 1 AS `pin`,
 1 AS `store`,
 1 AS `kode_shift`,
 1 AS `in_time`,
 1 AS `out_time`,
 1 AS `sunday`,
 1 AS `monday`,
 1 AS `tuesday`,
 1 AS `wednesday`,
 1 AS `thursday`,
 1 AS `friday`,
 1 AS `saturday`,
 1 AS `user_assign`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `nama_hari`
--

/*!50001 DROP VIEW IF EXISTS `nama_hari`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `nama_hari` AS select `t`.`shift_id` AS `shift_id`,`s`.`kode_shift` AS `kode_shift`,`s`.`in_time` AS `in_time`,`s`.`out_time` AS `out_time`,`s`.`earlyin_time` AS `earlyin_time`,`s`.`latein_time` AS `latein_time`,`s`.`earlyout_time` AS `earlyout_time`,`s`.`lateout_time` AS `lateout_time`,sum(if((`d`.`day_id` = '1_d'),1,0)) AS `sunday`,sum(if((`d`.`day_id` = '2_d'),1,0)) AS `monday`,sum(if((`d`.`day_id` = '3_d'),1,0)) AS `tuesday`,sum(if((`d`.`day_id` = '4_d'),1,0)) AS `wednesday`,sum(if((`d`.`day_id` = '5_d'),1,0)) AS `thursday`,sum(if((`d`.`day_id` = '6_d'),1,0)) AS `friday`,sum(if((`d`.`day_id` = '7_d'),1,0)) AS `saturday` from ((`pbu_dayshift` `t` left join `pbu_shift` `s` on((`s`.`shift_id` = `t`.`shift_id`))) left join `pbu_days` `d` on((`d`.`day_id` = `t`.`day_id`))) group by `t`.`shift_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_all_cbg_bu`
--

/*!50001 DROP VIEW IF EXISTS `pbu_all_cbg_bu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_all_cbg_bu` AS select `pbu_cabang`.`cabang_id` AS `cabang_id`,`pbu_cabang`.`kode_cabang` AS `kode_cabang`,`pbu_cabang`.`nama_cabang` AS `nama_cabang`,`pbu_cabang`.`bu_id` AS `bu_id`,`pbu_bu`.`bu_name` AS `bu_name`,`pbu_bu`.`bu_kode` AS `bu_kode`,0 AS `checked` from (`pbu_cabang` join `pbu_bu` on((`pbu_cabang`.`bu_id` = convert(`pbu_bu`.`bu_id` using utf8mb4)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_all_lvl_bu`
--

/*!50001 DROP VIEW IF EXISTS `pbu_all_lvl_bu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_all_lvl_bu` AS select `pbu_leveling`.`leveling_id` AS `leveling_id`,`pbu_leveling`.`kode` AS `kode`,`pbu_leveling`.`nama` AS `nama`,`pbu_leveling`.`bu_id` AS `bu_id`,`pbu_bu`.`bu_name` AS `bu_name`,`pbu_bu`.`bu_kode` AS `bu_kode`,0 AS `checked` from (`pbu_leveling` join `pbu_bu` on((`pbu_leveling`.`bu_id` = convert(`pbu_bu`.`bu_id` using utf8mb4)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_bu_tree`
--

/*!50001 DROP VIEW IF EXISTS `pbu_bu_tree`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_bu_tree` AS select `pb`.`bu_id` AS `node_id`,`pb`.`bu_kode` AS `kode`,`pb`.`bu_name` AS `nama`,'' AS `parent`,2 AS `item_type` from `pbu_bu` `pb` union all select `pc`.`cabang_id` AS `node_id`,`pc`.`kode_cabang` AS `kode`,`pc`.`alamat_cabang` AS `nama`,`pc`.`bu_id` AS `parent`,1 AS `item_type` from `pbu_cabang` `pc` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_fingerprint`
--

/*!50001 DROP VIEW IF EXISTS `pbu_fingerprint`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_fingerprint` AS select `p`.`nama_lengkap` AS `nama_lengkap`,`b`.`bu_name` AS `bu_name`,`b`.`bu_kode` AS `bu_kode`,`p`.`nik` AS `nik`,date_format(`v`.`in_time`,'%Y-%m-%d') AS `tgl`,date_format(`v`.`in_time`,'%H:%i:%s') AS `masuk`,date_format(`v`.`out_time`,'%H:%i:%s') AS `keluar`,`k`.`nama_ket` AS `keterangan`,'-' AS `alasan` from ((((((`pbu_validasi` `v` left join `pbu_pegawai` `p` on((`p`.`nik` = `v`.`PIN`))) left join `pbu_cabang` `c` on((`c`.`cabang_id` = `p`.`cabang_id`))) left join `pbu_bu` `b` on((convert(`b`.`bu_id` using utf8mb4) = `c`.`bu_id`))) left join `pbu_result` `r` on((`r`.`result_id` = `v`.`result_id`))) left join `pbu_periode` `pr` on((`pr`.`periode_id` = `r`.`periode_id`))) left join `pbu_keterangan` `k` on((`k`.`keterangan_id` = `v`.`kode_ket`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_master_gaji_view`
--

/*!50001 DROP VIEW IF EXISTS `pbu_master_gaji_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`rCpp9uvJpztw`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_master_gaji_view` AS select `pm`.`master_id` AS `master_id`,`pg`.`golongan_id` AS `golongan_id`,`pmg`.`amount` AS `amount` from ((`pbu_master_gaji` `pmg` join `pbu_master` `pm` on((`pmg`.`master_id` = `pm`.`master_id`))) join `pbu_golongan` `pg` on((`pmg`.`golongan_id` = `pg`.`golongan_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_pegawai_view`
--

/*!50001 DROP VIEW IF EXISTS `pbu_pegawai_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`rCpp9uvJpztw`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_pegawai_view` AS select `p`.`pegawai_id` AS `pegawai_id`,`p`.`nama_lengkap` AS `nama_lengkap`,`p`.`leveling_id` AS `leveling_id`,`p`.`golongan_id` AS `golongan_id`,`c`.`area_id` AS `area_id`,`mg`.`master_id` AS `master_id`,`mg`.`amount` AS `amount` from ((`pbu_pegawai` `p` join `pbu_cabang` `c` on((`c`.`cabang_id` = `p`.`cabang_id`))) join `pbu_master_gaji` `mg` on(((`mg`.`leveling_id` = `p`.`leveling_id`) and (`mg`.`golongan_id` = `p`.`golongan_id`) and (`mg`.`area_id` = `c`.`area_id`)))) order by `p`.`pegawai_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_pin`
--

/*!50001 DROP VIEW IF EXISTS `pbu_pin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_pin` AS select `pbu_pegawai`.`pegawai_id` AS `pegawai_id`,`pbu_pegawai`.`nik` AS `pin_id`,`pbu_pegawai`.`nik` AS `PIN`,`pbu_pegawai`.`nama_lengkap` AS `nama_lengkap`,`pbu_pegawai`.`store` AS `store`,`pbu_pegawai`.`cabang_id` AS `cabang_id`,concat(`pbu_pegawai`.`nik`,' ',`pbu_pegawai`.`nama_lengkap`) AS `pin_nama` from `pbu_pegawai` where ((`pbu_pegawai`.`nik` <> '') and (`pbu_pegawai`.`nik` is not null)) order by `pbu_pegawai`.`nik` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_sr_cbg_area_bu`
--

/*!50001 DROP VIEW IF EXISTS `pbu_sr_cbg_area_bu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_sr_cbg_area_bu` AS select `sc`.`sr_cabang` AS `sr_cabang`,`sc`.`cabang_id` AS `cabang_id`,`sc`.`security_roles_id` AS `security_roles_id`,`c`.`kode_cabang` AS `kode_cabang`,`c`.`nama_cabang` AS `nama_cabang`,`c`.`bu_id` AS `bu_id`,`c`.`alamat_id` AS `alamat_id`,`c`.`no_telp_cabang` AS `no_telp_cabang`,`c`.`alamat_cabang` AS `alamat_cabang`,`c`.`kepala_cabang_stat` AS `kepala_cabang_stat`,`c`.`area_id` AS `area_id`,`b`.`bu_name` AS `bu_name`,`b`.`bu_kode` AS `bu_kode`,`pa`.`kode` AS `kode`,`pa`.`nama` AS `nama`,`b`.`bu_nama_alias` AS `bu_nama_alias` from (((`pbu_cabang` `c` join `pbu_bu` `b` on((`c`.`bu_id` = convert(`b`.`bu_id` using utf8mb4)))) join `pbu_sr_cabang` `sc` on((convert(`sc`.`cabang_id` using utf8mb4) = `c`.`cabang_id`))) join `pbu_area` `pa` on((`pa`.`area_id` = `c`.`area_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_sr_level_bu`
--

/*!50001 DROP VIEW IF EXISTS `pbu_sr_level_bu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_sr_level_bu` AS select `sr`.`sr_leveling` AS `sr_leveling`,`sr`.`security_roles_id` AS `security_roles_id`,`sr`.`leveling_id` AS `leveling_id`,`l`.`kode` AS `kode`,`l`.`nama` AS `nama`,`l`.`bu_id` AS `bu_id`,`b`.`bu_name` AS `bu_name`,`b`.`bu_kode` AS `bu_kode` from ((`pbu_leveling` `l` join `pbu_bu` `b` on((`l`.`bu_id` = convert(`b`.`bu_id` using utf8mb4)))) join `pbu_sr_leveling` `sr` on((convert(`sr`.`leveling_id` using utf8mb4) = `l`.`leveling_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pbu_validasi_view`
--

/*!50001 DROP VIEW IF EXISTS `pbu_validasi_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `pbu_validasi_view` AS select `v`.`validasi_id` AS `validasi_id`,`v`.`pin_id` AS `pin_id`,`v`.`PIN` AS `PIN`,`v`.`pegawai_id` AS `pegawai_id`,`v`.`cabang_id` AS `cabang_validasi_id`,`v`.`in_time` AS `in_time`,`v`.`out_time` AS `out_time`,`v`.`min_early_time` AS `min_early_time`,`v`.`min_late_time` AS `min_late_time`,`v`.`min_least_time` AS `min_least_time`,`v`.`min_over_time_awal` AS `min_over_time_awal`,`v`.`min_over_time` AS `min_over_time`,(`v`.`min_over_time` + `v`.`min_over_time_awal`) AS `total_lembur`,`v`.`real_lembur_pertama` AS `min_over_time_awal_real`,`v`.`real_lembur_akhir` AS `min_over_time_real`,`v`.`real_lembur_hari` AS `lembur_hari`,`v`.`real_less_time` AS `real_less_time`,`v`.`tdate` AS `tdate`,`v`.`kode_ket` AS `kode_ket`,`v`.`no_surat_tugas` AS `no_surat_tugas`,`v`.`status_int` AS `status_int`,`v`.`result_id` AS `result_id`,`v`.`user_id` AS `user_id`,`v`.`status_pegawai_id` AS `status_pegawai_id`,`v`.`shift_id` AS `shift_id`,`v`.`approval_lembur` AS `approval_lembur`,`v`.`approval_lesstime` AS `approval_lesstime`,`p`.`cabang_id` AS `cabang_id`,`c`.`kode_cabang` AS `kode_cabang`,`c`.`nama_cabang` AS `nama_cabang`,`pp`.`cabang_id` AS `cabang_user`,`u`.`pegawai_id` AS `pegawai_user`,`v`.`cuti` AS `cuti`,`v`.`limit_lembur` AS `limit_lembur`,`v`.`short_shift` AS `short_shift` from ((((`pbu_validasi` `v` left join `pbu_pegawai` `p` on((`p`.`pegawai_id` = `v`.`pegawai_id`))) left join `pbu_cabang` `c` on((`c`.`cabang_id` = `p`.`cabang_id`))) left join `pbu_users` `u` on((`u`.`id` = `v`.`user_id`))) left join `pbu_pegawai` `pp` on((`pp`.`pegawai_id` = `u`.`pegawai_id`))) where (`v`.`status_int` = 1) order by `v`.`PIN`,`c`.`kode_cabang` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `template_hari`
--

/*!50001 DROP VIEW IF EXISTS `template_hari`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `template_hari` AS select `sp`.`template_id` AS `template_id`,`sp`.`shift_id` AS `shift_id`,`s`.`kode_shift` AS `kode_shift`,`s`.`in_time` AS `in_time`,`s`.`out_time` AS `out_time`,`h`.`sunday` AS `sunday`,`h`.`monday` AS `monday`,`h`.`tuesday` AS `tuesday`,`h`.`wednesday` AS `wednesday`,`h`.`thursday` AS `thursday`,`h`.`friday` AS `friday`,`h`.`saturday` AS `saturday` from ((`pbu_template_detail` `sp` left join `pbu_shift` `s` on((convert(`s`.`shift_id` using utf8mb4) = `sp`.`shift_id`))) left join `nama_hari` `h` on((`s`.`shift_id` = `h`.`shift_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_assignshift`
--

/*!50001 DROP VIEW IF EXISTS `v_assignshift`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER */
/*!50001 VIEW `v_assignshift` AS select `sp`.`shiftpin_id` AS `shiftpin_id`,`pp`.`nama_lengkap` AS `nama_lengkap`,`sp`.`shift_id` AS `shift_id`,`sp`.`pin_id` AS `pin_id`,`sp`.`pegawai_id` AS `pegawai_id`,`sp`.`visible` AS `visible`,concat(`sp`.`pin_id`,' ',`pp`.`nama_lengkap`) AS `pin_nama`,`p`.`PIN` AS `pin`,`p`.`store` AS `store`,`s`.`kode_shift` AS `kode_shift`,`s`.`in_time` AS `in_time`,`s`.`out_time` AS `out_time`,`h`.`sunday` AS `sunday`,`h`.`monday` AS `monday`,`h`.`tuesday` AS `tuesday`,`h`.`wednesday` AS `wednesday`,`h`.`thursday` AS `thursday`,`h`.`friday` AS `friday`,`h`.`saturday` AS `saturday`,`pu`.`name_` AS `user_assign` from (((((`pbu_shiftpin` `sp` left join `pbu_pin` `p` on((`sp`.`pegawai_id` = `p`.`pegawai_id`))) left join `pbu_shift` `s` on((`s`.`shift_id` = `sp`.`shift_id`))) left join `nama_hari` `h` on((`s`.`shift_id` = `h`.`shift_id`))) left join `pbu_pegawai` `pp` on((`pp`.`pegawai_id` = `sp`.`pegawai_id`))) left join `pbu_users` `pu` on((`pu`.`id` = `sp`.`user_id`))) where (`sp`.`visible` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-14 10:29:00
