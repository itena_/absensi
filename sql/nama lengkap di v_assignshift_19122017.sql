ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `v_assignshift` AS 
select `sp`.`shiftpin_id` AS `shiftpin_id`,`pp`.`nama_lengkap` AS `nama_lengkap`,`sp`.`shift_id` AS `shift_id`,`sp`.`pin_id` AS `pin_id`,`p`.`PIN` AS `pin`,`s`.`kode_shift` AS `kode_shift`,`s`.`in_time` AS `in_time`,`s`.`out_time` AS `out_time`,`h`.`sunday` AS `sunday`,`h`.`monday` AS `monday`,`h`.`tuesday` AS `tuesday`,`h`.`wednesday` AS `wednesday`,`h`.`thursday` AS `thursday`,`h`.`friday` AS `friday`,`h`.`saturday` AS `saturday` from ((((`pbu_shiftpin` `sp` left join `pbu_pin` `p` on((`sp`.`pin_id` = `p`.`pin_id`))) left join `pbu_shift` `s` on((`s`.`shift_id` = `sp`.`shift_id`))) left join `nama_hari` `h` on((`s`.`shift_id` = `h`.`shift_id`))) left join `pbu_pegawai` `pp` on((`pp`.`nik` = `sp`.`pin_id`))) ;