ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `v_assignshift` AS 
SELECT
	`sp`.`shiftpin_id` AS `shiftpin_id`,
	`pp`.`nama_lengkap` AS `nama_lengkap`,
	`sp`.`shift_id` AS `shift_id`,
	`sp`.`pin_id` AS `pin_id`,
	`p`.`PIN` AS `pin`,
	`p`.`store` AS `store`,
	`s`.`kode_shift` AS `kode_shift`,
	`s`.`in_time` AS `in_time`,
	`s`.`out_time` AS `out_time`,
	`h`.`sunday` AS `sunday`,
	`h`.`monday` AS `monday`,
	`h`.`tuesday` AS `tuesday`,
	`h`.`wednesday` AS `wednesday`,
	`h`.`thursday` AS `thursday`,
	`h`.`friday` AS `friday`,
	`h`.`saturday` AS `saturday`
FROM
	(
		(
			(
				(
					`pbu_shiftpin` `sp`
					LEFT JOIN `pbu_pin` `p` ON (
						(`sp`.`pin_id` = `p`.`pin_id`)
					)
				)
				LEFT JOIN `pbu_shift` `s` ON (
					(
						`s`.`shift_id` = `sp`.`shift_id`
					)
				)
			)
			LEFT JOIN `nama_hari` `h` ON (
				(
					`s`.`shift_id` = `h`.`shift_id`
				)
			)
		)
		LEFT JOIN `pbu_pegawai` `pp` ON ((`pp`.`nik` = `sp`.`pin_id`))
	) ;