ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`127.0.0.1` 
SQL SECURITY DEFINER 
VIEW `pbu_result_view` AS 
select r., c.kode_cabang, 
(SELECT ifnull(SUM(v.min_over_time_awal),0) FROM pbu_validasi v WHERE v.approval_lembur = 1 AND v.PIN = r.pin_id AND v.result_id = r.result_id) real_lembur_awal,
(SELECT ifnull(SUM(v.min_over_time),0) FROM pbu_validasi v WHERE v.approval_lembur = 1 AND v.PIN = r.pin_id AND v.result_id = r.result_id) real_lembur_akhir
from pbu_result r
LEFT JOIN pbu_pegawai p on p.nik = r.pin_id
LEFT JOIN pbu_cabang c on c.cabang_id = p.cabang_id
ORDER BY r.pin_id, c.kode_cabang ;