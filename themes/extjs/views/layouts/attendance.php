<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<script>
    function myFunction() {
        var nik = document.getElementById('nik').value;
        // console.log(nik);
        Ext.Ajax.request({
            url: 'Fp/SaveFpFromBeta',
            method: 'POST',
            params: {
                nik: nik
            },
            success: function (f, a) {
                var response = Ext.decode(f.response);
                console.log(response);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
</script>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body, html {
            height: 100%;
            margin: 0;
        }
        body {
            overflow:hidden;
        }
        .bg {
            /* The image used */
            background-image: url("<?php echo Yii::app()->request->baseUrl; ?>/images/greeting.jpg");

            /* Full height */
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        .centered {
            position: absolute;
            top: 58%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 60px;
        }
        .centered2 {
            position: absolute;
            text-align: center;
            top: 69%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 30px;
        }
        .button {
            position: absolute;
            top: 80%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: #04AA6D; /* Green */
            border: none;
            color: white;
            padding: 20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .button4 {border-radius: 12px;}
    </style>
</head>
<body>
<div class="bg">
    <script type="text/javascript" charset="utf-8">
        let a;
        let time;
        setInterval(() => {
            d = new Date();
            s = d.getSeconds();
            m = d.getMinutes();
            h = d.getHours();
            time = ("0" + h).substr(-2) + ":" + ("0" + m).substr(-2) + ":" + ("0" + s).substr(-2);
            document.getElementById('time').innerHTML = time;
        }, 1000);
    </script>
    <span id="time" class="centered"></span>
    <form action="" method="post">
        <input type="text" class="centered2" id="nik" name="nik" maxlength="6" placeholder="NIK"><br><br>
        <input type="button" class="button button4" onclick="myFunction()"
               value="Presensi">
    </form>
</div>

</body>
</html>