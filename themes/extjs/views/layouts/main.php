<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <link rel="shortcut icon" href="<?php echo bu();?>" /><?php //masukan gambar disini?>
        <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/css/GridFilters.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/css/RangeMenu.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/css/default.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo Yii::app()->request->baseUrl; ?>/codemirror/lib/codemirror.css"/>
        <style>
            #drop {
                border: 2px dashed #BBBBBB;
                border-radius: 5px;
                color: #BBBBBB;
                font: 20pt bold, "Vollkorn";
                padding: 25px;
                text-align: center;
            }
            * {
                font-size: 12px;
                font-family: Candara;
            }
            .container {
                display: table;
            }
            .search-item-table {
                display: table-row;
                /*color: #8B1D51;*/
            }

            .cell4 {
                display: table-cell;
                /*border: solid;*/
                /*border-width: thin;*/
                padding-left: 5px;
                padding-right: 5px;
            }

            .custom-sales-details .x-grid-row-selected .x-grid-cell-first {
                padding-left: 5px;
            }
            @media screen and (-webkit-min-device-pixel-ratio: 0) {
                .x-grid3-cell, /* Normal grid cell */
                .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                    box-sizing: border-box;
                }
            }
        </style>
    </head>
    <body>
        <script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/codemirror/lib/codemirror.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/codemirror/addon/mode/loadmode.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/menu/RangeMenu.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/menu/ListMenu.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/GridFilters.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/filter/Filter.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/filter/StringFilter.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/filter/DateFilter.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/filter/ListFilter.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/filter/NumericFilter.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/gridfilters/filter/BooleanFilter.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/xlsx.full.min.js"></script>
        <script>
            Ext.namespace('jun');
            const SYSTEM_TITLE              = '<img src="<?=bu(); ?>/images/logo.jpg" alt=""/>';
            const SYSTEM_LOGO               = '<img src="<?=bu(); ?>/images/logo.png" alt=""/>';
            const TEMPLATE_UPLOAD           = '<?=bu(); ?>/download/template_upload.xls';
            const TEMPLATE_UPLOAD_LARK      = '<?=bu(); ?>/download/template_upload_lark.xls';
            const FORMAT_UPLOAD_TIMESHEET   = '<?=bu(); ?>/download/FORMAT_UPLOAD_TIMESHEET.xls';

            const ALL_CABANG                    = '<?=GET_ALL_CABANG()?>';
            const SECURITY_ROLE_ADMINISTRATOR   = '<?=GET_SECURITY_ROLE_ADMINISTRATOR()?>';
            const SECURITY_ROLE_HRD             = '<?=GET_SECURITY_ROLE_HRD()?>';
            const DATE_NOW                      = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
            const SET_INTIME_BUAT_REVIEW2       = '<?=GET_SET_INTIME_BUAT_REVIEW2() == 'true' ? true : false?>';
            const SHOW_AMOS_PHASE_2             = '<?=GET_SHOW_AMOS_PHASE_2() == 'true' ? true : false?>';
            const codeMirrorPath                = "<?php echo Yii::app()->request->baseUrl; ?>/codemirror";
        </script>
<?php echo $content; ?>
        <!--<script type="text/javascript"
        src="?php /*echo Yii::app()->request->baseUrl; */?>/js/main.js"></script>-->
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/jsDate.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/mainpanel.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
        <script type="text/javascript"
                src="<?php echo Yii::app()->request->baseUrl; ?>/js/xlsx.full.min.js"></script>
        <?
        $dir = array('/js/view/');
        foreach ($dir as $path) {
            $templatePath = dirname(Yii::app()->basePath) . $path;
            $files = scandir($templatePath);
            foreach ($files as $file) {
                if (is_file($templatePath . '/' . $file)) {
                    ?>
                    <script type="text/javascript"
                            src="<?php echo(bu() . $path . $file . "?v=" . md5_file(dirname(Yii::app()->getBasePath()) . $path . $file)); ?>"></script>
                    <?
                }
            }
        }
        ?>
    </body>
</html>
