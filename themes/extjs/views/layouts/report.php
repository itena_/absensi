<html>
<?php if (!$this->is_excel): ?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style type="text/css">
        body {
            font-family: helvetica, tahoma, verdana, sans-serif;
            padding: 20px;
            padding-top: 32px;
            font-size: 13px;
            color: #336580 !important;
        }

        .grid-view table.items {
            background: white;
            border-collapse: collapse;
            width: 100%;
            border: 1px #CCCCCC solid !important;
        }

        .grid-view table.items th, .grid-view table.items td {
            color: #336580 !important;
            font-size: 0.9em;
            border: 1px white solid;
            border-color: #ba4c80 #CCCCCC #CCCCCC !important;
            padding: 0.3em;
        }

        .grid-view table.items th {
            color: #336580 !important;
            background: #EFEFEF !important;
            text-align: center;
        }

        .grid-view table.items th a {
            color: #EEE;
            font-weight: bold;
            text-decoration: none;
        }

        .grid-view table.items th a:hover {
            color: #dadada;
        }

        .grid-view table.items tr.even {
            background: #dadada;
        }

        .grid-view table.items tr.odd {
            background: #EFEFEF;
        }

        .grid-view table.items tr.selected {
            background: #EFEFEF;
            border-color: #CCCCCC;
        }

        .grid-view table.items tr:hover.selected {
            border-color: #CCCCCC;
            background: #c7c7c7;
        }

        .grid-view table.items tbody tr:hover {
            background: #e1e1e1;
        }

        .grid-view .link-column img {
            border: 0;
        }

        .grid-view .button-column {
            text-align: center;
            width: 60px;
        }

        .grid-view .button-column img {
            border: 0;
        }

        .grid-view .checkbox-column {
            width: 15px;
        }

        .grid-view .summary {
            margin: 0 0 5px 0;
            text-align: right;
        }

        .grid-view .pager {
            margin: 5px 0 0 0;
            text-align: right;
        }

        .grid-view .empty {
            font-style: italic;
        }

        .grid-view .filters input,
        .grid-view .filters select {
            width: 100%;
            border: 1px solid #ccc;
        }

        /* grid border */
        .grid-view table.items th, .grid-view table.items td {
            border: 1px solid #CCCCCC !important;
        }

        /* disable selection for extrarows */
        .grid-view td.extrarow {
            background: none repeat scroll 0 0 #F8F8F8;
        }

        .subtotal {
            font-size: 14px;
            color: brown;
            font-weight: bold;
        }
    </style>
</head>
<?php endif ?>
<body>
<?php if (!$this->is_excel): ?>
<!--<div style="height: 100px; left: 0px; top: 0px;"></div>-->
<?php endif ?>
<?php echo $content; ?>
</body>
</html>