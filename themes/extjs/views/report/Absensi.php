<h1><?=$title?></h1>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = $pageTitle;
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' )
        ),
        array(
            'header' => 'Jabatan',
            'name' => 'kode',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' )
        ),
        array(
            'header' => 'Total HK',
            'name' => 'HK',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total LK',
            'name' => 'LK',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total DK',
            'name' => 'DK',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'WFH',
            'name' => 'WFH',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Presentasi / Pameran',
            'name' => 'PP',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Sakit',
            'name' => 'S',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Off',
            'name' => 'OFF',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Jatah Off',
            'name' => 'jatah_off',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Ganti Off',
            'name' => 'ganti_off',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Tahunan',
            'name' => 'CT',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Menikah',
            'name' => 'CM',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Bersalin',
            'name' => 'CB',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Istimewa',
            'name' => 'CI',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Non Aktif',
            'name' => 'CNA',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Lembur 1 (menit)',
            'name' => 'real_lembur_pertama',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Lembur 2 (menit)',
            'name' => 'real_lembur_akhir',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),          
        array(
            'header' => 'Less Time (menit)',
            'name' => 'LT',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),          
        array(
            'header' => 'Off Tambahan',
            'name' => 'lemburh',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),            
        array(
            'header' => 'Total Hari',
            'name' => 'TOTAL',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        )  
    )
));
?>