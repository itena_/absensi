<h1>Laporan Presensi</h1>
<h3>PERIODE : <?= $kode_periode ?></h3>
<h3>CABANG : <?= $kode_cabang ?></h3>
<?php
$centerNoWrap = ['style' => 'text-align: center;white-space: nowrap;'];

$arr = [
    array(
        'header' => 'NIK',
        'name' => 'nik'
    ),
    array(
        'header' => 'Nama',
        'name' => 'nama_lengkap',
        'htmlOptions' => array('style' => 'white-space: nowrap;')
    ),
    array(
        'header' => 'Jabatan',
        'name' => 'kode',
        'htmlOptions' => array('style' => 'white-space: nowrap;')
    ),
    array(
        'header' => 'Status HK',
        'name' => 'status_hk',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total HK',
        'name' => 'HK',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total LK',
        'name' => 'LK',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'WFH',
        'name' => 'WFH',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Presentasi / Pameran',
        'name' => 'PP',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total Sakit',
        'name' => 'S',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total Off',
        'name' => 'OFF',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Jatah Off',
        'name' => 'jatah_off',
        'htmlOptions' => array ('style' => 'text-align: center;' )
    ),
    array(
        'header' => 'Ganti Off',
        'name' => 'ganti_off',
        'htmlOptions' => array ('style' => 'text-align: center;' )
    ),
    array(
        'header' => 'Total Cuti Tahunan',
        'name' => 'CT',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total Cuti Menikah',
        'name' => 'CM',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total Cuti Bersalin',
        'name' => 'CB',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total Cuti Istimewa',
        'name' => 'CI',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total Cuti Non Aktif',
        'name' => 'CNA',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Lembur 1 (menit)',
        'name' => 'real_lembur_pertama',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Lembur 2 (menit)',
        'name' => 'real_lembur_akhir',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Less Time (menit)',
        'name' => 'LT',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Off Tambahan',
        'name' => 'lemburh',
        'htmlOptions' => $centerNoWrap
    ),
    array(
        'header' => 'Total Hari',
        'name' => 'TOTAL',
        'htmlOptions' => $centerNoWrap
    )];

$arrJatahOff = [['header' => 'Jatah Off',
        'name' => 'jatah_off',
        'htmlOptions' => $centerNoWrap ]];

$arrSisaOff = [['header' => 'Sisa Off',
    'name' => 'sisa_off',
    'htmlOptions' => $centerNoWrap]];

if($jatah_off) {
    array_splice( $arr, 10, 0, $arrSisaOff );
    array_splice( $arr, 10, 0, $arrJatahOff );
}

$this->pageTitle = 'Laporan Presensi';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => $arr
));
?>