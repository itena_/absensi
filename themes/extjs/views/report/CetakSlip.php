<!DOCTYPE html>
<html>
<head>
    <style>
        .top {
            border-style: solid;
            border-top: thin #000000;
        }
        .left {
            border-style: solid;
            border-left: thin #000000;
        }
        .right {
            border-style: solid;
            border-right: thin #000000;
        }
        .bottom {
            border-style: solid;
            border-bottom: thin #000000;
        }
    </style>
</head>
<body>
<table style="border-collapse: collapse; width: 761px;" width="755">
    <tbody>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: center; vertical-align: middle; width: 354px;" colspan="3" rowspan="3" >
            <img style="display:block;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" />
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: underline; text-align: left; vertical-align: middle;  width: 406px; " colspan="3" >SLIP&nbsp;GAJI&nbsp;KARYAWAN</td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >PERIODE</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: left; vertical-align: middle;  width: 291px; " colspan="2" >:&nbsp;<?=$periode;?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >CABANG</td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: left; vertical-align: middle;  width: 291px; " colspan="2" >:&nbsp;<?=$cabang;?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <tr>
        <td class='top' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 125px; " >NIK</td>
        <td class='top' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 140px; " >:&nbsp;<?=$nik;?></td>
        <td class='top' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 89px; " >KTP</td>
        <td class='top' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 115px; " >:&nbsp;<?=$ktp;?></td>
        <td class='top' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 121px; " >JABATAN</td>
        <td class='top' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 170px; " >:&nbsp;<?=$jabatan;?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 125px; " >NAMA</td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 140px; " >:&nbsp;<?=$nama;?></td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 89px; " >NPWP</td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 115px; " >:&nbsp;<?=$npwp;?></td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 121px; " >GRADE</td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 170px; " >:&nbsp;<?=$gol;?>&nbsp;<?=$lvl;?></td>
    </tr>
    <tr>
        <td class='bottom' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 125px; " >EMAIL</td>
        <td class='bottom' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 140px; " >:&nbsp;<?=$email;?></td>
        <td class='bottom' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 89px; " >STATUS</td>
        <td class='bottom' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 115px; " >:&nbsp;<?=$status;?></td>
        <td class='bottom' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 121px; " >HARI&nbsp;KERJA</td>
        <td class='bottom' style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 170px; " >:&nbsp;<?=$hk;?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <tr>
        <td class='top' style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; text-align: left; vertical-align: middle;  width: 265px; " colspan="2" >PENDAPATAN</td>
        <td class='top' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; " >&nbsp;</td>
        <td class='top' style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >POTONGAN</td>
        <td class='top' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >&nbsp;</td>
        <td class='top' style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <? foreach ($details as $row) :?>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; " >&nbsp;<?=$row['in_n'];?></td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; " >&nbsp;<?=$row['in_a'];?></td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;<?=$row['ot_n'];?></td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >&nbsp;<?=$row['ot_a'];?></td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <? endforeach; ?>
    <tr>
        <td class='bottom' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; " >Total&nbsp;Pendapatan&nbsp;</td>
        <td class='bottom' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; " >: &nbsp;<?=$inc; ?></td>
        <td class='bottom' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; " >&nbsp;</td>
        <td class='bottom' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >Total&nbsp;Potongan</td>
        <td class='bottom' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >: &nbsp;<?=$pot; ?></td>
        <td class='bottom' style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; " >Take&nbsp;Home&nbsp;Pay</td>
        <td style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; " >: &nbsp;<?=$thp; ?></td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: center; vertical-align: middle;  width: 291px; " colspan="2" >Yogyakarta,&nbsp;<?=$tgl;?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: top; height: 57pt; width: 354px; " colspan="3" rowspan="4" height="76">Cat&nbsp;:</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: center; vertical-align: middle;  width: 291px; " colspan="2" >Dibuat,</td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; " >&nbsp;</td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; " >&nbsp;</td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: center; vertical-align: middle;  width: 291px; " colspan="2" >Payroll</td>
    </tr>
    </tbody>
</table>
</body>
</html>