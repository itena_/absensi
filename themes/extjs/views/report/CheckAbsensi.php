<h1>Laporan Absensi yg Bermasalah</h1>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<b>Jika </b><p style="color: red;display: inline;font-weight: bold">DATA DOUBLE</p> <b>, cek di Report FP pada nik dan tanggal bersangkutan.</b>
<br><br>
<b>Jika </b><p style="color: red;display: inline;font-weight: bold">BELUM DIISI</p> <b>, silahkan diisi Presensinya terlebih dahulu.</b>
<?php
$this->pageTitle = 'Laporan Absensi yg Bermasalah';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
        array(
            'header' => 'Tanggal',
            'name' => 'selected_date'
        ),
        array(
            'header' => 'Note',
            'name' => 'note'
        )
    )
));
?>