<h1>Rekap Cuti</h1>
<h3>TAHUN : <?=$tahun?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Rekap Cuti';
//$id = Yii::app()->user->getId();
//$user = Users::model()->findByPk($id);
if (GET_TAMPILKAN_SISA_CUTI()){
    $this->widget('ext.groupgridview.GroupGridView', array(
        'id' => 'the-table',
        'dataProvider' => $dp,
        'mergeType' => 'nested',
        'columns' => array(
            array(
                'header' => 'Cabang',
                'name' => 'kode_cabang'
            ),
            array(
                'header' => 'NIK',
                'name' => 'nik'
            ),
            array(
                'header' => 'Nama',
                'name' => 'nama_lengkap'
            ),
            array(
                'header' => 'Ambil Cuti',
                'name' => 'ambil_cuti',
                'htmlOptions' => array ('style' => 'text-align: center;' )
            ),
            array(
                'header' => 'Tanggal Ambil Cuti',
                'name' => 'tgl_ambil_cuti',
                'htmlOptions' => array ('style' => 'text-align: center;' )
            ),
        )
    ));
} else {
    $this->widget('ext.groupgridview.GroupGridView', array(
        'id' => 'the-table',
        'dataProvider' => $dp,
        'mergeType' => 'nested',
        'columns' => array(
            array(
                'header' => 'Cabang',
                'name' => 'kode_cabang'
            ),
            array(
                'header' => 'NIK',
                'name' => 'nik'
            ),
            array(
                'header' => 'Nama',
                'name' => 'nama_lengkap'
            ),
            array(
                'header' => 'Sisa Cuti ' . $tahunsebelumnya,
                'name' => 'sisa_cuti_tahun_sebelumnya',
                'htmlOptions' => array ('style' => 'text-align: center;' )
            ),
            array(
                'header' => 'Cuti Tahunan',
                'name' => 'jatah_cuti',
                'htmlOptions' => array ('style' => 'text-align: center;' )
            ),
            array(
                'header' => 'Ambil Cuti',
                'name' => 'ambil_cuti',
                'htmlOptions' => array ('style' => 'text-align: center;' )
            ),
            array(
                'header' => 'Sisa Cuti',
                'name' => 'sisa_cuti',
                'htmlOptions' => array ('style' => 'text-align: center;' )
            ),
            array(
                'header' => 'Tanggal Ambil Cuti',
                'name' => 'tgl_ambil_cuti',
                'htmlOptions' => array ('style' => 'text-align: center;' )
            ),
        )
    ));
}
?>