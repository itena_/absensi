<h1>Laporan FingerPrint</h1>
<h3>NAMA : <?=$nama?></h3>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Laporan FingerPrint';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'Unit Usaha',
            'name' => 'bu_kode',
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total :'
        ),
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' )
        ),
        array(
            'header' => 'Tanggal Masuk',
            'name' => 'tgl_in',
            'htmlOptions' => array ('style' => 'white-space: nowrap; text-align: center;' )
        ),
        array(
            'header' => 'Masuk',
            'name' => 'masuk',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Tanggal Keluar',
            'name' => 'tgl_out',
            'htmlOptions' => array ('style' => 'white-space: nowrap; text-align: center;' )
        ),
        array(
            'header' => 'Keluar',
            'name' => 'keluar',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => '6Jam',
            'name' => 'short_shift',
            'htmlOptions' => array ('style' => 'white-space: nowrap; text-align: center;' )
        ),
        array(
            'header' => 'Ket',
            'name' => 'keterangan',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' )
        ),
        array(
            'header' => 'Jam kerja',
            'name' => 'jam_kerja', 
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => $total_jam_kerja_time
        ),
        array(
            'header' => 'Lembur 1 (menit)',
            'name' => 'lembur1',
            'htmlOptions' => array ('style' => 'white-space: nowrap; text-align: center;' ),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => $total_lbr1
        ),
        array(
            'header' => 'Lembur 2 (menit)',
            'name' => 'lembur2',
            'htmlOptions' => array ('style' => 'white-space: nowrap; text-align: center;' ),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => $total_lbr2
        ),
        array(
            'header' => 'Less Time (menit)',
            'name' => 'LT',
            'htmlOptions' => array ('style' => 'white-space: nowrap; text-align: center;' ),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => $total_lt
        ),
        array(
            'header' => 'Off Tambahan',
            'name' => 'lemburh',
            'htmlOptions' => array ('style' => 'white-space: nowrap; text-align: center;' ),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => $total_lbrh
        ),
        array(
            'header' => 'No. ST / Alasan',
            'name' => 'no_surat_tugas',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' )
        )
    )
));
?>