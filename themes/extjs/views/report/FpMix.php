<h1>Laporan FingerPrint Mix</h1>
<h3>NAMA : <?=$nama?></h3>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Laporan FingerPrint Mix';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'Cabang',
            'name' => 'cabang'
        ),
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Tgl Masuk',
            'name' => 'tgl_masuk'
        ),
        array(
            'header' => 'Jam Masuk',
            'name' => 'jam_masuk'
        ),
        array(
            'header' => 'Tgl Keluar',
            'name' => 'tgl_keluar'
        ),
        array(
            'header' => 'Jam Keluar',
            'name' => 'jam_keluar'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
    )
));
?>