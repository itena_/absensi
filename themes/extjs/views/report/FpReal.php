<h1>Laporan FingerPrint Real</h1>
<h3>NAMA : <?=$nama?></h3>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Laporan FingerPrint Real';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
//        array(
//            'header' => 'Unit Usaha',
//            'name' => 'bu_kode'
//        ),
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
        array(
            'header' => 'Tgl',
            'name' => 'tgl'
        ),       
        array(
            'header' => 'Jam',
            'name' => 'jam'
        )
    )
));
?>