<h1>Laporan Finger Print Validasi</h1>
<h3>NAMA : <?=$nama?></h3>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Laporan FingerPrint Real';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'Cabang',
            'name' => 'store'
        ),
        array(
            'header' => 'NIK',
            'name' => 'PIN'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
        array(
            'header' => 'Tanggal Masuk',
            'name' => 'tgl_in'
        ),
        array(
            'header' => 'Masuk',
            'name' => 'masuk'
        ),
        array(
            'header' => 'Tanggal Keluar',
            'name' => 'tgl_out'
        ),
        array(
            'header' => 'Keluar',
            'name' => 'keluar'
        ),
        array(
            'header' => 'Status Fp',
            'name' => 'status'
        )
    )
));
?>