<h1>Report KPI</h1>
<h3>FROM : <?=$from?></h3>
<h3>TO : <?=$to?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Laporan KPI';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'Unit Usaha',
            'name' => 'bu_kode',
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            //'footer' => 'Total :'
        ),
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
//        array(
//            'header' => 'Tanggal Masuk',
//            'name' => 'tgl_in'
//        ),
//        array(
//            'header' => 'Masuk',
//            'name' => 'masuk',
//            'htmlOptions' => array ('style' => 'text-align: center;' )
//        ),
//        array(
//            'header' => 'Tanggal Keluar',
//            'name' => 'tgl_out'
//        ),
//        array(
//            'header' => 'Keluar',
//            'name' => 'keluar',
//            'htmlOptions' => array ('style' => 'text-align: center;' )
//        ),
//        array(
//            'header' => 'Ket',
//            'name' => 'keterangan'
//        ),
//        array(
//            'header' => 'No. ST / Alasan',
//            'name' => 'no_surat_tugas'
//        ),
        array(
            'header' => 'Jam kerja',
            'name' => 'group',
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            //'footer' => $total_jam_kerja_time
        ),
//        array(
//            'header' => 'Lembur 1 (Jam)',
//            'name' => 'lembur1',
//            'footerHtmlOptions' => array('style' => 'text-align: right;'),
//            //'footer' => $total_lbr1
//        ),
//        array(
//            'header' => 'Lembur 2 (Jam)',
//            'name' => 'lembur2',
//            'footerHtmlOptions' => array('style' => 'text-align: right;'),
//            //'footer' => $total_lbr2
//        ),
//        array(
//            'header' => 'Less Time (Jam)',
//            'name' => 'LT',
//            'footerHtmlOptions' => array('style' => 'text-align: right;'),
//            //'footer' => $total_lt
//        ),
//        array(
//            'header' => 'Pengganti Off',
//            'name' => 'lemburh',
//            'footerHtmlOptions' => array('style' => 'text-align: right;'),
//            //'footer' => $total_lbrh
//        )
    )
));
?>