<h1>Shift Presensi</h1>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Shift Presensi';
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
	    array(
		    'header' => 'NIK',
		    'name' => 'nik'
	    ),
	    array(
		    'header' => 'Nama',
		    'name' => 'nama_lengkap'
	    ),
	    array(
		    'header' => 'Tanggal',
		    'name' => 'tanggal'
	    ),
        array(
            'header' => 'Shift',
            'name' => 'kode_shift'
        ),
	    array(
		    'header' => 'Jadwal Masuk',
		    'name' => 'shift_in'
	    ),
	    array(
		    'header' => 'Jadwal Pulang',
		    'name' => 'shift_out'
	    ),
        array(
            'header' => 'Presensi Masuk',
            'name' => 'masuk'
        ),
	    array(
		    'header' => 'Presensi Pulang',
		    'name' => 'keluar'
	    ),
    )
));
?>