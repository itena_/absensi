<h1><?=$namafungsi?></h1>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = $namafungsi;
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(        
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
        array(
            'header' => 'Jabatan',
            'name' => 'kode',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),      
        array(
            'header' => 'Total Less Time (menit)',
            'name' => 'real_lembur_less_time',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        )  ,
        array(
            'header' => 'Total Less Time (hari)',
            'name' => 'real_lembur_less_time_hari',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        )
    )
));
?>