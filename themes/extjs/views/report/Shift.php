<h1>Report Shift </h1>
<h3>CABANG : <?= $kode_cabang ?></h3>
<?php
$this->pageTitle = 'Report Shift';
$this->widget('ext.groupgridview.GroupGridView', array(
        'id' => 'the-table',
        'dataProvider' => $dp,
        'mergeType' => 'nested',
        'columns' => array(
            array(
                'header' => 'Kode',
                'name' => 'kode_shift',
                'htmlOptions' => array('style' => 'white-space: nowrap;'),
            ),
            array(
                'header' => 'Status aktif',
                'name' => 'status_shift'
            ),
            array(
                'header' => 'Kelompok shift',
                'name' => 'name_kelompok',

            ),
            array(
                'header' => 'In time',
                'name' => 'in_time',

            ),
            array(
                'header' => 'Out time',
                'name' => 'out_time',

            ),
            array(
                'header' => 'Range (in time)',
                'name' => 'range_in',
                'htmlOptions' => array('style' => 'white-space: nowrap;'),
            ),
            array(
                'header' => 'Early time (<)',
                'name' => 'early_time',

            ),
            array(
                'header' => 'Late time (>)',
                'name' => 'late_time',

            ),
            array(
                'header' => 'Range (Out time)',
                'name' => 'range_out',
                'htmlOptions' => array('style' => 'white-space: nowrap;'),
            ),
            array(
                'header' => 'Least time (<)',
                'name' => 'least_time',

            ),
            array(
                'header' => 'Over time (>)',
                'name' => 'over_time',
            )
        )
    )
);
?>