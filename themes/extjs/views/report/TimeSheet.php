<h1>Time Sheet</h1>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Time Sheet';
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'CABANG',
            'name' => 'kode_cabang'
        ),
	    array(
		    'header' => 'NIK',
		    'name' => 'nik'
	    ),
        array(
            'header' => 'TANGGAL_MASUK',
            'name' => 'tgl'
        ),
	    array(
		    'header' => 'JAM_MASUK',
		    'name' => 'masuk'
	    ),
	    array(
		    'header' => 'TANGGAL_KELUAR',
		    'name' => 'tgl'
	    ),
        array(
            'header' => 'JAM_KELUAR',
            'name' => 'pulang'
        ),
	    array(
		    'header' => 'KODE_KETERANGAN',
		    'name' => 'kode_ket'
	    ),
	    array(
		    'header' => 'ALASAN',
		    'name' => ''
	    ),
	    array(
		    'header' => 'APPROVE_LEMBUR',
		    'name' => 'ket'
	    ),
	    array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
    )
));
?>