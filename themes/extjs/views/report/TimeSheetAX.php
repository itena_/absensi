<h1>Time Sheet</h1>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?php
$this->pageTitle = 'Time Sheet AX';
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
	    array(
		    'header' => 'NIK',
		    'name' => 'NIK'
	    ),
        array(
            'header' => 'TANGGAL',
            'name' => 'tanggal'
        ),
	    array(
		    'header' => 'JAM_MASUK',
		    'name' => 'masuk'
	    ),
        array(
            'header' => 'JAM_KELUAR',
            'name' => 'pulang'
        ),
	    array(
		    'header' => 'KODE_KETERANGAN',
		    'name' => 'ket'
	    ),
	    array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
	    array(
            'header' => 'Status WFH',
            'name' => 'status_wfh'
        ),
    )
));
?>